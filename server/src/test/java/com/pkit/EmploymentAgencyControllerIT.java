package com.pkit;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.generator.TestEmploymentAgencyGenerator;
import com.pkit.model.Company;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
public class EmploymentAgencyControllerIT {

    @Autowired
    private ObjectMapper om;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    TestEmploymentAgencyGenerator testEmploymentAgencyGenerator;

    private Company newCompany;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetList() throws Exception {
        log.debug("testGetList: ");
        MvcResult mvcResult = this.mockMvc.perform(get("/employment_agency/"))
                .andExpect(status().isOk()).andReturn();

        List<Company> employmentAgencies = om.readValue(mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Company>>(){});
        Assert.assertEquals( 1L, employmentAgencies.size());
    }
}