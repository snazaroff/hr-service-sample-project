package com.pkit;

import com.pkit.generator.PersonGenerator;
import com.pkit.model.User;
import com.pkit.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
public class UserManagementIT {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PersonGenerator testPersonGenerator;

    @Before
    public void init() {
        User user = testPersonGenerator.getUser("testUser");
        userRepository.save(user);

        User user2 = testPersonGenerator.getUser("testUser_2");
        userRepository.save(user2);
    }

    @After
    public void destroy() {
        userRepository.deleteAll();
    }

    @Test
    public void testGetAllUser() {
        Iterable<User> users = userRepository.findAll();
        final int[] counter = {0};
        users.forEach(r -> {
            counter[0]++;
            log.debug("role: {}", r.getUserId());
        });
        assertEquals(2, counter[0]);
    }
}