package com.pkit;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.*;
import org.springframework.web.context.WebApplicationContext;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
public class ImportControllerIT {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    /**
     * Run simple json data
     * @throws Exception
     */
    @Test
    public void testPdf() throws Exception {

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        byte[] file;
        try (InputStream is = classloader.getResourceAsStream("test.pdf")) {
            file = IOUtils.toByteArray(is);
        }

        MockMultipartFile mockFile = new MockMultipartFile("file", "test.pdf",
                "application/pdf", file);

        MockMultipartHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.fileUpload("/import/candidate");
        builder.with(request -> {
            request.setMethod("POST");
            return request;
        });

        MvcResult mvcResult = mockMvc.perform(builder
                .file(mockFile))
                .andExpect(status().isOk())
                .andReturn();
    }
}