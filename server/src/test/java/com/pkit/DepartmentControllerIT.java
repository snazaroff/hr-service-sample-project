package com.pkit;

import com.pkit.generator.DepartmentGenerator;
import com.pkit.generator.PersonGenerator;
import com.pkit.model.Person;
import com.pkit.service.DepartmentService;
import com.pkit.service.PersonService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import com.pkit.model.Department;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
public class DepartmentControllerIT {

    @Autowired
    private ObjectMapper om;

    @Autowired
    private DepartmentGenerator testDepartmentGenerator;

    @Autowired
    private PersonGenerator testPersonGenerator;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private PersonService personService;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetList() throws Exception {
        log.debug("testGetList: ");

        MvcResult mvcResult = this.mockMvc.perform(get("/department/"))
                .andExpect(status().isOk()).andReturn();

        List<Department> departmentList = om.readValue(mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Department>>(){});
        Assert.assertEquals(5L, departmentList.size());
    }

    @Test
    public void testAdd() throws Exception {
        log.debug("testAdd: ");

        Department department = testDepartmentGenerator.getDepartment();
        log.debug("department: " + department);

        String requestBody = om.writeValueAsString(department);
        MvcResult mr = this.mockMvc
                .perform(post("/department/")
                        .content(requestBody)
                        .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        department = om.readValue(mr.getResponse().getContentAsString(), Department.class);
        log.debug("department: " + department);

        Department newDepartment = departmentService.get(department.getDepartmentId());
        Assert.assertNotNull(newDepartment);
        Assert.assertEquals(department, newDepartment);
        departmentService.delete(department);
    }

    @Test
    public void testUpdate() throws Exception {
        log.debug("testUpdate: ");

        Department department = testDepartmentGenerator.getDepartment();
        department = departmentService.add(department);

        Person head = testPersonGenerator.getPerson("ООО Огонек");
        personService.add(head);

        department.setPhone("1111111");
        department.setHead(head);
        department.setName("Изменили название");
        department.setIsActive(false);

        String requestBody = om.writeValueAsString(department);
        MvcResult mr = this.mockMvc.perform(put("/department/" + department.getDepartmentId()).content(requestBody).contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        department = om.readValue(mr.getResponse().getContentAsString(), Department.class);

        log.debug("department: " + department);
        departmentService.delete(department);
        personService.delete(head);
    }

    @Test
    public void testDelete() throws Exception {
        log.debug("testDelete: ");

        Department department = testDepartmentGenerator.getDepartment();
        department = departmentService.add(department);

        MvcResult mr = this.mockMvc
                .perform(delete("/department/" + department.getDepartmentId()))
                .andExpect(status().isOk()).andReturn();

        department = departmentService.get(department.getDepartmentId());
        Assert.assertNull(department);
    }
}