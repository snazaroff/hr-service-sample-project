package com.pkit;

import com.pkit.generator.DemandGenerator;
import com.pkit.generator.PersonGenerator;
import com.pkit.model.*;
import com.pkit.service.*;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
@ContextConfiguration(classes = ServerApplication.class)
public class CandidateControllerIT {

    @Autowired
    private ObjectMapper om;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private DemandGenerator testDemandGenerator;

    @Autowired
    private PersonGenerator testPersonGenerator;

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private CompanyService customerService;

    @Autowired
    private DemandService demandService;

    @Autowired
    private DemandCandidateService demandCandidateService;

    @Autowired
    private EducationService educationService;

    @Autowired
    private PersonService personService;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetList() throws Exception {
        log.debug("testGetList: ");

        MvcResult mvcResult = this.mockMvc.perform(get("/candidate/"))
                .andExpect(status().isOk()).andReturn();

        List<Candidate> candidateList = om.readValue(mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Candidate>>(){});
        log.debug("candidateList: ");
        for(Candidate c: candidateList)
            log.debug(c.toString());
        Assert.assertEquals( 3L, candidateList.size());
    }

    @Test
    public void testAdd() throws Exception {
        Candidate candidate = testPersonGenerator.getCandidate();
        log.debug("testAdd: {}", candidate);

        String requestBody = om.writeValueAsString(candidate);
        MvcResult mr = this.mockMvc.perform(post("/candidate/").content(requestBody).contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        candidate = om.readValue(mr.getResponse().getContentAsString(), Candidate.class);

        Candidate newCandidate = candidateService.get(candidate.getCandidateId());
        Assert.assertNotNull(newCandidate);
        Assert.assertEquals(candidate, newCandidate);
        candidateService.delete(newCandidate);
    }

    @Test
    public void testUpdate() throws Exception {
        log.debug("testUpdate: ");

        Candidate candidate = testPersonGenerator.getCandidate();
        candidate = candidateService.add(candidate);
        log.debug("candidate: {}", candidate);

        candidate.setPhone("1111111");
        candidate.setFirstName("Изменили имя");
        candidate.setMiddleName("Изменили отчество");
        candidate.setLastName("Изменили фамилия");
        candidate.setSalary(new Salary(150L, Currency.EUR));
        candidate.setStatus(CandidateStatus.LOOKING_FOR_JOB);

        Set<Education> educations = new HashSet<>();
        Education education = new Education();
        education.setEducationName("Школа им. Клары Цеткин");
        education.setCandidate(candidate);
        education.setStartDate(LocalDate.now());
        education.setEducationType(EducationType.HIGH_SCHOOL);
        educations.add(education);
        candidate.setEducations(educations);

        String requestBody = om.writeValueAsString(candidate);
        MvcResult mr = this.mockMvc
                .perform(put("/candidate/" + candidate.getCandidateId())
                        .content(requestBody)
                        .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        Candidate newCandidate = om.readValue(mr.getResponse().getContentAsString(), Candidate.class);

        Assert.assertNotNull(newCandidate);
        Assert.assertEquals(candidate, newCandidate);

        log.debug("candidate: {}", candidate);
        candidateService.get(newCandidate.getCandidateId());
        log.debug("candidate: {}", candidate);
        candidateService.delete(newCandidate);
    }

    @Test
    public void testDelete() throws Exception {
        log.debug("testDelete: ");

        Candidate candidate = testPersonGenerator.getCandidate();
        candidate = candidateService.add(candidate);

        MvcResult mr = this.mockMvc.perform(delete("/candidate/" + candidate.getCandidateId()))
                .andExpect(status().isOk()).andReturn();

        candidate = candidateService.get(candidate.getCandidateId());
        Assert.assertNull(candidate);
    }

    @Test
    public void testDemandAdd() throws Exception {
        log.debug("testDemandAdd: ");

        Person person = testPersonGenerator.getPerson("ООО Васильки");
        personService.add(person);

        Candidate candidate = testPersonGenerator.getCandidate();
        candidateService.add(candidate);

        Demand demand = testDemandGenerator.createDemand();
        log.debug("demand: " + om.writeValueAsString(demand));

        DemandCandidate demandCandidate = new DemandCandidate();
        demandCandidate.setDemand(demand);
        demandCandidate.setCandidate(candidate);
        demandCandidate.setCreatedBy(person);
        demandCandidate.setCreateDate(LocalDateTime.now());
//        demandCandidate.setNote("It's test note");

        log.debug("demandCandidate: " + demandCandidate);

        String requestBody = om.writeValueAsString(demandCandidate);
        MvcResult mr = this.mockMvc
                .perform(post("/candidate/demand/" + candidate.getCandidateId() + "/" + demand.getDemandId())
                        .content(requestBody)
                        .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        demandCandidate = om.readValue(mr.getResponse().getContentAsString(), DemandCandidate.class);

        log.debug("demandCandidate: " + demandCandidate);

        DemandCandidate newDemandCandidate = demandCandidateService.get(demandCandidate.getDemandCandidateId());
        Assert.assertNotNull(newDemandCandidate);
        Assert.assertEquals(demandCandidate, newDemandCandidate);

        testDemandGenerator.destroyDemand(demand);
        candidateService.delete(candidate);
        personService.delete(person);
    }

    @Test
    public void testDemandUpdate() throws Exception {
        log.debug("testDemandUpdate: ");

        Person person = testPersonGenerator.getPerson("ООО Васильки");
        personService.add(person);

        Candidate candidate = testPersonGenerator.getCandidate();
        candidateService.add(candidate);

        Demand demand = testDemandGenerator.createDemand();
        log.debug("demand: " + om.writeValueAsString(demand));

        DemandCandidate demandCandidate = new DemandCandidate();
        demandCandidate.setDemand(demand);
        demandCandidate.setCandidate(candidate);
        demandCandidate.setCreatedBy(person);
        demandCandidate.setCreateDate(LocalDateTime.now());
//        demandCandidate.setNote("It's test note");
        demandCandidateService.add(demandCandidate);

        log.debug("demandCandidate: " + demandCandidate);

        demandCandidate.setCreateDate(LocalDateTime.now());
//        demandCandidate.setNote("Note changed");

        String requestBody = om.writeValueAsString(demandCandidate);
        MvcResult mr = this.mockMvc
                .perform(put("/candidate/demand/" + candidate.getCandidateId() + "/" + demand.getDemandId())
                        .content(requestBody)
                        .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        demandCandidate = om.readValue(mr.getResponse().getContentAsString(), DemandCandidate.class);

        log.debug("demandCandidate: " + demandCandidate);

        DemandCandidate newDemandCandidate = demandCandidateService.get(demandCandidate.getDemandCandidateId());
        Assert.assertNotNull(newDemandCandidate);
        Assert.assertEquals(demandCandidate, newDemandCandidate);
//        Assert.assertEquals("Note changed", newDemandCandidate.getNote());

        testDemandGenerator.destroyDemand(demand);
        candidateService.delete(candidate);
        personService.delete(person);
    }

    @Test
    public void testDemandDelete() throws Exception {
        log.debug("testDemandDelete: ");

        Person person = testPersonGenerator.getPerson("ООО Васильки");
        personService.add(person);

        Candidate candidate = testPersonGenerator.getCandidate();
        candidateService.add(candidate);

        Demand demand = testDemandGenerator.createDemand();
        log.debug("demand: " + om.writeValueAsString(demand));

        DemandCandidate demandCandidate = new DemandCandidate();
        demandCandidate.setDemand(demand);
        demandCandidate.setCandidate(candidate);
        demandCandidate.setCreatedBy(person);
        demandCandidate.setCreateDate(LocalDateTime.now());
//        demandCandidate.setNote("It's test note");
        demandCandidate = demandCandidateService.add(demandCandidate);

        log.debug("demandCandidate: " + demandCandidate);

        String requestBody = om.writeValueAsString(demandCandidate);
        MvcResult mr = this.mockMvc
                .perform(delete("/candidate/demand/" + candidate.getCandidateId() + "/" + demand.getDemandId())
                        .content(requestBody)
                        .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();

        DemandCandidate newDemandCandidate = demandCandidateService.get(demandCandidate.getDemandCandidateId());
        Assert.assertNull(newDemandCandidate);

        testDemandGenerator.destroyDemand(demand);
        candidateService.delete(candidate);
        personService.delete(person);
    }
}