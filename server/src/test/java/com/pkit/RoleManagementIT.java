package com.pkit;

import com.pkit.model.Role;
import com.pkit.model.RoleType;
import com.pkit.repository.RoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
public class RoleManagementIT {

    @Autowired
    private RoleRepository roleRepository;

    @Before
    public void init() {
        Role role = new Role(RoleType.ROLE_ADMIN.name());
        roleRepository.save(role);

        Role role2 = new Role(RoleType.ROLE_USER.name());
        roleRepository.save(role2);
    }

    @After
    public void destroy() {
        roleRepository.deleteAll();
    }

    @Test
    public void testGetAllRole() {
        Iterable<Role> roles = roleRepository.findAll();
        final int[] counter = {0};
        roles.forEach(r -> {
            counter[0]++;
            log.debug("role: {}", r.getRoleId());
        });
        assertEquals(2, counter[0]);
    }
}