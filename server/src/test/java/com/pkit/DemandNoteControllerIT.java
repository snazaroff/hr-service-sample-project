package com.pkit;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.generator.DemandGenerator;
import com.pkit.generator.PersonGenerator;
import com.pkit.model.*;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.time.LocalDateTime;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
public class DemandNoteControllerIT {

    @Autowired
    private ObjectMapper om;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private DemandGenerator testDemandGenerator;

    @Autowired
    private PersonGenerator testPersonGenerator;

    @Autowired
    private CompanyService customerService;

    @Autowired
    private DemandService demandService;

    @Autowired
    private DemandNoteService demandNoteService;

    @Autowired
    private PersonService personService;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetDemandNoteByDemandId() throws Exception {
        log.debug("testGetDemandNoteByDemandId: ");

        Person person = testPersonGenerator.getPerson("ООО Васильки");
        personService.add(person);

        Demand demand = testDemandGenerator.createDemand();
        log.debug("demand: " + om.writeValueAsString(demand));

        DemandNote demandNote = new DemandNote();
        demandNote.setDemand(demand);
        demandNote.setCreatedBy(person);
        demandNote.setCreateDate(LocalDateTime.now());
        demandNote.setNote("It's test note #1!");
        demandNote = demandNoteService.add(demandNote);

        demandNote = new DemandNote();
        demandNote.setDemand(demand);
        demandNote.setCreatedBy(person);
        demandNote.setCreateDate(LocalDateTime.now());
        demandNote.setNote("It's test note #2!");
        demandNote = demandNoteService.add(demandNote);

        demandNote = new DemandNote();
        demandNote.setDemand(demand);
        demandNote.setCreatedBy(person);
        demandNote.setCreateDate(LocalDateTime.now());
        demandNote.setNote("It's test note #3!");
        demandNote = demandNoteService.add(demandNote);

        MvcResult mvcResult = this.mockMvc.perform(get("/demand/note/" + demand.getDemandId()))
                .andExpect(status().isOk()).andReturn();

        List<Demand> demandList = om.readValue(mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<DemandNote>>(){});
        log.debug("demandList: " + demandList);
        Assert.assertEquals(3L, demandList.size());

        testDemandGenerator.destroyDemand(demand);
        personService.delete(person);
    }

    @Test
    public void testUpdate() throws Exception {
        log.debug("testUpdate: ");

        Person person = testPersonGenerator.getPerson("ООО Васильки");
        personService.add(person);

        Demand demand = testDemandGenerator.createDemand();
        log.debug("demand: " + om.writeValueAsString(demand));

        DemandNote demandNote = new DemandNote();
        demandNote.setDemand(demand);
        demandNote.setCreatedBy(person);
        demandNote.setCreateDate(LocalDateTime.now());
        demandNote.setNote("It's test note #1!");
        demandNote = demandNoteService.add(demandNote);

        demandNote.setNote("Note changed!");

        String requestBody = om.writeValueAsString(demandNote);
        MvcResult mr = this.mockMvc.perform(put("/demand/note/" + demandNote.getDemandNoteId()).content(requestBody).contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        demandNote = om.readValue(mr.getResponse().getContentAsString(), DemandNote.class);

        log.debug("demandNote: " + demandNote);
        Assert.assertEquals("Note changed!", demandNote.getNote());
        testDemandGenerator.destroyDemand(demand);
        personService.delete(person);
    }

    @Test
    public void testDelete() throws Exception {
        log.debug("testDelete: ");

        Person person = testPersonGenerator.getPerson("ООО Васильки");
        personService.add(person);

        Demand demand = testDemandGenerator.createDemand();
        log.debug("demand: " + om.writeValueAsString(demand));

        DemandNote demandNote = new DemandNote();
        demandNote.setDemand(demand);
        demandNote.setCreatedBy(person);
        demandNote.setCreateDate(LocalDateTime.now());
        demandNote.setNote("It's test note #1!");
        demandNote = demandNoteService.add(demandNote);

        MvcResult mr = this.mockMvc.perform(delete("/demand/note/" + demandNote.getDemandNoteId()))
                .andExpect(status().isOk()).andReturn();

        demandNote = demandNoteService.get(demandNote.getDemandNoteId());
        Assert.assertNull(demandNote);

        testDemandGenerator.destroyDemand(demand);
        personService.delete(person);
    }
}