package com.pkit;

import com.pkit.generator.PersonGenerator;
import com.pkit.model.*;
import com.pkit.service.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
public class ImageControllerIT {

    @Autowired
    private ObjectMapper om;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private PersonGenerator testPersonGenerator;

    @Autowired
    private PhotoService photoService;

    @Autowired
    private CandidateService candidateService;

    private Candidate candidate;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();

        candidate = testPersonGenerator.getCandidate();
        candidate = candidateService.add(candidate);

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        for (int i = 1; i <= 3; i++) {
            String fileName = "boys_" + i + ".jpg";
            try (InputStream is = classloader.getResourceAsStream("images/" + fileName)) {

                byte[] image = IOUtils.toByteArray(is);
                Photo photo = new Photo();
                photo.setCandidateId(candidate.getCandidateId());
                photoService.add(photo, fileName, image);
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    @After
    public void destroy() {
        photoService.deleteAll(candidate.getCandidateId());
        candidateService.delete(candidate);
    }

    @Test
    public void testGetPhotoInfo() throws Exception {
        log.debug("testGetPhotoInfo: ");

        MvcResult mvcResult = this.mockMvc.perform(get("/photo/candidate/" + candidate.getCandidateId()))
                .andExpect(status().isOk()).andReturn();

        List<Photo> photos = om.readValue(mvcResult.getResponse().getContentAsString(),new TypeReference<List<Photo>>(){});
        log.debug("photos: {}", photos);
    }

    @Test
    public void testAddPhoto() throws Exception {
        log.debug("testAddPhoto: ");

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("images/boys_4.jpg");

        byte[] file = IOUtils.toByteArray(is);

        MockMultipartFile mockFile = new MockMultipartFile("file", "boys_4.jpg",
                "image/jpeg", file);

        MockMultipartHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.fileUpload("/photo/candidate/" + candidate.getCandidateId());
        builder.with(request -> {
            request.setMethod("POST");
            return request;
        });

        MvcResult mr = mockMvc.perform(builder
                .file(mockFile))
                .andExpect(status().isOk())
                .andReturn();

        log.debug("result: " + mr.getResponse().getContentAsString());
    }

    @Test
    public void testUpdate() throws Exception {
        log.debug("testUpdate: ");

        ObjectMapper om = new ObjectMapper();
        List<Photo> photos = photoService.getByCandidate(candidate.getCandidateId());
        log.debug("Photos before: {}", photos.size());
        for (Photo p : photos) {
            log.debug("photo: {}", om.writeValueAsString(p));
        }

        MvcResult mr = this.mockMvc
                .perform(post("/photo/" + photos.get(0).getPhotoId())
                        .content("")
                        .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();

        log.debug("result: {}", mr.getResponse().getContentAsString());

        photos = photoService.getByCandidate(candidate.getCandidateId());
        log.debug("Photos after: {}", photos.size());
        for (Photo p : photos) {
            log.debug("photo: {}", om.writeValueAsString(p));
        }
    }

    @Test
    public void testDeletePhoto() throws Exception {
        log.debug("testDeletePhoto: ");

        ObjectMapper om = new ObjectMapper();
        List<Photo> photos = photoService.getByCandidate(candidate.getCandidateId());
        log.debug("Photos before: {}", photos.size());
        for (Photo p : photos) {
            log.debug("photo: {}", om.writeValueAsString(p));
        }

        int lastCount = photos.size();

        MvcResult mr = this.mockMvc
                .perform(delete("/photo/" + photos.get(0).getPhotoId())
                        .content(""))
                .andExpect(status().isOk()).andReturn();

        log.debug("result: {}", mr.getResponse().getContentAsString());

        photos = photoService.getByCandidate(candidate.getCandidateId());
        log.debug("Photos after: {}", photos.size());
        for (Photo p : photos) {
            log.debug("photo: {}", om.writeValueAsString(p));
        }

        Assert.assertEquals(lastCount - 1, photos.size());
    }
}