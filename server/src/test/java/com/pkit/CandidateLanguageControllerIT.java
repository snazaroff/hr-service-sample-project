package com.pkit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.data.Languages;
import com.pkit.generator.PersonGenerator;
import com.pkit.model.*;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
@ContextConfiguration(classes = ServerApplication.class)
public class CandidateLanguageControllerIT {

    @Autowired
    private ObjectMapper om;

    private MockMvc mockMvc;

    @Autowired
    Languages languages;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private PersonGenerator testPersonGenerator;

    @Autowired
    private CandidateService candidateService;

    private Candidate candidate;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        candidate = testPersonGenerator.getCandidate();
        candidate = candidateService.add(candidate);
    }

    @After
    public void destroy() {
        candidateService.delete(candidate);
    }

    @Test
    public void test() throws Exception {
        log.debug("test: ");

        addLanguage(candidate, languages.RUSSIAN, LanguageStatus.NATIVE);
        addLanguage(candidate, languages.ENGLISH, LanguageStatus.FLUENT);

        candidate = candidateService.get(candidate.getCandidateId());

        log.debug("candidateLanguages.size: {}", candidate.getLanguages().size());
        for(CandidateLanguage candidateLanguage: candidate.getLanguages()) {
            log.debug("candidateLanguage: {}", candidateLanguage);
        }

        Assert.assertEquals( 2L, candidate.getLanguages().size());

        deleteLanguage(candidate, languages.ENGLISH);

        candidate = candidateService.get(candidate.getCandidateId());

        log.debug("candidateLanguages.size: {}", candidate.getLanguages().size());
        for(CandidateLanguage candidateLanguage: candidate.getLanguages()) {
            log.debug("candidateLanguage: {}", candidateLanguage);
        }

        Assert.assertEquals( 1L, candidate.getLanguages().size());

    }

    private void addLanguage(Candidate candidate, Language language, LanguageStatus languageStatus) throws Exception {
        log.debug("addLanguage: candidate: {}, language: {}, languageStatus: {}", candidate, language, languageStatus);

        String url = String.format("/candidate/%s/language/%s/status/%s", candidate.getCandidateId(), language.getLanguageId(), languageStatus.getStatusId());
        log.debug("url: {}", url);

        String requestBody = om.writeValueAsString(candidate);
        MvcResult mr = this.mockMvc.perform(post(url).content(requestBody).contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        String str = mr.getResponse().getContentAsString();
        log.debug("str: {}", str);
    }

    private void deleteLanguage(Candidate candidate, Language language) throws Exception {
        log.debug("deleteLanguage: candidate: {}, language: {}", candidate, language);

        String url = String.format("/candidate/%s/language/%s", candidate.getCandidateId(), language.getLanguageId());
        log.debug("url: {}", url);

        MvcResult mr = this.mockMvc.perform(delete(url))
                .andExpect(status().isOk()).andReturn();

        String str = mr.getResponse().getContentAsString();
        log.debug("str: {}", str);
    }
}