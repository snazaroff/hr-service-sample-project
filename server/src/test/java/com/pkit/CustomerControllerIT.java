package com.pkit;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.generator.CompanyGenerator;
import com.pkit.model.Company;
import com.pkit.service.CompanyService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
public class CustomerControllerIT {

    @Autowired
    private ObjectMapper om;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private CompanyGenerator testFirmGenerator;

    @Autowired
    private CompanyService customerService;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetList() throws Exception {
        log.debug("testGetList: ");

        MvcResult mvcResult = this.mockMvc
                .perform(get("/company/"))
                .andExpect(status().isOk()).andReturn();

        List<Company> customerList = om.readValue(mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Company>>(){});
        Assert.assertEquals(5L, customerList.size());
    }

    @Test
    public void testAdd() throws Exception {
        log.debug("testAdd: ");

        Map<String, Object> firm = testFirmGenerator.getFirm(0);

        Company customer = (Company)firm.get("firm");

        String requestBody = om.writeValueAsString(customer);

        MvcResult mr = this.mockMvc
                .perform(post("/company/")
                        .content(requestBody)
                        .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        customer = om.readValue(mr.getResponse().getContentAsString(), Company.class);

        Company newCustomer = customerService.get(customer.getCompanyId());
        Assert.assertNotNull(newCustomer);
        Assert.assertEquals(customer, newCustomer);
        customerService.delete(newCustomer);
    }

    @Test
    public void testUpdate() throws Exception {
        log.debug("testUpdate: ");

        Map<String, Object> firm = testFirmGenerator.getFirm(0);
        Company customer = (Company)firm.get("firm");
        customer = customerService.add(customer);

        customer.setName("Name changed");
        customer.setAddress("Address changed");
//        company.setCreatedDate(new Date());

        String requestBody = om.writeValueAsString(customer);

        MvcResult mr = this.mockMvc
                .perform(put("/company/" + customer.getCompanyId())
                        .content(requestBody)
                        .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        Company newCustomer = om.readValue(mr.getResponse().getContentAsString(), Company.class);

        Assert.assertNotNull(newCustomer);
        Assert.assertEquals(customer, newCustomer);
        customerService.delete(customer);
    }

    @Test
    public void testDelete() throws Exception {
        log.debug("testDelete: ");

        Map<String, Object> firm = testFirmGenerator.getFirm(0);
        Company customer = (Company)firm.get("firm");
        customer = customerService.add(customer);

        MvcResult mr = this.mockMvc
                .perform(delete("/company/" + customer.getCompanyId()))
                .andExpect(status().isOk()).andReturn();

        customer = customerService.get(customer.getCompanyId());
        Assert.assertNull(customer);
    }
}