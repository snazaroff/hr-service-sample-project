package com.pkit;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.generator.TestDataGenerator;
import com.pkit.generator.DemandGenerator;
import com.pkit.generator.CompanyGenerator;
import com.pkit.generator.PersonGenerator;
import com.pkit.model.*;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
public class DemandControllerIT extends TestDataGenerator {

    @Autowired
    private ObjectMapper om;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private DemandGenerator testDemandGenerator;

    @Autowired
    private CompanyGenerator testFirmGenerator;

    @Autowired
    private PersonGenerator testPersonGenerator;

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private CompanyService customerService;

    @Autowired
    private DemandService demandService;

    @Autowired
    private DemandCandidateService demandCandidateService;

    @Autowired
    private PersonService personService;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetList() throws Exception {
        log.debug("testGetList: ");
        MvcResult mvcResult = this.mockMvc.perform(get("/demand/"))
                .andExpect(status().isOk()).andReturn();

        List<Demand> demandList = om.readValue(mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Demand>>(){});
        Assert.assertEquals( 1L, demandList.size());
    }

    @Test
    public void testAdd() throws Exception {
        log.debug("testAdd: ");

        Demand demand = testDemandGenerator.createDemand();
        log.debug("demand: " + om.writeValueAsString(demand));

        String requestBody = om.writeValueAsString(demand);
        MvcResult mr = this.mockMvc
                .perform(post("/demand/")
                        .content(requestBody)
                        .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        demand = om.readValue(mr.getResponse().getContentAsString(), Demand.class);

        Demand newDemand = demandService.get(demand.getDemandId());
        Assert.assertNotNull(newDemand);
        Assert.assertEquals(demand, newDemand);

        testDemandGenerator.destroyDemand(demand);
    }

    @Test
    public void testUpdate() throws Exception {
        log.debug("testUpdate: ");

        Demand demand = testDemandGenerator.createDemand();
        log.debug("demand: " + om.writeValueAsString(demand));

        Person oldContactPerson = demand.getContactPerson();
        Person oldResponsiblePerson = demand.getResponsiblePerson();
        Company oldCustomer = demand.getCompany();

        Map<String, Object> firm = testFirmGenerator.getFirm(2);

        Company customer = (Company)firm.get("firm");
        customer = customerService.add(customer);

        List<Person> persons = (List<Person>)firm.get("persons");

        Person contactPerson = persons.get(0);
        log.debug("contactPerson: " + contactPerson);
        contactPerson = personService.add(contactPerson);

        Person responsiblePerson = persons.get(1);
        log.debug("responsiblePerson: " + responsiblePerson);
        responsiblePerson = personService.add(responsiblePerson);

        demand.setCompany(customer);
        demand.setContactPerson(contactPerson);
        demand.setResponsiblePerson(responsiblePerson);
        demand.setCreateDate(LocalDateTime.now());
        demand.setSalary(new Salary(Math.round(Math.random() * 500), Currency.USD));
        demand.setPosition("Position changed");
        demand.setStatus(DemandStatus.CLOSE);
        demand.setDescription("New description");

        String requestBody = om.writeValueAsString(demand);
        MvcResult mr = this.mockMvc
                .perform(put("/demand/" + demand.getDemandId())
                        .content(requestBody)
                        .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        Demand newDemand = om.readValue(mr.getResponse().getContentAsString(), Demand.class);

        log.debug("newDemand: " + newDemand);
        Assert.assertNotNull(newDemand);
        Assert.assertEquals(demand, newDemand);

        testDemandGenerator.destroyDemand(demand);

        personService.delete(oldContactPerson);
        personService.delete(oldResponsiblePerson);
        customerService.delete(oldCustomer);
    }

    @Test
    public void testDelete() throws Exception {
        log.debug("testDelete: ");

        Demand demand = testDemandGenerator.createDemand();
        log.debug("demand: " + demand);
        log.debug("demand: " + om.writeValueAsString(demand));

        MvcResult mr = this.mockMvc.perform(delete("/demand/" + demand.getDemandId()))
                .andExpect(status().isOk()).andReturn();

        Demand newDemand = demandService.get(demand.getDemandId());
        Assert.assertNull(newDemand);

        testDemandGenerator.destroyDemand(demand);
    }

    @Test
    public void testCandidateAdd() throws Exception {
        log.debug("testCandidateAdd: ");

        Person person = testPersonGenerator.getPerson("ООО Васильки");
        personService.add(person);

        Candidate candidate = testPersonGenerator.getCandidate();
        candidateService.add(candidate);

        Demand demand = testDemandGenerator.createDemand();
        log.debug("demand: " + om.writeValueAsString(demand));

        DemandCandidate demandCandidate = new DemandCandidate();
        demandCandidate.setDemand(demand);
        demandCandidate.setCandidate(candidate);
        demandCandidate.setCreatedBy(person);
        demandCandidate.setCreateDate(LocalDateTime.now());

        log.debug("demandCandidate: " + demandCandidate);

        String requestBody = om.writeValueAsString(demandCandidate);
        MvcResult mr = this.mockMvc
                .perform(post("/demand/candidate/" + demand.getDemandId() + "/" + candidate.getCandidateId())
                        .content(requestBody)
                        .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        demandCandidate = om.readValue(mr.getResponse().getContentAsString(), DemandCandidate.class);

        log.debug("demandCandidate: " + demandCandidate);

        DemandCandidate newDemandCandidate = demandCandidateService.get(demandCandidate.getDemandCandidateId());
        Assert.assertNotNull(newDemandCandidate);
        Assert.assertEquals(demandCandidate, newDemandCandidate);

        testDemandGenerator.destroyDemand(demand);
        candidateService.delete(candidate);
        personService.delete(person);
    }

    @Test
    public void testCandidateUpdate() throws Exception {
        log.debug("testCandidateUpdate: ");

        Person person = testPersonGenerator.getPerson("ООО Васильки");
        personService.add(person);

        Candidate candidate = testPersonGenerator.getCandidate();
        candidateService.add(candidate);

        Demand demand = testDemandGenerator.createDemand();
        log.debug("demand: " + om.writeValueAsString(demand));

        DemandCandidate demandCandidate = new DemandCandidate();
        demandCandidate.setDemand(demand);
        demandCandidate.setCandidate(candidate);
        demandCandidate.setCreatedBy(person);
        demandCandidate.setCreateDate(LocalDateTime.now());
        demandCandidateService.add(demandCandidate);

        log.debug("demandCandidate: " + demandCandidate);

        demandCandidate.setCreateDate(LocalDateTime.now());

        String requestBody = om.writeValueAsString(demandCandidate);
        MvcResult mr = this.mockMvc
                .perform(put("/demand/candidate/" + demand.getDemandId() + "/" + candidate.getCandidateId())
                        .content(requestBody)
                        .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        demandCandidate = om.readValue(mr.getResponse().getContentAsString(), DemandCandidate.class);

        log.debug("demandCandidate: " + demandCandidate);

        DemandCandidate newDemandCandidate = demandCandidateService.get(demandCandidate.getDemandCandidateId());
        Assert.assertNotNull(newDemandCandidate);
        Assert.assertEquals(demandCandidate, newDemandCandidate);

        testDemandGenerator.destroyDemand(demand);
        candidateService.delete(candidate);
        personService.delete(person);
    }

    @Test
    public void testCandidateDelete() throws Exception {
        log.debug("testCandidateDelete: ");

        Person person = testPersonGenerator.getPerson("ООО Васильки");
        personService.add(person);

        Candidate candidate = testPersonGenerator.getCandidate();
        candidateService.add(candidate);

        Demand demand = testDemandGenerator.createDemand();
        log.debug("demand: " + om.writeValueAsString(demand));

        DemandCandidate demandCandidate = new DemandCandidate();
        demandCandidate.setDemand(demand);
        demandCandidate.setCandidate(candidate);
        demandCandidate.setCreatedBy(person);
        demandCandidate.setCreateDate(LocalDateTime.now());
        demandCandidateService.add(demandCandidate);

        log.debug("demandCandidate: " + demandCandidate);

        String requestBody = om.writeValueAsString(demandCandidate);
        MvcResult mr = this.mockMvc
                .perform(delete("/demand/candidate/" + demand.getDemandId() + "/" + candidate.getCandidateId())
                        .content(requestBody)
                        .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();

        DemandCandidate newDemandCandidate = demandCandidateService.get(demandCandidate.getDemandCandidateId());
        Assert.assertNull(newDemandCandidate);

        testDemandGenerator.destroyDemand(demand);
        candidateService.delete(candidate);
        personService.delete(person);
    }
}