package com.pkit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.generator.TestDataGenerator;
import com.pkit.generator.PersonGenerator;
import com.pkit.model.Candidate;
import com.pkit.model.Education;
import com.pkit.service.CandidateService;
import com.pkit.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
@ContextConfiguration(classes = ServerApplication.class)
public class CandidateEducationControllerIT {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestDataGenerator testDataGenerator;

    @Autowired
    private PersonGenerator testPersonGenerator;

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private PersonService personService;

    private Candidate candidate;

    @Autowired
    private ObjectMapper om;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        candidate = testPersonGenerator.getCandidate();
        candidate.setCreatedBy(personService.get(0L));
        candidate = candidateService.add(candidate);
    }

    @After
    public void destroy() {
        candidateService.delete(candidate);
    }

    @Test
    public void test() throws Exception {
        log.debug("test: candidate: {}", candidate);

        Education education = testDataGenerator.getSchool(candidate.getBirthDate().plusYears(7));
        education.setCandidate(candidate);

        addEducation(candidate, education);

        education = testDataGenerator.getInstitute(candidate.getBirthDate().plusYears(17));
        education.setCandidate(candidate);

        addEducation(candidate, education);

        candidate = candidateService.get(candidate.getCandidateId());

        log.debug("candidateEducations.size: {}", candidate.getEducations().size());
        for(Education emp: candidate.getEducations()) {
            log.debug("education: {}", emp);
            education = emp;
        }

        Assert.assertEquals( 2L, candidate.getEducations().size());

        education.setEducationName(education.getEducationName() + "!!!!");
        editEducation(candidate, education);

        candidate = candidateService.get(candidate.getCandidateId());

        log.debug("candidateEducations.size: {}", candidate.getEducations().size());
        for(Education emp: candidate.getEducations()) {
            log.debug("education: {}", emp);
            education = emp;
        }

        Assert.assertEquals( 2L, candidate.getEducations().size());

        deleteEducation(candidate, education);

        candidate = candidateService.get(candidate.getCandidateId());

        log.debug("candidateEducations.size: {}", candidate.getEducations().size());
        for(Education emp: candidate.getEducations()) {
            log.debug("education: {}", emp);
        }

        Assert.assertEquals( 1L, candidate.getEducations().size());
    }

    private void addEducation(Candidate candidate, Education education) throws Exception {
        log.debug("addEducation: candidate: {}, education: {}", candidate, education);

        String url = String.format("/candidate/%s/education", candidate.getCandidateId());
        log.debug("url: {}", url);

        log.debug("education: {}", om.writeValueAsString(education));

        String requestBody = om.writeValueAsString(education);
        MvcResult mr = this.mockMvc.perform(post(url).content(requestBody).contentType("application/json"))
                .andExpect(status().isOk()).andReturn();

        String str = mr.getResponse().getContentAsString();
        log.debug("str: {}", str);
    }

    private void editEducation(Candidate candidate, Education education) throws Exception {
        log.debug("editEducation: candidate: {}, education: {}", candidate, education);

        String url = String.format("/candidate/%s/education", candidate.getCandidateId());
        log.debug("url: {}", url);

        String requestBody = om.writeValueAsString(education);
        MvcResult mr = this.mockMvc.perform(put(url).content(requestBody).contentType("application/json"))
                .andExpect(status().isOk()).andReturn();

        String str = mr.getResponse().getContentAsString();
        log.debug("str: {}", str);
    }

    private void deleteEducation(Candidate candidate, Education education) throws Exception {
        log.debug("deleteEducation: candidate: {}, education: {}", candidate, education);

        String url = String.format("/candidate/education/%s", education.getEducationId());
        log.debug("url: {}", url);

        MvcResult mr = this.mockMvc.perform(delete(url))
                .andExpect(status().isOk()).andReturn();

        String str = mr.getResponse().getContentAsString();
        log.debug("str: {}", str);
    }
}