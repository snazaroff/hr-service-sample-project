package com.pkit;

import com.pkit.generator.PersonGenerator;
import com.pkit.service.PersonService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import com.pkit.model.Person;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
public class PersonControllerIT {

    @Autowired
    private ObjectMapper om;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private PersonGenerator testPersonGenerator;

    @Autowired
    private PersonService personService;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetList() throws Exception {
        log.debug("testGetList: ");

        MvcResult mvcResult = this.mockMvc.perform(get("/person/"))
                .andExpect(status().isOk()).andReturn();

        List<Person> personList = om.readValue(mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Person>>(){});
        log.debug("personList: " + om.writeValueAsString(personList));
        Assert.assertEquals(7L, personList.size());
    }

    @Test
    public void testAdd() throws Exception {
        log.debug("testAdd: ");

        Person person = testPersonGenerator.getPerson("ООО Василек");

        log.debug("person: " + person);

        String requestBody = om.writeValueAsString(person);
        MvcResult mr = this.mockMvc
                .perform(post("/person/")
                        .content(requestBody)
                        .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        person = om.readValue(mr.getResponse().getContentAsString(), Person.class);

        log.debug("person: " + person);

        Person newPerson = personService.get(person.getPersonId());
        Assert.assertNotNull(newPerson);
        Assert.assertEquals(person, newPerson);
        personService.delete(newPerson);
    }

    @Test
    public void testUpdate() throws Exception {
        log.debug("testUpdate: ");

        Person person = testPersonGenerator.getPerson("ООО Василек");
        person = personService.add(person);
        log.debug("person: " + person);

        person.setPhone("1111111");
        person.setFirstName("Изменили имя");
        person.setMiddleName("Изменили отчество");
        person.setLastName("Изменили фамилия");
        person.setIsActive(false);

        String requestBody = om.writeValueAsString(person);
        MvcResult mr = this.mockMvc
                .perform(put("/person/" + person.getPersonId())
                        .content(requestBody)
                        .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        Person newPerson = om.readValue(mr.getResponse().getContentAsString(), Person.class);

        Assert.assertNotNull(newPerson);
        Assert.assertEquals(person, newPerson);
        log.debug("person: " + newPerson);
        personService.delete(newPerson);
    }

    @Test
    public void testDelete() throws Exception {
        log.debug("testDelete: ");

        Person person = testPersonGenerator.getPerson("ООО Василек");
        person = personService.add(person);
        log.debug("department: " + person);

        MvcResult mr = this.mockMvc.perform(delete("/person/" + person.getPersonId()))
                .andExpect(status().isOk()).andReturn();

        person = personService.get(person.getPersonId());
        Assert.assertNull(person);
    }
}