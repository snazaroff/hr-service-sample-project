package com.pkit;

import com.pkit.generator.PersonGenerator;
import com.pkit.model.Candidate;
import com.pkit.model.Person;
import com.pkit.service.CandidateNoteService;
import com.pkit.service.CandidateService;
import com.pkit.service.PersonService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import com.pkit.model.CandidateNote;

import java.time.LocalDateTime;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
public class CandidateNoteControllerIT {

    @Autowired
    private ObjectMapper om;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private PersonGenerator testPersonGenerator;

    @Autowired
    private CandidateNoteService candidateNoteService;

    @Autowired
    private PersonService personService;

    @Autowired
    private CandidateService candidateService;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetCandidateNoteByCandidateId() throws Exception {
        log.debug("testGetCandidateNoteByCandidateId: ");

        Person person = testPersonGenerator.getPerson("ООО Васильки");
        personService.add(person);

        Candidate candidate = testPersonGenerator.getCandidate();
        candidateService.add(candidate);

        CandidateNote candidateNote = new CandidateNote();
        candidateNote.setCandidate(candidate);
        candidateNote.setCreatedBy(person);
        candidateNote.setCreateDate(LocalDateTime.now());
        candidateNote.setNote("It's test note #1!");
        candidateNote = candidateNoteService.add(candidateNote);

        candidateNote = new CandidateNote();
        candidateNote.setCandidate(candidate);
        candidateNote.setCreatedBy(person);
        candidateNote.setCreateDate(LocalDateTime.now());
        candidateNote.setNote("It's test note #2!");
        candidateNote = candidateNoteService.add(candidateNote);

        candidateNote = new CandidateNote();
        candidateNote.setCandidate(candidate);
        candidateNote.setCreatedBy(person);
        candidateNote.setCreateDate(LocalDateTime.now());
        candidateNote.setNote("It's test note #3!");
        candidateNote = candidateNoteService.add(candidateNote);

        MvcResult mvcResult = this.mockMvc.perform(get("/candidate/note/" + candidate.getCandidateId()))
                .andExpect(status().isOk()).andReturn();

        List<Candidate> candidateList = om.readValue(mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<CandidateNote>>(){});
        log.debug("candidateList: " + candidateList);
        Assert.assertEquals(3L, candidateList.size());
        candidateService.delete(candidate);
        personService.delete(person);
    }

    @Test
    public void testUpdate() throws Exception {
        log.debug("testUpdate: ");

        Person person = testPersonGenerator.getPerson("ООО Васильки");
        personService.add(person);

        Candidate candidate = testPersonGenerator.getCandidate();
        candidateService.add(candidate);

        CandidateNote candidateNote = new CandidateNote();
        candidateNote.setCandidate(candidate);
        candidateNote.setCreatedBy(person);
        candidateNote.setCreateDate(LocalDateTime.now());
        candidateNote.setNote("It's test note #1!");
        candidateNote = candidateNoteService.add(candidateNote);

        candidateNote.setNote("Note changed!");

        String requestBody = om.writeValueAsString(candidateNote);
        MvcResult mr = this.mockMvc.perform(put("/candidate/note/" + candidateNote.getCandidateNoteId()).content(requestBody).contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        candidateNote = om.readValue(mr.getResponse().getContentAsString(), CandidateNote.class);

        log.debug("candidateNote: " + candidateNote);
        Assert.assertEquals("Note changed!", candidateNote.getNote());
        candidateService.delete(candidate);
        personService.delete(person);
    }

    @Test
    public void testDelete() throws Exception {
        log.debug("testDelete: ");

        Person person = testPersonGenerator.getPerson("ООО Васильки");
        personService.add(person);

        Candidate candidate = testPersonGenerator.getCandidate();
        candidateService.add(candidate);

        CandidateNote candidateNote = new CandidateNote();
        candidateNote.setCandidate(candidate);
        candidateNote.setCreatedBy(person);
        candidateNote.setCreateDate(LocalDateTime.now());
        candidateNote.setNote("It's test note #1!");
        candidateNote = candidateNoteService.add(candidateNote);

        MvcResult mr = this.mockMvc.perform(delete("/candidate/note/" + candidateNote.getCandidateNoteId()))
                .andExpect(status().isOk()).andReturn();

        candidateNote = candidateNoteService.get(candidateNote.getCandidateNoteId());
        Assert.assertNull(candidateNote);
        candidateService.delete(candidate);
        personService.delete(person);
    }
}