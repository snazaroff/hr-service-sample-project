package com.pkit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.generator.TestDataGenerator;
import com.pkit.generator.CompanyGenerator;
import com.pkit.generator.PersonGenerator;
import com.pkit.model.Candidate;
import com.pkit.model.Employment;
import com.pkit.service.CandidateService;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.text.SimpleDateFormat;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
@ContextConfiguration(classes = ServerApplication.class)
public class CandidateEmploymentControllerIT {

    @Autowired
    private ObjectMapper om;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestDataGenerator testDataGenerator;

    @Autowired
    private PersonGenerator testPersonGenerator;

    @Autowired
    private CompanyGenerator testFirmGenerator;

    @Autowired
    private CandidateService candidateService;

    private Candidate candidate;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        candidate = testPersonGenerator.getCandidate();
        candidate = candidateService.add(candidate);
    }

    @After
    public void destroy() {
        candidateService.delete(candidate);
    }

    @Test
    public void test() throws Exception {
        log.debug("test: candidate: {}", candidate);

        Employment employment = new Employment();
        employment.setFirmName(testFirmGenerator.getFirmName());
        employment.setCandidate(candidate);
        employment.setStartDate(sdf.parse("1983.04.05"));
        employment.setEndDate(sdf.parse("1987.10.13"));
        employment.setPosition(testDataGenerator.getPosition());

        addEmployment(candidate, employment);

        employment = new Employment();
        employment.setFirmName(testFirmGenerator.getFirmName());
        employment.setCandidate(candidate);
        employment.setStartDate(sdf.parse("1987.10.15"));
        employment.setEndDate(sdf.parse("1988.01.01"));
        employment.setPosition(testDataGenerator.getPosition());

        addEmployment(candidate, employment);

        candidate = candidateService.get(candidate.getCandidateId());

        log.debug("candidateEmployments.size: {}", candidate.getEmployments().size());
        for(Employment emp: candidate.getEmployments()) {
            log.debug("employment: {}", emp);
            employment = emp;
        }

        Assert.assertEquals( 2L, candidate.getEmployments().size());

        employment.setFirmName(employment.getFirmName() + "!!!!");
        editEmployment(candidate, employment);

        candidate = candidateService.get(candidate.getCandidateId());

        log.debug("candidateEmployments.size: {}", candidate.getEmployments().size());
        for(Employment emp: candidate.getEmployments()) {
            log.debug("employment: {}", emp);
            employment = emp;
        }

        Assert.assertEquals( 2L, candidate.getEmployments().size());

        deleteEmployment(candidate, employment);

        candidate = candidateService.get(candidate.getCandidateId());

        log.debug("candidateEmployments.size: {}", candidate.getEmployments().size());
        for(Employment emp: candidate.getEmployments()) {
            log.debug("employment: {}", emp);
        }

        Assert.assertEquals( 1L, candidate.getEmployments().size());
    }

    private void addEmployment(Candidate candidate, Employment employment) throws Exception {
        log.debug("addEmployment: candidate: {}, employment: {}", candidate, employment);

        String url = String.format("/candidate/%s/employment", candidate.getCandidateId());
        log.debug("url: {}", url);

        String requestBody = om.writeValueAsString(employment);
        MvcResult mr = this.mockMvc.perform(post(url).content(requestBody).contentType("application/json"))
                .andExpect(status().isOk()).andReturn();

        String str = mr.getResponse().getContentAsString();
        log.debug("str: {}", str);
    }

    private void editEmployment(Candidate candidate, Employment employment) throws Exception {
        log.debug("editEmployment: candidate: {}, employment: {}", candidate, employment);

        String url = String.format("/candidate/%s/employment", candidate.getCandidateId());
        log.debug("url: {}", url);

        String requestBody = om.writeValueAsString(employment);
        MvcResult mr = this.mockMvc.perform(put(url).content(requestBody).contentType("application/json"))
                .andExpect(status().isOk()).andReturn();

        String str = mr.getResponse().getContentAsString();
        log.debug("str: {}", str);
    }

    private void deleteEmployment(Candidate candidate, Employment employment) throws Exception {
        log.debug("deleteEmployment: candidate: {}, employment: {}", candidate, employment);

        String url = String.format("/candidate/employment/%s", employment.getEmploymentId());
        log.debug("url: {}", url);

        MvcResult mr = this.mockMvc.perform(delete(url))
                .andExpect(status().isOk()).andReturn();

        String str = mr.getResponse().getContentAsString();
        log.debug("str: {}", str);
    }
}