package com.pkit.service;

import com.pkit.ServerApplication;
import com.pkit.model.Country;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Collection;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
@ContextConfiguration(classes = ServerApplication.class)
public class DictionaryServiceIT {

    @Autowired
    private DictionaryService dictionaryService;

    @Test
    public void testGetContries() {
        log.debug("testGetEventList: ");

        Collection<Country> countryList = dictionaryService.getAllCountries();
        log.debug("countryList.size(): {}", countryList.size());
        countryList.stream().limit(5).forEach(System.out::println);

        countryList = dictionaryService.getPrimaryCountries();
        log.debug("countryList.size(): {}", countryList.size());
        countryList.stream().limit(5).forEach(System.out::println);
    }
}