package com.pkit.service;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.ServerApplication;
import com.pkit.data.Languages;
import com.pkit.generator.TestDataGenerator;
import com.pkit.generator.CompanyGenerator;
import com.pkit.generator.PersonGenerator;
import com.pkit.model.*;
import com.pkit.model.Currency;
import com.pkit.model.filter.CandidateFilter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
@ContextConfiguration(classes = ServerApplication.class)
public class CandidateServiceIT {

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");

    private final DefaultPrettyPrinter printer = new DefaultPrettyPrinter();

    private final ObjectMapper om = new ObjectMapper();

    @Autowired
    Languages languages;

    @Autowired
    private TestDataGenerator testDataGenerator;

    @Autowired
    private CompanyGenerator testFirmGenerator;

    @Autowired
    private PersonGenerator testPersonGenerator;

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private EducationService educationService;

    @Test
    public void testGetList() {
        log.debug("testGetList: ");

        Collection<Candidate> candidateList = candidateService.getAll();
        log.debug("candidateList: ");
        for(Candidate c: candidateList)
            log.debug(c.toString());
        Assert.assertEquals( 3L, candidateList.size());
    }

    @Test
    public void testAdd() throws Exception {
        log.debug("testAdd: ");

        Candidate candidate = testPersonGenerator.getCandidate();
        candidate.setStatus(CandidateStatus.NOT_LOOKING_FOR_JOB);

        Education education = new Education();
        education.setEducationName("Школа им. Полиграфа Полиграфовича Шарикова");
        education.setCandidate(candidate);
        education.setStartDate(LocalDate.of(1980, 9, 1));
        education.setEndDate(LocalDate.of(1990, 5, 31));
        education.setEducationType(EducationType.HIGH_SCHOOL);
        candidate.getEducations().add(education);

        education = new Education();
        education.setEducationName("Музыкальная школа им. Бременских музыкантов");
        education.setCandidate(candidate);
        education.setStartDate(LocalDate.of(1983, 9, 1));
        education.setEndDate(LocalDate.of(1985, 1, 1));
        education.setEducationType(EducationType.HIGH_SCHOOL);
        candidate.getEducations().add(education);

        Employment employment = new Employment();
        employment.setFirmName(testFirmGenerator.getFirmName());
        employment.setCandidate(candidate);
        employment.setStartDate(sdf.parse("1983.04.05"));
        employment.setEndDate(sdf.parse("1987.10.13"));
        employment.setPosition(testDataGenerator.getPosition());
        candidate.getEmployments().add(employment);

        employment = new Employment();
        employment.setFirmName(testFirmGenerator.getFirmName());
        employment.setCandidate(candidate);
        employment.setStartDate(sdf.parse("1987.10.15"));
        employment.setEndDate(sdf.parse("1988.01.01"));
        employment.setPosition(testDataGenerator.getPosition());
        candidate.getEmployments().add(employment);

        CandidateLanguage candidateLanguage = new CandidateLanguage(candidate, languages.findByName("Английский"));
        candidateLanguage.setStatus(LanguageStatus.BASIC);
        candidate.getLanguages().add(candidateLanguage);

        candidateLanguage = new CandidateLanguage(candidate, languages.findByName("Немецкий"));
        candidateLanguage.setStatus(LanguageStatus.CAN_PASS_INTERVIEW);
        candidate.getLanguages().add(candidateLanguage);

        log.debug("candidate: " + om.writer(printer).writeValueAsString(candidate));
        candidate = candidateService.add(candidate);
        log.debug("candidate: " + om.writer(printer).writeValueAsString(candidate));

        Candidate newCandidate = candidateService.get(candidate.getCandidateId());
        Assert.assertNotNull(newCandidate);
        Assert.assertEquals(candidate, newCandidate);
        candidateService.delete(newCandidate);
    }

    @Test
    public void testUpdate() throws Exception {
        log.debug("testUpdate: ");

        Candidate candidate = testPersonGenerator.getCandidate();
        log.debug("candidate: " + candidate);

        candidate.setPhone("1111111");
        candidate.setFirstName("Изменили имя");
        candidate.setMiddleName("Изменили отчество");
        candidate.setLastName("Изменили фамилия");
        candidate.setSalary(new Salary(150L, Currency.RUB));
        candidate.setStatus(CandidateStatus.LOOKING_FOR_JOB);

        Set<Education> educations = new HashSet<>();
        Education education = new Education();
        education.setEducationName("Школа им. Полиграфа Полиграфовича Шарикова");
        education.setCandidate(candidate);
        education.setStartDate(LocalDate.now());
        education.setEducationType(EducationType.HIGH_SCHOOL);
        educations.add(education);
        candidate.setEducations(educations);

        candidate = candidateService.add(candidate);

        Collection<Education> eds = educationService.getAll();
        log.debug("eds.size(): " + eds.size());
        log.debug("eds: " + eds);

        log.debug("candidate: " + om.writer(printer).writeValueAsString(candidate));

        education = new Education();
        education.setEducationName("Музыкальная школа им. Бременских музыкантов");
        education.setCandidate(candidate);
        education.setStartDate(LocalDate.now());
        education.setEducationType(EducationType.HIGH_SCHOOL);
        candidate.getEducations().add(education);

        candidate = candidateService.update(candidate);

        eds = educationService.getAll();
        log.debug("eds.size(): " + eds.size());
        log.debug("eds: " + eds);

        log.debug("candidate: " + om.writer(printer).writeValueAsString(candidate));

        candidate.getEducations().remove(candidate.getEducations().iterator().next());

        log.debug("candidate.getEducations().size(): " + candidate.getEducations().size());

        candidate = candidateService.update(candidate);

        eds = educationService.getAll();
        log.debug("eds.size(): " + eds.size());
        log.debug("eds: " + eds);

        log.debug("candidate: " + om.writer(printer).writeValueAsString(candidate));

        candidateService.delete(candidate);
    }

    @Test
    public void testDelete() {
        log.debug("testDelete: ");

        Candidate candidate = testPersonGenerator.getCandidate();
        candidate = candidateService.add(candidate);

        candidateService.delete(candidate);

        candidate = candidateService.get(candidate.getCandidateId());
        Assert.assertNull(candidate);
    }

   @Test
    public void testGetFilteredList() {
        log.debug("testGetFilteredList: ");

        Collection<Candidate> candidateList = candidateService.getAll();
        log.debug("candidateList: {}", candidateList.size());
        for(Candidate c: candidateList)
            log.debug(c.toString());
        Assert.assertEquals( 3L, candidateList.size());

        CandidateFilter candidateFilter = new CandidateFilter();
        candidateFilter.setLastName("Головешкин");
        candidateList = candidateService.findByFilter(candidateFilter, new PageRequest(1, 10)).getContent();
        log.debug("candidateList: {}", candidateList.size());
        for(Candidate c: candidateList)
            log.debug(c.toString());
        Assert.assertEquals( 1L, candidateList.size());

        candidateFilter.setLastName("");
        candidateFilter.setFirstName("Ипполит");
        candidateList = candidateService.findByFilter(candidateFilter, new PageRequest(1, 10)).getContent();
        log.debug("candidateList: {}", candidateList.size());
        for(Candidate c: candidateList)
            log.debug(c.toString());
        Assert.assertEquals( 1L, candidateList.size());

        candidateFilter.setLastName(null);
        candidateFilter.setFirstName("Ипполит");
        candidateFilter.setMiddleName("Макарыч");
        candidateList = candidateService.findByFilter(candidateFilter, new PageRequest(1, 10)).getContent();
        log.debug("candidateList: {}", candidateList.size());
        for(Candidate c: candidateList)
            log.debug(c.toString());
        Assert.assertEquals( 1L, candidateList.size());

       candidateFilter.setLastName(null);
       candidateFilter.setFirstName(null);
       candidateFilter.setMiddleName(null);
       candidateFilter.setCategory(categoryService.getCategory(136L));
       candidateList = candidateService.findByFilter(candidateFilter, new PageRequest(1, 10)).getContent();
       log.debug("candidateList: {}", candidateList.size());
       for(Candidate c: candidateList)
           log.debug(c.toString());
       Assert.assertEquals( 1L, candidateList.size());

       Candidate candidate = testPersonGenerator.getCandidate();

       Education education = testDataGenerator.getSchool(candidate.getBirthDate().plusYears(7));
       education.setCandidate(candidate);

       education = testDataGenerator.getInstitute(candidate.getBirthDate().plusYears(7));
       education.setCandidate(candidate);

       candidateService.add(candidate);
   }
}