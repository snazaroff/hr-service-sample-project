package com.pkit.service;

import com.pkit.ServerApplication;
import com.pkit.model.Candidate;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.*;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ServerApplication.class)
@Slf4j
@WebAppConfiguration
@ContextConfiguration(classes = {ServerApplication.class})
public class DownloadServiceIT {

    @Autowired
    private DownloadService downloadService;

    @Autowired
    private CandidateService candidateService;

    @Test
//    @Ignore
    public void testDownloadPdf() throws Exception {
        log.debug("testDownloadPdf: ");

        List<Candidate> candidates = candidateService.getAll();

        DownloadService.Result result = downloadService.download(candidates.get(0).getCandidateId(), "PDF");
        byte[] resume = result.file;

        IOUtils.write(resume, new FileOutputStream(new File("resume.pdf")));
   }

    @Test
//    @Ignore
    public void testDownloadDocx() throws Exception {
        log.debug("testDownloadDocx: ");

        List<Candidate> candidates = candidateService.getAll();

        DownloadService.Result result = downloadService.download(candidates.get(0).getCandidateId(), "DOCX");
        byte[] resume = result.file;

        IOUtils.write(resume, new FileOutputStream(new File("resume.docx")));
    }
}