package com.pkit.service;

import com.pkit.ServerApplication;
import com.pkit.model.event.Event;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Collection;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
@ContextConfiguration(classes = ServerApplication.class)
public class CandidateHistoryServiceIT {

    private final CandidateHistoryService candidateHistoryService;

    public CandidateHistoryServiceIT(CandidateHistoryService candidateHistoryService) {
        this.candidateHistoryService = candidateHistoryService;
    }

    @Test
    public void testGetEventList() {
        log.debug("testGetEventList: ");

        Collection<Event> eventList = candidateHistoryService.get(1L, (short)0);
        log.debug("eventList: ");
        for(Event e: eventList)
            log.debug(e.toString());
        Assert.assertEquals( 3L, eventList.size());
    }

    @Test
    public void testGteLatEventList() {
        log.debug("testGteLatEventList: ");

        Collection<Event> eventList = candidateHistoryService.get(1L, (short)0);
        log.debug("eventList: ");
        for(Event e: eventList)
            log.debug(e.toString());
        Assert.assertEquals( 3L, eventList.size());
    }

    @Test
    public void testGetEventList5() {
        log.debug("testGetEventList5: ");

        Collection<Event> eventList = candidateHistoryService.get(1L, (short)3);
        log.debug("eventList: ");
        for(Event e: eventList)
            log.debug(e.toString());
        Assert.assertEquals( 3L, eventList.size());
    }

    @Test
    public void testGteLatEventList5() {
        log.debug("testGteLatEventList5: ");

        Collection<Event> eventList = candidateHistoryService.get(1L, (short)3);
        log.debug("eventList: ");
        for(Event e: eventList)
            log.debug(e.toString());
        Assert.assertEquals( 3L, eventList.size());
    }

}