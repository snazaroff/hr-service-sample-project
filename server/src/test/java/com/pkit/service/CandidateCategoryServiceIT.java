package com.pkit.service;

import com.pkit.ServerApplication;
import com.pkit.model.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
@ContextConfiguration(classes = ServerApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CandidateCategoryServiceIT {

    private final CandidateService candidateService;

    public CandidateCategoryServiceIT(CandidateService candidateService) {
        this.candidateService = candidateService;
    }

    @Test
    @Ignore
    public void testGetList() {
        log.debug("testGetList: ");

        Collection<Category> categoryList = candidateService.getCategoryList(1L);
        log.debug("candidateList: ");
        for(Category c: categoryList)
            log.debug(c.toString());
        Assert.assertEquals( 6L, categoryList.size());
    }

    @Test
    @Ignore
    public void testGetListWithTop() {
        log.debug("testGetListWithTop: ");

        Collection<Category> categoryList = candidateService.getCategoryListWithTopCategory(1L);
        log.debug("candidateList: ");
        for(Category c: categoryList)
            log.debug(c.toString());
        Assert.assertEquals( 9L, categoryList.size());
    }

    @Test
    public void testAdd() {
        log.debug("testAdd: ");

        Collection<Category> categoryList = candidateService.getCategoryList(1L);
        log.debug("candidateList: {}", categoryList.size());
        for (Category c : categoryList)
            log.debug(c.toString());

        Assert.assertEquals( 6L, categoryList.size());

        candidateService.addCategory(1L, 155L);

        categoryList = candidateService.getCategoryList(1L);
        log.debug("candidateList: {}", categoryList.size());
        for (Category c : categoryList)
            log.debug(c.toString());

        Assert.assertEquals( 7L, categoryList.size());

        candidateService.deleteCategory(1L, 155L);

        categoryList = candidateService.getCategoryList(1L);
        Assert.assertEquals( 6L, categoryList.size());
    }

    @Test
    @Ignore
    public void testDelete() {
        log.debug("testDelete: ");

        Collection<Category> categoryList = candidateService.getCategoryList(1L);
        log.debug("candidateList: {}", categoryList.size());
        for (Category c : categoryList)
            log.debug(c.toString());

        Assert.assertEquals( 6L, categoryList.size());

        candidateService.addCategory(1L, 155L);

        categoryList = candidateService.getCategoryList(1L);
        log.debug("candidateList: {}", categoryList.size());
        for (Category c : categoryList)
            log.debug(c.toString());
        Assert.assertEquals( 7L, categoryList.size());

        candidateService.deleteCategory(1L, 155L);

        categoryList = candidateService.getCategoryList(1L);
        log.debug("candidateList: {}", categoryList.size());
        for (Category c : categoryList)
            log.debug(c.toString());
        Assert.assertEquals( 6L, categoryList.size());
    }
}