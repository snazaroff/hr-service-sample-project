package com.pkit.service;

import com.pkit.ServerApplication;
import com.pkit.generator.TestDataGenerator;
import com.pkit.generator.PersonGenerator;
import com.pkit.model.*;
import com.pkit.model.filter.CandidateFilter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Collection;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
@ContextConfiguration(classes = ServerApplication.class)
public class CandidateFilterServiceIT {

    @Autowired
    private TestDataGenerator testDataGenerator;

    @Autowired
    private PersonGenerator testPersonGenerator;

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private CategoryService categoryService;

   @Test
    public void testGetFilteredList() {
        log.debug("testGetFilteredList: ");

	//Все кандидаты
        Collection<Candidate> candidateList = candidateService.getAll();
        log.debug("candidateList: {}", candidateList.size());
        for(Candidate c: candidateList)
            log.debug(c.toString());
        Assert.assertEquals( 3L, candidateList.size());

	//Ищет только по фамилии "Головешкин"
        CandidateFilter candidateFilter = new CandidateFilter();
        candidateFilter.setLastName("Головешкин");
        candidateList = candidateService.findByFilter(candidateFilter, new PageRequest(1, 10)).getContent();
        log.debug("candidateList: {}", candidateList.size());
        for(Candidate c: candidateList)
            log.debug(c.toString());
        Assert.assertEquals( 1L, candidateList.size());

	//Ищет только по имени "Ипполит"
        candidateFilter.clear();
        candidateFilter.setFirstName("Ипполит");
        candidateList = candidateService.findByFilter(candidateFilter, new PageRequest(1, 10)).getContent();
        log.debug("candidateList: {}", candidateList.size());
        for(Candidate c: candidateList)
            log.debug(c.toString());
        Assert.assertEquals( 1L, candidateList.size());

	//Ищет только по имени "Ипполит" и отчеству "Макарыч"
        candidateFilter.clear();
        candidateFilter.setFirstName("Ипполит");
        candidateFilter.setMiddleName("Макарыч");
        candidateList = candidateService.findByFilter(candidateFilter, new PageRequest(1, 10)).getContent();
        log.debug("candidateList: {}", candidateList.size());
        for(Candidate c: candidateList)
            log.debug(c.toString());
        Assert.assertEquals( 1L, candidateList.size());

	//Ищет только по категории
        candidateFilter.clear();
        candidateFilter.setCategory(categoryService.getCategory(136L));
        candidateList = candidateService.findByFilter(candidateFilter, new PageRequest(1, 10)).getContent();
        log.debug("candidateList: {}", candidateList.size());
        for(Candidate c: candidateList)
            log.debug(c.toString());
        Assert.assertEquals( 1L, candidateList.size());

	//Ищет только по метро
        candidateFilter.clear();
        candidateFilter.setMetroId(50L);
        candidateList = candidateService.findByFilter(candidateFilter, new PageRequest(1, 10)).getContent();
        log.debug("candidateList: {}", candidateList.size());
        for(Candidate c: candidateList)
            log.debug(c.toString());
        Assert.assertEquals( 1L, candidateList.size());

	//Ищет только по зарплате
        candidateFilter.clear();
        candidateFilter.setSalaryMin(400L);
        candidateFilter.setSalaryMax(550L);
        candidateList = candidateService.findByFilter(candidateFilter, new PageRequest(1, 10)).getContent();
        log.debug("candidateList: {}", candidateList.size());
        for(Candidate c: candidateList)
            log.debug(c.toString());
        Assert.assertEquals( 1L, candidateList.size());

       //Ищет только по зарплате
       candidateFilter.clear();
       candidateFilter.setSalaryMin(400L);
       candidateFilter.setSalaryMax(600L);
       candidateList = candidateService.findByFilter(candidateFilter, new PageRequest(1, 10)).getContent();
       log.debug("candidateList: {}", candidateList.size());
       for(Candidate c: candidateList)
           log.debug(c.toString());
       Assert.assertEquals( 2L, candidateList.size());

       //Ищет только по слову
        candidateFilter.clear();
        candidateFilter.setWords("Василек");
        candidateList = candidateService.findByFilter(candidateFilter, new PageRequest(1, 10)).getContent();
        log.debug("candidateList: {}", candidateList.size());
        for(Candidate c: candidateList)
            log.debug(c.toString());
        Assert.assertEquals( 1L, candidateList.size());

        Candidate candidate = testPersonGenerator.getCandidate();

        Education education = testDataGenerator.getSchool(candidate.getBirthDate().plusYears(7));
        education.setCandidate(candidate);

        education = testDataGenerator.getInstitute(candidate.getBirthDate().plusYears(7));
        education.setCandidate(candidate);

        candidateService.add(candidate);
    }
}