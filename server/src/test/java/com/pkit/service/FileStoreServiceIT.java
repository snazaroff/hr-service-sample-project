package com.pkit.service;

import com.pkit.ServerApplication;
import com.pkit.model.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.*;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ServerApplication.class)
@Slf4j
@WebAppConfiguration
@ContextConfiguration(classes = {ServerApplication.class})
public class FileStoreServiceIT {

    private final FileStoreService fileStoreService;

    public FileStoreServiceIT(FileStoreService fileStoreService) {
        this.fileStoreService = fileStoreService;
    }

    @Test
//    @Ignore
    public void test() throws Exception {
        log.debug("test()");

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();

        byte[] file;

        try (InputStream is = classloader.getResourceAsStream("test.pdf")) {
            file = IOUtils.toByteArray(is);
        }

        List<Long> ownerIds = Arrays.asList(1000L, 2000L);
        int index = 0;
        for(Long ownerId : ownerIds) {
            for(int i = 0; i < 10; i++) {
                FileInfo fileInfo = new FileInfo();
                fileInfo.setOwnerId(ownerId);
                fileInfo.setContentType("image/jpeg");
                fileInfo.setName("image_" + index++ + ".pdf");

                fileInfo = fileStoreService.addFile(fileInfo, file);
                log.debug("fileInfo.getFileId(): {}", fileInfo.getFileId());
                log.debug("fileInfo: {}", fileInfo);
            }
        }

        List<FileInfo> fileInfos = fileStoreService.getByOwnerId(1000L);
        log.debug("fileInfos.size(): {}", fileInfos.size());

        Long fileId1 = fileInfos.get(0).getFileId();
        log.debug("fileId1: {}", fileId1);

        fileInfos = fileStoreService.getByOwnerId(2000L);
        log.debug("fileInfos.size(): {}", fileInfos.size());

        Long fileId2 = fileInfos.get(0).getFileId();
        log.debug("fileId2: {}", fileId2);

        byte[] fileContent;

        FileInfo fileInfo = fileStoreService.getByFileId(fileId1);

        log.debug("fileInfo: {}", fileInfo);

        fileContent = fileStoreService.getFileContent(fileId1);
        log.debug("fileContent: {}", fileContent.length);

        fileInfo = fileStoreService.getByFileId(fileId2);

        log.debug("fileInfo: {}", fileInfo);

        fileContent = fileStoreService.getFileContent(fileId1);
        log.debug("fileContent: {}", fileContent.length);

        fileStoreService.deleteByFileId(fileId1);

        fileInfos = fileStoreService.getByOwnerId(1000L);
        log.debug("fileInfos.size(): {}", fileInfos.size());

//        log.debug("fileInfos.size(): {}", fileInfos.size());
//
//        byte[] fileContent = fileStoreService.getFileContent(fileInfo.getFileId());
//        log.debug("fileContent: {}", fileContent.length);
//
//        byte[] fileContent = fileStoreService.deleteByFileId(fileInfo.getFileId());
//        log.debug("fileContent: {}", fileContent.length);

    }
}