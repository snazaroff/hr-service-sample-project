package com.pkit.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.ServerApplication;
import com.pkit.model.Candidate;
import com.pkit.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.swing.text.Document;
import javax.swing.text.rtf.RTFEditorKit;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ServerApplication.class)
@Slf4j
@WebAppConfiguration
@ContextConfiguration(classes = {ServerApplication.class})
public class ImportServiceIT {

    @Autowired
    private ObjectMapper om;

    @Autowired
    private ImportService importService;

    @Autowired
    private PersonService personService;

    @Test
    @Ignore
    public void testImportResume() throws Exception {
        log.debug("testImportResume: ");

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        byte[] file;
        try (InputStream is = classloader.getResourceAsStream("test.pdf")) {
            file = IOUtils.toByteArray(is);
        }

        byte[] fileRtf;
        try (InputStream is = classloader.getResourceAsStream("test.rtf")) {
            fileRtf = IOUtils.toByteArray(is);
        }

        RTFEditorKit rtfParser = new RTFEditorKit();
        Document document = rtfParser.createDefaultDocument();
        rtfParser.read(new ByteArrayInputStream(fileRtf), document, 0);
        String text = document.getText(0, document.getLength());

        IOUtils.write(text.getBytes(), new FileOutputStream(new File("test.rtf.txt")));

        Person person = personService.get(0L);
        importService.importResume(file, "test.file", person, person.getDepartment().getCompany());
    }

    @Test
    public void testImportRtf() throws Exception {
        log.debug("testImportRtf: ");

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        byte[] file;
        try (InputStream is = classloader.getResourceAsStream("test.rtf")) {
            file = IOUtils.toByteArray(is);
        }

        Person person = personService.get(0L);
        Candidate candidate = importService.importResume(file, "test.file", person, person.getDepartment().getCompany());
        log.debug("candidate: {}", om.writeValueAsString(candidate));
    }
}