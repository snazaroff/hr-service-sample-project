package com.pkit.configuration;

import com.pkit.model.CandidateIndex;
import org.elasticsearch.client.Client;
import org.elasticsearch.node.Node;
import org.elasticsearch.common.settings.Settings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import javax.annotation.PreDestroy;

import static org.elasticsearch.node.NodeBuilder.nodeBuilder;

@Slf4j
@Configuration
public class EsConfig {

    Node node;

    @Bean
    public Client es(@Value("${elasticsearch.cluster:none}") String cluster
    ) {
        log.debug("EsConfig.es.cluster: {}", cluster);
        if (cluster.equals("none")) {
            node = nodeBuilder().local(true).settings(Settings.builder().put("path.home", System.getProperty("java.io.tmpdir") + "/es")).node();
        } else {
            node = nodeBuilder().clusterName(cluster).node(); // loads from a local elasticsearch.yml definition
        }

//        ElasticsearchTemplate elasticsearchTemplate = new ElasticsearchTemplate(node.client());
//        elasticsearchTemplate.deleteIndex(CandidateIndex.class);
//        elasticsearchTemplate.createIndex(CandidateIndex.class);
//        elasticsearchTemplate.putMapping(CandidateIndex.class);
//        elasticsearchTemplate.refresh(CandidateIndex.class);
        return node.client();
    }


    @Bean
    public ElasticsearchOperations elasticsearchTemplate(@Value("${elasticsearch.cluster:none}") String cluster) {

        log.debug("cluster: {}", cluster);

        if (cluster.equals("none")) {
            node = nodeBuilder()
                    .local(true)
                    .settings(
                            Settings.builder()
                                    .put("http.enabled", "true")
                                    .put("path.home", "/es")).node();
        } else {
            node = nodeBuilder().clusterName(cluster).node(); // loads from a local elasticsearch.yml definition
        }

        ElasticsearchTemplate elasticsearchTemplate = new ElasticsearchTemplate(node.client());
        elasticsearchTemplate.deleteIndex(CandidateIndex.class);
        elasticsearchTemplate.createIndex(CandidateIndex.class);
        elasticsearchTemplate.putMapping(CandidateIndex.class);
        elasticsearchTemplate.refresh(CandidateIndex.class);

        log.debug("elasticsearchTemplate: {}", elasticsearchTemplate);
        return elasticsearchTemplate;
    }

    @PreDestroy
    void destroy() {
        if (node != null) {
            log.info("stopping es server");
            try {
                node.close();
            } catch (Exception e) {
                log.error("could not stop es server", e);
            }
        }
    }
}