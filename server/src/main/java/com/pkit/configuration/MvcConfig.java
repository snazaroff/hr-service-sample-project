package com.pkit.configuration;

import com.pkit.conversion.DateFormatter;
import com.pkit.conversion.DateTimeFormatter;
import com.pkit.conversion.LocalDateFormatter;
import com.pkit.conversion.LocalDateTimeFormatter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.resource.WebJarsResourceResolver;

@Slf4j
@Configuration
@EnableWebMvc
@ComponentScan("com.pkit")
public class MvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        log.debug("addResourceHandlers");
        registry.addResourceHandler("/html/**").addResourceLocations("classpath:/static/html/");
        registry.addResourceHandler("/css/**").addResourceLocations("classpath:/static/css/");
        registry.addResourceHandler("/js/**").addResourceLocations("classpath:/static/js/");
        registry.addResourceHandler("/gif/**").addResourceLocations("classpath:/static/gif/");
        registry.addResourceHandler("/fonts/**").addResourceLocations("classpath:/static/fonts/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("/webjars/").resourceChain(true).addResolver(new WebJarsResourceResolver());
        super.addResourceHandlers(registry);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        log.debug("addViewControllers");

        registry.addViewController("/").setViewName("home");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/home").setViewName("home");
        registry.addViewController("/index").setViewName("index");
        registry.addViewController("/hello").setViewName("hello");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/main").setViewName("main");
        registry.addViewController("/admin").setViewName("admin");
        registry.addViewController("/comments").setViewName("comments");
        registry.addViewController("/tasks").setViewName("tasks");
        registry.addViewController("/candidate/").setViewName("candidates");
        registry.addViewController("/company/").setViewName("companies");
        registry.addViewController("/customer/").setViewName("customers");
        registry.addViewController("/department/").setViewName("departments");
        registry.addViewController("/demand/").setViewName("demands");
        registry.addViewController("/person/").setViewName("persons");
        registry.addViewController("/person_profile").setViewName("person_profile");
        registry.addViewController("/role/").setViewName("roles");
        registry.addViewController("/workspace/").setViewName("workspace");
        registry.addViewController("/userlist").setViewName("userlist");
        registry.addViewController("/employment-agency/").setViewName("employment_agency");

        registry.addViewController("/403").setViewName("403");

        registry.addViewController("/demand-candidate").setViewName("demand-candidate");

        registry.addViewController("/import").setViewName("import");

        registry.addViewController("/test").setViewName("test");
    }
    
    @Override
    public void addFormatters(final FormatterRegistry registry) {
        log.debug("addFormatters");
        super.addFormatters(registry);
        registry.addFormatter(dateFormatter());
        registry.addFormatter(localDateTimeFormatter());
        registry.addFormatter(localDateFormatter());
    }

    @Bean
    public DateFormatter dateFormatter() {
        log.debug("dateFormatter");
        return new DateFormatter();
    }

    @Bean
    public DateTimeFormatter dateTimeFormatter() {
        return new DateTimeFormatter();
    }

    @Bean
    public LocalDateTimeFormatter localDateTimeFormatter() {
        return new LocalDateTimeFormatter();
    }

    @Bean
    public LocalDateFormatter localDateFormatter() {
        return new LocalDateFormatter();
    }
}