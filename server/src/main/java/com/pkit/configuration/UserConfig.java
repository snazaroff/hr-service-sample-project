package com.pkit.configuration;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@ConfigurationProperties//(prefix="users")
@Setter
@Getter
@ToString
public class UserConfig {
    public Map<String, User> users = new HashMap<>();

    @Setter
    @Getter
    @ToString
    public static class User {
        String password;
        List<String> roles;
    }
}
