package com.pkit.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.pkit.model")
@EntityScan(basePackages = {"com.pkit.model"})
//@EnableJpaRepositories("com.farzoom.uni.common.api.repository")
public class Config {

    @Autowired
    private ApplicationContext applicationContext;
}