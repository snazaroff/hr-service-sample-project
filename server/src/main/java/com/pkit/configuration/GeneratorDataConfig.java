package com.pkit.configuration;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
//@Configuration
//@PropertySource("classpath:test-generator.yml")
@ConfigurationProperties(prefix = "test-data")
@Setter
@Getter
@ToString
public class GeneratorDataConfig {

    private List<String> firstNames = new ArrayList<>();
    private List<String> lastNames = new ArrayList<>();
    private List<String> middleNames = new ArrayList<>();

    private List<String> positions = new ArrayList<>();
    private List<String> firmTypes = new ArrayList<>();
    private List<String> firmNames = new ArrayList<>();
    private List<String> cities = new ArrayList<>();
    private List<String> streets = new ArrayList<>();
    private List<String> departments = new ArrayList<>();
    private Map<String, Object> structure = new HashMap<>();
}