package com.pkit;

import com.pkit.model.*;
import com.pkit.repository.CandidateIndexRepository;
import com.pkit.repository.DemandIndexRepository;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Aspect
@Configuration
public class CacheAspect {

    private final CandidateIndexRepository candidateIndexRepository;
    private final DemandIndexRepository demandIndexRepository;

    public CacheAspect(
            CandidateIndexRepository candidateIndexRepository,
            DemandIndexRepository demandIndexRepository) {
        this.candidateIndexRepository = candidateIndexRepository;
        this.demandIndexRepository = demandIndexRepository;
    }

    /**
     * Удаляем кэш из эластика
     *
     * @param joinPoint
     */
    @After("execution(* com.pkit.service.impl.*.delete(..))")
    public void after(JoinPoint joinPoint) {
        //Advice
        if (joinPoint.getTarget() instanceof CandidateService || joinPoint.getTarget() instanceof DemandService) {
            log.info("After  methodName: delete");
            log.info("joinPoint: {}", joinPoint);
        }
        if (joinPoint.getTarget() instanceof CandidateService) {
            Object[] args = joinPoint.getArgs();
            if (args.length > 0) {
                Long candidateId = 0L;
                if (args[0] instanceof Candidate) {
                    Candidate candidate = (Candidate) args[0];
                    candidateId = candidate.getCandidateId();
                } else if (args[0] instanceof Long) {
                    candidateId = (Long) args[0];
                }
                log.debug("candidateId: {}", candidateId);
                candidateIndexRepository.delete(Long.toString(candidateId));
            }
        } else if (joinPoint.getTarget() instanceof DemandService) {
            Object[] args = joinPoint.getArgs();
            if (args.length > 0) {
                Long demandId = 0L;
                if (args[0] instanceof Demand) {
                    Demand demand = (Demand) args[0];
                    demandId = demand.getDemandId();
                } else if (args[0] instanceof Long) {
                    demandId = (Long) args[0];
                }
                log.debug("demandId: {}", demandId);
                candidateIndexRepository.delete(Long.toString(demandId));
            }
        }
    }

    /**
     * Добавляем или изменяем кэш в эластике
     *
     * @param joinPoint
     * @param result
     */
    @AfterReturning(value = "execution(* com.pkit.service.impl.*.*(..))", returning = "result")
    public void afterReturning(JoinPoint joinPoint, Object result) {
        //Advice
        String methodName = joinPoint.getSignature().getName();
        if (("add".equals(methodName) || "update".equals(methodName))) {
            if (joinPoint.getTarget() instanceof CandidateService || joinPoint.getTarget() instanceof DemandService) {
                log.info("AfterReturning methodName: {}", methodName);
                log.info("joinPoint: {}", joinPoint);
                log.info("result: {}", result);
            }
            if (joinPoint.getTarget() instanceof CandidateService) {
                Candidate candidate = (Candidate) result;
                CandidateIndex candidateIndex = new CandidateIndex();
                candidateIndex.setCandidateId(Long.toString(candidate.getCandidateId()));
                candidateIndex.setData(candidate.toCacheString());
                candidateIndexRepository.save(candidateIndex);
            } else if (joinPoint.getTarget() instanceof DemandService) {
                Demand demand = (Demand) result;
                DemandIndex demandIndex = new DemandIndex();
                demandIndex.setDemandId(Long.toString(demand.getDemandId()));
                demandIndex.setData(demand.toCacheString());
                demandIndexRepository.save(demandIndex);
            }
        }
    }
}