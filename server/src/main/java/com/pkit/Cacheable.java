package com.pkit;

public interface Cacheable {

    String toCacheString();
}
