package com.pkit.repository;

import com.pkit.model.PickedCandidate;
import com.pkit.model.PickedCandidatePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PickedCandidateRepository extends JpaRepository<PickedCandidate, PickedCandidatePK> { }

