package com.pkit.repository;

import com.pkit.model.PickedDemand;
import com.pkit.model.PickedDemandPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PickedDemandRepository extends JpaRepository<PickedDemand, PickedDemandPK> { }

