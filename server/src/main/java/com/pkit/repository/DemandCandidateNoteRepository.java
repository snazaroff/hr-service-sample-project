package com.pkit.repository;

import com.pkit.model.DemandCandidate;
import com.pkit.model.DemandCandidateNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DemandCandidateNoteRepository extends JpaRepository<DemandCandidateNote, Long> {

  List<DemandCandidateNote> findByDemandCandidate(DemandCandidate demandCandidate);
  List<DemandCandidateNote> findByDemandCandidateIn(List<DemandCandidate> demandCandidates);
}

