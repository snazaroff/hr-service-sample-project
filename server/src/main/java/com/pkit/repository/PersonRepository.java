package com.pkit.repository;

import com.pkit.model.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long>, QueryDslPredicateExecutor<Person> {

    @Query(value =
            "SELECT p.* FROM person p " +
            "INNER JOIN department d ON d.department_id = p.department_id " +
            "WHERE d.company_id = ?1",
            nativeQuery = true)
    List<Person> findByCompanyId(Long companyId);

    @Query(value =
            "SELECT p.* FROM person p " +
                    "INNER JOIN department d ON d.department_id = p.department_id " +
                    "WHERE d.company_id = :companyId /* #pageable */  ",
            nativeQuery = true)
    Page<Person> findByCompanyId(Long companyId, Pageable pageable);

    @Query(value =
            "SELECT p.* FROM person p " +
            "WHERE p.department_id = ?1 " +
            "ORDER BY p.last_name, p.first_name, p.middle_name",
            nativeQuery = true)
    List<Person> findByDepartmentId(Long departmentId);

    @Query(value =
            "SELECT p.* FROM person p " +
                    "WHERE p.department_id = :departmentId " +
//                    "ORDER BY p.last_name, p.first_name, p.middle_name /* #pageable */  ",
                    "ORDER BY 1 /* #pageable */  ",
            nativeQuery = true)
    Page<Person> findByDepartmentId(Long departmentId, Pageable pageable);
}

