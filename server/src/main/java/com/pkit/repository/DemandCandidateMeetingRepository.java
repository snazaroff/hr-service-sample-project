package com.pkit.repository;

import com.pkit.model.DemandCandidateMeeting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DemandCandidateMeetingRepository extends JpaRepository<DemandCandidateMeeting, Long> { }

