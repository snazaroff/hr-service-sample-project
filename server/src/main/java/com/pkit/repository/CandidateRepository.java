package com.pkit.repository;

import com.pkit.model.Candidate;
import com.pkit.model.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface CandidateRepository
        extends JpaRepository<Candidate, Long>, QueryDslPredicateExecutor<Candidate>, CandidateRepositoryCustom {

    List<Candidate> findByCompany(Company company);

    Page<Candidate> findByCompany(Company company, Pageable pageable);
}