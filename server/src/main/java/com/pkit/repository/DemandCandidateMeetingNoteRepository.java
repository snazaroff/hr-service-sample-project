package com.pkit.repository;

import com.pkit.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DemandCandidateMeetingNoteRepository extends JpaRepository<DemandCandidateMeetingNote, Long> { }

