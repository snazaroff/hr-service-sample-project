package com.pkit.repository;

import com.pkit.model.CandidateLanguage;
import com.pkit.model.CandidateLanguagePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CandidateLanguageRepository
        extends JpaRepository<CandidateLanguage, CandidateLanguagePK> { }