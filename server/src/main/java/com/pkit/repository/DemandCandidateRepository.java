package com.pkit.repository;

import com.pkit.model.DemandCandidate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DemandCandidateRepository extends JpaRepository<DemandCandidate, Long> {

    @Query(value = "SELECT dc.* FROM demand_candidate dc WHERE dc.demand_id = ?1", nativeQuery = true)
    List<DemandCandidate> findByDemandId(Long demandId);

    @Query(value = "SELECT dc.* FROM demand_candidate dc WHERE dc.demand_id = ?1 limit ?2", nativeQuery = true)
    List<DemandCandidate> findByDemandIdAndLimit(Long candidateId, Short limit);

    @Query(value = "SELECT dc.* FROM demand_candidate dc WHERE dc.demand_id = ?1 order by create_date limit ?2", nativeQuery = true)
    List<DemandCandidate> findByDemandIdAndLimitAndSortAsc(Long demandId, Short limit);

    @Query(value = "SELECT dc.* FROM demand_candidate dc WHERE dc.demand_id = ?1 order by create_date desc limit ?2", nativeQuery = true)
    List<DemandCandidate> findByDemandIdAndLimitAndSortDesc(Long demandId, Short limit);

    @Query(value = "SELECT dc.* FROM demand_candidate dc WHERE dc.candidate_id = ?1", nativeQuery = true)
    List<DemandCandidate> findByCandidateId(Long candidateId);

    @Query(value = "SELECT dc.* FROM demand_candidate dc WHERE dc.candidate_id = ?1 limit ?2", nativeQuery = true)
    List<DemandCandidate> findByCandidateIdAndLimit(Long candidateId, Short limit);

    @Query(value = "SELECT dc.* FROM demand_candidate dc WHERE dc.candidate_id = ?1 order by create_date limit ?2", nativeQuery = true)
    List<DemandCandidate> findByCandidateIdAndLimitAndSortAsc(Long candidateId, Short limit);

    @Query(value = "SELECT dc.* FROM demand_candidate dc WHERE dc.candidate_id = ?1 order by create_date desc limit ?2", nativeQuery = true)
    List<DemandCandidate> findByCandidateIdAndLimitAndSortDesc(Long candidateId, Short limit);
}