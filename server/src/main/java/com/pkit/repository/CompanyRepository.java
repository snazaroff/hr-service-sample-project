package com.pkit.repository;

import com.pkit.model.Company;
import com.pkit.model.CompanyType;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long>, QueryDslPredicateExecutor<Company> {

    List<Company> findByCompanyType(CompanyType companyType);

    Page<Company> findByCompanyType(CompanyType companyType, Pageable pageable);

    @Query(value =
            "SELECT * FROM company c " +
                    "WHERE c.company_id IN " +
                    "(SELECT company_id FROM picked_company WHERE person_id = ?1) " +
                    "ORDER BY c.name",
            nativeQuery = true)
    List<Company> getAvailableCompanies(Long personId);

    @Query(value = "SELECT c.* FROM company c\n" +
            "INNER JOIN person p ON p.person_id = c.created_by\n" +
            "INNER JOIN department d ON p.department_id = d.department_id\n" +
            "WHERE c.company_type_id = 0 \n" +
            "  AND d.company_id = :managerCompanyId " +
            " /* #pageable */  ",
            nativeQuery = true)
    Page<Company> getCustomerByManagerCompanyId(Long managerCompanyId, Pageable pageable);
}