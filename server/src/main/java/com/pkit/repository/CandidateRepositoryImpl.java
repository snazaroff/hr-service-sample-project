package com.pkit.repository;

import com.pkit.model.Candidate;
import com.pkit.model.filter.CandidateFilter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.*;

@Slf4j
public class CandidateRepositoryImpl implements CandidateRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Candidate> find(CandidateFilter candidateFilter, List<Long> candidateIds) {
        log.debug("find({})", candidateFilter);

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Candidate> q = cb.createQuery(Candidate.class);
        Root<Candidate> candidate = q.from(Candidate.class);

        Path<String> firstNamePath = candidate.get("firstName");
        Path<String> lastNamePath = candidate.get("lastName");
        List<Predicate> predicates = new ArrayList<>();
        if (candidateFilter.getFirstName() != null && !candidateFilter.getFirstName().equals("")) {
            predicates.add(cb.equal(cb.lower(firstNamePath), cb.lower(cb.literal(candidateFilter.getFirstName()))));
        }
        if (candidateFilter.getLastName() != null && !candidateFilter.getLastName().equals("")) {
            predicates.add(cb.equal(cb.lower(lastNamePath), cb.lower(cb.literal(candidateFilter.getLastName()))));
        }
        if (candidateFilter.getMetroId() != null && !candidateFilter.getMetroId().equals(0L)) {
            Path<String> metroIdPath = candidate.get("metro");
            predicates.add(cb.equal(metroIdPath, cb.literal(candidateFilter.getMetroId())));
        }
        if (candidateFilter.getSalaryMin() != null && !candidateFilter.getSalaryMin().equals(0L)) {
            Path<Long> salaryPath = candidate.get("salary").get("amount");
            predicates.add(cb.ge(salaryPath, candidateFilter.getSalaryMin()));
        }
        if (candidateFilter.getSalaryMax() != null && !candidateFilter.getSalaryMax().equals(0L)) {
            Path<Long> salaryPath = candidate.get("salary").get("amount");
            predicates.add(cb.le(salaryPath, candidateFilter.getSalaryMax()));
        }
        if (candidateFilter.getCategory() != null) {
            From join = candidate.join("categories");
            Path<String> c = join.get("categoryId");
            predicates.add(cb.equal(c, candidateFilter.getCategory().getCategoryId()));
        }
        if (candidateIds != null) {
            Path<Long> candidateIdPath = candidate.get("candidateId");
            CriteriaBuilder.In<Long> in = cb.in(candidateIdPath);
            for (Long id : candidateIds)
                in.value(id);
            predicates.add(in);
        }
        q.select(candidate).where(
                cb.and(predicates.toArray(new Predicate[predicates.size()]))
        );
        return entityManager.createQuery(q).getResultList();
    }
}