package com.pkit.repository;

import com.pkit.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandidateCategoryRepository extends JpaRepository<CandidateCategory, CandidateCategoryPK> {
 
    List<CandidateCategory> findByCandidateId(Long candidateId);

    @Modifying
    @Query(value = "DELETE FROM candidate_category WHERE candidate_id = ?1 AND category_id = ?2", nativeQuery = true)
    void deleteItem(Long candidateId, Long categoryId);
}