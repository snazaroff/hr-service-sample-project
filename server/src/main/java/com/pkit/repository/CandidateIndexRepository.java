package com.pkit.repository;

import com.pkit.model.CandidateIndex;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandidateIndexRepository extends ElasticsearchRepository<CandidateIndex, String> {

    @Query("{\"query\":{\"match\":{\"data\":\"?0\"}}}}")
    List<CandidateIndex> findByData(String word);
}