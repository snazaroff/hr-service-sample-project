package com.pkit.repository;

import com.pkit.model.DemandNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DemandNoteRepository extends JpaRepository<DemandNote, Long> {

    @Query(value = "SELECT dn.* FROM demand_note dn WHERE dn.demand_id = ?1 ORDER BY dn.create_date", nativeQuery = true)
    List<DemandNote> findByDemandId(Long demandId);

    @Query(value = "SELECT dc.* FROM demand_note dc WHERE dc.demand_id = ?1 limit ?2", nativeQuery = true)
    List<DemandNote> findByDemandIdAndLimit(Long candidateId, Short limit);

    @Query(value = "SELECT dc.* FROM demand_note dc WHERE dc.demand_id = ?1 order by create_date limit ?2", nativeQuery = true)
    List<DemandNote> findByDemandIdAndLimitAndSortAsc(Long demandId, Short limit);

    @Query(value = "SELECT dc.* FROM demand_note dc WHERE dc.demand_id = ?1 order by create_date desc limit ?2", nativeQuery = true)
    List<DemandNote> findByDemandIdAndLimitAndSortDesc(Long demandId, Short limit);
}

