package com.pkit.repository;

import com.pkit.model.Demand;
import com.pkit.model.filter.DemandFilter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class DemandRepositoryImpl implements DemandRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Demand> find(DemandFilter demandFilter, List<Long> demandIds) {
        log.debug("find({})", demandFilter);

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Demand> q = cb.createQuery(Demand.class);
        Root<Demand> demand = q.from(Demand.class);

        List<Predicate> predicates = new ArrayList<>();

        if (demandFilter.getCustomerId() != null && !demandFilter.getCustomerId().equals(0L)) {
            Path<String> metroIdPath = demand.get("company");
            predicates.add(cb.equal(metroIdPath, cb.literal(demandFilter.getCustomerId())));
        }
        if (demandIds != null) {
            Path<Long> demandIdPath = demand.get("demandId");
            CriteriaBuilder.In<Long> in = cb.in(demandIdPath);
            for (Long id : demandIds)
                in.value(id);
            predicates.add(in);
        }
        q.select(demand).where(
                cb.and(predicates.toArray(new Predicate[predicates.size()]))
        );
        return entityManager.createQuery(q).getResultList();
    }
}