package com.pkit.repository;

import com.pkit.model.Person;
import com.pkit.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByLogin(String login);

    User findByPerson(Person person);
}

