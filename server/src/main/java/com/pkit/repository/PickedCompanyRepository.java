package com.pkit.repository;

import com.pkit.model.PickedCompany;
import com.pkit.model.PickedCompanyPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PickedCompanyRepository extends JpaRepository<PickedCompany, PickedCompanyPK> { }

