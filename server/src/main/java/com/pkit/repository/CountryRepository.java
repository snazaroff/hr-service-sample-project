package com.pkit.repository;

import com.pkit.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {

    @Query(value = "SELECT c.* FROM country c WHERE c.status > 0 ORDER BY c.status",nativeQuery = true)
    List<Country> findPrimaryCountries();
}

