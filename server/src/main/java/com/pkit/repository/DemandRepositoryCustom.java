package com.pkit.repository;

import com.pkit.model.Demand;
import com.pkit.model.filter.DemandFilter;

import java.util.List;

public interface DemandRepositoryCustom {
    List<Demand> find(DemandFilter demandFilter, List<Long> demandIds);
}
