package com.pkit.repository;

import com.pkit.model.Town;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TownRepository extends JpaRepository<Town, Long> {
    List<Town> findByCountryIdOrderByNameAsc(Long countryId);
    List<Town> findByRegionIdOrderByNameAsc(Long regionId);
}

