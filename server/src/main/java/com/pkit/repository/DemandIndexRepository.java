package com.pkit.repository;

import com.pkit.model.DemandIndex;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DemandIndexRepository extends ElasticsearchRepository<DemandIndex, String> {

    @Query("{\"query\":{\"match\":{\"data\":\"?0\"}}}}")
    List<DemandIndex> findByData(String word);
}

