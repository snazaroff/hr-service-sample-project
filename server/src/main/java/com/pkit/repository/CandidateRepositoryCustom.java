package com.pkit.repository;

import com.pkit.model.Candidate;
import com.pkit.model.filter.CandidateFilter;

import java.util.List;

public interface CandidateRepositoryCustom {
    List<Candidate> find(CandidateFilter candidateFilter, List<Long> candidateIds);
}