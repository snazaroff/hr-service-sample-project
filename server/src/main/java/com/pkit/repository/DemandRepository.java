package com.pkit.repository;

import com.pkit.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DemandRepository
        extends JpaRepository<Demand, Long>, QueryDslPredicateExecutor<Demand>, DemandRepositoryCustom {

    @Query(value = "SELECT d FROM Demand d " +
            "WHERE d.createdBy IN (" +
            "   SELECT p FROM Person p " +
            "   WHERE p.department IN (" +
            "       SELECT p2.department FROM Person p2 WHERE p2.personId = ?1))")
    List<Demand> getAll(Long personId);

//    @Query(value = "SELECT d FROM Demand d WHERE d.company = ?1")
    List<Demand> findByCompany(Company customer);

    Page<Demand> findByCompany(Company customer, Pageable pageable);

    @Query(value = "SELECT d.* FROM DEMAND d " +
            "INNER JOIN department dep ON dep.department_id = p.department_id " +
            "INNER JOIN person p ON p.person_id = d.created_by " +
            "WHERE p.department_id = :departmentId",
            nativeQuery = true)
    List<Demand> findByDepartmentId(Long departmentId);

    @Query(value = "SELECT d.* FROM DEMAND d " +
            "INNER JOIN department dep ON dep.department_id = p.department_id " +
            "INNER JOIN person p ON p.person_id = d.created_by " +
            "WHERE p.department_id = :departmentId " +
            "ORDER BY 1 /* #pageable */  ",
            nativeQuery = true)
    Page<Demand> findByDepartmentId(Long departmentId, Pageable pageable);
}