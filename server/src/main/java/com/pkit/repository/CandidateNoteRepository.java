package com.pkit.repository;

import com.pkit.model.CandidateNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandidateNoteRepository extends JpaRepository<CandidateNote, Long> {

    @Query(value = "SELECT cn.* FROM candidate_note cn WHERE cn.candidate_id = ?1",nativeQuery = true)
    List<CandidateNote> findByCandidateId(Long candidateId);

    @Query(value = "SELECT cn.* FROM candidate_note cn WHERE cn.candidate_id = ?1 limit ?2", nativeQuery = true)
    List<CandidateNote> findByCandidateIdAndLimit(Long candidateId, Short limit);

    @Query(value = "SELECT cn.* FROM candidate_note cn WHERE cn.candidate_id = ?1 order by create_date limit ?2", nativeQuery = true)
    List<CandidateNote> findByCandidateIdAndLimitAndSortAsc(Long candidateId, Short limit);

    @Query(value = "SELECT cn.* FROM candidate_note cn WHERE cn.candidate_id = ?1 order by create_date desc limit ?2", nativeQuery = true)
    List<CandidateNote> findByCandidateIdAndLimitAndSortDesc(Long candidateId, Short limit);
}