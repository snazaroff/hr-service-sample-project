package com.pkit.conversion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.format.Formatter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;


public class LocalDateTimeFormatter implements Formatter<LocalDateTime> {

    @Autowired
    private MessageSource messageSource;

    public LocalDateTimeFormatter() {
        super();
    }

    public LocalDateTime parse(final String text, final Locale locale) {
        final DateTimeFormatter formatter = createDateFormat(locale);
        return LocalDateTime.parse(text, formatter);
    }

    public String print(final LocalDateTime object, final Locale locale) {
        final DateTimeFormatter formatter = createDateFormat(locale);
        return object.format(formatter);
    }

    private DateTimeFormatter createDateFormat(final Locale locale) {
        final String format = this.messageSource.getMessage("datetime.format", null, locale);
        return DateTimeFormatter.ofPattern(format);
    }
}