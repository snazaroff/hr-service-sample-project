package com.pkit.controller;

import com.pkit.Utils;
import com.pkit.model.Candidate;
import com.pkit.model.Person;
import com.pkit.service.ImportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.stream.Collectors;

@SuppressWarnings("SameReturnValue")
@Slf4j
@Controller
@RequestMapping("/import")
public class ImportController {

    private final ImportService importService;

    public ImportController(
            ImportService importService
    ) {
        this.importService = importService;
    }

    @GetMapping("/candidate")
    String getImportResume() {
        return "import";
    }

    @PostMapping("/candidate")
    String importResume(
            @RequestParam("file") MultipartFile file,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication) throws Exception {
        log.debug("importResume: {}", file.getContentType());

        Person person = Utils.getPerson(authentication);

        if (!file.isEmpty()) {
            byte[] bytes = file.getBytes();
            Candidate candidate = importService.importResume(bytes, file.getName(), person, person.getDepartment().getCompany());
            log.debug("candidate: {}", candidate);
            model.addAttribute("candidate", candidate);
            model.addAttribute("educations", candidate.getEducations().stream().sorted().collect(Collectors.toList()));
            model.addAttribute("employments", candidate.getEmployments().stream().sorted().collect(Collectors.toList()));

            log.debug("educations: {}", model.get("educations"));

            return "redirect:/candidate/" + candidate.getCandidateId();
        } else {
            throw new RuntimeException("Empty file!");
        }
    }
}