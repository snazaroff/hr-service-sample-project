package com.pkit.controller;

import com.pkit.Utils;
import com.pkit.data.Languages;
import com.pkit.exception.*;
import com.pkit.model.*;
import com.pkit.model.Currency;
import com.pkit.model.filter.CandidateFilter;
import com.pkit.service.*;

import lombok.extern.slf4j.Slf4j;

import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.validation.Valid;
import java.util.*;

import static java.util.stream.Collectors.toList;

@SuppressWarnings("ALL")
@Slf4j
@Controller
@RequestMapping("/candidate")
public class CandidateController {

//    @Autowired
//    private ContextAwarePolicyEnforcement policy;

    private final CandidateService candidateService;
    private final CandidateHistoryService candidateHistoryService;
    private final CandidateNoteService candidateNoteService;
    private final DemandService demandService;
    private final DemandCandidateService demandCandidateService;
    private final CategoryService categoryService;
    private final DictionaryService dictionaryService;
    private final PersonService personService;
    private final PhotoService photoService;

    @Autowired
    private Languages languages;

    @Autowired
    private EntityManager entityManager;

    public CandidateController(
            CandidateService candidateService,
            CandidateHistoryService candidateHistoryService,
            CandidateNoteService candidateNoteService,
            DemandService demandService,
            DemandCandidateService demandCandidateService,
            CategoryService categoryService,
            DictionaryService dictionaryService,
            PersonService personService,
            PhotoService photoService
    ) {
        this.candidateService = candidateService;
        this.candidateHistoryService = candidateHistoryService;
        this.candidateNoteService = candidateNoteService;
        this.demandService = demandService;
        this.demandCandidateService = demandCandidateService;
        this.categoryService = categoryService;
        this.dictionaryService = dictionaryService;
        this.personService = personService;
        this.photoService = photoService;
    }

    @SuppressWarnings("SameReturnValue")
    @GetMapping("/")
    @Transactional
    String getAllCandidate(
            @RequestParam(name = "page", defaultValue = "1") Integer page,
            @RequestParam(name = "size", defaultValue = "10") Integer size,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getAllCandidate(page: {}, size: {})", page, size);

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        Session session = entityManager.unwrap(Session.class);

        Filter filter = session.enableFilter("pickedCandidate");
        filter.setParameter("currentPerson", loggedPersonId);

        Page<Candidate> candidatePage;// = candidateService.getAll(new PageRequest(page - 1, size));
        User user = Utils.getUser(authentication);
        Department department = user.getPerson().getDepartment();

        Pageable pageable = new PageRequest(page - 1, size);
//        if (user.getRoles().contains(new Role(RoleType.ROLE_EA_ADMIN))) {
            Company company = department.getCompany();
            candidatePage = candidateService.getByCompany(company, pageable);
//        } else {
//            candidatePage = demandService.getByDepartmentId(department.getDepartmentId(), pageable);
//        }

        ControllerUtils.processPageInfo(candidatePage, page, model);

        model.addAttribute("candidates", candidatePage);
        model.addAttribute("candidateFilter", new CandidateFilter());
        model.addAttribute("categories", categoryService.getTopCategory());
        model.addAttribute("languages", languages.getLanguages());

        model.addAttribute("metros", dictionaryService.getMetroByTown(dictionaryService.MOSCOW.getTownId()));
        model.addAttribute("countries", dictionaryService.getPrimaryCountries());
        model.addAttribute("top_categories", categoryService.getTopCategory());

        long usedBytes = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        log.debug("usedBytes: {}", usedBytes);
        return "candidates";
    }

    @PostMapping("/filter/")
    String getFilteredCandidate(
            @RequestParam(name = "page", defaultValue = "1") Integer page,
            @RequestParam(name = "size", defaultValue = "10") Integer size,
            CandidateFilter candidateFilter,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getFilteredCandidate( candidateFilter: {}, page: {}, size: {})", candidateFilter, page, size);

        log.debug("candidateFilter.isEmpty(): {}", candidateFilter.isEmpty());

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        Page<Candidate> candidatePage = candidateService.findByFilter(candidateFilter, new PageRequest(page - 1, size));

        ControllerUtils.processPageInfo(candidatePage, page, model);

        model.addAttribute("candidates", candidatePage);
        model.addAttribute("candidateFilter", candidateFilter);
        model.addAttribute("categories", categoryService.getTopCategory());
        model.addAttribute("languages", languages.getLanguages());
        model.addAttribute("metros", dictionaryService.getMetroByTown(dictionaryService.MOSCOW.getTownId()));
        model.addAttribute("countries", dictionaryService.getPrimaryCountries());
        model.addAttribute("top_categories", categoryService.getTopCategory());
        return "candidates";
    }

    @PostMapping
    String saveCandidate(
            @Valid @ModelAttribute Candidate candidate,
            BindingResult errors,
            Model model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("saveCandidate( candidate: {})", candidate);
        if (errors.hasErrors()) {
            log.debug("errors: {}", errors.getAllErrors());
        }

        Person loggedUser = Utils.getPerson(authentication);
        model.addAttribute("loggedPersonId", loggedUser.getPersonId());

        if (!errors.hasErrors()) {
            if (candidate.getCandidateId() == null) {

                candidate.setCreatedBy(loggedUser);
                candidate = candidateService.add(candidate);
                return "redirect:/candidate/" + candidate.getCandidateId();
            } else {
                //todo: что-то я не понял, обязательно переподгружать проперти-наборы данных?
                //Все эти свойства мы меняем через отдельные методы добавления-удаления
                //В сохраннение объекта целиком их можно не сохранять.
                Candidate oldCandidate = candidateService.get(candidate.getCandidateId());
                candidate.setLanguages(oldCandidate.getLanguages());
                candidate.setEducations(oldCandidate.getEducations());
                candidate.setEmployments(oldCandidate.getEmployments());
//                candidate.setDemands(oldCandidate.getDemands());

                candidate.setPickeds(oldCandidate.getPickeds());

                candidate = candidateService.update(candidate);
                log.debug("candidate: {}", candidate);
//                Collection<Candidate> candidates = candidateService.getAll();
//                model.addAttribute("candidates", candidates);
//                model.addAttribute("candidateFilter", new CandidateFilter());
//                model.addAttribute("categories", categoryService.getTopCategory());
//                model.addAttribute("metros", dictionaryService.getMetroByTown(dictionaryService.MOSCOW.getTownId()));
//                model.addAttribute("countries", dictionaryService.getPrimaryCountries());
//                return "candidates";
                return "redirect:/candidate/";
            }
        } else {
            model.addAttribute("categories", categoryService.getTopCategory());
            model.addAttribute("metros", dictionaryService.getMetroByTown(dictionaryService.MOSCOW.getTownId()));
            model.addAttribute("countries", dictionaryService.getPrimaryCountries());
            return candidate.getCandidateId() == null ? "candidate_new" : "candidate";
        }
    }

    @PutMapping("/{candidateId}")
    Candidate updateCandidate(
            @PathVariable Long candidateId,
            @Valid @RequestBody Candidate candidate,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("updateCandidate( candidateId: {}, candidate: {})", candidateId, candidate);

        if (candidateService.exists(candidateId)) throw new CandidateNotFoundException(candidateId);
        candidate.setCandidateId(candidateId);
//        Set<Education> educations = candidate.getEducations();
//        candidate.setEducations(null);
//        candidate = candidateService.update(candidate);
//        for(Education e: educations) {
//            if (e.getEducationId() == null)
//                educationService.add(e);
//            else
//                educationService.update(e);
//            e.setCandidate(candidate);
//        }
//        candidate.setEducations(educations);
        candidate = candidateService.update(candidate);
        return candidate;
    }

    @GetMapping("/{candidateId}")
    String getCandidate(
            @PathVariable Long candidateId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getCandidate(candidateId: {})", candidateId);


        Person loggedPerson = Utils.getPerson(authentication);
        model.addAttribute("loggedPersonId", loggedPerson.getPersonId());

        model.addAttribute("status", Arrays.asList(CandidateStatus.values()));
        model.addAttribute("languageStatus", Arrays.asList(LanguageStatus.values()));
        model.addAttribute("educationTypes", Arrays.asList(EducationType.values()));
        model.addAttribute("allLanguages", languages.getLanguages());
        model.addAttribute("metros", dictionaryService.getMetroByTown(dictionaryService.MOSCOW.getTownId()));
        model.addAttribute("currencies", Arrays.asList(Currency.values()));
        model.addAttribute("sex", Arrays.asList(SexStatus.values()));
        if (candidateId == -1) {
            Candidate candidate = new Candidate();
            candidate.setCompany(loggedPerson.getDepartment().getCompany());
            log.debug("candidate: {}", candidate);
            model.addAttribute("candidate", candidate);
            return "candidate_new";
        } else {
            Candidate candidate = candidateService.get(candidateId);
            if (candidate == null) throw new CandidateNotFoundException(candidateId);
            log.debug("candidate: {}", candidate);
            model.addAttribute("candidate", candidate);
            model.addAttribute("history", candidateHistoryService.get(candidateId, (short) 0));
            model.addAttribute("educations", candidate.getEducations().stream().sorted(Collections.reverseOrder()).collect(toList()));
            model.addAttribute("employments", candidate.getEmployments().stream().sorted(Collections.reverseOrder()).collect(toList()));
            model.addAttribute("languages", candidate.getLanguages().stream().sorted().collect(toList()));

//            log.debug("employment: 1");
//            for(Employment employment : candidate.getEmployments()) {
//                log.debug("employment.id: {}, employment.startDate: {}", employment.getEmploymentId(), employment.getStartDate());
//            }
//
//            log.debug("employment: 2");
//            for(Employment employment : candidate.getEmployments().stream().sorted(Collections.reverseOrder()).collect(toList())) {
//                log.debug("employment.id: {}, employment.startDate: {}", employment.getEmploymentId(), employment.getStartDate());
//            }
//
//            Set<Category> sortedCategories = new TreeSet();
//            List<Long> parentCategoryIds = new ArrayList<>();
//
//            for(CandidateCategory candidateCategory: candidate.getCategories()) {
//                Category category = candidateCategory.getCategory();
//                sortedCategories.add(category);
//                if (category.getParentId() != null && !parentCategoryIds.contains(category.getParentId()))  {
//                    parentCategoryIds.add(category.getParentId());
//                    sortedCategories.add(categoryService.getCategory(category.getParentId()));
//                }
//            }
//            log.debug("sortedCategories: {}", sortedCategories.size());
//            model.addAttribute("categories", sortedCategories.stream().sorted().collect(Collectors.toList()));
            model.addAttribute("categories", candidateService.getCategoryListWithTopCategory(candidateId));
            List<DemandCandidate> demands = demandCandidateService.getByCandidateId(candidateId);
            Collections.sort(demands);
            model.addAttribute("demands", demands);
            List<CandidateNote> candidateNotes = candidateNoteService.getByCandidateId(candidateId);
            Collections.sort(candidateNotes);
            model.addAttribute("notes", candidateNotes);
            model.addAttribute("photos", photoService.getByCandidate(candidateId));
            model.addAttribute("top_categories", categoryService.getTopCategory());
            return "candidate";
        }
    }

    /**
     * Возвращает список заметок по кандидату
     *
     * @param candidateId
     * @param authentication
     * @return
     */
    @GetMapping("/{candidateId}/note/")
    List<CandidateNote> getCandidateNoteByCandidateId(
            @PathVariable Long candidateId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getCandidateNoteByCandidateId(candidateId: {})", candidateId);

        if (candidateService.exists(candidateId)) throw new CandidateNotFoundException(candidateId);

//        Long loggedPersonId = Utils.getPersonId(authentication);
//        model.addAttribute("loggedPersonId", loggedPersonId);

        return candidateNoteService.getByCandidateId(candidateId);
    }

    /**
     * Возвращает список заказов, к которым прикреплен кандидат
     *
     * @param candidateId
     * @param authentication
     * @return
     */
    @GetMapping("/{candidateId}/demand")
    List<DemandCandidate> getDemandCandidates(
            @PathVariable Long candidateId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getDemandCandidates(candidateId: {})", candidateId);

        if (candidateService.exists(candidateId)) throw new CandidateNotFoundException(candidateId);

//        Long loggedPersonId = Utils.getPersonId(authentication);
//        model.addAttribute("loggedPersonId", loggedPersonId);

        return demandCandidateService.getByCandidateId(candidateId);
    }

    /**
     * Прикрепляет кандидата к заказу
     *
     * @param candidateId
     * @param demandId
     * @param demandCandidate
     * @param authentication
     * @return
     */
    @PostMapping("/{candidateId}/demand/{demandId}")
    DemandCandidate addDemandCandidate(
            @PathVariable Long candidateId,
            @PathVariable Long demandId,
            @Valid @RequestBody DemandCandidate demandCandidate,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("addDemandCandidate(candidateId: {}, demandId: {}, demandCandidate: {})", candidateId, demandId, demandCandidate);

        if (candidateService.exists(candidateId)) throw new CandidateNotFoundException(candidateId);

//        Long loggedPersonId = Utils.getPersonId(authentication);
//        model.addAttribute("loggedPersonId", loggedPersonId);

        Demand demand = demandService.get(demandId);
        demandCandidate.setDemand(demand);
        Candidate candidate = candidateService.get(candidateId);
        demandCandidate.setCandidate(candidate);
        return demandCandidateService.add(demandCandidate);
    }

    @PutMapping("/{candidateId}/demand/{demandId}")
    DemandCandidate updateDemandCandidate(
            @PathVariable Long candidateId,
            @PathVariable Long demandId,
            @Valid @RequestBody DemandCandidate demandCandidate,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("updateDemandCandidate(candidateId: {}, demandId: {}, demandCandidate: {})", candidateId, demandId, demandCandidate);

//        Long loggedPersonId = Utils.getPersonId(authentication);
//        model.addAttribute("loggedPersonId", loggedPersonId);

//        if (!demandCandidateService.exists(id)) throw new DemandCandidateNotFoundException();
        Demand demand = demandService.get(demandId);
        demandCandidate.setDemand(demand);
        Candidate candidate = candidateService.get(candidateId);
        demandCandidate.setCandidate(candidate);
        return demandCandidateService.update(demandCandidate);
    }
}