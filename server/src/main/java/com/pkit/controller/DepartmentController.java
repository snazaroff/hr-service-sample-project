package com.pkit.controller;

import com.pkit.Utils;
import com.pkit.exception.DepartmentNotFoundException;
import com.pkit.exception.PersonNotFoundException;
import com.pkit.model.Company;
import com.pkit.model.Department;
import com.pkit.model.Person;
import com.pkit.model.SexStatus;
import com.pkit.model.filter.PersonFilter;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.CompanyService;
import com.pkit.service.DepartmentService;

import com.pkit.service.PersonService;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    private final CompanyService companyService;
    private final DepartmentService departmentService;
    private final PersonService personService;

    public DepartmentController(
            CompanyService companyService,
            DepartmentService departmentService,
            PersonService personService
    ) {
        this.companyService = companyService;
        this.departmentService = departmentService;
        this.personService = personService;
    }

    /**
     * Возвращает список департаментов
     *
     * @param model
     * @param authentication
     * @return
     */
    @GetMapping("/")
    String getAllDepartment(
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getAllDepartment()");

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        Collection<Department> departments = departmentService.getDepartments();
        Collection<Person> persons = personService.getAll();
        model.addAttribute("departments", departments);
        return "departments";
    }

    /**
     * Возвращает департамент по id
     *
     * @param departmentId
     * @param model
     * @param authentication
     * @return
     */
    @GetMapping("/{departmentId}")
    String getDepartment(
            @PathVariable Long departmentId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getDepartment( departmentId: {})", departmentId);

        Person loggedUser = Utils.getPerson(authentication);
        model.addAttribute("loggedPersonId", loggedUser.getPersonId());

        Department department;
        Collection<Person> persons;
        if (departmentId == -1) {
            department = new Department();
            department.setCompany(new Company());
            persons = Collections.EMPTY_LIST;
            Collection<Company> companies = companyService.getCustomers();
            log.debug("companies: {}", companies);
            model.addAttribute("companies", companies);
        } else {
            department = departmentService.get(departmentId);
            persons = personService.getByDepartmentId(departmentId);
        }
        log.debug("department: {}", department);
        log.debug("persons: {}", persons);
        model.addAttribute("department", department);
        model.addAttribute("persons", persons);

        return (departmentId == -1) ? "department_new" : "department";
    }

    @PostMapping
    String saveDepartment(
            @Valid @ModelAttribute Department department,
            BindingResult errors,
            Model model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("saveDepartment( department: {})", department);
        if (errors.hasErrors()) {
            log.debug("errors: {}", errors.getAllErrors());
        }

        Person loggedUser = Utils.getPerson(authentication);
        model.addAttribute("loggedPersonId", loggedUser.getPersonId());

        log.debug("department.contactPerson: {}", department.getContactPerson());
        if (!errors.hasErrors()) {
            if (department.getDepartmentId() == null) {
                department.setCreatedBy(loggedUser);
                department = departmentService.add(department);
            } else
                department = departmentService.update(department);
            log.debug("department: {}", department);
            Collection<Department> departments = departmentService.getAll(Department::getName);
            model.addAttribute("departments", departments);
            return "departments";
        } else {
            Collection<Company> companies = companyService.getCustomers();
            log.debug("companies: {}", companies);
            model.addAttribute("companies", companies);
            model.addAttribute("department", department);
            return department.getDepartmentId() == null
                    ? "department_new"
                    : "department";
        }
    }

    @PutMapping("/{departmentId}")
    Department updateDepartment(
            @PathVariable Long departmentId,
            @Valid @RequestBody Department department,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("updateDepartment(departmentId: {}, department: {})", departmentId, department);
        if (departmentService.exists(departmentId)) throw new DepartmentNotFoundException(departmentId);
        department.setDepartmentId(departmentId);
        return departmentService.update(department);
    }

    @GetMapping("/{departmentId}/company/{companyId}")
    String getDepartmentCompany(
            @PathVariable Long departmentId,
            @PathVariable Long companyId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getDepartmentCompany(departmentId: {}, companyId: {})", departmentId, companyId);

        Department department;
        Collection<Person> persons;
        if (departmentId == -1) {
            department = new Department();
            Company company = companyService.get(companyId);
            if (company != null)
                department.setCompany(company);
            else
                department.setCompany(new Company());
            persons = Collections.EMPTY_LIST;
            Collection<Company> companies = companyService.getCustomers();
            log.debug("companies: {}", companies);
            model.addAttribute("companies", companies);
        } else {
            department = departmentService.get(departmentId);
            persons = personService.getByDepartmentId(departmentId);
        }

        log.debug("department: {}", department);
        log.debug("persons: {}", persons);
        model.addAttribute("department", department);
        model.addAttribute("persons", persons);

        return (departmentId == -1) ? "department_new" : "department";
    }

    /**
     * Форма для редактирования сотрудника департамента
     *
     * @param departmentId
     * @param personId
     * @param model
     * @param authentication
     * @return
     */
    @GetMapping("/{departmentId}/person/{personId}")
    String getDepartmentPerson(
            @PathVariable Long departmentId,
            @PathVariable Long personId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getDepartmentPerson(departmentId: {}, personId: {})", departmentId, personId);

        Department department = departmentService.get(departmentId);
        if (department == null) throw new DepartmentNotFoundException(departmentId);

        Person person;
        if (personId == -1) {
            person = new Person();
            person.setDepartment(department);
        } else {
            person = personService.get(personId);
            if (person == null) throw new PersonNotFoundException(personId);
        }
        log.debug("person: {}", person);
        model.addAttribute("sex", Arrays.asList(SexStatus.values()));
        model.addAttribute("departments", Collections.singletonList(department));
        model.addAttribute("department", department);
        model.addAttribute("person", person);

        return "department-person";
    }

    @PostMapping(value = "/{departmentId}/person/")
    String saveDepartmentPerson(
            @PathVariable Long departmentId,
            @Valid @ModelAttribute("person") Person person,
            BindingResult errors,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("saveDepartmentPerson(departmentId: {})", departmentId);
        if (errors.hasErrors()) {
            log.debug("errors: {}", errors.getAllErrors());
        }

        Department department = departmentService.get(departmentId);
        if (department == null) throw new DepartmentNotFoundException(departmentId);

        Person loggedUser = Utils.getPerson(authentication);
        model.addAttribute("loggedPersonId", loggedUser.getPersonId());

        if (!errors.hasErrors()) {
            if (person.getPersonId() == null) {
                person.setCreatedBy(loggedUser);
                person.setDepartment(department);
            }
            personService.save(person);
            Collection<Person> persons = personService.getAll();
            model.addAttribute("persons", persons);
            model.addAttribute("personFilter", new PersonFilter());
            return "redirect:/department/" + departmentId + "#persons";
        } else {
            model.addAttribute("sex", Arrays.asList(SexStatus.values()));
            model.addAttribute("person", person);
            model.addAttribute("department", department);
            model.addAttribute("departments", Collections.singletonList(department));

            return (person.getPersonId() == null) ? "department-person" : "department-person";
        }
    }
}