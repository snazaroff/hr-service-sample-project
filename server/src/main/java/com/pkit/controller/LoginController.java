package com.pkit.controller;

import com.pkit.Utils;
import com.pkit.model.Person;
import com.pkit.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Controller
public class LoginController {

    private final UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/home")
    String home(
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("home()");

        Person loggedUser = Utils.getPerson(authentication);
        model.addAttribute("loggedPersonId", loggedUser.getPersonId());

//        model.addAttribute("users", userService.getAll());
        return "home";
    }

    @RequestMapping("/")
    String top(
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("top()");

        Person loggedUser = Utils.getPerson(authentication);
        model.addAttribute("loggedPersonId", loggedUser.getPersonId());
//        model.addAttribute("users", userService.getAll());
        return "home";
    }

    @RequestMapping("/login")
    String login(ModelMap model) {
        log.debug("login()");

        model.addAttribute("users", userService.getAll());
        return "login";
    }

    @PostMapping("/login")
    String login_(Object o) {
        log.debug("login_()");
        return "login";
    }

    @GetMapping("/403")
    String error() {
        log.debug("error()");
        return "403";
    }

//    @RequestMapping("/logout")
//    String logout() {
//        log.debug("logout()");
//        return "home";
//    }
}