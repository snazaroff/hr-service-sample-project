package com.pkit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.Utils;
import com.pkit.model.*;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@Slf4j
@Controller
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    @Autowired
    private ObjectMapper om;

    private final RoleService roleService;

    public RoleController(
            RoleService roleService
    ) {
        this.roleService = roleService;
    }

    @GetMapping("/")
    String getAllRole(
            ModelMap model,
            @RequestParam(name = "page", defaultValue = "1") Integer page,
            @RequestParam(name = "size", defaultValue = "10") Integer size,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getAllRole()");

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        Page<Role> rolePage = roleService.getAll(new PageRequest(page - 1, size));
        ControllerUtils.processPageInfo(rolePage, page, model);

        model.addAttribute("roles", rolePage);
        return "roles";
    }

    @GetMapping("/{roleId}")
    String getRole(
            @PathVariable String roleId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getRole: {}", roleId);

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        Role role = (roleId == null || "".equals(roleId))
                ? new Role()
                : roleService.get(roleId);

        model.addAttribute("sex", Arrays.asList(SexStatus.values()));
        log.debug("role: {}", role);
        model.addAttribute("role", role);
        return "role";
    }

    @PostMapping
    String saveRole(
            @Valid @ModelAttribute Role role,
            BindingResult errors,
            Model model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("saveRole: {}", role);
        if (errors.hasErrors()) {
            log.debug("errors: {}", errors.getAllErrors());
        }

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        if (!errors.hasErrors()) {
            if (role.getRoleId() == null)
                role = roleService.add(role);
            else
                role = roleService.add(role);
            Collection<Role> roles = roleService.getAll();
            model.addAttribute("roles", roles);
        }
        return errors.hasErrors() ? ((role.getRoleId() == null) ? "role_new" : "role") : "roles";
    }
}