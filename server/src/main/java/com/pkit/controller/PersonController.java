package com.pkit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.Utils;
import com.pkit.exception.PersonNotFoundException;
import com.pkit.model.*;
import com.pkit.model.filter.PersonFilter;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

import static java.util.stream.Collectors.toList;

@Slf4j
@Controller
@RequestMapping("/person")
public class PersonController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    @Autowired
    private ObjectMapper om;

    private final CompanyService companyService;
    private final DepartmentService departmentService;
    private final PersonService personService;
    private final UserService userService;

    public PersonController(
            CompanyService companyService,
            DepartmentService departmentService,
            PersonService personService,
            UserService userService
    ) {
        this.companyService = companyService;
        this.departmentService = departmentService;
        this.personService = personService;
        this.userService = userService;
    }

    /**
     * Возвращает список сотрудкиков
     *
     * @param model
     * @param page
     * @param size
     * @param authentication
     * @return
     */
    @GetMapping("/")
    String getAllPerson(
            @RequestParam(name = "page", defaultValue = "1") Integer page,
            @RequestParam(name = "size", defaultValue = "10") Integer size,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getAllPerson()");

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        Page<Person> personPage;

        User user = Utils.getUser(authentication);
        Department department = user.getPerson().getDepartment();
        Pageable pageable = new PageRequest(page - 1, size, new Sort("last_name", "first_name", "middle_name"));
        if (user.getRoles().contains(new Role(RoleType.ROLE_EA_ADMIN))) {
            Company company = department.getCompany();
            personPage = personService.getByCompanyId(company.getCompanyId(), pageable);
        } else {
            personPage = personService.getByDepartmentId(department.getDepartmentId(), pageable);
        }

//        Page<Person> personPage = personService.getAll(new PageRequest(page - 1, size, new Sort("lastName", "firstName", "middleName")));

        ControllerUtils.processPageInfo(personPage, page, model);

        model.addAttribute("persons", personPage);
        model.addAttribute("personFilter", new PersonFilter());

//        Collection<Person> persons = personService.getAll();
//        model.addAttribute("persons", persons.stream().sorted().collect(toList()));
        return "persons";
    }

    /**
     * Возвращает сотрудника по id
     *
     * @param personId
     * @param model
     * @param authentication
     * @return
     */
    @GetMapping("/{personId}")
    String getPerson(
            @PathVariable Long personId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getPerson( personId: {})", personId);

        User user = Utils.getUser(authentication);
        Person loggedPerson = user.getPerson();
        model.addAttribute("loggedPersonId", loggedPerson.getPersonId());

        Person person;
        if (personId == -1) {
            person = new Person();
            person.setDepartment(loggedPerson.getDepartment());
        } else {
            person = personService.get(personId);
            if (person == null) throw new PersonNotFoundException(personId);
        }
        model.addAttribute("sex", Arrays.asList(SexStatus.values()));
        log.debug("person: {}", person);
        log.debug("person: {}", person.getDepartment());
        model.addAttribute("person", person);
        List<Department> departments;
        if (user.getRoles().contains(new Role(RoleType.ROLE_EA_ADMIN))) {
            departments = departmentService.findByCompany(loggedPerson.getDepartment().getCompany());
        } else {
            departments = Collections.singletonList(departmentService.get(loggedPerson.getDepartment().getDepartmentId()));
        }
//        List<Department> departments = (person.getDepartment() != null && person.getDepartment().getCompany() != null)
//                ? departmentService.findByCompany(person.getDepartment().getCompany())
//                : departmentService.getAll(Department::getName);
        model.addAttribute("departments", departments);
        return "person/person";
    }

    /**
     * Выборка сотрудников по фильтру
     *
     * @param personFilter
     * @param model
     * @param page
     * @param size
     * @param authentication
     * @return
     */
    @PostMapping("/filter/")
    String getFilteredPerson(
            @RequestParam(name = "page", defaultValue = "1") Integer page,
            @RequestParam(name = "size", defaultValue = "10") Integer size,
            PersonFilter personFilter,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getFilteredPerson( personFilter: {}, page: {}, size: {})", personFilter, page, size);

        log.debug("personFilter.isEmpty(): {}", personFilter.isEmpty());

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        Page<Person> personPage = personService.findByFilter(personFilter, new PageRequest(page - 1, size));
        ControllerUtils.processPageInfo(personPage, page, model);

        model.addAttribute("persons", personPage);
        model.addAttribute("personFilter", personFilter);
        return "persons";
    }

    @PostMapping
//("/{personId}")
    String savePerson(
//            @PathVariable Long personId,
            @Valid @ModelAttribute Person person,
            BindingResult errors,
            Model model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("savePerson(person: {})", person);
        if (errors.hasErrors()) {
            log.debug("errors: {}", errors.getAllErrors());
        }

        Person loggedUser = Utils.getPerson(authentication);
        model.addAttribute("loggedPersonId", loggedUser.getPersonId());

        if (!errors.hasErrors()) {
            if (person.getPersonId() == null) {
                person.setCreatedBy(loggedUser);
                personService.add(person);
            } else
                personService.update(person);
            Collection<Person> persons = personService.getAll();
            model.addAttribute("persons", persons);
            model.addAttribute("personFilter", new PersonFilter());
            return "redirect:/person/";
//            return "person/";
        } else {
            model.addAttribute("sex", Arrays.asList(SexStatus.values()));
            model.addAttribute("person", person);
            List<Department> departments = departmentService.getAll(Department::getName);
            model.addAttribute("departments", departments);
            return (person.getPersonId() == null) ? "person_new" : "person/person";
        }
    }

    @GetMapping("/{personId}/company/{companyId}")
    String getPersonCompany(
            @PathVariable Long personId,
            @PathVariable Long companyId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getPersonCompany(personId: {}, companyId: {})", personId, companyId);

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        Person person;
        if (personId == -1) {
            person = new Person();
        } else {
            person = personService.get(personId);
            if (person == null) throw new PersonNotFoundException(personId);
        }
        model.addAttribute("sex", Arrays.asList(SexStatus.values()));
        log.debug("person: {}", person);
        log.debug("person: {}", person.getDepartment());
        model.addAttribute("person", person);
        Company company = companyService.get(companyId);
        List<Department> departments = (company == null)
                ? Collections.EMPTY_LIST
                : departmentService.findByCompany(company)
                .stream()
                .sorted(Comparator.comparing(Department::getName))
                .collect(toList());
        model.addAttribute("departments", departments);
        return "person/person";
    }

    /**
     * Выводит профиль пользователя.
     * Посмотреть информацию по залогиненому пользователю
     *
     * @param personId
     * @param model
     * @param authentication
     * @return
     */
    @GetMapping("/{personId}/profile")
    String getPersonProfile(
            @PathVariable Long personId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getPersonProfile: {}", personId);

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        Person person = personService.get(personId);
        if (person == null) throw new PersonNotFoundException(personId);

        log.debug("person: {}", person);
        log.debug("person: {}", person.getDepartment());

        model.addAttribute("sex", Arrays.asList(SexStatus.values()));
        model.addAttribute("person", person);

        User user = userService.getUserByPerson(person);
        model.addAttribute("user", user);

//        List<Department> departments = departmentService
//                .getAll()
//                .stream()
//                .sorted((dep1, dep2) -> dep1.getName().compareTo(dep2.getName()))
//                .collect(toList());
//        model.addAttribute("departments", departments);
        return "person_profile";
    }
}