package com.pkit.controller;

import com.pkit.Utils;
import com.pkit.exception.*;
import com.pkit.model.*;
import com.pkit.model.filter.CompanyFilter;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.CompanyService;
import com.pkit.exception.EmploymentAgencyNotFoundException;
import com.pkit.service.DepartmentService;
import com.pkit.service.PersonService;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping("/employment-agency")
public class EmploymentAgencyController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    private final CompanyService companyService;
    private final DepartmentService departmentService;
    private final PersonService personService;

    public EmploymentAgencyController(
            CompanyService companyService,
            DepartmentService departmentService,
            PersonService personService) {
        this.companyService = companyService;
        this.departmentService = departmentService;
        this.personService = personService;
    }

    @GetMapping("/")
    String getAllEmploymentAgency(
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getAllEmploymentAgency()");

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        Collection<Company> employmentAgencies = companyService.getEmploymentAgencies();
        model.addAttribute("employment_agencies", employmentAgencies);
        return "employment_agencies";
    }

    @GetMapping("/{employmentAgencyId}")
    String getEmploymentAgency(
            @PathVariable Long employmentAgencyId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getEmploymentAgency: {}", employmentAgencyId);

        Person loggedUser = Utils.getPerson(authentication);
        model.addAttribute("loggedPersonId", loggedUser.getPersonId());

        Company company = companyService.get(employmentAgencyId);
        if (company == null) {
            company = new Company();
        }
        log.debug("company: {}", company);
        model.addAttribute("company", company);

        Collection<Department> departments = company.getDepartments()
                .stream()
                .sorted(Comparator.comparing(Department::getName))
                .collect(Collectors.toList());
        model.addAttribute("departments", departments);
        Collection<Person> persons = personService.getByCompanyId(employmentAgencyId)
                .stream()
                .sorted(Comparator.comparing(Human::getFio))
                .collect(Collectors.toList());
        model.addAttribute("persons", persons);

        return "employment-agency";
    }

    @PostMapping
    String saveEmploymentAgency(
            @Valid @ModelAttribute Company employmentAgency,
            BindingResult errors,
            Model model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("createEmploymentAgency: {}", employmentAgency);
        if (errors.hasErrors()) {
            log.debug("errors: {}", errors.getAllErrors());
        }

        Person loggedUser = Utils.getPerson(authentication);
        model.addAttribute("loggedPersonId", loggedUser.getPersonId());

        if (!errors.hasErrors()) {
            companyService.save(employmentAgency);
//            if (employmentAgency.getCompanyId() == null) {
//                employmentAgency = companyService.add(employmentAgency);
//            } else
//                employmentAgency = companyService.update(employmentAgency);
            Collection<Company> employmentAgencies = companyService.getAll();
            model.addAttribute("employmentAgencies", employmentAgencies);
        }
        return errors.hasErrors() ? ((employmentAgency.getCompanyId() == null) ? "employmentAgency_new" : "employmentAgency") : "employmentAgencies";
    }

    @PutMapping("/{employmentAgencyId}")
    Company updateEmploymentAgency(
            @PathVariable Long employmentAgencyId,
            @Valid @RequestBody Company employmentAgency,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("updateEmploymentAgency(employmentAgencyId: {}, employmentAgency: {})", employmentAgencyId, employmentAgency);

//        Long loggedPersonId = Utils.getPersonId(authentication);
//        model.addAttribute("loggedPersonId", loggedPersonId);

        if (companyService.exists(employmentAgencyId)) throw new EmploymentAgencyNotFoundException(employmentAgencyId);
        employmentAgency.setCompanyId(employmentAgencyId);
        return companyService.update(employmentAgency);
    }

    /**
     * Форма для редактирования отдела компании
     *
     * @param companyId
     * @param departmentId
     * @param model
     * @param authentication
     * @return
     */
    @GetMapping("/{companyId}/department/{departmentId}")
    String getEmploymentAgencyDepartment(
            @PathVariable Long companyId,
            @PathVariable Long departmentId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getEmploymentAgencyDepartment(companyId: {}, departmentId: {})", companyId, departmentId);

        Company company = companyService.get(companyId);
        if (company == null) throw new CompanyNotFoundException(companyId);

        Department department;
        if (departmentId == -1) {
            department = new Department();
            department.setCompany(company);
        } else {
            department = departmentService.get(departmentId);
            if (department == null) throw new DepartmentNotFoundException(departmentId);
        }
        log.debug("department: {}", department);

        model.addAttribute("companies", Collections.singletonList(company));
        model.addAttribute("companyId", company.getCompanyId());
        model.addAttribute("department", department);

        model.addAttribute("persons", personService.getByDepartmentId(departmentId));

        model.addAttribute("form_action", "/employment-agency/" + company.getCompanyId() + "/department/");

        return (departmentId == -1) ? "department-new" : "department";
    }

    @PostMapping(value = "/{companyId}/department/")
    String saveEmploymentAgencyDepartment(
            @PathVariable Long companyId,
            @Valid @ModelAttribute("department") Department department,
            BindingResult errors,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("saveEmploymentAgencyDepartment(companyId: {})", companyId);

        Person loggedUser = Utils.getPerson(authentication);

        if (errors.hasErrors() && errors.getFieldError("createdBy") != null) {
            department.setCreatedBy(loggedUser);
        }

        if (errors.hasErrors()) {
            log.debug("errors: {}", errors.getAllErrors());
        }

        Company company = companyService.get(companyId);
        if (company == null) throw new CompanyNotFoundException(companyId);

        model.addAttribute("loggedPersonId", loggedUser.getPersonId());

        if (!errors.hasErrors() || (errors.getFieldErrors().size() == 1 && errors.getFieldError("createdBy") != null)) {
            if (department.getDepartmentId() == null) {
                department.setCreatedBy(loggedUser);
                department.setCompany(company);
            }
            departmentService.save(department);
            Collection<Company> companies = companyService.getAll();
            model.addAttribute("companies", companies);
            model.addAttribute("companyFilter", new CompanyFilter());
            return "redirect:/employment-agency/" + companyId + "#departments";
        } else {
            model.addAttribute("department", department);
            model.addAttribute("companies", Collections.singletonList(company));

            return (department.getDepartmentId() == null) ? "department-new" : "department";
        }
    }

    /**
     * Форма для редактирования сотрудника компании
     *
     * @param companyId
     * @param persontId
     * @param model
     * @param authentication
     * @return
     */
    @GetMapping("/{companyId}/person/{persontId}")
    String getEmploymentAgencyPerson(
            @PathVariable Long companyId,
            @PathVariable Long persontId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getEmploymentAgencyPerson(companyId: {}, persontId: {})", companyId, persontId);

        Company company = companyService.get(companyId);
        if (company == null) throw new CompanyNotFoundException(companyId);

        Person person;
        if (persontId == -1) {
            person = new Person();
//            person.setCompany(company);
        } else {
            person = personService.get(persontId);
            if (person == null) throw new PersonNotFoundException(persontId);
        }
        log.debug("person: {}", person);

//        model.addAttribute("companies", Collections.singletonList(company));
//        model.addAttribute("companyId", company.getCompanyId());
        model.addAttribute("person", person);
        model.addAttribute("sex", Arrays.asList(SexStatus.values()));
        List<Department> departments = (person.getDepartment() != null && person.getDepartment().getCompany() != null)
                ? departmentService.findByCompany(person.getDepartment().getCompany())
                : departmentService.getAll(Department::getName);
        model.addAttribute("departments", departments);

        model.addAttribute("form_action", "/employment-agency/" + company.getCompanyId() + "/person/");

        return (persontId == -1) ? "person-new" : "person/person";
    }

    @PostMapping(value = "/{companyId}/person/")
    String saveEmploymentAgencyPerson(
            @PathVariable Long companyId,
            @Valid @ModelAttribute("person") Person person,
            BindingResult errors,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("saveEmploymentAgencyPerson(companyId: {})", companyId);

        Person loggedUser = Utils.getPerson(authentication);

        if (errors.hasErrors() && errors.getFieldError("createdBy") != null) {
            person.setCreatedBy(loggedUser);
        }

        if (errors.hasErrors()) {
            log.debug("errors: {}", errors.getAllErrors());
        }

        Company company = companyService.get(companyId);
        if (company == null) throw new CompanyNotFoundException(companyId);

        model.addAttribute("loggedPersonId", loggedUser.getPersonId());

        model.addAttribute("sex", Arrays.asList(SexStatus.values()));
        List<Department> departments = (person.getDepartment() != null && person.getDepartment().getCompany() != null)
                ? departmentService.findByCompany(person.getDepartment().getCompany())
                : departmentService.getAll(Department::getName);
        model.addAttribute("departments", departments);
        if (!errors.hasErrors() || (errors.getFieldErrors().size() == 1 && errors.getFieldError("createdBy") != null)) {
            if (person.getPersonId() == null) {
                person.setCreatedBy(loggedUser);
            }
            personService.save(person);
//            Collection<Company> companies = companyService.getAll();
//            model.addAttribute("companies", companies);
//            model.addAttribute("companyFilter", new CompanyFilter());
            return "redirect:/employment-agency/" + companyId + "#persons";
        } else {
            model.addAttribute("person", person);
//            model.addAttribute("companies", Collections.singletonList(company));

            return (person.getPersonId() == null) ? "person-new" : "person/person";
        }
    }
}