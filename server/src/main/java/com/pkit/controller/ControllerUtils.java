package com.pkit.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
public class ControllerUtils {

    public static void processPageInfo(Page page, Integer currentPage, ModelMap model) {
        String pageContentName = (page.hasContent() && page.getContent().size() > 0)
                ? page.getContent().get(0).getClass().getSimpleName()
                : "";
        log.debug("{}: size: {}, totalPages: {}, totalElements: {}, number: {}, numberOfElements(): {}, sort: {}",
                pageContentName, page.getSize(), page.getTotalPages(), page.getTotalElements(),
                page.getNumber(), page.getNumberOfElements(), page.getSort());

        int totalPages = page.getTotalPages();
        if (totalPages > 1) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
            model.addAttribute("currentPage", currentPage);
        }
    }
}
