package com.pkit.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.Utils;
import com.pkit.exception.*;
import com.pkit.model.*;
import com.pkit.model.Currency;
import com.pkit.model.filter.CompanyFilter;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    @Autowired
    private ObjectMapper om;

    private final CompanyService companyService;
    private final DemandService demandService;
    private final DemandCandidateService demandCandidateService;
    private final DemandHistoryService demandHistoryService;
    private final DemandNoteService demandNoteService;
    private final DepartmentService departmentService;
    private final PersonService personService;

    public CompanyController(
            CompanyService companyService,
            DemandService demandService,
            DemandCandidateService demandCandidateService,
            DemandHistoryService demandHistoryService,
            DemandNoteService demandNoteService,
            DepartmentService departmentService,
            PersonService personService
    ) {
        this.companyService = companyService;
        this.demandService = demandService;
        this.demandCandidateService = demandCandidateService;
        this.demandHistoryService = demandHistoryService;
        this.demandNoteService = demandNoteService;
        this.departmentService = departmentService;
        this.personService = personService;
    }

    @GetMapping("/")
    String getAllCompany(
            @RequestParam(name = "page", defaultValue = "1") Integer page,
            @RequestParam(name = "size", defaultValue = "10") Integer size,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) throws JsonProcessingException {
        log.debug("getAllCompany()");

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

//        Collection<Company> companies = companyService.getCustomers(personId);
        Collection<Company> companies = companyService.getCustomers();

        Collection<Person> persons = personService.getAll();
        model.addAttribute("companies", companies);
        model.addAttribute("persons", persons);

        log.debug("companies: {}", om.writeValueAsString(companies));
        log.debug("persons: {}", om.writeValueAsString(persons));

        return "companies";
    }

    @Transactional
    @GetMapping("/{companyId}")
    String getCompany(
            @PathVariable Long companyId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getCompany: {}", companyId);

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        if (companyId == -1) {
            Company company = new Company();
            company.setCompanyType(CompanyType.CLIENT);
            log.debug("company: {}", company);

            Collection<Person> persons = Collections.EMPTY_LIST;
            Collection<Demand> demands = Collections.EMPTY_LIST;
            model.addAttribute("company", company);
            model.addAttribute("persons", persons);

            return "company-new";
        } else {
            Company company = companyService.get(companyId);
            log.debug("company: {}", company);

            Collection<Department> departments = company.getDepartments()
                    .stream()
                    .sorted(Comparator.comparing(Department::getName))
                    .collect(Collectors.toList());
            Collection<Person> persons = personService.getByCompanyId(companyId)
                    .stream()
                    .sorted(Comparator.comparing(Human::getFio))
                    .collect(Collectors.toList());
            Collection<Demand> demands = companyService.getDemands(companyId)
                    .stream()
                    .sorted(Comparator.comparing(Demand::getCreateDate).reversed())
                    .collect(Collectors.toList());

            model.addAttribute("company", company);
            model.addAttribute("departments", departments);
            model.addAttribute("persons", persons);
            model.addAttribute("demands", demands);

            model.addAttribute("company_types", CompanyType.values());

            return "company";
        }
    }

    @Transactional
    @PostMapping
    String saveCompany(
            @Valid @ModelAttribute Company company,
            BindingResult errors,
            Model model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("saveCompany: {}", company);
        if (errors.hasErrors()) {
            log.debug("errors: {}", errors.getAllErrors());
        }

        Person loggedUser = Utils.getPerson(authentication);
        model.addAttribute("loggedPersonId", loggedUser.getPersonId());

        if (!errors.hasErrors()) {
            if (company.getCompanyId() == null) {
                company.setCreatedBy(loggedUser);
                company.setCompanyType(CompanyType.CLIENT);
                company = companyService.add(company);
            } else {
                //todo: фигня какая-то. Надо исправлять
                Company oldCompany = companyService.get(company.getCompanyId());
                company.setPickeds(oldCompany.getPickeds());
//                company.setPickeds(new HashSet<>());
                company = companyService.update(company);
            }
            Collection<Company> companies = companyService.getAll();
            model.addAttribute("companies", companies);
        }
        return errors.hasErrors() ? ((company.getCompanyId() == null) ? "company-new" : "company") : "companies";
    }

    @Transactional
    @PutMapping("/{companyId}")
    Company updateCompany(
            @PathVariable Long companyId,
            @Valid @RequestBody Company company,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("updateCompany(companyId: {}, company: {})", companyId, company);

        Person loggedUser = Utils.getPerson(authentication);

        if (companyService.exists(companyId)) throw new CompanyNotFoundException(companyId);
        company.setCompanyId(companyId);
        return companyService.update(company);
    }

    /**
     * Форма для редактирования отдела компании
     *
     * @param companyId
     * @param departmentId
     * @param model
     * @param authentication
     * @return
     */
    @GetMapping("/{companyId}/department/{departmentId}")
    String getCompanyPerson(
            @PathVariable Long companyId,
            @PathVariable Long departmentId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getCompanyDepartment(companyId: {}, departmentId: {})", companyId, departmentId);

        Company company = companyService.get(companyId);
        if (company == null) throw new CompanyNotFoundException(companyId);

        Department department;
        if (departmentId == -1) {
            department = new Department();
            department.setCompany(company);
        } else {
            department = departmentService.get(departmentId);
            if (department == null) throw new DepartmentNotFoundException(departmentId);
        }
        log.debug("department: {}", department);

        model.addAttribute("companies", Collections.singletonList(company));
        model.addAttribute("companyId", company.getCompanyId());
        model.addAttribute("department", department);

        model.addAttribute("form_action", "/company/" + company.getCompanyId() + "/department/");

        return (departmentId == -1) ? "department-new" : "department";
    }

    @PostMapping(value = "/{companyId}/department/")
    String saveCompanyDepartment(
            @PathVariable Long companyId,
            @Valid @ModelAttribute("department") Department department,
            BindingResult errors,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("saveCompanyDepartment(companyId: {})", companyId);

        Person loggedUser = Utils.getPerson(authentication);

        if (errors.hasErrors() && errors.getFieldError("createdBy") != null) {
            department.setCreatedBy(loggedUser);
        }

        if (errors.hasErrors()) {
            log.debug("errors: {}", errors.getAllErrors());
        }

        Company company = companyService.get(companyId);
        if (company == null) throw new CompanyNotFoundException(companyId);

        model.addAttribute("loggedPersonId", loggedUser.getPersonId());

        if (!errors.hasErrors() || (errors.getFieldErrors().size() == 1 && errors.getFieldError("createdBy") != null)) {
            if (department.getDepartmentId() == null) {
                department.setCreatedBy(loggedUser);
                department.setCompany(company);
                departmentService.add(department);
            } else
                departmentService.update(department);
            Collection<Company> companies = companyService.getAll();
            model.addAttribute("companies", companies);
            model.addAttribute("companyFilter", new CompanyFilter());
            return "redirect:/company/" + companyId + "#departments";
        } else {
            model.addAttribute("department", department);
            model.addAttribute("companies", Collections.singletonList(company));

            return (department.getDepartmentId() == null) ? "department-new" : "department";
        }
    }

    /**
     * Форма для редактирования заказа компании
     *
     * @param companyId
     * @param demandId
     * @param model
     * @param authentication
     * @return
     */
    @GetMapping("/{companyId}/demand/{demandId}")
    String getCompanyDemand(
            @PathVariable Long companyId,
            @PathVariable Long demandId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getCompanyDemand(companyId: {}, demandId: {})", companyId, demandId);

        Company company = companyService.get(companyId);
        if (company == null) throw new CompanyNotFoundException(companyId);

        Demand demand;
        if (demandId == -1) {
            demand = new Demand();
            demand.setCompany(company);
        } else {
            demand = demandService.get(demandId);
            if (demand == null) throw new DemandNotFoundException(demandId);
        }
        log.debug("demand: {}", demand);
        model.addAttribute("companies", Collections.singletonList(company));
        model.addAttribute("companyId", company.getCompanyId());
        model.addAttribute("demand", demand);

        model.addAttribute("status", Arrays.asList(DemandStatus.values()));
        model.addAttribute("currencies", Arrays.asList(Currency.values()));

        Collection<Person> persons = personService.getByCompanyId(companyId);
        log.debug("persons: {}", persons);

        model.addAttribute("persons", persons);

        List<DemandNote> notes = demandNoteService.getByDemandId(demandId);
        Collections.sort(notes);

        List<DemandCandidate> candidates = demandCandidateService.getByDemandId(demandId);

        model.addAttribute("persons", persons);
        model.addAttribute("demand", demand);
        model.addAttribute("history", demandHistoryService.get(demandId, (short) 0));
        model.addAttribute("notes", notes);
        model.addAttribute("candidates", candidates);

        model.addAttribute("form_action", "/company/" + company.getCompanyId() + "/demand/");

        return (demandId == -1) ? "demand-new" : "demand";
    }

    @PostMapping(value = "/{companyId}/demand/")
    String saveCompanyDemand(
            @PathVariable Long companyId,
            @Valid @ModelAttribute("demand") Demand demand,
            BindingResult errors,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("saveCompanyDemand(companyId: {})", companyId);
        if (errors.hasErrors()) {
            log.debug("errors: {}", errors.getAllErrors());
        }

        Company company = companyService.get(companyId);
        if (company == null) throw new CompanyNotFoundException(companyId);

        Person loggedUser = Utils.getPerson(authentication);
        model.addAttribute("loggedPersonId", loggedUser.getPersonId());

        if (!errors.hasErrors()) {
            if (demand.getDemandId() == null) {
                demand.setCreatedBy(loggedUser);
                demand.setCompany(company);
            } else {
                Demand oldDemand = demandService.get(demand.getDemandId());
                demand.setPickeds(oldDemand.getPickeds());
            }
            demandService.save(demand);
            Collection<Company> companies = companyService.getAll();
            model.addAttribute("companies", companies);
            model.addAttribute("companyFilter", new CompanyFilter());
            return "redirect:/company/" + companyId + "#demands";
        } else {
            model.addAttribute("demand", demand);
            model.addAttribute("companies", Collections.singletonList(company));

            return (demand.getDemandId() == null) ? "demand-new" : "demand";
        }
    }

    @PostMapping(value = "/{companyId}/person/")
    String saveCompanyPerson(
            @PathVariable Long companyId,
            @Valid @ModelAttribute("person") Person person,
            BindingResult errors,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("saveCompanyPerson(companyId: {})", companyId);

        Person loggedUser = Utils.getPerson(authentication);

        if (errors.hasErrors() && errors.getFieldError("createdBy") != null) {
            person.setCreatedBy(loggedUser);
        }

        if (errors.hasErrors()) {
            log.debug("errors: {}", errors.getAllErrors());
        }

        Company company = companyService.get(companyId);
        if (company == null) throw new CompanyNotFoundException(companyId);

        model.addAttribute("loggedPersonId", loggedUser.getPersonId());

        List<Department> departments = departmentService.findByCompany(company);
        model.addAttribute("departments", departments);

        if (!errors.hasErrors() || (errors.getFieldErrors().size() == 1 && errors.getFieldError("createdBy") != null)) {
            if (person.getPersonId() == null) {
                person.setCreatedBy(loggedUser);
            }
            personService.save(person);
            return "redirect:/company/" + companyId + "#persons";
        } else {
            model.addAttribute("person", person);

            return (person.getPersonId() == null) ? "person-new" : "person";
        }
    }
}