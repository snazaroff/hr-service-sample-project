package com.pkit.controller;

import com.pkit.Utils;
import com.pkit.model.*;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@SuppressWarnings("SameReturnValue")
@Slf4j
@Controller
@RequestMapping("/demand/candidate")
public class DemandCandidateController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    private final DemandCandidateService demandCandidateService;
    private final DemandCandidateNoteService demandCandidateNoteService;
    private final PersonService personService;

    public DemandCandidateController(
            DemandCandidateService demandCandidateService,
            DemandCandidateNoteService demandCandidateNoteService,
            PersonService personService
    ) {
        this.demandCandidateService = demandCandidateService;
        this.demandCandidateNoteService = demandCandidateNoteService;
        this.personService = personService;
    }

    @GetMapping("/{demandCandidateId}")
    String getDemandCandidate(
            @PathVariable Long demandCandidateId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getDemandCandidate(demandCandidateId: {})", demandCandidateId);

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        DemandCandidate demandCandidate = demandCandidateService.get(demandCandidateId);

        demandCandidate.setMeetings(new TreeSet<>(demandCandidate.getMeetings()));
        model.addAttribute("demandCandidate", demandCandidate);
        model.addAttribute("demandCandidateNotes", demandCandidateNoteService.findByDemandCandidate(demandCandidate));
        Collection<Person> persons = personService.getAll();
        model.addAttribute("persons", persons);
        model.addAttribute("meetingTypes", Arrays.asList(MeetingType.values()));

        return "demand-candidate";
    }
}