package com.pkit.controller.advicer;

import com.pkit.exception.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class CommonRestControllerAdvice {//extends ResponseEntityExceptionHandler {

//    @ExceptionHandler({HttpClientErrorException.class })
//    protected ResponseEntity<Object> handleHttpClientErrorException(HttpClientErrorException ex, WebRequest request) {
//        return handleExceptionInternal(ex,  ex.getResponseBodyAsString(), new HttpHeaders(), ex.getStatusCode(), request);
//    }
//
//    @ExceptionHandler({HttpServerErrorException.class })
//    protected ResponseEntity<Object> handleHttpServerErrorException(HttpServerErrorException ex, WebRequest request) {
//        return handleExceptionInternal(ex,  ex.getResponseBodyAsString(), new HttpHeaders(), ex.getStatusCode(), request);
//    }

//    @ExceptionHandler({DBException.class })
//    protected ResponseEntity<Object> handleDBException(DBException ex, WebRequest request) {
//        return handleExceptionInternal(ex,  ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
//    }
//
//    @ExceptionHandler({PersonBindToDepartmentException.class })
//    protected ResponseEntity<Object> handlePersonBindToDepartmentException(PersonBindToDepartmentException ex, WebRequest request) {
//        return handleExceptionInternal(ex,  ex.getMessage(), new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY, request);
//    }
//
//    @ExceptionHandler({AccessDeniedException.class })
//    protected ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex, WebRequest request) {
//        return handleExceptionInternal(ex,  "Нет прав на такое действие" , new HttpHeaders(), HttpStatus.FORBIDDEN, request);
//    }

//    @ExceptionHandler({DepartmentNotFoundException.class })
//    protected ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex, WebRequest request) {
//        return handleExceptionInternal(ex,  "Нет прав на такое действие" , new HttpHeaders(), HttpStatus.FORBIDDEN, request);
//    }

    @ExceptionHandler(DBException.class)
    public ResponseEntity<Object> handle(DBException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DepartmentNotFoundException.class)
    public ResponseEntity<Object> handle(DepartmentNotFoundException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({HttpMessageNotReadableException.class })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handle(HttpMessageNotReadableException e) {
        log.error("Returning HTTP 400 Bad Request", e);
    }
}