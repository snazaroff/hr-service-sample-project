package com.pkit.controller;

import com.pkit.Utils;
import com.pkit.model.*;
import com.pkit.model.Currency;
import com.pkit.model.filter.DemandFilter;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.*;
import com.pkit.exception.*;
import lombok.extern.slf4j.Slf4j;

import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.validation.Valid;
import java.util.*;

@Slf4j
@Controller
@RequestMapping("/demand")
public class DemandController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    @Autowired
    private EntityManager entityManager;

    private final CompanyService customerService;
    private final DemandService demandService;
    private final DemandCandidateService demandCandidateService;
    private final DemandHistoryService demandHistoryService;
    private final DemandNoteService demandNoteService;
    private final DictionaryService dictionaryService;
    private final PersonService personService;

    public DemandController(
            CompanyService customerService,
            DemandService demandService,
            DemandCandidateService demandCandidateService,
            DemandHistoryService demandHistoryService,
            DemandNoteService demandNoteService,
            DictionaryService dictionaryService,
            PersonService personService
    ) {
        this.customerService = customerService;
        this.demandService = demandService;
        this.demandCandidateService = demandCandidateService;
        this.demandHistoryService = demandHistoryService;
        this.demandNoteService = demandNoteService;
        this.dictionaryService = dictionaryService;
        this.personService = personService;
    }

    /**
     * Возвращает список заказов
     *
     * @param model
     * @param page
     * @param size
     * @param authentication
     * @return
     */
    @GetMapping("/")
    String getAllDemand(
            @RequestParam(name = "page", defaultValue = "1") Integer page,
            @RequestParam(name = "size", defaultValue = "10") Integer size,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getAllDemand()");

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        Session session = entityManager.unwrap(Session.class);

        Filter filter = session.enableFilter("pickedDemand");
        filter.setParameter("currentPerson", loggedPersonId);

        Page<Demand> demandPage;
        User user = Utils.getUser(authentication);
        Department department = user.getPerson().getDepartment();
//        Pageable pageable = new PageRequest(page - 1, size, new Sort("last_name", "first_name", "middle_name"));
        Pageable pageable = new PageRequest(page - 1, size);
        if (user.getRoles().contains(new Role(RoleType.ROLE_EA_ADMIN))) {
            Company company = department.getCompany();
            demandPage = demandService.getByCompanyId(company.getCompanyId(), pageable);
        } else {
            demandPage = demandService.getByDepartmentId(department.getDepartmentId(), pageable);
        }

//        Page<Demand> demandPage = demandService.getAll(new PageRequest(page - 1, size));

        ControllerUtils.processPageInfo(demandPage, page, model);

        model.addAttribute("demands", demandPage);
        model.addAttribute("demandFilter", new DemandFilter());

        Collection<Company> customers = customerService.getAll();
        model.addAttribute("customers", customers);

        return "demands";
    }

    /**
     * Возвращает список заказов по фильтру
     *
     * @param demandFilter
     * @param model
     * @param page
     * @param size
     * @param authentication
     * @return
     */
    @PostMapping("/filter/")
    String getFilteredDemand(
            @RequestParam(name = "page", defaultValue = "1") Integer page,
            @RequestParam(name = "size", defaultValue = "10") Integer size,
            DemandFilter demandFilter,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getFilteredDemand( demandFilter: {}, page: {}, size: {})", demandFilter, page, size);

        Person loggedPerson = Utils.getPerson(authentication);
        model.addAttribute("loggedPersonId", loggedPerson.getPersonId());

        Page<Demand> demandPage = demandService.findByFilter(demandFilter, new PageRequest(page - 1, size));
        ControllerUtils.processPageInfo(demandPage, page, model);

        model.addAttribute("demands", demandPage);

        model.addAttribute("metros", dictionaryService.getMetroByTown(dictionaryService.MOSCOW.getTownId()));
        model.addAttribute("demandFilter", demandFilter);

        Collection<Company> customers = customerService.getAll();
        model.addAttribute("customers", customers);

        return "demands";
    }

    /**
     * Возвращает заказ по id
     *
     * @param demandId
     * @param model
     * @param authentication
     * @return
     */
    @GetMapping("/{demandId}")
    String getDemand(
            @PathVariable Long demandId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getDemand( demandId: {})", demandId);

        Person loggedUser = Utils.getPerson(authentication);
        model.addAttribute("loggedPersonId", loggedUser.getPersonId());

        Collection<Company> customers = customerService.getAll();
//        Collection<Company> customers = customerService.getAvailableCustomers(loggedUser.getPersonId());

        model.addAttribute("customers", customers);
        model.addAttribute("status", Arrays.asList(DemandStatus.values()));
        model.addAttribute("currencies", Arrays.asList(Currency.values()));

        Collection<Person> persons = personService.getByCompanyId(loggedUser.getDepartment().getCompany().getCompanyId());
        log.debug("persons: {}", persons);
        model.addAttribute("persons", persons);

        //Определяем список компаний, к которым можно будет привязать заказ
        List<Company> companies = Arrays.asList(loggedUser.getDepartment().getCompany());
        model.addAttribute("companies", companies);

        if (demandId == -1) {
            Demand demand = new Demand();
            demand.setCreatedBy(loggedUser);
            demand.setStatus(DemandStatus.CREATE);
            log.debug("demand: {}", demand);
            model.addAttribute("demand", demand);

            return "demand-new";
        } else {
            Demand demand = demandService.get(demandId);
            if (demand == null) throw new DemandNotFoundException(demandId);

            log.debug("demand: {}", demand);
            model.addAttribute("demand", demand);
            model.addAttribute("history", demandHistoryService.get(demandId, (short) 0));
            List<DemandNote> notes = demandNoteService.getByDemandId(demandId);
//            Collections.sort(notes);
            model.addAttribute("notes", notes);
            List<DemandCandidate> candidates = demandCandidateService.getByDemandId(demandId);
            Collections.sort(candidates);
            model.addAttribute("candidates", candidates);
            String action = "view";
            try {
                action = policy.checkPermission(demand, Arrays.asList("edit", "view"));
            } catch (AccessDeniedException e) {
                log.error(e.getMessage());
                return "403";
            }

//            String action = policy.checkPermission(demand, Arrays.asList(new String[]{"edit", "view"}));

            log.debug("action: {}", action);
            return "demand_" + action;
        }
    }

    /**
     * Возвращает компанию привязанную к заказу
     *
     * @param demandId
     * @param companyId
     * @param model
     * @param authentication
     * @return
     */
    @GetMapping("/{demandId}/company/{companyId}")
    String getCompanyDemand(
            @PathVariable Long demandId,
            @PathVariable Long companyId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getDemand(demandId: {}, companyId: {})", demandId, companyId);

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        Collection<Person> persons = personService.getAll();
        Collection<Company> customers = customerService.getAll();

        model.addAttribute("persons", persons);
        model.addAttribute("customers", customers);
        model.addAttribute("status", Arrays.asList(DemandStatus.values()));
        model.addAttribute("currencies", Arrays.asList(Currency.values()));

        if (demandId == -1) {
            Demand demand = new Demand();
            demand.setStatus(DemandStatus.CREATE);
            log.debug("demand: {}", demand);
            model.addAttribute("demand", demand);

            Company company = customerService.get(companyId);
            demand.setCompany(company);

            return "demand-new";
        } else {
            Demand demand = demandService.get(demandId);
            if (demand == null) throw new DemandNotFoundException(demandId);
            log.debug("demand: {}", demand);

            List<DemandNote> notes = demandNoteService.getByDemandId(demandId);
//            Collections.sort(notes);

            List<DemandCandidate> candidates = demandCandidateService.getByDemandId(demandId);

            model.addAttribute("demand", demand);
            model.addAttribute("history", demandHistoryService.get(demandId, (short) 0));
            model.addAttribute("notes", notes);
            model.addAttribute("candidates", candidates);

            return "demand_edit";
        }
    }

    @PostMapping
    String saveDemand(
            @Valid @ModelAttribute Demand demand,
            BindingResult errors,
            Model model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("saveDemand(demand: {})", demand);
        if (errors.hasErrors()) {
            log.debug("errors: {}", errors.getAllErrors());
        }

        Person loggedUser = Utils.getPerson(authentication);
        model.addAttribute("loggedPersonId", loggedUser.getPersonId());

        if (!errors.hasErrors()) {
            if (demand.getDemandId() == null) {
                demand.setCreatedBy(loggedUser);
                demand = demandService.add(demand);
                model.addAttribute("demand", demand);

                Collection<Person> persons = personService.getAll();
                Collection<Company> customers = customerService.getAll();

                model.addAttribute("persons", persons);
                model.addAttribute("customers", customers);
                model.addAttribute("status", Arrays.asList(DemandStatus.values()));

                return "redirect:/demand/" + demand.getDemandId();
            } else {
                log.debug("demand.getPickeds(): {}", demand.getPickeds());
//                demand.setPickeds(new HashSet<>());
                //todo: фигня какая-то. Надо исправлять
                Demand oldDemand = demandService.get(demand.getDemandId());
                demand.setPickeds(oldDemand.getPickeds());
                demand = demandService.update(demand);
//                Set<PickedDemand> pickeds = demand.getPickeds();
//                pickeds.forEach(p->{
//                    p.demandId = null;
//                    p.personId = null;
//                });
////                demand.getPickeds().clear();
////                demand.getPickeds().addAll(pickeds);
//                demand.setPickeds(new HashSet(){{
//                    addAll(pickeds);
//                }});
                log.debug("demand: {}", demand);
                Collection<Demand> demands = demandService.getAll();
                model.addAttribute("demands", demands);
                model.addAttribute("demands", demands);
                model.addAttribute("demandFilter", new DemandFilter());
                return "demands";
            }
        } else {
            return (demand.getDemandId() == null) ? "demand-new" : "demand";
        }
    }

    /**
     * Редактирует заказ
     *
     * @param demandId
     * @param demand
     * @param authentication
     * @return
     */
    @PutMapping("/{demandId}")
    Demand updateDemand(
            @PathVariable Long demandId,
            @Valid @RequestBody Demand demand,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("updateDemand(demandId: {}, demand: {})", demandId, demand);

//        Long loggedPersonId = Utils.getPersonId(authentication);
//        model.addAttribute("loggedPersonId", loggedPersonId);

        if (demandService.exists(demandId)) throw new DemandNotFoundException(demandId);

        demand.setDemandId(demandId);
        return demandService.update(demand);
    }
}