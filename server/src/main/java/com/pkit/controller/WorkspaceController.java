package com.pkit.controller;

import com.pkit.Utils;
import com.pkit.model.*;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.ModelMap;

import javax.persistence.EntityManager;

import java.util.Collection;

@Slf4j
@Controller
@RequestMapping("/workspace")
public class WorkspaceController {

    private final CandidateService candidateService;
    private final CompanyService customerService;
    private final DemandService demandService;

    @Autowired
    private EntityManager entityManager;

    public WorkspaceController(
            CandidateService candidateService,
            CompanyService customerService,
            DemandService demandService
    ) {
        this.candidateService = candidateService;
        this.customerService = customerService;
        this.demandService = demandService;
    }

    @GetMapping("/")
    String getWorkspaceData(
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getWorkspaceData()");

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        Session session = entityManager.unwrap(Session.class);

        Filter filter = session.enableFilter("pickedCompany");
        filter.setParameter("currentPerson", loggedPersonId);

        filter = session.enableFilter("pickedCandidate");
        filter.setParameter("currentPerson", loggedPersonId);

        filter = session.enableFilter("pickedDemand");
        filter.setParameter("currentPerson", loggedPersonId);

        Collection<Candidate> candidates = candidateService.getAll();
        Collection<Company> customers = customerService.getCustomers();
        Collection<Demand> demands = demandService.getAll();
        model.addAttribute("customers", customers);
        model.addAttribute("demands", demands);
        model.addAttribute("candidates", candidates);
        return "workspace";
    }
}