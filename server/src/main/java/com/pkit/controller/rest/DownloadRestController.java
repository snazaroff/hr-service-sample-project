package com.pkit.controller.rest;

import com.pkit.service.DownloadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@RestController
@RequestMapping("/export")
public class DownloadRestController {

    private final DownloadService downloadService;

    public DownloadRestController(
            DownloadService downloadService
    ) {
        this.downloadService = downloadService;
    }

    @GetMapping("/{candidateId}/{type}")
    void download(@PathVariable Long candidateId, @PathVariable String type, HttpServletResponse response) throws Exception {
        log.debug("getByCandidate(candidateId: {})", candidateId);

        DownloadService.Result result = downloadService.download(candidateId, type);

        response.setHeader("Content-disposition", "attachment; filename=resume." + result.getResumeOutputType().getName());
        response.setHeader("Content-Type", result.getResumeOutputType().getContentType());
        response.setStatus(HttpStatus.OK.value());
        response.getOutputStream().write(result.getFile());
        response.getOutputStream().flush();
    }
}

