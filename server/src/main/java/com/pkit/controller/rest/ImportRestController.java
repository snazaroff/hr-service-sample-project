package com.pkit.controller.rest;

import com.pkit.file_store.service.ConvertService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@SuppressWarnings("SameReturnValue")
@Slf4j
//@RestController
//@RequestMapping("/import")
public class ImportRestController {

    private final ConvertService importService;

    public ImportRestController(
            ConvertService importService
    ) {
        this.importService = importService;
    }

    @PostMapping("/candidate")
    String addCandidateResume(@RequestParam("file") MultipartFile file) throws Exception {
        String filePath = "load_" + UUID.randomUUID() + file.getName();
        if (!file.isEmpty()) {
            byte[] bytes = file.getBytes();
            importService.importResume(bytes, file.getName());
            return "candidate";
        } else {
            throw new RuntimeException("Empty file!");
        }
    }
}

