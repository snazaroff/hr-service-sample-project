package com.pkit.controller.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.Utils;
import com.pkit.exception.*;
import com.pkit.model.*;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.*;

@Slf4j
@RestController
@RequestMapping(value = "/demand/candidate/meeting", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
public class DemandCandidateMeetingRestController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    @Autowired
    private ObjectMapper om;

    private final DemandCandidateService demandCandidateService;
    private final DemandCandidateMeetingService demandCandidateMeetingService;
    private final DemandCandidateMeetingNoteService demandCandidateMeetingNoteService;
//    private final PersonService personService;

    public DemandCandidateMeetingRestController(
            DemandCandidateService demandCandidateService,
            DemandCandidateMeetingService demandCandidateMeetingService,
            DemandCandidateMeetingNoteService demandCandidateMeetingNoteService//,
//            PersonService personService
    ) {
        this.demandCandidateService = demandCandidateService;
        this.demandCandidateMeetingService = demandCandidateMeetingService;
        this.demandCandidateMeetingNoteService = demandCandidateMeetingNoteService;
//        this.personService = personService;
    }

    @Transactional
    @PostMapping("/")
    DemandCandidateMeeting addDemandCandidateMeeting(
            @Valid @RequestBody DemandCandidateMeeting demandCandidateMeeting,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("addDemandCandidateMeeting( demandCandidateMeeting: {})", demandCandidateMeeting);

        Person loggedUser = Utils.getPerson(authentication);

        demandCandidateMeeting.setDemandCandidate(demandCandidateService.get(demandCandidateMeeting.getDemandCandidate().getDemandCandidateId()));
        demandCandidateMeeting.setCreatedBy(loggedUser);
        return demandCandidateMeetingService.add(demandCandidateMeeting);
    }

    @Transactional
    @PutMapping("/")
    DemandCandidateMeeting updateDemandCandidateMeeting(
            @Valid @RequestBody DemandCandidateMeeting demandCandidateMeeting,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("updateDemandCandidateMeeting(demandCandidateMeeting: {})", demandCandidateMeeting);

        DemandCandidateMeeting oldDemandCandidateMeeting = demandCandidateMeetingService.get(demandCandidateMeeting.getDemandCandidateMeetingId());
        oldDemandCandidateMeeting.setMeetingDateTime(demandCandidateMeeting.getMeetingDateTime());
        oldDemandCandidateMeeting.setType(demandCandidateMeeting.getType());
        demandCandidateMeeting = oldDemandCandidateMeeting;

        policy.checkPermission(demandCandidateMeeting, Collections.singletonList("edit"));

        Person loggedUser = Utils.getPerson(authentication);

        return demandCandidateMeetingService.update(demandCandidateMeeting);
    }

    @Transactional
    @DeleteMapping("/{demandCandidateMeetingId}")
    ResponseEntity<Void> deleteDemandCandidateMeeting(
            @PathVariable Long demandCandidateMeetingId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("deleteDemandCandidateMeeting( demandCandidateMeetingId: {})", demandCandidateMeetingId);

        if (demandCandidateMeetingService.exists(demandCandidateMeetingId))
            throw new DemandCandidateMeetingNotFoundException(demandCandidateMeetingId);

        DemandCandidateMeeting demandCandidateMeeting = demandCandidateMeetingService.get(demandCandidateMeetingId);

        policy.checkPermission(demandCandidateMeeting, Collections.singletonList("delete"));

        demandCandidateMeetingService.delete(demandCandidateMeetingId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/{demandCandidateMeetingId}")
    DemandCandidateMeeting getDemandCandidateMeeting(@PathVariable Long demandCandidateMeetingId) {
        return demandCandidateMeetingService.get(demandCandidateMeetingId);
    }

    /**
     * Удаление комментария по связке заказ-кандидат-встреча
     *
     * @param demandCandidateMeetingNoteId
     * @param authentication
     * @return
     */
    @Transactional
    @DeleteMapping("/note/{demandCandidateMeetingNoteId}")
    ResponseEntity<Void> deleteDemandCandidateMeetingNote(
            @PathVariable Long demandCandidateMeetingNoteId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("deleteDemandCandidateMeetingNote(demandCandidateMeetingNoteId: {})", demandCandidateMeetingNoteId);

        DemandCandidateMeetingNote demandCandidateMeetingNote = demandCandidateMeetingNoteService.get(demandCandidateMeetingNoteId);
        policy.checkPermission(demandCandidateMeetingNote, Collections.singletonList("delete"));

        demandCandidateMeetingNoteService.delete(demandCandidateMeetingNoteId);
        return ResponseEntity.noContent().build();
    }

    /**
     * Добавляет комментарий по связке заказ-кандидат-встреча
     *
     * @param demandCandidateMeetingNote
     * @param authentication
     * @return
     */
    @Transactional
    @PostMapping("/note")
    DemandCandidateMeetingNote saveDemandCandidateMeetingNote(
            @Valid @RequestBody DemandCandidateMeetingNote demandCandidateMeetingNote,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("saveDemandCandidateMeetingNote( demandCandidateMeetingNote: {})", demandCandidateMeetingNote);

        Person loggedUser = Utils.getPerson(authentication);

        if (demandCandidateMeetingNote.getDemandCandidateMeetingNoteId() != null) {
            String note = demandCandidateMeetingNote.getNote();
            demandCandidateMeetingNote = demandCandidateMeetingNoteService.get(demandCandidateMeetingNote.getDemandCandidateMeetingNoteId());
            demandCandidateMeetingNote.setNote(note);
        } else {
            demandCandidateMeetingNote.setCreatedBy(loggedUser);
        }
        DemandCandidateMeeting demandCandidateMeeting = demandCandidateMeetingService.get(demandCandidateMeetingNote.getDemandCandidateMeeting().getDemandCandidateMeetingId());
        demandCandidateMeetingNote.setDemandCandidateMeeting(demandCandidateMeeting);
        demandCandidateMeetingNote.setCreateDate(LocalDateTime.now());

        log.debug("demandCandidateMeetingNote: {}", demandCandidateMeetingNote);

        return demandCandidateMeetingNoteService.add(demandCandidateMeetingNote);
    }
}