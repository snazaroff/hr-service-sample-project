package com.pkit.controller.rest;

import com.pkit.model.event.Event;
import com.pkit.service.CandidateHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/candidate/{candidateId}/history")
public class CandidateHistoryRestController {

    private final CandidateHistoryService candidateHistoryService;

    public CandidateHistoryRestController(
            CandidateHistoryService candidateHistoryService
    ) {
        this.candidateHistoryService = candidateHistoryService;
    }

    @GetMapping("/")
    public List<Event> getByCompanyId(@PathVariable Long candidateId) {
        log.debug("getByCompanyId(candidateId: {})", candidateId);
        return candidateHistoryService.get(candidateId, (short)0);
    }

    @GetMapping("/{count}")
    public List<Event> getByCompanyIdAndCount(@PathVariable Long candidateId, @PathVariable Short count) {
        log.debug("getByCompanyIdAndCount(candidateId: {}, count: {})", candidateId, count);
        return candidateHistoryService.get(candidateId, count);
    }

    @GetMapping("/last")
    public List<Event> getLastByCompanyId(@PathVariable Long candidateId) {
        log.debug("getLastByCompanyId(candidateId: {})", candidateId);
        return candidateHistoryService.getLast(candidateId, (short)0);
    }

    @GetMapping("/last/{count}")
    public List<Event> getLastByCompanyIdAndCount(@PathVariable Long candidateId, @PathVariable Short count) {
        log.debug("getLastByCompanyIdAndCount(candidateId: {}, count: {})", candidateId, count);
        return candidateHistoryService.getLast(candidateId, count);
    }
}