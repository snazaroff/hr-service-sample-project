package com.pkit.controller.rest;

import com.pkit.Utils;
import com.pkit.exception.*;
import com.pkit.model.Person;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@RestController
@RequestMapping(value = "/person", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class PersonRestController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    private final PersonService personService;

    public PersonRestController(
            PersonService personService
    ) {
        this.personService = personService;
    }

    /**
     * Удаляет сотрудника
     * @param personId
     * @param response
     * @return
     */
    @DeleteMapping("/{personId}")
    ResponseEntity<Void> deletePerson(@PathVariable Long personId, HttpServletResponse response) {
        log.debug("deletePerson( personId: {})", personId);

        response.setCharacterEncoding("UTF-8");
        Person person = personService.get(personId);
        if (person == null) throw new PersonNotFoundException(personId);

        try {
            personService.delete(personId);
        } catch (DataIntegrityViolationException e) {
            throw new DBException(Utils.getDBException(e));
        } catch (Exception e) {
            log.debug("e.getMessage(): {}", e.getMessage());
            log.debug("e.getClass(): {}", e.getClass().getSimpleName());
            throw e;
        }
        return ResponseEntity.noContent().build();
    }
}