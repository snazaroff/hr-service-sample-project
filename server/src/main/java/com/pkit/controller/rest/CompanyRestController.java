package com.pkit.controller.rest;

import com.pkit.exception.CompanyNotFoundException;
import com.pkit.model.Person;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.CompanyService;
import com.pkit.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/company", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
public class CompanyRestController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    private final CompanyService companyService;
    private final PersonService personService;

    public CompanyRestController(
            CompanyService companyService,
            PersonService personService
    ) {
        this.companyService = companyService;
        this.personService = personService;
    }

    /**
     * Возвращает список сотрудников компании
     * @param companyId
     * @return
     */
    @GetMapping(value = "/{companyId}/person")
    List<Person> getPersonByCompanyId(@PathVariable Long companyId) {
        log.debug("getPersonByCompanyId( companyId: {})", companyId);

        if (companyService.exists(companyId)) throw new CompanyNotFoundException(companyId);

        List<Person> persons = personService.getByCompanyId(companyId);
        log.debug("persons: {}", persons);
        return persons;
    }
}