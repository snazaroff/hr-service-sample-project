package com.pkit.controller.rest;

import com.pkit.Utils;
import com.pkit.exception.DemandNotFoundException;
import com.pkit.model.*;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;

@Slf4j
@RestController
@RequestMapping(value = "/demand/{demandId}/candidate/note", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
public class DemandCandidateRestController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    private final DemandCandidateNoteService demandCandidateNoteService;
    private final DemandService demandService;

    public DemandCandidateRestController(
            DemandService demandService,
            DemandCandidateNoteService demandCandidateNoteService
    ) {
        this.demandCandidateNoteService = demandCandidateNoteService;
        this.demandService          = demandService;
    }

    /**
     * Удаление комментария по связке заказ-кандидат
     * @param demandId
     * @param authentication
     * @return
     */
    @Transactional
    @DeleteMapping("/{demandCandidateNoteId}")
    ResponseEntity<Void> deleteDemandCandidateNote(
            @PathVariable Long demandId,
            @PathVariable Long demandCandidateNoteId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("deleteDemandCandidateNote(demandId: {}, demandCandidateNoteId: {})", demandId, demandCandidateNoteId);

        if (demandService.exists(demandId)) throw new DemandNotFoundException(demandId);

        DemandCandidateNote demandCandidateNote = demandCandidateNoteService.get(demandCandidateNoteId);
        policy.checkPermission(demandCandidateNote, Collections.singletonList("delete"));

        demandCandidateNoteService.delete(demandCandidateNoteId);
        return ResponseEntity.noContent().build();
    }

    /**
     * Добавляет комментарий по связке заказ-кандидат
     * @param demandId
     * @param demandCandidateNote
     * @param authentication
     * @return
     */
    @Transactional
    @PostMapping("/")
    DemandCandidateNote saveDemandCandidateNote(
            @PathVariable Long demandId,
            @Valid @RequestBody DemandCandidateNote demandCandidateNote,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("saveDemandCandidateNote( demandId: {}, demandCandidateNote: {})", demandId, demandCandidateNote);

        log.debug("demandCandidateNote.demandCandidate: {}", demandCandidateNote.demandCandidate);

        if (demandService.exists(demandId)) throw new DemandNotFoundException(demandId);

        Person loggedUser = Utils.getPerson(authentication);

        if (demandCandidateNote.getDemandCandidateNoteId() == null) {
            demandCandidateNote.setCreatedBy(loggedUser);
        }
//        DemandCandidate demandCandidate = demandCandidateService.get(demandCandidateNote.demandCandidate.getDemandCandidateId());
//        demandCandidateNote.setDemandCandidate(demandCandidate);

        log.debug("demandCandidateNote: {}", demandCandidateNote);
        log.debug("demandCandidateNote.demandCandidate: {}", demandCandidateNote.demandCandidate);

        return demandCandidateNoteService.add(demandCandidateNote);
    }
}