package com.pkit.controller.rest;

import com.pkit.Utils;
import com.pkit.exception.*;
import com.pkit.model.Department;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.*;

@Slf4j
@RestController
@RequestMapping(value = "/department", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class DepartmentRestController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    private final DepartmentService departmentService;

    public DepartmentRestController(
            DepartmentService departmentService
    ) {
        this.departmentService = departmentService;
    }

    /**
     * Удаляет департамент
     *
     * @param departmentId
     * @return
     */
    @Transactional
    @DeleteMapping("/{departmentId}")
    ResponseEntity<Void> deleteDepartment(
            @PathVariable Long departmentId
    ) {
        log.debug("deleteDepartment( departmentId: {})", departmentId);

        Department department = departmentService.get(departmentId);
        if (department == null) throw new DepartmentNotFoundException(departmentId);

        policy.checkPermission(department, Collections.singletonList("delete"));

        try {
            departmentService.delete(departmentId);
        } catch (DataIntegrityViolationException e) {
            throw new DBException(Utils.getDBException(e));
        }
        return ResponseEntity.noContent().build();
    }
}