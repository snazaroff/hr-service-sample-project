package com.pkit.controller.rest;

import com.pkit.Utils;
import com.pkit.exception.*;
import com.pkit.model.*;
import com.pkit.repository.PickedDemandRepository;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import javax.validation.Valid;
import java.util.*;

@Slf4j
@RestController
@RequestMapping(value = "/demand", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
public class DemandRestController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    private final CandidateService candidateService;
    private final DemandCandidateService demandCandidateService;
    private final DemandNoteService demandNoteService;
    private final DemandService demandService;
    private final PickedDemandRepository pickedDemandRepository;

    public DemandRestController(
            CandidateService candidateService,
            DemandService demandService,
            DemandNoteService demandNoteService,
            DemandCandidateService demandCandidateService,
            PickedDemandRepository pickedDemandRepository
    ) {
        this.candidateService = candidateService;
        this.demandCandidateService = demandCandidateService;
        this.demandNoteService = demandNoteService;
        this.demandService = demandService;
        this.pickedDemandRepository = pickedDemandRepository;
    }

    @GetMapping(value = "/search")
    List<Demand> search(@RequestParam String term) {
        log.debug("search( term: {})", term);
        return demandService.search(term);
    }

    /**
     * Удаление требования
     *
     * @param demandId
     * @param authentication
     * @return
     */
    @Transactional
    @DeleteMapping("/{demandId}")
    ResponseEntity<Void> deleteDemand(
            @PathVariable Long demandId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("deleteDemand(demandId: {})", demandId);

        if (demandService.exists(demandId)) throw new DemandNotFoundException(demandId);

        Demand demand = demandService.get(demandId);
        policy.checkPermission(demand, Collections.singletonList("delete"));

        pickedDemandRepository.delete(demand.getPickeds());
        demandService.delete(demandId);
        return ResponseEntity.noContent().build();
    }

    //Работа с кандидатом в заказе

    /**
     * Возвращает список кандидатов по заказу
     *
     * @param demandId
     * @return
     */
    @GetMapping(value = "/{demandId}/candidate")
    List<DemandCandidate> getCandidates(@PathVariable Long demandId) {
        log.debug("getCandidates( demandId: {})", demandId);

        if (demandService.exists(demandId)) throw new DemandNotFoundException(demandId);
        return demandCandidateService.getByDemandId(demandId);
    }

    /**
     * Добавляет кандидата к заказу.
     *
     * @param demandId
     * @param demandCandidate
     * @param authentication
     * @return
     */
    @Transactional
    @PostMapping("/{demandId}/candidate")
    DemandCandidate addDemandCandidate(
            @PathVariable Long demandId,
            @Valid @RequestBody DemandCandidate demandCandidate,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("addDemandCandidate( demandId: {}, demandCandidate: {})", demandId, demandCandidate);

        if (demandService.exists(demandId)) throw new DemandNotFoundException(demandId);
        Long candidateId = demandCandidate.getCandidate().getCandidateId();
        Candidate candidate = candidateService.get(candidateId);
        if (candidate == null) throw new CandidateNotFoundException(candidateId);

        Demand demand = demandService.get(demandId);
        if (demand == null) throw new DemandNotFoundException(demandId);

        Person loggedUser = Utils.getPerson(authentication);

        demandCandidate.setCreatedBy(loggedUser);
        demandCandidate.setCandidate(candidate);
        demandCandidate.setDemand(demand);
        return demandCandidateService.add(demandCandidate);
    }

    /**
     * Добавляет кандидата к заказу
     *
     * @param candidateId
     * @param demandId
     * @param demandCandidate
     * @param authentication
     * @return
     */
    @Transactional
    @PostMapping("/{demandId}/candidate/{candidateId}")
    DemandCandidate addDemandCandidate(
            @PathVariable Long candidateId,
            @PathVariable Long demandId,
            @Valid @RequestBody DemandCandidate demandCandidate,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("addDemandCandidate(candidateId: {}, demandId: {}, demandCandidate: {})", candidateId, demandId, demandCandidate);

        Demand demand = demandService.get(demandId);
        if (demandService.exists(demandId)) throw new DemandNotFoundException(demandId);
        demandCandidate.setDemand(demand);
        Candidate candidate = candidateService.get(candidateId);
        if (candidate == null) throw new CandidateNotFoundException(candidateId);
        demandCandidate.setCandidate(candidate);
        return demandCandidateService.add(demandCandidate);
    }

    @Transactional
    @PutMapping("/{demandId}/candidate/{candidateId}")
    DemandCandidate updateDemandCandidate(
            @PathVariable Long candidateId,
            @PathVariable Long demandId,
            @Valid @RequestBody DemandCandidate demandCandidate,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("updateDemandCandidate(candidateId: {}, demandId: {}, demandCandidate: {})", candidateId, demandId, demandCandidate);

        Demand demand = demandService.get(demandId);
        if (demandService.exists(demandId)) throw new DemandNotFoundException(demandId);
        demandCandidate.setDemand(demand);
        Candidate candidate = candidateService.get(candidateId);
        if (candidate == null) throw new CandidateNotFoundException(candidateId);
        demandCandidate.setCandidate(candidate);
        return demandCandidateService.update(demandCandidate);
    }

    /**
     * Убирает кандидата из заказа
     *
     * @param demandId
     * @param demandCandidateId
     * @param authentication
     * @return
     */
    @Transactional
    @DeleteMapping("/{demandId}/candidate/{demandCandidateId}")
    ResponseEntity<Void> removeDemandCandidate(
            @PathVariable Long demandId,
            @PathVariable Long demandCandidateId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("removeDemandCandidate(demandId: {}, demandCandidateId: {})", demandId, demandCandidateId);

        if (demandService.exists(demandId)) throw new DemandNotFoundException(demandId);

        DemandCandidate demandCandidate = demandCandidateService.get(demandCandidateId);

//        policy.checkPermission(demandCandidate, Collections.singletonList("delete"));

        demandCandidateService.delete(demandCandidate);
        return ResponseEntity.noContent().build();
    }

    @Transactional
    @DeleteMapping("/{demandId}/note/{demandNoteId}")
    ResponseEntity<Void> removeDemandNote(
            @PathVariable Long demandId,
            @PathVariable Long demandNoteId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("removeDemandNote(demandId: {}, demandNoteId: {})", demandId, demandNoteId);

        if (demandService.exists(demandId)) throw new DemandNotFoundException(demandId);
        if (demandNoteService.exists(demandNoteId)) throw new DemandNoteNotFoundException(demandNoteId);
        DemandNote demandNote = demandNoteService.get(demandNoteId);

        try {
            policy.checkPermission(demandNote, Collections.singletonList("delete"));
        } catch (AccessDeniedException e) {
            throw new AccessDeniedException("Нет прав на удаление этого комментария");
        }

        demandNoteService.delete(demandNoteId);
        return ResponseEntity.noContent().build();
    }

    @Transactional
    @PostMapping("/{demandId}/note/")
    DemandNote addDemandNote(
            @PathVariable Long demandId,
            @Valid @RequestBody DemandNote demandNote,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("addDemandNote( demandId: {}, demandNote: {})", demandId, demandNote);

        if (demandService.exists(demandId)) throw new DemandNoteNotFoundException(demandId);

        Person loggedUser = Utils.getPerson(authentication);

        if (demandNote.getDemandNoteId() == null) {
            demandNote.setCreatedBy(loggedUser);
        }
        return demandNoteService.add(demandNote);
    }

//    @Transactional
//    @PutMapping("/{demandId}/note/")
//    DemandNote updateDemandNote(
//            @PathVariable Long demandId,
//            @Valid @RequestBody DemandNote demandNote,
//            UsernamePasswordAuthenticationToken authentication
//    ) {
//        log.debug("updateDemandNote( demandId: {}, demandNote: {})", demandId, demandNote);
//
//        if (demandService.exists(demandId)) throw new DemandNoteNotFoundException(demandId);
//        if (demandNoteService.exists(demandNote.getDemandNoteId())) throw new DemandNoteNotFoundException(demandNote.getDemandNoteId());
//
//        DemandNote newDemandNote = demandNoteService.get(demandNote.getDemandNoteId());
//
//        policy.checkPermission(newDemandNote, Collections.singletonList("edit"));
//
//        newDemandNote.setNote(demandNote.getNote());
//        return demandNoteService.add(newDemandNote);
//    }

    /**
     * Возвращает список заметок по заказу
     *
     * @param demandId
     * @return
     */
    @GetMapping(value = "/{demandId}/note")
    List<DemandNote> getDemandNoteByDemandId(@PathVariable Long demandId) {
        log.debug("getDemandNoteByDemandId( demandId: {})", demandId);

        if (demandService.exists(demandId)) throw new DemandNoteNotFoundException(demandId);
        return demandNoteService.getByDemandId(demandId);
    }
}