package com.pkit.controller.rest;

import com.pkit.exception.EmploymentAgencyNotFoundException;
import com.pkit.model.Company;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.CompanyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

@Slf4j
//@RestController
//@RequestMapping("/employment_agency")
public class EmploymentAgencyRestController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    private final CompanyService companyService;

    public EmploymentAgencyRestController(
            CompanyService companyService
    ) {
        this.companyService = companyService;
    }

    /**
     * Удаляем кадровое агентство
     * @param employmentAgencyId
     * @return
     */
    @DeleteMapping("/{employmentAgencyId}")
    ResponseEntity<Void> deleteEmploymentAgency(@PathVariable Long employmentAgencyId) {
        if (companyService.exists(employmentAgencyId)) throw new EmploymentAgencyNotFoundException(employmentAgencyId);
        Company employmentAgency = companyService.get(employmentAgencyId);
        if(employmentAgency == null) throw new EmploymentAgencyNotFoundException(employmentAgencyId);
        companyService.delete(employmentAgencyId);
        return ResponseEntity.noContent().build();
    }
}