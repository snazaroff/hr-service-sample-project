package com.pkit.controller.rest;

import com.pkit.Utils;
import com.pkit.model.PickedCandidate;
import com.pkit.model.PickedCompany;
import com.pkit.model.PickedDemand;
import com.pkit.repository.PickedCandidateRepository;
import com.pkit.repository.PickedCompanyRepository;
import com.pkit.repository.PickedDemandRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/picked", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class PickedRestController {

    private final PickedCandidateRepository pickedCandidateRepository;
    private final PickedCompanyRepository pickedCustomerRepository;
    private final PickedDemandRepository pickedDemandRepository;

    public PickedRestController(
            PickedCandidateRepository pickedCandidateRepository,
            PickedCompanyRepository pickedCustomerRepository,
            PickedDemandRepository pickedDemandRepository
    ) {
        this.pickedCandidateRepository = pickedCandidateRepository;
        this.pickedCustomerRepository = pickedCustomerRepository;
        this.pickedDemandRepository = pickedDemandRepository;
    }

    @RequestMapping(value = "/person/{personId}/candidate/{candidateId}", method = RequestMethod.POST)
    PickedCandidate addPickedCandidate(
            @PathVariable Long personId,
            @PathVariable Long candidateId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("addPickedCandidate( personId: {}, candidateId: {})", personId, candidateId);

        PickedCandidate pickedCandidate = new PickedCandidate(personId, candidateId);
        log.debug("pickedCandidate: {}", pickedCandidate);
        pickedCandidate = pickedCandidateRepository.save(pickedCandidate);
        return pickedCandidate;
    }

    @RequestMapping(value = "/candidate/{candidateId}", method = RequestMethod.POST)
    PickedCandidate addPickedCandidate(
            @PathVariable Long candidateId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("addPickedCandidate( candidateId: {})", candidateId);

        Long personId = Utils.getPersonId(authentication);

        PickedCandidate pickedCandidate = new PickedCandidate(personId, candidateId);
        log.debug("pickedCandidate: {}", pickedCandidate);
        pickedCandidate = pickedCandidateRepository.save(pickedCandidate);
        return pickedCandidate;
    }

    @DeleteMapping("/person/{personId}/candidate/{candidateId}")
    void removePickedCandidate(
            @PathVariable Long personId,
            @PathVariable Long candidateId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("removePickedCandidate( personId: {}, candidateId: {})", personId, candidateId);

        PickedCandidate pickedCandidate = new PickedCandidate(personId, candidateId);
        pickedCandidateRepository.delete(pickedCandidate);
    }

    @DeleteMapping("/candidate/{candidateId}")
    void removePickedCandidate(
            @PathVariable Long candidateId,
            UsernamePasswordAuthenticationToken authentication
    )  {
        log.debug("removePickedCandidate( candidateId: {})", candidateId);

        Long personId = Utils.getPersonId(authentication);

        PickedCandidate pickedCandidate = new PickedCandidate(personId, candidateId);
        pickedCandidateRepository.delete(pickedCandidate);
    }

    @RequestMapping(value = "/person/{personId}/customer/{customerId}", method = RequestMethod.POST)
    PickedCompany addPickedCustomer(
            @PathVariable Long personId,
            @PathVariable Long customerId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("addPickedCustomer( personId: {}, customerId: {})", personId, customerId);

        PickedCompany pickedCustomer = new PickedCompany(personId, customerId);
        pickedCustomer = pickedCustomerRepository.save(pickedCustomer);
        return pickedCustomer;
    }

    @RequestMapping(value = "/customer/{customerId}", method = RequestMethod.POST)
    PickedCompany addPickedCustomer(
            @PathVariable Long customerId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("addPickedCustomer( customerId: {})", customerId);

        Long personId = Utils.getPersonId(authentication);

        PickedCompany pickedCustomer = new PickedCompany(personId, customerId);
        pickedCustomer = pickedCustomerRepository.save(pickedCustomer);
        return pickedCustomer;
    }

    @DeleteMapping("/person/{personId}/customer/{customerId}")
    void removePickedCustomer(
            @PathVariable Long personId,
            @PathVariable Long customerId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("removePickedCustomer( personId: {}, customerId: {})", personId, customerId);

        PickedCompany pickedCustomer = new PickedCompany(personId, customerId);
        pickedCustomerRepository.delete(pickedCustomer);
    }

    @DeleteMapping("/customer/{customerId}")
    void removePickedCustomer(
            @PathVariable Long customerId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("removePickedCustomer( customerId: {})", customerId);

        Long personId = Utils.getPersonId(authentication);

        PickedCompany pickedCustomer = new PickedCompany(personId, customerId);
        pickedCustomerRepository.delete(pickedCustomer);
    }

    @RequestMapping(value = "/person/{personId}/demand/{demandId}", method = RequestMethod.POST)
    PickedDemand addPickedDemand(
            @PathVariable Long personId,
            @PathVariable Long demandId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("addPickedDemand( personId: {}, demandId: {})", personId, demandId);

        PickedDemand pickedDemand = new PickedDemand(personId, demandId);
        pickedDemand = pickedDemandRepository.save(pickedDemand);
        return pickedDemand;
    }

    @RequestMapping(value = "/demand/{demandId}", method = RequestMethod.POST)
    PickedDemand addPickedDemand(
            @PathVariable Long demandId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("addPickedDemand( demandId: {})", demandId);

        Long personId = Utils.getPersonId(authentication);

        PickedDemand pickedDemand = new PickedDemand(personId, demandId);
        pickedDemand = pickedDemandRepository.save(pickedDemand);
        return pickedDemand;
    }

    @DeleteMapping("/person/{personId}/demand/{demandId}")
    void removePickedDemand(
            @PathVariable Long personId,
            @PathVariable Long demandId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("removePickedDemand( personId: {}, demandId: {})", personId, demandId);

        PickedDemand pickedDemand = new PickedDemand(personId, demandId);
        pickedDemandRepository.delete(pickedDemand);
    }

    @DeleteMapping("/demand/{demandId}")
    void removePickedDemand(
            @PathVariable Long demandId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("removePickedDemand( demandId: {})", demandId);

        Long personId = Utils.getPersonId(authentication);

        PickedDemand pickedDemand = new PickedDemand(personId, demandId);
        log.debug("pickedDemand: {}", pickedDemand);
        pickedDemandRepository.delete(pickedDemand);
    }
}