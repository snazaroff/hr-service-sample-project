package com.pkit.controller.rest;

import com.pkit.Utils;
import com.pkit.data.Languages;
import com.pkit.exception.*;
import com.pkit.model.*;
import com.pkit.repository.*;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import javax.validation.Valid;
import java.util.*;

@Slf4j
@RestController
@RequestMapping(value = "/candidate", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
public class CandidateRestController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    @Autowired
    private Languages languages;

    private final CandidateService candidateService;
    private final EmploymentService employmentService;
    private final EducationService educationService;

    private final CandidateLanguageRepository candidateLanguageRepository;
    private final PickedCandidateRepository pickedCandidateRepository;

    public CandidateRestController(
            CandidateService candidateService,
            CandidateLanguageRepository candidateLanguageRepository,
            EmploymentService employmentService,
            EducationService educationService,
            PickedCandidateRepository pickedCandidateRepository
    ) {
        this.candidateService = candidateService;
        this.candidateLanguageRepository = candidateLanguageRepository;
        this.employmentService = employmentService;
        this.educationService = educationService;
        this.pickedCandidateRepository = pickedCandidateRepository;
    }

    @GetMapping(value = "/search")
    List<Candidate> search(@RequestParam String term) {
        log.debug("search( term: {})", term);
        return candidateService.search(term);
    }

    /**
     * Удаляет кандидата
     *
     * @param candidateId
     * @param authentication
     * @return
     */
    @Transactional
    @DeleteMapping("/{candidateId}")
    ResponseEntity<Void> deleteCandidate(
            @PathVariable Long candidateId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("deleteCandidate( candidateId: {})", candidateId);

        if (candidateService.exists(candidateId)) throw new CandidateNotFoundException(candidateId);

        Long loggedPersonId = Utils.getPersonId(authentication);

        Candidate candidate = candidateService.get(candidateId);

        policy.checkPermission(candidate, Collections.singletonList("delete"));

        //todo: пересмотреть удаление(каскадное?)
        pickedCandidateRepository.delete(candidate.getPickeds());
        candidateService.delete(candidateId);
        return ResponseEntity.noContent().build();
    }

    /**
     * Удаляет у кандидата область деятельности
     *
     * @param candidateId
     * @param categoryId
     * @param authentication
     * @return
     */
    @DeleteMapping("/{candidateId}/category/{categoryId}")
    List<Category> deleteCandidateCategory(
            @PathVariable Long candidateId,
            @PathVariable Long categoryId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("deleteCandidateCategory( candidateId: {}; categoryId: {})", candidateId, categoryId);

        if (candidateService.exists(candidateId)) throw new CandidateNotFoundException(candidateId);

        Long loggedPersonId = Utils.getPersonId(authentication);

        return candidateService.deleteCategory(candidateId, categoryId);
    }

    /**
     * Добавляет область деятельности кандидату
     *
     * @param candidateId
     * @param categoryId
     * @return
     */
    @Transactional
    @PostMapping("/{candidateId}/category/{categoryId}")
    Category addCandidateCategory(
            @PathVariable Long candidateId,
            @PathVariable Long categoryId
    ) {
        log.debug("addCandidateCategory( candidateId: {}; categoryId: {})", candidateId, categoryId);

        if (candidateService.exists(candidateId)) throw new CandidateNotFoundException(candidateId);

        return candidateService.addCategory(candidateId, categoryId);
    }

    /**
     * Добавляет области деятельности кандидата одним пакетом
     *
     * @param candidateId
     * @param categoryIds
     * @return
     */
    @Transactional
    @PostMapping("/{candidateId}/category/batch")
    List<Category> addBatchCandidateCategory(
            @PathVariable Long candidateId,
            @RequestBody List<Long> categoryIds) {
        log.debug("deleteCandidateCategory( candidateId: {}; categoryIds: {})", candidateId, categoryIds);

        if (candidateService.exists(candidateId)) throw new CandidateNotFoundException(candidateId);

        return candidateService.addBatchCategory(candidateId, categoryIds);
    }

    /**
     * Возвращает список областей деятельности, в которых кандидат ищет работу
     *
     * @param candidateId
     * @return
     */
    @GetMapping(value = "/{candidateId}/category")
    List<Category> getCandidateCategory(@PathVariable Long candidateId) {
        log.debug("getCandidateCategory( candidateId: {})", candidateId);

        if (candidateService.exists(candidateId)) throw new CandidateNotFoundException(candidateId);

        return candidateService.getCategoryListWithTopCategory(candidateId);
    }

    /**
     * Добавляет иностранный язык кандидату
     *
     * @param candidateId -  кандидат
     * @param languageId  - язык
     * @param statusId    - степень знания языка
     * @return
     */
    @Transactional
    @RequestMapping(value = "/{candidateId}/language/{languageId}/status/{statusId}", method = RequestMethod.POST)
    CandidateLanguage addCandidateLanguage(
            @PathVariable Long candidateId,
            @PathVariable Long languageId,
            @PathVariable Short statusId) {
        log.debug("addCandidateLanguage( candidateId: {}; languageId: {}; statusId: {})", candidateId, languageId, statusId);

        Candidate candidate = candidateService.get(candidateId);
        if (candidate == null) throw new CandidateNotFoundException(candidateId);

        CandidateLanguage candidateLanguage = new CandidateLanguage(candidate, languages.findById(languageId));
        candidateLanguage.setStatus(LanguageStatus.getById(statusId));
        log.debug("candidateLanguage: {}", candidateLanguage);
        return candidateLanguageRepository.save(candidateLanguage);
    }

    /**
     * Удаляет иностранный язык кандидата
     *
     * @param candidateId
     * @param languageId
     */
    @Transactional
    @DeleteMapping("/{candidateId}/language/{languageId}")
    ResponseEntity<Void> deleteCandidateLanguage(
            @PathVariable Long candidateId,
            @PathVariable Long languageId) {
        log.debug("deleteCandidateLanguage( candidateId: {}; languageId: {})", candidateId, languageId);

        if (candidateService.exists(candidateId)) throw new CandidateNotFoundException(candidateId);

        CandidateLanguagePK candidateLanguagePK = new CandidateLanguagePK(candidateId, languageId);
        candidateLanguageRepository.delete(candidateLanguagePK);
        return ResponseEntity.noContent().build();
    }

    /**
     * Добавляет трудовой опыт кандидату
     *
     * @param candidateId
     * @param employment
     * @param authentication
     * @return
     */
    @Transactional
    @RequestMapping(value = "/{candidateId}/employment", method = RequestMethod.POST)
    Employment addCandidateEmployment(
            @PathVariable Long candidateId,
            @Valid @RequestBody Employment employment,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("addCandidateEmployment( candidateId: {}, employment: {})", candidateId, employment);

        Candidate candidate = candidateService.get(candidateId);
        if (candidate == null) throw new CandidateNotFoundException(candidateId);

        Person loggedUser = Utils.getPerson(authentication);

        employment.setCreatedBy(loggedUser);
        employment.setCandidate(candidate);
        return employmentService.add(employment);
    }

    /**
     * Редактирует трудовой опыт кандидата
     *
     * @param candidateId
     * @param employment
     * @return
     */
    @Transactional
    @RequestMapping(value = "/{candidateId}/employment", method = RequestMethod.PUT)
    Employment editCandidateEmployment(
            @PathVariable Long candidateId,
            @Valid @RequestBody Employment employment) {
        log.debug("editCandidateEmployment( candidateId: {}, employment: {})", candidateId, employment);

        Candidate candidate = candidateService.get(candidateId);
        if (candidate == null) throw new CandidateNotFoundException(candidateId);

        if (employmentService.exists(employment.getEmploymentId()))
            throw new EmploymentNotFoundException(employment.getEmploymentId());

        employment.setCandidate(candidate);
        return employmentService.add(employment);
    }

    /**
     * Удаляет трудовой опыт у кандидата
     *
     * @param candidateId
     * @param employmentId
     */
    @Transactional
    @DeleteMapping("/{candidateId}/employment/{employmentId}")
    ResponseEntity<Void> deleteCandidateEmployment(
            @PathVariable Long candidateId,
            @PathVariable Long employmentId) {
        log.debug("deleteCandidateEmployment( candidateId: {}, employmentId: {})", candidateId, employmentId);

        if (candidateService.exists(candidateId)) throw new CandidateNotFoundException(candidateId);
        if (employmentService.exists(employmentId)) throw new EmploymentNotFoundException(employmentId);

        employmentService.delete(employmentId);
        return ResponseEntity.noContent().build();
    }

    /**
     * Добавляет кандидату образование
     *
     * @param candidateId
     * @param education
     * @param authentication
     * @return
     */
    @Transactional
    @PostMapping(value = "/{candidateId}/education")
    Education addCandidateEducation(
            @PathVariable Long candidateId,
            @Valid @RequestBody Education education,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("addCandidateEducation( candidateId: {}, education: {})", candidateId, education);

        Candidate candidate = candidateService.get(candidateId);
        if (candidate == null) throw new CandidateNotFoundException(candidateId);

        Person loggedUser = Utils.getPerson(authentication);

        education.setCreatedBy(loggedUser);
        education.setCandidate(candidate);
        return educationService.add(education);
    }

    /**
     * Редактирут образование кандидата
     *
     * @param candidateId
     * @param education
     * @return
     */
    @Transactional
    @RequestMapping(value = "/{candidateId}/education", method = RequestMethod.PUT)
    Education editCandidateEducation(
            @PathVariable Long candidateId,
            @Valid @RequestBody Education education) {
        log.debug("editCandidateEducation( candidateId: {}, education: {})", candidateId, education);

        Candidate candidate = candidateService.get(candidateId);
        if (candidate == null) throw new CandidateNotFoundException(candidateId);

        if (educationService.exists(education.getEducationId()))
            throw new EducationNotFoundException(education.getEducationId());

        education.setCandidate(candidate);
        return educationService.add(education);
    }

    /**
     * Удаляет образование у кандидата
     *
     * @param candidateId
     * @param educationId
     */
    @Transactional
    @DeleteMapping("/{candidateId}/education/{educationId}")
    ResponseEntity<Void> deleteCandidateEducation(
            @PathVariable Long candidateId,
            @PathVariable Long educationId) {
        log.debug("deleteCandidateEducation( candidateId: {}, educationId: {})", candidateId, educationId);

        if (candidateService.exists(candidateId)) throw new CandidateNotFoundException(candidateId);
        if (educationService.exists(educationId)) throw new EducationNotFoundException(educationId);

        educationService.delete(educationId);
        return ResponseEntity.noContent().build();
    }
}