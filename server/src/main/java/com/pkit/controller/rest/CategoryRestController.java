package com.pkit.controller.rest;

import com.pkit.model.*;
import com.pkit.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/category", produces = {"application/json"})
public class CategoryRestController {

    private final CategoryService categoryService;

    public CategoryRestController(
            CategoryService categoryService
    ) {
        this.categoryService = categoryService;
    }

    @GetMapping(value = "/")
    public List<Category> getAllCategory() {
        return categoryService.getTopCategory();
    }

    @GetMapping(value = "/{categoryId}")
    public Category getCategory(@PathVariable Long categoryId) {
        log.debug("getCategory({})", categoryId);
        return categoryService.getCategory(categoryId);
    }

    @GetMapping(value = "/{categoryId}/item")
    public List<Category> getCategoryItem(@PathVariable Long categoryId) {
        return categoryService.getSubCategory(categoryId);
    }
}