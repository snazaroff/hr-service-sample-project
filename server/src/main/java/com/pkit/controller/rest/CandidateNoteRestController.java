package com.pkit.controller.rest;

import com.pkit.Utils;
import com.pkit.exception.*;
import com.pkit.model.CandidateNote;
import com.pkit.model.Person;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.CandidateNoteService;
import com.pkit.service.CandidateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/candidate")
public class CandidateNoteRestController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    private final CandidateService candidateService;
    private final CandidateNoteService candidateNoteService;

    public CandidateNoteRestController(
            CandidateService candidateService,
            CandidateNoteService candidateNoteService
    ) {
        this.candidateService = candidateService;
        this.candidateNoteService = candidateNoteService;
    }

    /**
     * Добавляет заметку по кандидату
     *
     * @param candidateId
     * @param candidateNote
     * @param authentication
     * @return
     */
    @Transactional
    @PostMapping("/{candidateId}/note/")
    CandidateNote addCandidateNote(
            @PathVariable Long candidateId,
            @Valid @RequestBody CandidateNote candidateNote,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("addCandidateNote( candidateId: {}, candidateNote: {})", candidateId, candidateNote);

        Person loggedUser = Utils.getPerson(authentication);

        if (candidateNote.getCandidateNoteId() == null) {
            candidateNote.setCreatedBy(loggedUser);
        }
        log.debug("candidateNote: {}", candidateNote);
        return candidateNoteService.add(candidateNote);
    }

    /**
     * Удаление заметки по кандидату
     *
     * @param candidateId
     * @param candidateNoteId
     * @param authentication
     * @return
     */
    @Transactional
    @DeleteMapping("/{candidateId}/note/{candidateNoteId}")
    ResponseEntity<Void> deleteCandidateNote(
            @PathVariable Long candidateId,
            @PathVariable Long candidateNoteId,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("deleteCandidateNote( candidateId: {}, candidateNoteId: {})", candidateId, candidateNoteId);

        if (candidateService.exists(candidateId)) throw new CandidateNotFoundException(candidateId);
        if (candidateNoteService.exists(candidateNoteId)) throw new CandidateNoteNotFoundException(candidateNoteId);
        candidateNoteService.delete(candidateNoteId);
        return ResponseEntity.noContent().build();
    }
}