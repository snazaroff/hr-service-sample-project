package com.pkit.controller.rest;

import com.pkit.Utils;
import com.pkit.exception.*;
import com.pkit.model.Company;
import com.pkit.repository.PickedCompanyRepository;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.CompanyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

@Slf4j
@RestController
@RequestMapping(value = "/customer", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class CustomerRestController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    private final CompanyService customerService;
    private final PickedCompanyRepository pickedCompanyRepository;

    public CustomerRestController(
            CompanyService customerService,
            PickedCompanyRepository pickedCompanyRepository
    ) {
        this.customerService = customerService;
        this.pickedCompanyRepository = pickedCompanyRepository;
    }

    @Transactional
    @DeleteMapping("/{customerId}")
    ResponseEntity<Void> deleteCustomer(
            @PathVariable Long customerId,
            UsernamePasswordAuthenticationToken authentication
    ) throws DBException {
        log.debug("deleteCustomer( customerId: {})", customerId);

        if (customerService.exists(customerId)) throw new CustomerNotFoundException(customerId);
	      Company customer = customerService.get(customerId);
        pickedCompanyRepository.delete(customer.getPickeds());
	      try {
            customerService.delete(customerId);
//        } catch (ConstraintViolationException e) {
//            log.error("ConstraintViolationException");
//            throw new DBException(Utils.getDBException(e));
        } catch (DataIntegrityViolationException e) {
            log.error("DataIntegrityViolationException");
            throw new DBException(Utils.getDBException(e));




//            log.error(e.fillInStackTrace().getClass().getName());
//
//            JdbcSQLException ex = (JdbcSQLException)e.getMostSpecificCause();
//            log.error("ex: {}", ex);
//            log.error("ex.getMessage(): {}", ex.getMessage());
//            log.error("ex.getOriginalMessage(): {}", ex.getOriginalMessage());
//            log.error("ex.getSuppressed(): {}", ex.getSuppressed());
//            log.error("ex.getErrorCode(): {}", ex.getErrorCode());
//            log.error("ex.getOriginalCause(): {}", ex.getOriginalCause());
//            log.error("ex.getSQLState(): {}", ex.getSQLState());
//            log.error("ex.getSQL(): {}", ex.getSQL());
//
//            log.error(e.getMostSpecificCause().getClass().getName());
//            log.error(e.getRootCause().getClass().getName());
//            log.error(e.getMostSpecificCause().getClass().getName());
//
//            Throwable[] throwables = e.getSuppressed();
//            log.error("throwables.length: {}", throwables.length);
//            for(Throwable t: throwables) {
//                log.error("throwable: {}", t);
//            }
//            log.error("1: " + e.getClass().getName());
//            log.error("2: " + e.getLocalizedMessage());
//            log.error("3: " + e.getMostSpecificCause().getLocalizedMessage());
//            log.error("4: " + e.getRootCause().getLocalizedMessage());
//            log.error("5: " + e.getCause().getLocalizedMessage());
//            log.error("6: " + e.getCause().getLocalizedMessage());
        } catch (Exception e) {
	        log.error(e.getMessage());
            log.error(e.getClass().getName());
        }
        return ResponseEntity.noContent().build();
    }
}

