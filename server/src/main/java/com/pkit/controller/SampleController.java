package com.pkit.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SampleController {

    @GetMapping("/api/hello")
    public String hello() {
        return "Hello From Spring Boot!";
    }

    @GetMapping("/test")
    public String test() {
        return "test";
    }

}
