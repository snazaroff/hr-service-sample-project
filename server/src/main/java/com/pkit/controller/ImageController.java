package com.pkit.controller;

import com.pkit.model.Photo;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.PhotoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Slf4j
@Controller
@RequestMapping(value = "/photo", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ImageController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    private final PhotoService photoService;

    public ImageController(
            PhotoService photoService
    ) {
        this.photoService = photoService;
    }

    @RequestMapping(value = "/candidate/{candidateId}", method = RequestMethod.POST)
    String uploadImage(@RequestParam("file") MultipartFile file, @PathVariable Long candidateId) throws Exception {
        log.debug("uploadImage: {}", file.getContentType());

        if (!file.isEmpty()) {
            byte[] bytes = file.getBytes();
            Photo photo = new Photo();
            photo.setCandidateId(candidateId);
            photo.setMain(photoService.getByCandidate(candidateId).size() == 0);
            photoService.add(photo, file.getOriginalFilename(), bytes);
        } else {
            throw new RuntimeException("Empty file!");
        }
//        return "/candidate/" + candidateId;
        return "redirect:/candidate/" + candidateId + "/#gallery";
    }

//    /**
//     * Возвращает список картинок для кандидата
//     * @param candidateId
//     * @return
//     */
//    @GetMapping(value = "/candidate/{candidateId}")
//    List<Photo> getByCandidate(@PathVariable Long candidateId) {
//        log.debug("getByCandidate(candidateId: {})", candidateId);
//        return photoService.getByCandidate(candidateId);
//    }
//
//    /**
//     * Возвращает информацию по картинке
//     * @param imageId
//     * @return
//     */
//    @GetMapping(value = "/{imageId}")
//    Photo get(@PathVariable Long imageId) {
//        log.debug("get(imageId: {})", imageId);
//        return photoService.get(imageId);
//    }
//
//    /**
//     * Возвращает байты картинки
//     * @param imageId
//     * @param response
//     * @throws IOException
//     */
//    @GetMapping("/{imageId}/content")
//    void getContent(@PathVariable Long imageId, HttpServletResponse response) throws IOException {
//        log.debug("getContent(imageId: {})", imageId);
//        Photo photo = photoService.get(imageId);
//        if (photo == null)
//            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
//
//        byte[] image = photoService.getPhotoContent(imageId);
//        log.debug("image: {}", image.length);
//
//        response.setHeader("Content-Disposition", "filename=photo.jpg");
//        response.setContentType("image/jpeg");
//        response.setStatus(HttpStatus.OK.value());
//
//        response.getOutputStream().write(image);
//        response.getOutputStream().flush();
//    }
//
//    /**
//     * Сделать картинку основной (влияет на отображение)
//     * @param imageId
//     * @return
//     */
//    @RequestMapping(value = "/{imageId}", method = RequestMethod.POST)
//    Photo setMain(@PathVariable Long imageId) {
//        log.debug("setMain(imageId: {})", imageId);
//        return photoService.setMain(imageId);
//    }
//
//    /**
//     * Удаляет картинку
//     * @param imageId
//     * @return
//     */
//    @DeleteMapping("/{imageId}")
//    ResponseEntity<Void> delete(@PathVariable Long imageId) {
//        log.debug("delete(imageId: {})", imageId);
//        photoService.delete(imageId);
//        return ResponseEntity.noContent().build();
//    }
//
//    /**
//     * Удаляет все картинки у кандидата
//     * @param candidateId
//     * @return
//     */
//    @DeleteMapping("/candidate/{candidateId}")
//    ResponseEntity<Void> deleteAll(@PathVariable Long candidateId) {
//        log.debug("deleteAll(candidateId: {})", candidateId);
//        photoService.deleteAll(candidateId);
//        return ResponseEntity.noContent().build();
//    }
}