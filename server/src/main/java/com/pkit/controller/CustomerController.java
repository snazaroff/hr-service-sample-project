package com.pkit.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.Utils;
import com.pkit.model.*;
import com.pkit.model.filter.CompanyFilter;
import com.pkit.model.filter.CustomerFilter;
import com.pkit.security.spring.ContextAwarePolicyEnforcement;
import com.pkit.service.CompanyService;
import com.pkit.exception.CustomerNotFoundException;
import com.pkit.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import org.springframework.ui.ModelMap;

import javax.persistence.EntityManager;
import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private ContextAwarePolicyEnforcement policy;

    @Autowired
    private ObjectMapper om;

    private final CompanyService companyService;
    private final PersonService personService;

    @Autowired
    private EntityManager entityManager;

    public CustomerController(
            CompanyService companyService,
            PersonService personService
    ) {
        this.companyService = companyService;
        this.personService = personService;
    }

    @GetMapping("/")
    String getAllCustomer(
            @RequestParam(name = "page", defaultValue = "1") Integer page,
            @RequestParam(name = "size", defaultValue = "10") Integer size,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) throws JsonProcessingException {
        log.debug("getAllCustomer()");

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        Session session = entityManager.unwrap(Session.class);

        Filter filter = session.enableFilter("pickedCompany");
        filter.setParameter("currentPerson", loggedPersonId);

        Pageable pageable = new PageRequest(page - 1, size, new Sort("name"));

        User user = Utils.getUser(authentication);
        Department department = user.getPerson().getDepartment();

//        Collection<Company> customers = customerService.getCustomers(personId);
        Page<Company> customerPage = companyService.getCustomerByManagerCompanyId(department.getCompany().getCompanyId(), pageable);
        ControllerUtils.processPageInfo(customerPage, page, model);

        model.addAttribute("customers", customerPage);
        model.addAttribute("customerFilter", new CustomerFilter());

        Collection<Person> persons = personService.getAll();
        model.addAttribute("persons", persons);

        log.debug("customers: {}", om.writeValueAsString(customerPage.getContent()));
        log.debug("persons: {}", om.writeValueAsString(persons));

        return "customers";
    }

    @GetMapping("/{customerId}")
    String getCustomer(
            @PathVariable Long customerId,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getCompany: {}", customerId);

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        if (customerId == -1) {
            Company customer = new Company();
            customer.setCompanyType(CompanyType.CUSTOMER);
            log.debug("company: {}", customer);

            Collection<Person> persons = Collections.EMPTY_LIST;
            model.addAttribute("company", customer);
            model.addAttribute("persons", persons);

            return "customer_new";
        } else {
            Company customer = companyService.get(customerId);
            log.debug("company: {}", customer);

            Collection<Person> persons = personService.getByDepartmentId(customerId)
                    .stream()
                    .sorted(Comparator.comparing(Human::getFio))
                    .collect(Collectors.toList());
            Collection<Demand> demands = companyService.getDemands(customerId);

            model.addAttribute("company", customer);
            model.addAttribute("persons", persons);

            model.addAttribute("departments", customer.getDepartments());
            model.addAttribute("demands", demands);

            model.addAttribute("company_types", CompanyType.values());

            return "customer";
        }
    }

    @PostMapping("/filter/")
    String getFilteredCustomer(
            @RequestParam(name = "page", defaultValue = "1") Integer page,
            @RequestParam(name = "size", defaultValue = "10") Integer size,
            CompanyFilter customerFilter,
            ModelMap model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("getFilteredCustomer( customerFilter: {}, page: {}, size: {})", customerFilter, page, size);

        log.debug("customerFilter.isEmpty(): {}", customerFilter.isEmpty());

        Long loggedPersonId = Utils.getPersonId(authentication);
        model.addAttribute("loggedPersonId", loggedPersonId);

        Page<Company> customerPage = companyService.findByFilter(customerFilter, new PageRequest(page - 1, size));

        ControllerUtils.processPageInfo(customerPage, page, model);

        model.addAttribute("customers", customerPage);
        model.addAttribute("customerFilter", customerFilter);
        return "customers";
    }

    /**
     * Добавляет заказчика
     *
     * @param company
     * @param errors
     * @param model
     * @param authentication
     * @return
     */
    @Transactional
    @PostMapping
    String saveCustomer(
            @Valid @ModelAttribute Company company,
            BindingResult errors,
            Model model,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("saveCustomer(company: {})", company);
        if (errors.hasErrors()) {
            log.debug("errors: {}", errors.getAllErrors());
        }

        Person loggedUser = Utils.getPerson(authentication);

        if (!errors.hasErrors()) {
            if (company.getCompanyId() == null) {
                company.setCreatedBy(loggedUser);
                company.setCompanyType(CompanyType.CUSTOMER);
                company = companyService.add(company);
                return "redirect:/customer/" + company.getCompanyId();
            } else {
                //todo: фигня какая-то. Надо исправлять
                Company oldCompany = companyService.get(company.getCompanyId());
                company.setPickeds(oldCompany.getPickeds());
                company = companyService.update(company);
                return "redirect:/customer/";
            }
        } else {
            model.addAttribute("company", company);
            return company.getCompanyId() == null
                    ? "customer_new"
                    : "customer";
        }
    }

    @Transactional
    @PutMapping("/{customerId}")
    Company updateCustomer(
            @PathVariable Long customerId,
            @Valid @RequestBody Company customer,
            UsernamePasswordAuthenticationToken authentication
    ) {
        log.debug("updateCustomer(customerId: {}, customer: {})", customerId, customer);

        if (companyService.exists(customerId)) throw new CustomerNotFoundException(customerId);

        Person loggedUser = Utils.getPerson(authentication);
        customer.setCompanyId(customerId);
        return companyService.update(customer);
    }
}