package com.pkit.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
@ToString
@Entity
@Table(name="candidate_note")
public class CandidateNote implements Serializable, Comparable<CandidateNote> {

    @JsonProperty("candidate_note_id")
    private Long candidateNoteId;

    private Candidate candidate;

    @JsonProperty("create_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;

    @JsonProperty("created_by")
    private Person createdBy;

    private String note;

    @Id
    @Column(name = "candidate_note_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getCandidateNoteId() {
        return candidateNoteId;
    }

    @Column(name = "create_date", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME NOT NULL DEFAULT SYSTIMESTAMP")
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    @ManyToOne
    @JoinColumn(name = "candidate_id", nullable = false, updatable = false)
    public Candidate getCandidate() {
        return candidate;
    }

    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    public Person getCreatedBy() {
        return createdBy;
    }

    @Column(name = "note")
    public String getNote() {
        return note;
    }

    @PrePersist
    private void prePersist(){
        createDate = LocalDateTime.now();
    }

    @Override
    public int compareTo(@NotNull CandidateNote o) {
        if (null == createDate) return -1;
        if (null == o || null == o.getCreateDate()) return 1;
        return createDate.compareTo(o.getCreateDate());
    }
}