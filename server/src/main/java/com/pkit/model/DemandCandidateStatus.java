package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.Objects;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonSerialize(using = DemandCandidateStatus.Serializer.class)
@JsonDeserialize(using = DemandCandidateStatus.Deserializer.class)
public enum DemandCandidateStatus {

    PICKED(new Status((short)0, "Отобран")),
    PHONE(new Status((short)1, "Телефонное интервью")),
    HEAD_TO_HEAD(new Status((short)2, "Встреча")),
    TESTING(new Status((short)3, "Тестирование")),
    GO_TO_WORKING(new Status((short)4, "Выход на работу")),
    REJECT(new Status((short)5, "Отказ"));

    @JsonProperty
    private final Status status;

    DemandCandidateStatus(Status status) {
        this.status = status;
    }

    public String getName() {
        return this.status.getName();
    }

    public Short getId() {
        return this.status.getStatusId();
    }

    public static Status getStatus(Short id) {

        if (id == null) {
            return null;
        }

        for (DemandCandidateStatus status : DemandCandidateStatus.values()) {
            if (id.equals(status.status.getStatusId())) {
                return status.status;
            }
        }
        throw new IllegalArgumentException("No matching type for id " + id);
    }

    public static class Serializer extends StdSerializer<DemandCandidateStatus> {
        public Serializer() {
            super(DemandCandidateStatus.class);
        }

        @Override
        public void serialize(DemandCandidateStatus ds,
                              JsonGenerator jgen,
                              SerializerProvider sp) throws IOException {

            jgen.writeStartObject();
            jgen.writeNumberField("status_id", ds.status.getStatusId());
            jgen.writeStringField("name", ds.status.getName());

            jgen.writeEndObject();
        }
    }

    public static class Deserializer extends StdDeserializer<DemandCandidateStatus> {
        public Deserializer() {
            super(DemandCandidateStatus.class);
        }

        @Override
        public DemandCandidateStatus deserialize(JsonParser jp, DeserializationContext dc) throws IOException {
            final JsonNode jsonNode = jp.readValueAsTree();
            short statusId = (short)jsonNode.get("status_id").asInt();

            for (DemandCandidateStatus ds: DemandCandidateStatus.values()) {
                if (Objects.equals(ds.status.getStatusId(), statusId)) {
                    return ds;
                }
            }
            throw dc.mappingException("Cannot deserialize Status from key " + statusId);
        }
    }
}