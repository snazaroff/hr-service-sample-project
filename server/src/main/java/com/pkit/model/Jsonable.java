package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.text.SimpleDateFormat;

/**
 * Created by NazarovS on 17.04.2015.
 */
public class Jsonable {

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().disable(SerializationFeature.FAIL_ON_EMPTY_BEANS).setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")).setSerializationInclusion(JsonInclude.Include.NON_NULL);

    public String toJson() {

        try {
            DefaultPrettyPrinter printer = new DefaultPrettyPrinter();
            printer.indentArraysWith(new DefaultPrettyPrinter.FixedSpaceIndenter());
            return "\n{\"" + this.getClass().getSimpleName().toLowerCase() + "\":" + OBJECT_MAPPER.writer(printer).writeValueAsString(this) + "}";
        } catch(Exception e) {
            e.printStackTrace();
            return "\n{\"" + this.getClass().getSimpleName().toLowerCase() + "\":" + "Wrong format" + "}";
        }
    }
}