package com.pkit.model;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
@Entity
@Table(name="person")
public class Person extends Human implements Serializable {

    @JsonProperty("person_id")
    private Long personId;

    @JsonIgnore
    private Person createdBy;

    @JsonProperty("birth_date")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private LocalDate birthDate;

    @JsonProperty("address")
    private String address;

    @JsonProperty("email")
    @NotEmpty(message = "Поле email не должно быть пустым")
    @Pattern(regexp = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$",
            message = "Формат email нарушен")
    private String email;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("cell")
    private String cell;

    @JsonProperty("start_date")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date startDate;

    @JsonProperty("end_date")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date endDate;

    @JsonProperty("department")
    private Department department;

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonProperty("position")
    private String position;

    @Enumerated(EnumType.ORDINAL)
    @JsonProperty("sex")
    private SexStatus sex;

    public Person() {
        this.setIsActive(true);
    }

    @Id
    @Column(name = "person_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getPersonId() {
        return personId;
    }

    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    public Person getCreatedBy() {
        return createdBy;
    }

    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    @Column(name = "middle_name")
    public String getMiddleName() {
        return middleName;
    }

    @Column(name = "last_name")
    public String getLastName() {
        return lastName;
    }

    @Column(name = "birth_date")
    public LocalDate getBirthDate() {
        return birthDate;
    }

    @Column(name = "start_date")
    public Date getStartDate() {
        return startDate;
    }

    @Column(name = "end_date")
    public Date getEndDate() {
        return endDate;
    }

    @ManyToOne
    @JoinColumn(name="department_id")
    public Department getDepartment() {
        return department;
    }

    @Column(name = "is_active")
    public Boolean getIsActive() {
        return isActive;
    }

    @Column(name = "position")
    public String getPosition() {
        return position;
    }

    @Column(name = "sex")
    public SexStatus getSex() {
        return sex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person p = (Person) o;
        return this.personId.equals(p.personId);
    }

    @Override
    public String toString() {
        return "Person{" +
                "personId=" + personId +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", cell='" + cell + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", department=" + ((department == null)? null:department.getDepartmentId()) +
                ", isActive=" + isActive +
                ", position='" + position + '\'' +
                ", sex=" + sex +
                '}';
    }
}