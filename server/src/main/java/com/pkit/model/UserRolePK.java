package com.pkit.model;

import lombok.*;

@Getter
@Setter
public class UserRolePK implements java.io.Serializable {

    private Long userId;
    private String roleId;

    public UserRolePK() {
    }

    public UserRolePK(Long userId, String roleId) {
        this.userId = userId;
        this.roleId = roleId;
    }

    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof UserRolePK)) return false;
        UserRolePK pk = (UserRolePK) obj;
        return userId.equals(pk.getUserId()) && roleId.equals(pk.getRoleId());
    }

    public int hashCode() {
        return userId.hashCode() + 33 * roleId.hashCode();
    }
}
