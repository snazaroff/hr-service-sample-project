package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Objects;

@Slf4j
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonSerialize(using = SexStatus.Serializer.class)
@JsonDeserialize(using = SexStatus.Deserializer.class)
@ToString
public enum SexStatus {

    MALE((short)0, "Мужчина"),
    FEMALE((short)1, "Женщина");

    @JsonProperty
    private final Short id;

    @JsonProperty
    private final String name;

    SexStatus(Short id, String name) {
        this.id = id;
        this.name = name;
    }

    public static SexStatus getStatus(Short id) {

        if (id == null) {
            return null;
        }

        for (SexStatus status : SexStatus.values()) {
            if (id.equals(status.id)) {
                return status;
            }
        }
        throw new IllegalArgumentException("No matching type for id " + id);
    }

    public static SexStatus getByName(String name) {

        if (name == null || "".equals(name)) {
            return null;
        }

        for (SexStatus status : SexStatus.values()) {
            if (name.equals(status.name)) {
                return status;
            }
        }
        throw new IllegalArgumentException("No matching type for name " + name);
    }

    public Short getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static class Serializer extends StdSerializer<SexStatus> {
        public Serializer() {
            super(SexStatus.class);
        }

        @Override
        public void serialize(SexStatus ds,
                              JsonGenerator jgen,
                              SerializerProvider sp) throws IOException {

            jgen.writeStartObject();
            jgen.writeNumberField("status_id", ds.id);
            jgen.writeStringField("name", ds.name);
            jgen.writeEndObject();
        }
    }

    public static class Deserializer extends StdDeserializer<SexStatus> {
        public Deserializer() {
            super(SexStatus.class);
        }

        @Override
        public SexStatus deserialize(JsonParser jp, DeserializationContext dc) throws IOException {
            final JsonNode jsonNode = jp.readValueAsTree();
            short statusId = (short)jsonNode.get("status_id").asInt();

            for (SexStatus ds: SexStatus.values()) {
                if (Objects.equals(ds.id, statusId)) {
                    return ds;
                }
            }
            throw dc.mappingException("Cannot deserialize Status from key " + statusId);
        }
    }
}