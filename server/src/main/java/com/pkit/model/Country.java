package com.pkit.model;

import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.NotEmpty;

@Slf4j
@Setter
@Getter
@ToString
@Entity
@Table(name="country")
public class Country implements java.io.Serializable {

    @JsonProperty("country_id")
    private Long countryId;

    @NotEmpty(message = "Поле name не должно быть пустым")
    @JsonProperty("name")
    private String name;

    @Id
    @Column(name = "country_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getCountryId() {
        return countryId;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Country c = (Country) o;
        return this.countryId.equals(c.countryId);
    }
}
