package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Transient;

@Setter
@Getter
public class Human implements Comparable<Human> {

    @NotEmpty(message = "Поле firstName не должно быть пустым")
    @JsonProperty("first_name")
    protected String firstName;

    @JsonProperty("middle_name")
    protected String middleName;

    @NotEmpty(message = "Поле lastName не должно быть пустым")
    @JsonProperty("last_name")
    protected String lastName;

    @Transient
    public String getFio() {
        return this.lastName + " " + this.firstName +
                ((this.middleName != null && this.middleName.trim().length() > 0) ? " " + this.middleName : "");
    }

    @Override
    public int compareTo(@NotNull Human o) {
        //todo: наверное можно переписать через сравнение last_name, first_name, middle_name. Для ускорения, так сказать
        return getFio().compareTo(o.getFio());
    }
}