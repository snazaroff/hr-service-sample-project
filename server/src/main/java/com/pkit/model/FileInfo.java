package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.time.LocalDateTime;

@Slf4j
@Setter
@Getter
@Entity
@Table(schema = "file_store", name = "file")
@JsonIgnoreProperties(ignoreUnknown = true)
public class FileInfo implements java.io.Serializable {

    @JsonProperty("file_id")
    private Long fileId;

    @JsonProperty("owner_id")
    private Long ownerId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private LocalDateTime createDate;

//    @JsonProperty("created_by")
//    private Person createdBy;

    @JsonProperty("name")
    private String name;

    @JsonProperty("content_type")
    private String contentType;

    @Id
    @Column(name = "file_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getFileId() {
        return fileId;
    }

    @Column(name = "owner_id")
    public Long getOwnerId() {
        return ownerId;
    }

    @Column(name = "create_date", nullable = false, insertable = false, updatable = false)
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    //    @ManyToOne
//    @JoinColumn(name = "created_by", nullable = false, updatable = false)
//    public Person getCreatedBy() {
//        return createdBy;
//    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Column(name = "content_type")
    public String getContentType() {
        return contentType;
    }

    @PrePersist
    private void prePersist() {
        createDate = LocalDateTime.now();
    }

    @Transient
    public String getStorePath() {
        return fileId + "_" + name;
    }

    @Override
    public String toString() {
        return "FileInfo{" +
                "fileId=" + fileId +
                ", ownerId=" + ownerId +
                ", createDate=" + createDate +
//                ", createdBy=" + createdBy +
                ", name='" + name + '\'' +
                ", contentType='" + contentType + '\'' +
                '}';
    }
}