package com.pkit.model;

import lombok.ToString;

@ToString
public enum ResumeOutputType {

    PDF("pdf", "application/pdf"),
    DOCX("docx", "application/msword"),
    TXT("txt", "plain/text"),
    HTML("html", "plain/html");

    private final String name;
    private final String contentType;


    ResumeOutputType(String name, String contentType) {
        this.name = name;
        this.contentType = contentType;
    }

    public static ResumeOutputType getOutputType(String name) {

        if (name == null) {
            return null;
        }

        for (ResumeOutputType outputType : ResumeOutputType.values()) {
            if (name.equalsIgnoreCase(outputType.name)) {
                return outputType;
            }
        }
        throw new IllegalArgumentException("No matching type for name: " + name);
    }

    public String getName() {
        return name;
    }

    public String getContentType() {
        return contentType;
    }
}