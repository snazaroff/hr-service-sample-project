package com.pkit.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PickedCandidatePK implements java.io.Serializable {

	private Long personId;
	private Long candidateId;

    public PickedCandidatePK( ) {}

    public PickedCandidatePK(Long personId, Long candidateId) {
        this.personId = personId;
        this.candidateId = candidateId;
    }

    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof PickedCandidatePK)) return false;
        PickedCandidatePK pk = (PickedCandidatePK) obj;
        return personId.equals(pk.getPersonId()) && candidateId.equals(pk.getCandidateId());
    }

    public int hashCode() {
        return personId.hashCode() + 33*candidateId.hashCode();
    }   
}
