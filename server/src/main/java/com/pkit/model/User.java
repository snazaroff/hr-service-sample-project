package com.pkit.model;

import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

@Data
@Entity
@Table(name = "USERS")
public class User {

    @JsonProperty("user_id")
    private Long userId;
    @NotEmpty(message = "Поле login не должно быть пустым")
    @JsonProperty("login")
    private String login;
    @NotEmpty(message = "Поле password не должно быть пустым")
    @JsonProperty("password")
    private String password;
    @JsonProperty("person")
    private Person person;
    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonProperty("roles")
    private Set<Role> roles = new HashSet<>();

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getUserId() {
        return userId;
    }

    @NotNull
    @Column(name = "login")
    public String getLogin() {
        return login;
    }

    @NotNull
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    @NotNull
    @OneToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "person_id")
    public Person getPerson() {
        return person;
    }

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    @JoinTable(name = "user_role",
            joinColumns = {@JoinColumn(name = "USER_ID", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "ROLE_ID", nullable = false, updatable = false)})
    public Set<Role> getRoles() {
        return roles;
    }

    @Column(name = "is_active")
    public Boolean getIsActive() {
        return isActive;
    }
}