package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Objects;

@Slf4j
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonSerialize(using = LanguageStatus.Serializer.class)
@JsonDeserialize(using = LanguageStatus.Deserializer.class)
@ToString
public enum LanguageStatus {

    NATIVE(         (short)0, "родной"),
    BASIC(          (short)1, "базовые знания"),
    CAN_READ(       (short)2, "читаю профессиональную литературу"),
    CAN_PASS_INTERVIEW((short)3, "могу проходить интервью"),
    FLUENT(         (short)4, "свободно владею");

    @JsonProperty
    private final Short id;

    @JsonProperty
    private final String name;

    LanguageStatus(Short id, String name) {
        this.id = id;
        this.name = name;
    }

    public Short getStatusId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static LanguageStatus getById(Short id) {
        log.debug("getStatus( id: {})", id);

        if (id == null) {
            return null;
        }

        for (LanguageStatus status : LanguageStatus.values()) {
            log.debug("status: {}", status);

            if (id.equals(status.id)) {
                return status;
            }
        }
        throw new IllegalArgumentException("No matching type for id " + id);
    }

    public static LanguageStatus getByName(String name) {

        if (name == null || "".equals(name)) {
            return null;
        }

        for (LanguageStatus status : LanguageStatus.values()) {
            if (name.equals(status.name)) {
                return status;
            }
        }
        throw new IllegalArgumentException("No matching type for name " + name);
    }

    public static class Serializer extends StdSerializer<LanguageStatus> {
        public Serializer() {
            super(LanguageStatus.class);
        }

        @Override
        public void serialize(LanguageStatus ds,
                              JsonGenerator jgen,
                              SerializerProvider sp) throws IOException {

            jgen.writeStartObject();
            jgen.writeNumberField("status_id", ds.id);
            jgen.writeStringField("name", ds.name);
            jgen.writeEndObject();
        }
    }

    public static class Deserializer extends StdDeserializer<LanguageStatus> {
        public Deserializer() {
            super(LanguageStatus.class);
        }

        @Override
        public LanguageStatus deserialize(JsonParser jp, DeserializationContext dc) throws IOException {
            final JsonNode jsonNode = jp.readValueAsTree();
            short statusId = (short)jsonNode.get("status_id").asInt();

            for (LanguageStatus ds: LanguageStatus.values()) {
                if (Objects.equals(ds.id, statusId)) {
                    return ds;
                }
            }

            throw dc.mappingException("Cannot deserialize Status from key " + statusId);
        }
    }
}