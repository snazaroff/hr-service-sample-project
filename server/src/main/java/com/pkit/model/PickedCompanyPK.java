package com.pkit.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PickedCompanyPK implements java.io.Serializable {

	private Long personId;
	private Long companyId;

    public PickedCompanyPK( ) {}

    public PickedCompanyPK(Long personId, Long companyId) {
        this.personId = personId;
        this.companyId = companyId;
    }

    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof PickedCompanyPK)) return false;
        PickedCompanyPK pk = (PickedCompanyPK) obj;
        return personId.equals(pk.getPersonId()) && companyId.equals(pk.getCompanyId());
    }

    public int hashCode() {
        return personId.hashCode() + 33*companyId.hashCode();
    }   
}
