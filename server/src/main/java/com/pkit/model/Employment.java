package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pkit.Cacheable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name="employment")
public class Employment implements Serializable, Cacheable, Comparable<Employment> {

    private Long employmentId;

    @JsonProperty("created_by")
    private Person createdBy;

    @JsonProperty("firm_name")
    private String firmName;
    @JsonIgnore
    private Candidate candidate;
    private String position;
    private String description;
    @JsonProperty("start_date")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date startDate;
    @JsonProperty("end_date")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date endDate;

    @Id
    @Column(name = "employment_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("employment_id")
    public Long getEmploymentId() {
		return employmentId;
	}

    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    public Person getCreatedBy() {
        return createdBy;
    }

    @Column(name = "firm_name")
    public String getFirmName() {
        return firmName;
    }

    @ManyToOne
    @JoinColumn(name="candidate_id", nullable=false)
    public Candidate getCandidate() {
        return candidate;
    }

    @Column(name = "position")
	public String getPosition() {
		return position;
	}

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    @Column(name = "start_date")
    public Date getStartDate() {
        return startDate;
    }

    @Column(name = "end_date")
    public Date getEndDate() {
        return endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        if (this.getEmploymentId() == null) return false;

        Employment ea = (Employment) o;
        return Objects.equals(this.getEmploymentId(), ea.getEmploymentId());
    }

    @Override
    public int compareTo(Employment o) {
        if (null == startDate) return -1;
        if (null == o || null == o.getStartDate()) return 1;
        return startDate.compareTo(o.getStartDate());
    }

    @Override
    public String toString() {
        return "Employment{" +
                "employmentId=" + employmentId +
                ", firmName='" + firmName + '\'' +
                ", position='" + position + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }

    @Override
    public String toCacheString() {
        StringBuilder sb = new StringBuilder();
        if (firmName != null && !firmName.equals(""))
            sb.append(firmName).append(' ');
        if (position != null && !position.equals(""))
            sb.append(position).append(' ');
        if (description != null && !description.equals(""))
            sb.append(description).append(' ');

        return sb.toString().trim();
    }
}
