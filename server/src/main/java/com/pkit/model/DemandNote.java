package com.pkit.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.NotEmpty;

@Slf4j
@Setter
@ToString
@Entity
@Table(name="demand_note")
public class DemandNote implements Serializable, Comparable<DemandNote> {

    @JsonProperty("demand_note_id")
    private Long demandNoteId;

    private Demand demand;

    @JsonProperty("create_date")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;

    @JsonProperty("created_by")
    private Person createdBy;

    @NotEmpty(message = "Поле firstName не должно быть пустым")
    private String note;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "demand_note_id")
    public Long getDemandNoteId() {
        return demandNoteId;
    }

    @Column(name = "create_date", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME NOT NULL DEFAULT SYSTIMESTAMP")
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    @ManyToOne
    @JoinColumn(name = "demand_id", nullable = false, updatable = false)
    public Demand getDemand() {
        return demand;
    }

    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    public Person getCreatedBy() {
        return createdBy;
    }

    @Column(name = "note")
    public String getNote() {
        return note;
    }

    @PrePersist
    private void prePersist(){
        createDate = LocalDateTime.now();
    }

    @Override
    public int compareTo(@NotNull DemandNote o) {
        if (null == createDate) return -1;
        if (null == o || null == o.getCreateDate()) return 1;
        return createDate.compareTo(o.getCreateDate());
    }
}
