package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.Mapping;
import org.springframework.data.elasticsearch.annotations.Setting;

import static org.springframework.data.elasticsearch.annotations.FieldType.Date;

@Slf4j
@Setter
@Getter
@ToString
@Document(indexName = "index", type = "demand")
@Setting(settingPath = "/es/settings.json")
@Mapping(mappingPath = "/es/demand_mappings.json")
public class DemandIndex {

    @Id
    private String demandId;

    private String data;

    @JsonProperty("create_date")
    @Field(type = Date)
    @JsonFormat (shape = JsonFormat.Shape.STRING, pattern ="yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
    private java.util.Date createDate = new java.util.Date();
}
