package com.pkit.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@IdClass(PickedCompanyPK.class)
@Table(name = "picked_company")
public class PickedCompany implements Serializable {

    public PickedCompany() { }

    public PickedCompany(Long personId, Long companyId) {
        this.personId = personId;
        this.companyId = companyId;
    }

    @Id
    @Column(name = "person_id", insertable = false, updatable = false)
    public Long personId;

    @Id
    @Column(name = "company_id", insertable = false, updatable = false)
    public Long companyId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PickedCompany pc = (PickedCompany) o;
        return pc.personId != null && pc.companyId != null && this.companyId.equals(pc.companyId) && this.personId.equals(pc.personId);
    }

    @Override
    public String toString() {
        return "PickedCompany{" +
                "personId=" + personId +
                ", companyId=" + companyId +
                '}';
    }
}