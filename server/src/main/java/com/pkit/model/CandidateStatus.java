package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.ToString;

import java.io.IOException;
import java.util.Objects;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonSerialize(using = CandidateStatus.Serializer.class)
@JsonDeserialize(using = CandidateStatus.Deserializer.class)
@ToString
public enum CandidateStatus {

    NOT_LOOKING_FOR_JOB(new Status((short)0, "Не ищет работу")),
    LOOKING_FOR_JOB(new Status((short)1, "Ищет работу")),
    REVIERED(new Status((short)2, "В работе"));

    @JsonProperty
    private final Status status;

    CandidateStatus(Status status) {
        this.status = status;
    }

    public static Status getStatus(Short id) {

        if (id == null) {
            return null;
        }

        for (CandidateStatus status : CandidateStatus.values()) {
            if (id.equals(status.status.getStatusId())) {
                return status.status;
            }
        }
        throw new IllegalArgumentException("No matching type for id " + id);
    }

    public Short getStatusId() {
        return status.getStatusId();
    }

    public String getName() {
        return status.getName();
    }

    public int getId() {
        return status.getStatusId();
    }

    public static class Serializer extends StdSerializer<CandidateStatus> {
        public Serializer() {
            super(CandidateStatus.class);
        }

        @Override
        public void serialize(CandidateStatus ds,
                              JsonGenerator jgen,
                              SerializerProvider sp) throws IOException {

            jgen.writeStartObject();
            jgen.writeNumberField("status_id", ds.status.getStatusId());
            jgen.writeStringField("name", ds.status.getName());

            jgen.writeEndObject();
        }
    }

    public static class Deserializer extends StdDeserializer<CandidateStatus> {
        public Deserializer() {
            super(CandidateStatus.class);
        }

        @Override
        public CandidateStatus deserialize(JsonParser jp, DeserializationContext dc) throws IOException {
            final JsonNode jsonNode = jp.readValueAsTree();
            short statusId = (short)jsonNode.get("status_id").asInt();

            for (CandidateStatus ds: CandidateStatus.values()) {
                if (Objects.equals(ds.status.getStatusId(), statusId)) {
                    return ds;
                }
            }
            throw dc.mappingException("Cannot deserialize Status from key " + statusId);
        }
    }
}