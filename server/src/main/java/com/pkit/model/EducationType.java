package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Objects;

@Slf4j
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonSerialize(using = EducationType.Serializer.class)
@JsonDeserialize(using = EducationType.Deserializer.class)
public enum EducationType {

    UNEDUCATED((short) 0, "Без образования"),
    PRIMARY_SCHOOL((short) 1, "Начальная школа"),
    SECONDARY_SCHOOL((short) 2, "Средняя школа"),
    SECONDARY_SPECIAL_SCHOOL((short) 3, "Среднее специальное"),
    HIGH_SCHOOL((short) 4, "Высшая школа"),
    BACHELOR((short) 5, "Бакалавр"),
    MASTER((short) 6, "Магистр"),
    CANDIDATE((short) 7, "Кандидат"),
    DOCTOR((short) 8, "Доктор"),
    COURSES((short) 9, "Курсы"),
    UNFINISHED_HIGHER((short) 10, "Неоконченное высшее");

    @JsonProperty
    private final Short id;

    @JsonProperty
    private final String name;

    EducationType(Short id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Short getId() {
        return id;
    }

    public static EducationType getByName(String name) {
        log.debug("getByName( name: {})", name);

        if (name == null || "".equals(name)) {
            return null;
        }

        for (EducationType status : EducationType.values()) {
            log.debug("{}.equals({}): {}", name, status.name, name.equals(status.name));
            log.debug("{}.startsWith({}): {}", status.name, name, status.name.startsWith(name));

            if (name.equals(status.name) || status.name.startsWith(name)) {
                return status;
            }
        }
        if ("Высшее".equals(name))
            return HIGH_SCHOOL;
        throw new IllegalArgumentException("No matching type for name " + name);
    }

    public static class Serializer extends StdSerializer<EducationType> {
        public Serializer() {
            super(EducationType.class);
        }

        @Override
        public void serialize(EducationType ds,
                              JsonGenerator jgen,
                              SerializerProvider sp) throws IOException {

            jgen.writeStartObject();
            jgen.writeNumberField("education_type_id", ds.id);
            jgen.writeStringField("name", ds.name);
            jgen.writeEndObject();
        }
    }

    public static class Deserializer extends StdDeserializer<EducationType> {
        public Deserializer() {
            super(EducationType.class);
        }

        @Override
        public EducationType deserialize(JsonParser jp, DeserializationContext dc) throws IOException {
            final JsonNode jsonNode = jp.readValueAsTree();
            Integer statusId = jsonNode.get("education_type_id").asInt();

            for (EducationType ds : EducationType.values()) {
                if (Objects.equals(ds.id, statusId.shortValue())) {
                    return ds;
                }
            }
            throw dc.mappingException("Cannot deserialize EducationType from key " + statusId);
        }
    }
}