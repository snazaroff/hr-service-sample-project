package com.pkit.model;

import lombok.*;

@Getter
@Setter
@ToString
public class Status {

    private Short statusId;
    private String name;

    public Status(Short statusId, String name) {
        this.statusId = statusId;
        this.name = name;
    }
}
