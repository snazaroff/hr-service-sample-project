package com.pkit.model;

import java.time.LocalDate;
import java.time.Period;

public class Age {
    private final LocalDate birthDate;

    public Age(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getValue() {
        return (birthDate == null)? "0": "" + Period.between(LocalDate.now(), birthDate).getYears();
    }
}
