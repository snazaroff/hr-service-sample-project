package com.pkit.model;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

import java.time.LocalDateTime;
import java.util.*;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.validator.constraints.NotEmpty;

@Slf4j
@Getter
@Setter
@Entity
@Table(name = "company")
@FilterDef(name = "pickedCompany",
        parameters = @ParamDef(name = "currentPerson", type = "long"))
public class Company implements java.io.Serializable {

    @JsonProperty("company_id")
    private Long companyId;
    @JsonProperty("create_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDateTime createDate;
    @JsonProperty("created_by")
    private Person createdBy;
    @NotEmpty(message = "Поле name не должно быть пустым")
    private String name;
    @NotEmpty(message = "Поле address не должно быть пустым")
    private String address;
    @JsonProperty("head_person")
    private Person headPerson;
    @JsonProperty("contact_person")
    private Person contactPerson;
    private String phone;
    private String cell;

    @NotEmpty(message = "Поле email не должно быть пустым")
    @Pattern(regexp = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$",
            message = "Формат email нарушен")
    private String email;

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonIgnore
    private Set<Department> departments = new HashSet<>();

    @JsonProperty("company_type")
    private CompanyType companyType;

    private Set<PickedCompany> pickeds;

    public Company() {
        this.setIsActive(true);
    }

    @Id
    @Column(name = "company_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getCompanyId() {
        return companyId;
    }

    @Column(name = "create_date", nullable = false, insertable = false, updatable = false)
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    public Person getCreatedBy() {
        return createdBy;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    @ManyToOne
    @JoinColumn(name = "head_person_id")
    public Person getHeadPerson() {
        return headPerson;
    }

    @ManyToOne
    @JoinColumn(name = "contact_person_id")
    public Person getContactPerson() {
        return contactPerson;
    }

    @Column(name = "is_active")
    public Boolean getIsActive() {
        return isActive;
    }

    @OneToMany(mappedBy = "company", fetch = FetchType.EAGER, orphanRemoval = false)
    @JsonIgnore
    public Set<Department> getDepartments() {
        return departments;
    }

    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "company_id")
    @Filters({
            @Filter(name = "pickedCompany", condition = "person_id = :currentPerson")
    })
    public Set<PickedCompany> getPickeds() {
        return pickeds;
    }

    @Column(name = "company_type_id")
    public CompanyType getCompanyType() {
        return companyType;
    }

    public boolean isPicked(Long personId) {
        return pickeds != null && pickeds.stream().anyMatch(f -> f.personId.equals(personId));
    }

    @PrePersist
    private void prePersist() {
        createDate = LocalDateTime.now();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        if (this.companyId == null) return false;

        Company c = (Company) o;
        log.debug("company: {}", this);
        return this.companyId.equals(c.companyId);
    }

    @Override
    public String toString() {
        return "Company{" +
                "companyId=" + companyId +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", headPerson=" + headPerson +
                ", contactPerson=" + contactPerson +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", isActive=" + isActive +
                ", companyType=" + companyType +
                '}';
    }
}