package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.Objects;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonSerialize(using = DemandStatus.Serializer.class)
@JsonDeserialize(using = DemandStatus.Deserializer.class)
public enum DemandStatus {

    CREATE(new Status((short)0, "Создано")),
    OPEN(new Status((short)1, "В работе")),
    CLOSE(new Status((short)2, "Закрыто"));

    @JsonProperty
    private final Status status;

    DemandStatus(Status status) {
        this.status = status;
    }

    public String getName() {
        return this.status.getName();
    }

    public Short getId() {
        return this.status.getStatusId();
    }

    public static Status getStatus(Short id) {

        if (id == null) {
            return null;
        }

        for (DemandStatus status : DemandStatus.values()) {
            if (id.equals(status.status.getStatusId())) {
                return status.status;
            }
        }
        throw new IllegalArgumentException("No matching type for id " + id);
    }

    public static class Serializer extends StdSerializer<DemandStatus> {
        public Serializer() {
            super(DemandStatus.class);
        }

        @Override
        public void serialize(DemandStatus ds,
                              JsonGenerator jgen,
                              SerializerProvider sp) throws IOException {

            StringBuilder lang = new StringBuilder();
            jgen.writeStartObject();
            jgen.writeNumberField("status_id", ds.status.getStatusId());
            jgen.writeStringField("name", ds.status.getName());

            jgen.writeEndObject();
        }
    }

    public static class Deserializer extends StdDeserializer<DemandStatus> {
        public Deserializer() {
            super(DemandStatus.class);
        }

        @Override
        public DemandStatus deserialize(JsonParser jp, DeserializationContext dc) throws IOException {
            final JsonNode jsonNode = jp.readValueAsTree();
            short statusId = (short)jsonNode.get("status_id").asInt();

            for (DemandStatus ds: DemandStatus.values()) {
                if (Objects.equals(ds.status.getStatusId(), statusId)) {
                    return ds;
                }
            }
            throw dc.mappingException("Cannot deserialize Status from key " + statusId);
        }
    }
}