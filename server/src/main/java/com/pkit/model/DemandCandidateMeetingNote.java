package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Slf4j
@Getter
@Setter
@Entity
@Table(name="demand_candidate_meeting_note")
public class DemandCandidateMeetingNote extends Jsonable implements Serializable {

    @JsonProperty("demand_candidate_meeting_note_id")
    private Long demandCandidateMeetingNoteId;

    @NotNull(message = "Поле demandCandidateMeeting не должно быть пустым")
    @JsonProperty(value = "demand_candidate_meeting", access = JsonProperty.Access.WRITE_ONLY)
    private DemandCandidateMeeting demandCandidateMeeting;

    @JsonProperty("created_by")
    private Person createdBy;

    @JsonProperty("create_date")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;

    @NotEmpty(message = "Поле note не должно быть пустым")
    @JsonProperty("note")
    private String note;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "demand_candidate_meeting_note_id")
    public Long getDemandCandidateMeetingNoteId() {
        return demandCandidateMeetingNoteId;
    }

    @ManyToOne
    @JoinColumn(name="demand_candidate_meeting_id", nullable=false)
    public DemandCandidateMeeting getDemandCandidateMeeting() {
        return demandCandidateMeeting;
    }
	
    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    public Person getCreatedBy() {
        return createdBy;
    }

    @Column(name = "create_date", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME NOT NULL DEFAULT SYSTIMESTAMP")
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    @Column(name = "note")
    public String getNote() {
        return note;
    }

    @PrePersist
    private void prePersist(){
        createDate = LocalDateTime.now();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DemandCandidateMeetingNote c = (DemandCandidateMeetingNote) o;
        return this.demandCandidateMeetingNoteId.equals(c.demandCandidateMeetingNoteId);
    }

    @Override
    public String toString() {
        return "DemandCandidateMeetingNote{" +
                "demandCandidateMeetingNoteId=" + demandCandidateMeetingNoteId +
                ", demandCandidateMeeting=" + demandCandidateMeeting.getDemandCandidateMeetingId() +
                ", demandCandidateMeeting.getType()=" + demandCandidateMeeting.getType() +
                ", demandCandidateMeeting.getMeetingDateTime()=" + demandCandidateMeeting.getMeetingDateTime() +
                ", demandCandidateMeeting.getNotes().size()=" + demandCandidateMeeting.getNotes().size() +

                ", createdBy=" + createdBy +
                ", createDate=" + createDate +
                ", note='" + note + '\'' +
                '}';
    }
}