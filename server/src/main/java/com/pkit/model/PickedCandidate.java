package com.pkit.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@IdClass(PickedCandidatePK.class)
@Table(name = "picked_candidate")
public class PickedCandidate implements Serializable {

    public PickedCandidate() { }

    public PickedCandidate(Long personId, Long candidateId) {
        this.personId = personId;
        this.candidateId = candidateId;
    }

    @Id
    @Column(name = "person_id", insertable = false, updatable = false)
    public Long personId;

    @Id
    @Column(name = "candidate_id", insertable = false, updatable = false)
    public Long candidateId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PickedCandidate pc = (PickedCandidate) o;
        return pc.personId != null && pc.candidateId != null && this.candidateId.equals(pc.candidateId) && this.personId.equals(pc.personId);
    }

    @Override
    public String toString() {
        return "PickedCandidate{" +
                "personId=" + personId +
                ", candidateId=" + candidateId +
                '}';
    }
}