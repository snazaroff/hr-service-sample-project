package com.pkit.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Slf4j
@Getter
@Setter
@Entity
@Table(name="demand_candidate_note")
public class DemandCandidateNote extends Jsonable implements Serializable {

    public DemandCandidateNote() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("demand_candidate_note_id")
    @Column(name = "demand_candidate_note_id", insertable = false, updatable = false)
    public Long demandCandidateNoteId;

//    @ManyToOne(cascade={CascadeType.ALL})
    @ManyToOne
//    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "demand_candidate_id", nullable = false, updatable = false)
    @JsonProperty(value = "demand_candidate", access = JsonProperty.Access.WRITE_ONLY)
    public DemandCandidate demandCandidate;

    @JsonProperty("created_by")
    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    private Person createdBy;

    @JsonProperty("create_date")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name = "create_date", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME NOT NULL DEFAULT SYSTIMESTAMP")
    private LocalDateTime createDate;

    @NotEmpty(message = "Поле note не должно быть пустым")
    @Column(name = "note")
    private String note;

    @PrePersist
    private void prePersist(){
        createDate = LocalDateTime.now();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DemandCandidateNote c = (DemandCandidateNote) o;
        return this.demandCandidateNoteId.equals(c.demandCandidateNoteId);
    }

    @Override
    public String toString() {
        return "DemandCandidateNote{" +
                "demandCandidateNoteId=" + demandCandidateNoteId +
                ", demandCandidateId=" + ((demandCandidate != null)?"" + demandCandidate.getDemandCandidateId():"") +
                ", createdBy=" + createdBy +
                ", createDate=" + createDate +
                ", note='" + note + '\'' +
                '}';
    }
}