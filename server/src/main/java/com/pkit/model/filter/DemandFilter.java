package com.pkit.model.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class DemandFilter implements java.io.Serializable {

    @JsonProperty("position")
    private String position;
    @JsonProperty("customer_id")
    private Long customerId;

    public boolean isEmpty() {
        return (position == null || position.equals("")) &&
                (customerId == null || customerId == 0);
    }
}
