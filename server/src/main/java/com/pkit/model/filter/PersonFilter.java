package com.pkit.model.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
@ToString
public class PersonFilter implements java.io.Serializable {

    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("middle_name")
    private String middleName;
    @JsonProperty("last_name")
    private String lastName;

    public void clear() {
        firstName = null;
        middleName = null;
        lastName = null;
    }

    public boolean isEmpty() {
        return (firstName == null || firstName.equals("")) &&
                (middleName == null || middleName.equals("")) &&
                (lastName == null || lastName.equals(""));
    }
}