package com.pkit.model.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pkit.model.Category;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
@ToString
public class CandidateFilter implements java.io.Serializable {

    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("middle_name")
    private String middleName;
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("words")
    private String words;
    @JsonProperty("education")
    private String education;
    @JsonProperty("metro_id")
    private Long metroId;
    @JsonProperty("category")
    private Category category;
    @JsonProperty("language_id")
    private Long languageId;
    @JsonProperty("salary_min")
    private Long salaryMin;
    @JsonProperty("salary_max")
    private Long salaryMax;

    public void clear() {
        firstName = null;
        middleName = null;
        lastName = null;
        words = null;
        education = null;
        metroId = null;
        category = null;
        languageId = null;
        salaryMin = null;
        salaryMax = null;
    }

    public boolean isEmpty() {
        return (firstName == null || firstName.equals("")) &&
                (middleName == null || middleName.equals("")) &&
                (lastName == null || lastName.equals("")) &&
                (words == null || words.equals("")) &&
                (education == null || education.equals("")) &&
                (metroId == null || metroId.equals(0L)) &&
                (languageId == null || languageId.equals(0L)) &&
                (salaryMin == null || salaryMin.equals(0L)) &&
                (salaryMax == null || salaryMax.equals(0L)) &&
                (category == null || category.getCategoryId() == null || category.getCategoryId() == 0);
    }
}
