package com.pkit.model.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
@ToString
public class CompanyFilter implements java.io.Serializable {

    @JsonProperty("name")
    private String name;

    public void clear() {
        name = null;
    }

    public boolean isEmpty() {
        return (name == null || name.equals(""));
    }
}
