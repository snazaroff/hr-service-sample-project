package com.pkit.model.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pkit.model.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Setter
@Getter
@ToString
public class AddDemandCandidateMeetingNoteEvent extends Event {

	@JsonProperty
	private Demand demand;
	@JsonProperty
	private Candidate candidate;
	@JsonProperty
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createDateTime;
	@JsonProperty
	private Person createdBy;
	@JsonProperty
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime meetingDateTime;
	@JsonProperty
	private String note;

	public AddDemandCandidateMeetingNoteEvent(Demand demand, Candidate candidate, Person createdBy, LocalDateTime createDateTime, LocalDateTime meetingDateTime, String note) {
		this.type = "add-demand-candidate-meeting-note";
		this.demand = demand;
		this.candidate = candidate;
		this.createDateTime = createDateTime;
		this.createdBy = createdBy;
		this.meetingDateTime = meetingDateTime;
		this.note = note;
		this.eventTime = createDateTime;
	}

	public AddDemandCandidateMeetingNoteEvent(
					DemandCandidate demandCandidate,
					DemandCandidateMeeting demandCandidateMeeting,
					DemandCandidateMeetingNote demandCandidateMeetingNote) {
		this.type = "add-demand-candidate-meeting-note";
		this.demand = demandCandidate.getDemand();
		this.candidate = demandCandidate.getCandidate();
		this.createDateTime = demandCandidateMeeting.getCreateDate();
		this.createdBy = demandCandidateMeeting.getCreatedBy();
		this.meetingDateTime = demandCandidateMeeting.getMeetingDateTime();
		this.note = demandCandidateMeetingNote.getNote();
		this.eventTime = this.createDateTime;
	}
}