package com.pkit.model.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pkit.model.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Setter
@Getter
@ToString
public class AddDemandCandidateMeetingEvent extends Event {

	@JsonProperty
	private Demand demand;
	@JsonProperty
	private Candidate candidate;
	@JsonProperty
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createDateTime;
	private Person createdBy;
	@JsonProperty
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime meetingDateTime;

	public AddDemandCandidateMeetingEvent(Demand demand, Candidate candidate, Person createdBy, LocalDateTime createDateTime, LocalDateTime meetingDateTime) {
		this.type = "add-demand-candidate-meeting";
		this.demand = demand;
		this.candidate = candidate;
		this.createDateTime = createDateTime;
		this.createdBy = createdBy;
		this.meetingDateTime = meetingDateTime;
		this.eventTime = createDateTime;
	}

	public AddDemandCandidateMeetingEvent(DemandCandidate demandCandidate, DemandCandidateMeeting demandCandidateMeeting) {
		this.type = "add-demand-candidate-meeting";
		this.demand = demandCandidate.getDemand();
		this.candidate = demandCandidate.getCandidate();
		this.createDateTime = demandCandidateMeeting.getCreateDate();
		this.createdBy = demandCandidateMeeting.getCreatedBy();
		this.meetingDateTime = demandCandidateMeeting.getMeetingDateTime();
		this.eventTime = createDateTime;
	}
}