package com.pkit.model.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pkit.model.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Setter
@Getter
@ToString
public class AddDemandCandidateNoteEvent extends Event {

	@JsonProperty
	private Demand demand;
	@JsonProperty
	private Candidate candidate;
	@JsonProperty
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createDateTime;
	@JsonProperty
	private Person createdBy;
	@JsonProperty
	private String note;

	public AddDemandCandidateNoteEvent(Demand demand, Candidate candidate, Person createdBy, LocalDateTime createDateTime, String note) {
		this.type = "add-demand-candidate-note";
		this.demand = demand;
		this.candidate = candidate;
		this.createDateTime = createDateTime;
		this.createdBy = createdBy;
		this.note = note;
		this.eventTime = createDateTime;
	}

	public AddDemandCandidateNoteEvent(DemandCandidate demandCandidate, DemandCandidateNote demandCandidateNote) {
		this.type = "add-demand-candidate-note";
		this.demand = demandCandidate.getDemand();
		this.candidate = demandCandidate.getCandidate();
		this.createDateTime = demandCandidateNote.getCreateDate();
		this.createdBy = demandCandidateNote.getCreatedBy();
		this.note = demandCandidateNote.getNote();
		this.eventTime = createDateTime;
	}

}