package com.pkit.model.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pkit.model.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Setter
@Getter
@ToString
public class AddDemandCandidateEvent extends Event {

	@JsonProperty
	private Demand demand;
	@JsonProperty
	private Candidate candidate;
	@JsonProperty
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createDateTime;
	@JsonProperty
	private Person createdBy;

	public AddDemandCandidateEvent(Demand demand, Candidate candidate, LocalDateTime createDateTime, Person createdBy) {
		this.type = "add-demand-candidate";
		this.demand = demand;
		this.candidate = candidate;
		this.createDateTime = createDateTime;
		this.createdBy = createdBy;
		this.eventTime = createDateTime;
	}

	public AddDemandCandidateEvent(DemandCandidate demandCandidate) {
		this.type = "add-demand-candidate";
		this.demand = demandCandidate.getDemand();
		this.candidate = demandCandidate.getCandidate();
		this.createDateTime = demandCandidate.getCreateDate();
		this.createdBy = demandCandidate.getCreatedBy();
		this.eventTime = createDateTime;
	}
}