package com.pkit.model.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pkit.model.Demand;
import com.pkit.model.Person;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Setter
@Getter
@ToString
public class CreateDemandEvent extends Event {

	@JsonProperty
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createDateTime;
	@JsonProperty
	private Person createdBy;

	public CreateDemandEvent(LocalDateTime createDateTime, Person createdBy) {
		this.type = "add-demand";
		this.createDateTime = createDateTime;
		this.createdBy = createdBy;
		this.eventTime = createDateTime;
	}

	public CreateDemandEvent(Demand demand) {
		this.type = "add-demand";
		this.createDateTime = demand.getCreateDate();
		this.createdBy = demand.getCreatedBy();
		this.eventTime = demand.getCreateDate();
	}
}