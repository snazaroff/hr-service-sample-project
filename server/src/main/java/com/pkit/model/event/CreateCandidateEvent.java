package com.pkit.model.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pkit.model.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Setter
@Getter
@ToString
public class CreateCandidateEvent extends Event {

	@JsonProperty
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createDateTime;
	@JsonProperty
	private Candidate candidate;
	@JsonProperty
	private Person createdBy;

	public CreateCandidateEvent(Candidate candidate) {
		this.type = "add-candidate";
		this.createDateTime = candidate.getCreateDate();
		this.createdBy = candidate.getCreatedBy();
		this.candidate = candidate;
		this.eventTime = candidate.getCreateDate();
	}
}