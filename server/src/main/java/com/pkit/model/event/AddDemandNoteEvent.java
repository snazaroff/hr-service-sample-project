package com.pkit.model.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pkit.model.Demand;
import com.pkit.model.DemandNote;
import com.pkit.model.Person;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Setter
@Getter
@ToString
public class AddDemandNoteEvent extends Event {

	@JsonProperty
	Demand demand;
	@JsonProperty
	Person createdBy;
	@JsonProperty
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	LocalDateTime createDateTime;
	@JsonProperty
	String note;

	public AddDemandNoteEvent(Demand demand, Person createdBy, LocalDateTime createDateTime, String note) {
		this.type = "add-demand-note";
		this.demand = demand;
		this.createdBy = createdBy;
		this.createDateTime = createDateTime;
		this.note = note;
		this.eventTime = createDateTime;
	}

	public AddDemandNoteEvent(DemandNote demandNote) {
		this.type = "add-demand-note";
		this.demand = demandNote.getDemand();
		this.createdBy = demandNote.getCreatedBy();
		this.createDateTime = demandNote.getCreateDate();
		this.note = demandNote.getNote();
		this.eventTime = createDateTime;
	}
}