package com.pkit.model.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pkit.model.Candidate;
import com.pkit.model.CandidateNote;
import com.pkit.model.Person;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Setter
@Getter
@ToString
public class CommentCandidateEvent extends Event {

	@JsonProperty
	Candidate candidate;
	@JsonProperty
	Person createdBy;
	@JsonProperty
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	LocalDateTime createDateTime;
	@JsonProperty
	String note;

	public CommentCandidateEvent(Candidate candidate, Person createdBy, LocalDateTime createDateTime, String note) {
		this.type = "add-candidate-note";
		this.candidate = candidate;
		this.createdBy = createdBy;
		this.createDateTime = createDateTime;
		this.note = note;
		this.eventTime = createDateTime;
	}

	public CommentCandidateEvent(CandidateNote candidateNote) {
		this.type = "add-candidate-note";
		this.candidate = candidateNote.getCandidate();
		this.createdBy = candidateNote.getCreatedBy();
		this.createDateTime = candidateNote.getCreateDate();
		this.note = candidateNote.getNote();
		this.eventTime = candidateNote.getCreateDate();
	}
}