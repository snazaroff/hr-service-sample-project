package com.pkit.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Slf4j
@Setter
@Getter
public class Event implements Comparable<Event>{

	@JsonIgnore
	protected LocalDateTime eventTime;
	@JsonProperty
	protected String type;

	@Override
	public int compareTo(@NotNull Event o) {
		if (null == eventTime) return -1;
		if (null == o || null == o.eventTime) return 1;
		return eventTime.compareTo(o.eventTime);
	}
}