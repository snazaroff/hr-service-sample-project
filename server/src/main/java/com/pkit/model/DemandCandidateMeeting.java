package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Getter
@Setter
@Entity
@Table(name="demand_candidate_meeting")
public class DemandCandidateMeeting implements Serializable, Comparable<DemandCandidateMeeting>  {

    @JsonProperty("demand_candidate_meeting_id")
    private Long demandCandidateMeetingId;
    @JsonProperty(value = "demand_candidate", access = JsonProperty.Access.WRITE_ONLY)
    private DemandCandidate demandCandidate;
    @JsonProperty("created_by")
    private Person createdBy;

    @JsonProperty("create_date")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;

    @NotNull(message = "Поле meetingDateTime не должно быть пустым")
    @JsonProperty("meeting_datetime")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime meetingDateTime;

    @OrderBy("createDate DESC")
    @JsonProperty("notes")
    private Set<DemandCandidateMeetingNote> notes = new HashSet<>();

    @NotNull(message = "Поле type не должно быть пустым")
    @Enumerated(EnumType.ORDINAL)
    @JsonProperty("type")
    private MeetingType type;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "demand_candidate_meeting_id")
    public Long getDemandCandidateMeetingId() {
        return demandCandidateMeetingId;
    }

    @ManyToOne
    @JoinColumn(name="demand_candidate_id", nullable=false)
    public DemandCandidate getDemandCandidate() {
        return demandCandidate;
    }
	
    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    public Person getCreatedBy() {
        return createdBy;
    }

    @Column(name = "create_date", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME NOT NULL DEFAULT SYSTIMESTAMP")
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    @Column(name = "meeting_datetime")
    public LocalDateTime getMeetingDateTime() {
        return meetingDateTime;
    }

    @OneToMany(mappedBy = "demandCandidateMeeting", fetch=FetchType.EAGER)
    public Set<DemandCandidateMeetingNote> getNotes() {
        return notes;
    }

    @Column(name = "meeting_type_id")
    public MeetingType getType() {
        return type;
    }

    @PrePersist
    private void prePersist(){
        createDate = LocalDateTime.now();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DemandCandidateMeeting dcm = (DemandCandidateMeeting) o;
        return this.getDemandCandidateMeetingId().equals(dcm.getDemandCandidateMeetingId());
    }

    @Override
    public int compareTo(DemandCandidateMeeting o) {
        if (null == meetingDateTime) return -1;
        if (null == o || null == o.getMeetingDateTime()) return 1;
        return meetingDateTime.compareTo(o.getMeetingDateTime());
    }

    @Override
    public String toString() {
        return "DemandCandidateMeeting{" +
                "demandCandidateMeetingId=" + demandCandidateMeetingId +
                ", demandCandidate=" + demandCandidate.getDemandCandidateId() +
                ", createdBy=" + createdBy +
                ", createDate=" + createDate +
                ", meetingDateTime=" + meetingDateTime +
                ", notes=" + notes +
                ", type=" + type +
                '}';
    }
}