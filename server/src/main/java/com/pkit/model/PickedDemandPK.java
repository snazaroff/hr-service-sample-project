package com.pkit.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PickedDemandPK implements java.io.Serializable {

	private Long personId;
	private Long demandId;

    public PickedDemandPK( ) {}

    public PickedDemandPK(Long personId, Long demandId) {
        this.personId = personId;
        this.demandId = demandId;
    }

    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof PickedDemandPK)) return false;
        PickedDemandPK pk = (PickedDemandPK) obj;
        return personId.equals(pk.getPersonId()) && demandId.equals(pk.getDemandId());
    }

    public int hashCode() {
        return personId.hashCode() + 33*demandId.hashCode();
    }   
}
