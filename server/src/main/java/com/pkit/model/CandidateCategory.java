package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pkit.Cacheable;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Slf4j
@Getter
@Setter
@Entity
@IdClass(CandidateCategoryPK.class)
@Table(name="candidate_category")
public class CandidateCategory extends Jsonable implements Serializable, Cacheable, Comparable<CandidateCategory> {

    public CandidateCategory() { }

    public CandidateCategory(Candidate candidate, Category category) {
        this.candidate = candidate;
        this.candidateId = candidate.getCandidateId();
        this.category = category;
        this.categoryId = category.getCategoryId();
    }

    @Id
    @Column(name = "category_id", insertable = false, updatable = false)
    @JsonProperty("category_id")
    public Long categoryId;

    @Id
    @Column(name = "candidate_id", insertable = false, updatable = false)
    @JsonProperty("candidate_id")
    public Long candidateId;

    @ManyToOne
    @JoinColumn(name="category_id", nullable=false, insertable = false, updatable = false)
	private Category category;
	
    @ManyToOne
    @JoinColumn(name="candidate_id", nullable=false, insertable = false, updatable = false)
    @JsonIgnore
	private Candidate candidate;

    @Override
    public boolean equals(Object o) {
        log.debug("this: {}; o: {}", this, o);

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Candidate c = ((CandidateCategory) o).getCandidate();
        if (c == null) return false;
        Category l = ((CandidateCategory) o).getCategory();
        return l != null && this.candidateId != null && this.candidateId.equals(c.getCandidateId()) && this.categoryId.equals(l.getCategoryId());
    }

    @Override
    public String toString() {
        return "CandidateCategory{" +
                "categoryId=" + categoryId +
                ", candidateId=" + candidateId +
                '}';
    }

    @Override
    public int compareTo(@NotNull CandidateCategory o) {
        if (null == o) return -1;
        if (null == o.candidateId || null == o.categoryId) return 1;
        if (Objects.equals(candidateId, o.candidateId)) {
            return categoryId.compareTo(o.categoryId);
        } else {
            return candidateId.compareTo(o.candidateId);
        }
    }

    @Override
    public String toCacheString() {
        return category.getName();
    }
}
