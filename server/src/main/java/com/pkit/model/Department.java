package com.pkit.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;

@Getter
@Setter
@Entity
@ToString
@Table(name="department")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class,
        property = "@id")
public class Department implements java.io.Serializable {

    @JsonProperty("department_id")
    private Long departmentId;

    @NotNull(message = "Поле createdBy не должно быть пустым")
    @JsonProperty("created_by")
    private Person createdBy;

    @NotEmpty(message = "Поле name не должно быть пустым")
    private String name;

    private Person head;

    @JsonProperty("contact_person")
    private Person contactPerson;

    @NotEmpty(message = "Поле phone не должно быть пустым")
    private String phone;

    @NotEmpty(message = "Поле email не должно быть пустым")
    @Pattern(regexp = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$",
            message = "Формат email нарушен")
    private String email;

    @JsonProperty("is_active")
    private Boolean isActive;

    @NotNull(message = "Поле company не должно быть пустым")
    @JsonProperty("company")
    private Company company;

    @JsonIgnore
    private Set<Person> persons = new HashSet<>();

    public Department() {
        this.setIsActive(true);
    }

    @Id
    @Column(name = "department_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getDepartmentId() {
        return departmentId;
    }

    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    public Person getCreatedBy() {
        return createdBy;
    }

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name="head_id")
    public Person getHead() {
        return head;
    }

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name="contact_person_id")
    public Person getContactPerson(){
        return contactPerson;
    }

    @Column(name = "is_active")
    public Boolean getIsActive() {
        return isActive;
    }

    @ManyToOne
    @JoinColumn(name="company_id", nullable=false)
    public Company getCompany() {
        return company;
    }

    @OneToMany(mappedBy = "department", fetch=FetchType.EAGER, cascade = CascadeType.DETACH)
    @JsonIgnore
    public Set<Person> getPersons() {
        return persons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Department d = (Department) o;
        return this.departmentId.equals(d.departmentId);
    }
}