package com.pkit.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonCandidatePK implements java.io.Serializable {

	private Long personId;
	private Long candidateId;

    public PersonCandidatePK( ) {}

    public PersonCandidatePK(Long personId, Long candidateId) {
        this.personId = personId;
        this.candidateId = candidateId;
    }

    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof PersonCandidatePK)) return false;
        PersonCandidatePK pk = (PersonCandidatePK) obj;
        return personId.equals(pk.getPersonId()) && candidateId.equals(pk.getCandidateId());
    }

    public int hashCode() {
        return personId.hashCode() + 33*candidateId.hashCode();
    }   
}
