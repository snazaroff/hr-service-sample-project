package com.pkit.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@IdClass(UserRolePK.class)
@Table(name="user_role")
public class UserRole implements java.io.Serializable {

    public UserRole() {
    }

    public UserRole(User user, Role role) {
        this.user = user;
        this.userId = user.getUserId();
        this.role = role;
        this.roleId = role.getRoleId();
    }

    @Id
    @Column(name = "user_id", insertable = false, updatable = false)
    public Long userId;

    @Id
    @Column(name = "role_id", insertable = false, updatable = false)
    public String roleId;

    @ManyToOne
    @JoinColumn(name="user_id", nullable=false, insertable = false, updatable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name="role_id", nullable=false, insertable = false, updatable = false)
    private Role role;

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        Candidate c = ((UserRole) o).getCandidate();
//        if (c == null) return false;
//        Person p = ((UserRole) o).getPerson();
//        return p != null && this.candidateId.equals(c.getCandidateId()) && this.personId.equals(p.getPersonId());
//    }

    @Override
    public String toString() {
        return "UserRole{" +
                "userId=" + userId +
                ", roleId='" + roleId + '\'' +
                '}';
    }
}
