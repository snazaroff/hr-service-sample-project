package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pkit.Cacheable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name="education")
public class Education implements Serializable, Cacheable, Comparable<Education> {

    private Long educationId;

    @JsonProperty("created_by")
    private Person createdBy;

    @JsonProperty("faculity_name")
    private String faculityName;
    @JsonProperty("education_name")
    private String educationName;
    @JsonIgnore
    private Candidate candidate;
    private String specialization;
    @JsonProperty("start_date")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone = "Europe/Moscow")
    private LocalDate startDate;
    @JsonProperty("end_date")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone = "Europe/Moscow")
    private LocalDate endDate;
    @Enumerated(EnumType.ORDINAL)
    @JsonProperty("education_type")
    private EducationType educationType;

    @Id
    @Column(name = "education_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("education_id")
    public Long getEducationId() {
		return educationId;
	}

    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    public Person getCreatedBy() {
        return createdBy;
    }

    @Column(name = "education_name")
    public String getEducationName() {
        return educationName;
    }

    @Column(name = "faculity_name")
    public String getFaculityName() {
        return faculityName;
    }

    @Column(name = "specialization")
    public String getSpecialization() {
        return specialization;
    }

    @ManyToOne
    @JoinColumn(name="candidate_id", nullable=false)
    public Candidate getCandidate() {
        return candidate;
    }

    @Column(name = "start_date")
    public LocalDate getStartDate() {
        return startDate;
    }

    @Column(name = "end_date")
    public LocalDate getEndDate() {
        return endDate;
    }

    @Column(name = "education_type_id")
    public EducationType getEducationType() {
        return educationType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Education ea = (Education) o;
        return this.getEducationId().equals(ea.getEducationId());
    }

    @Override
    public int compareTo(@NotNull Education d) {
        if (null == startDate) return -1;
        if (null == d || null == d.getStartDate()) return 1;
        return startDate.compareTo(d.getStartDate());
    }

    @Override
    public String toString() {
        return "Education{" +
                "educationId=" + educationId +
                ", faculityName='" + faculityName + '\'' +
                ", educationName='" + educationName + '\'' +
                ", specialization='" + specialization + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", educationType=" + educationType +
                '}';
    }

    @Override
    public String toCacheString() {
        StringBuilder sb = new StringBuilder();
        if (faculityName != null && !faculityName.equals(""))
            sb.append(faculityName).append(' ');
        if (educationName != null && !educationName.equals(""))
            sb.append(educationName).append(' ');
        if (specialization != null && !specialization.equals(""))
            sb.append(specialization).append(' ');

        return sb.toString().trim();
    }
}