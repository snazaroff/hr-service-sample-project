package com.pkit.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Getter
@Setter
@Entity
@IdClass(PersonCandidatePK.class)
@Table(name="person_candidate")
public class PersonCandidate implements Serializable {

    public PersonCandidate() {
    }

    public PersonCandidate(Person person, Candidate candidate) {
        this.person = person;
        this.personId = person.getPersonId();
        this.candidate = candidate;
        this.candidateId = candidate.getCandidateId();
    }

    @Id
    @Column(name = "person_id", insertable = false, updatable = false)
    public Long personId;

    @Id
    @Column(name = "candidate_id", insertable = false, updatable = false)
    public Long candidateId;

    @ManyToOne
    @JoinColumn(name="person_id", nullable=false, insertable = false, updatable = false)
    private Person person;

    @ManyToOne
    @JoinColumn(name="candidate_id", nullable=false, insertable = false, updatable = false)
    private Candidate candidate;

    @Column(name = "create_date")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ssZ")
    private Date createDate;

    @Column(name = "status")
    private Short status;

    @Column(name = "note")
    private String note;

    @JsonProperty("create_date")
    public Date getCreateDate() {
        return createDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Candidate c = ((PersonCandidate) o).getCandidate();
        if (c == null) return false;
        Person p = ((PersonCandidate) o).getPerson();
        return p != null && this.candidateId.equals(c.getCandidateId()) && this.personId.equals(p.getPersonId());
    }

    @Override
    public String toString() {
        return "PersonCandidate{" +
                "personId=" + personId +
                ", candidateId=" + candidateId +
                ", createdBy=" + person +
                ", candidate=" + candidate +
                ", createDate=" + createDate +
                ", status=" + status +
                ", note='" + note + '\'' +
                '}';
    }
}
