package com.pkit.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@IdClass(PickedDemandPK.class)
@Table(name = "picked_demand")
public class PickedDemand implements Serializable {

    public PickedDemand() {
    }

    public PickedDemand(Long personId, Long demandId) {
        this.personId = personId;
        this.demandId = demandId;
    }

    @Id
    @Column(name = "person_id", insertable = false, updatable = false, nullable = false)
    public Long personId;

    @Id
    @Column(name = "demand_id", insertable = false, updatable = false, nullable = false)
    public Long demandId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PickedDemand pd = (PickedDemand) o;
        return pd.demandId != null && pd.getPersonId() != null && this.demandId.equals(pd.getDemandId()) && this.personId.equals(pd.getPersonId());
    }

    @Override
    public String toString() {
        return "PickedDemand{" +
                "personId=" + personId +
                ", demandId=" + demandId +
                '}';
    }
}