package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ROLES")
@Setter
@Getter
public class Role implements java.io.Serializable {

//    @Enumerated(EnumType.STRING)
//    @EmbeddedId
    @JsonProperty("role_id")
    private String roleId;
//    @JsonIgnore
//    private Set<User> users;

    public Role() {}

    public Role(String roleId) {
        this.roleId = roleId;
    }

    public Role(RoleType roleType) {
        this.roleId = (roleType != null) ? roleType.name() : null;
    }

    @Id
    @Column(name = "role_id")
    public String getRoleId() {
        return roleId;
    }

//    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
//    public Set<User> getUsers() {
//        return users;
//    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return Objects.equals(roleId, role.roleId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleId);
    }

    @Override
    public String toString() {
//        return "Role{" +
//                "roleId='" + roleId + '\'' +
//                '}';
        return roleId.toString();
    }
}