package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Objects;

@Slf4j
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonSerialize(using = MeetingType.Serializer.class)
@JsonDeserialize(using = MeetingType.Deserializer.class)
public enum MeetingType {

    PHONE(new Status((short)0, "Телефонное интервью")),
    HEAD_TO_HEAD(new Status((short)1, "Встреча")),
    TESTING(new Status((short)2, "Тестирование"));

    @JsonProperty
    private final Status status;

    MeetingType(Status status) {
        this.status = status;
    }

    public String getName() {
        return this.status.getName();
    }

    public Short getId() {
        return this.status.getStatusId();
    }

    public static Status getStatus(Short id) {

        if (id == null) {
            return null;
        }

        for (MeetingType status : MeetingType.values()) {
            if (id.equals(status.status.getStatusId())) {
                return status.status;
            }
        }
        throw new IllegalArgumentException("No matching type for id " + id);
    }

    public static class Serializer extends StdSerializer<MeetingType> {
        public Serializer() {
            super(MeetingType.class);
        }

        @Override
        public void serialize(MeetingType ds,
                              JsonGenerator jgen,
                              SerializerProvider sp) throws IOException {

            jgen.writeStartObject();
            jgen.writeNumberField("status_id", ds.status.getStatusId());
            jgen.writeStringField("name", ds.status.getName());

            jgen.writeEndObject();
        }
    }

    public static class Deserializer extends StdDeserializer<MeetingType> {
        public Deserializer() {
            super(MeetingType.class);
        }

        @Override
        public MeetingType deserialize(JsonParser jp, DeserializationContext dc) throws IOException {
            final JsonNode jsonNode = jp.readValueAsTree();
            short statusId = (short)jsonNode.get("status_id").asInt();

            for (MeetingType ds: MeetingType.values()) {
                if (Objects.equals(ds.status.getStatusId(), statusId)) {
                    return ds;
                }
            }
            throw dc.mappingException("Cannot deserialize Status from key " + statusId);
        }
    }
}