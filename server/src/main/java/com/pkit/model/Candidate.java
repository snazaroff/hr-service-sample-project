package com.pkit.model;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.*;
import java.util.*;

import com.fasterxml.jackson.annotation.*;
import com.pkit.Cacheable;
import com.pkit.Utils;
import lombok.Getter;
import lombok.Setter;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.*;
import org.hibernate.validator.constraints.NotEmpty;

@Slf4j
@Setter
@Getter
@Entity
@Table(name = "candidate")
@FilterDef(name = "pickedCandidate",
        parameters = @ParamDef(name = "currentPerson", type = "long"))
@JsonIgnoreProperties(ignoreUnknown = true)
public class Candidate extends Human implements java.io.Serializable, Cacheable {

    @JsonProperty("candidate_id")
    private Long candidateId;

    @NotNull
    private Company company;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private LocalDateTime createDate;

    @JsonProperty("created_by")
    private Person createdBy;

    @JsonProperty("birth_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "Europe/Moscow")
    private LocalDate birthDate;

    @JsonProperty("address")
    private String address;

    @JsonProperty("metro")
    private Metro metro;

    @NotEmpty(message = "Поле email не должно быть пустым")
    @Pattern(regexp = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$",
            message = "Формат email нарушен")
    @JsonProperty("email")
    private String email;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("cell")
    private String cell;

    @NotEmpty(message = "Поле position не должно быть пустым")
    @JsonProperty("position")
    private String position;

    @JsonProperty("salary")
    private Salary salary;

    @Enumerated(EnumType.ORDINAL)
    @JsonProperty("status")
    private CandidateStatus status;

    @Enumerated(EnumType.ORDINAL)
    @JsonProperty("sex")
    private SexStatus sex;
    private String degree;

    @JsonProperty("education")
    private Set<Education> educations = new HashSet<>();

    @JsonProperty("employment")
    private Set<Employment> employments = new HashSet<>();

//    @JsonIgnore
//    private Set<DemandCandidate> demands;

    @JsonIgnore
    private Set<PickedCandidate> pickeds;

    @JsonProperty("language")
    private Set<CandidateLanguage> languages = new HashSet<>();

    @JsonProperty("category")
    private Set<CandidateCategory> categories = new HashSet<>();

    @Id
    @Column(name = "candidate_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getCandidateId() {
        return candidateId;
    }

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    public Company getCompany() {
        return company;
    }

    @Column(name = "create_date", nullable = false, insertable = false, updatable = false)
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    public Person getCreatedBy() {
        return createdBy;
    }

    @Column(name = "first_name", nullable = false)
    public String getFirstName() {
        return firstName;
    }

    @Column(name = "middle_name")
    public String getMiddleName() {
        return middleName;
    }

    @Column(name = "last_name", nullable = false)
    public String getLastName() {
        return lastName;
    }

    @Column(name = "birth_date", nullable = false)
    public LocalDate getBirthDate() {
        return birthDate;
    }

    @Column(name = "status", nullable = false)
    public CandidateStatus getStatus() {
        return status;
    }

    @ManyToOne
    @JoinColumn(name = "metro_id")
    public Metro getMetro() {
        return metro;
    }

    @Column(name = "sex")
    public SexStatus getSex() {
        return sex;
    }

    @Column(name = "degree")
    public String getDegree() {
        return degree;
    }

//    @OneToMany(mappedBy = "candidate", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
//    public Set<DemandCandidate> getDemands() {
//        return demands;
//    }

    @OneToMany(mappedBy = "candidate", fetch = FetchType.EAGER, orphanRemoval = true)
    public Set<Education> getEducations() {
        return educations;
    }

    @OneToMany(mappedBy = "candidate", fetch = FetchType.EAGER, orphanRemoval = true)
    public Set<Employment> getEmployments() {
        return employments;
    }

    @OneToMany(mappedBy = "candidate", fetch = FetchType.EAGER, orphanRemoval = true)
    public Set<CandidateLanguage> getLanguages() {
        return languages;
    }

    @OneToMany(mappedBy = "candidate", fetch = FetchType.EAGER, orphanRemoval = true)
    public Set<CandidateCategory> getCategories() {
        return categories;
    }

    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "candidate_id")
    @Filters({
            @Filter(name = "pickedCandidate", condition = "person_id = :currentPerson")
    })
    public Set<PickedCandidate> getPickeds() {
        return pickeds;
    }

    public boolean isPicked(Long personId) {
        return pickeds.stream().anyMatch(f -> f.personId.equals(personId));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Candidate c = (Candidate) o;
        return this.candidateId.equals(c.candidateId);
    }

    @Transient
    public String getAge() {
        if (birthDate == null) return "";

        return (birthDate.isAfter(LocalDate.now()))
                ? Utils.yearToText(0)
                : Utils.yearToText(LocalDate.now().getYear() - birthDate.getYear());
    }

    @PrePersist
    private void prePersist(){
        createDate = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return "Candidate{" +
                "candidateId=" + candidateId +
                ", createDate=" + createDate +
                ", createdBy=" + createdBy +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", address='" + address + '\'' +
                ", metro=" + metro +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", cell='" + cell + '\'' +
                ", position='" + position + '\'' +
                ", salary=" + salary +
                ", status=" + status +
                ", sex=" + sex +
                ", degree='" + degree + '\'' +
                ", educations=" + educations +
                ", employments=" + employments +
                ", pickeds=" + pickeds +
                ", languages=" + languages +
                ", categories=" + categories +
                '}';
    }

    @Override
    public String toCacheString() {

        StringBuilder sb = new StringBuilder();
        sb.append(getFio()).append(' ');
        if (address != null)
            sb.append(address).append(' ');
        if (email != null)
            sb.append(email).append(' ');
        if (position != null)
            sb.append(position).append(' ');
        if (languages != null)
            sb.append(Utils.toNormalize(languages)).append(' ');
        if (educations != null)
            sb.append(Utils.toNormalize(educations)).append(' ');
        if (educations != null)
            sb.append(Utils.toNormalize(educations)).append(' ');
        return sb.toString().trim();
    }
}