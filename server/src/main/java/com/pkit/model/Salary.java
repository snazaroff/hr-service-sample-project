package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Setter
@Getter
@Embeddable
public class Salary implements Serializable {

    @JsonProperty("salary_amount")
    private long amount;
    @JsonProperty("salary_currency")
    private Currency currency = Currency.RUB;

    public Salary() { }

    public Salary(long amount, Currency currency) {
        this.amount = amount;
        this.currency = currency;
    }

    @Column(name = "salary_amount")
    public long getAmount() {
        return amount;
    }

    @Column(name = "salary_currency")
    public Currency getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return "Salary{" +
                "amount=" + amount +
                ", currency=" + currency +
                '}';
    }
}
