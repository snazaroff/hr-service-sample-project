package com.pkit.model;

public enum RoleType {

    ROLE_USER,
    ROLE_MANAGER,
    ROLE_ADMIN,

    ROLE_OWNER_ADMIN,
    ROLE_EA_ADMIN,
    ROLE_EA_MANAGER,
    ROLE_EA_USER;
}