package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.*;

import static org.springframework.data.elasticsearch.annotations.FieldType.Date;

@Slf4j
@Setter
@Getter
@ToString
@Document(indexName = "index", type = "candidate")
@Setting(settingPath = "/es/settings.json")
@Mapping(mappingPath = "/es/candidate_mappings.json")
public class CandidateIndex {

    @Id
    private String candidateId;

    private String data;

    @JsonProperty("create_date")
    @Field(type = Date)
    @JsonFormat (shape = JsonFormat.Shape.STRING, pattern ="yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
    private java.util.Date createDate = new java.util.Date();
}
