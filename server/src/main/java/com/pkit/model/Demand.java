package com.pkit.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.pkit.Cacheable;
import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.validator.constraints.NotEmpty;

@Slf4j
@Getter
@Setter
@Entity
@Table(name = "demand")
@FilterDef(name = "pickedDemand",
        parameters = @ParamDef(name = "currentPerson", type = "long"))
public class Demand implements Serializable, Cacheable, Comparable<Demand> {

    @JsonProperty("demand_id")
    private Long demandId;

    @NotNull
    @JsonProperty("company")
    @JsonUnwrapped
    private Company company;

    @JsonProperty("contact_person")
    private Person contactPerson;

    @JsonProperty("responsible_person")
    private Person responsiblePerson;

    @JsonProperty("create_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDateTime createDate;

    @JsonProperty("created_by")
    private Person createdBy;

    @JsonProperty("finish_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate finishDate;

    @JsonProperty("salary")
    private Salary salary;

    @NotEmpty(message = "Поле position не должно быть пустым")
    @JsonProperty("position")
    private String position;

    @Enumerated(EnumType.ORDINAL)
    @JsonProperty("status")
    private DemandStatus status;

    @JsonProperty("description")
    private String description;

//    @JsonIgnore
//    private Set<DemandCandidate> candidates;

    @JsonIgnore
    private Set<PickedDemand> pickeds;

    public Demand() {
        this.setStatus(DemandStatus.CREATE);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "demand_id", unique = true, nullable = false)
    public Long getDemandId() {
        return demandId;
    }

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false, updatable = false)
    public Company getCompany() {
        return company;
    }

    @ManyToOne
    @JoinColumn(name = "contact_person_id")
    public Person getContactPerson() {
        return contactPerson;
    }

    @ManyToOne
    @JoinColumn(name = "responsible_person_id")
    public Person getResponsiblePerson() {
        return responsiblePerson;
    }

    @Column(name = "create_date", nullable = false, insertable = false, updatable = false)
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    public Person getCreatedBy() {
        return createdBy;
    }

    @Column(name = "finish_date")
    public LocalDate getFinishDate() {
        return finishDate;
    }

    @Column(name = "salary")
    public Salary getSalary() {
        return salary;
    }

    @Column(name = "position")
    public String getPosition() {
        return position;
    }

    @Column(name = "status")
    public DemandStatus getStatus() {
        return status;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

//    @OneToMany(mappedBy = "demand", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
//    public Set<DemandCandidate> getCandidates() {
//        return candidates;
//    }

    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "demand_id")
    @Filters({
            @Filter(name = "pickedDemand", condition = "person_id = :currentPerson")
    })
    public Set<PickedDemand> getPickeds() {
        return pickeds;
    }

    public boolean isPicked(Long personId) {
        return pickeds.stream().anyMatch(f -> f.personId.equals(personId));
    }

    @PrePersist
    private void prePersist() {
        createDate = LocalDateTime.now();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Demand d = (Demand) o;
        return this.demandId.equals(d.demandId);
    }

    @Override
    public int compareTo(@NotNull Demand d) {
        if (null == createDate) return -1;
        if (null == d || null == d.getCreateDate()) return 1;
        return createDate.compareTo(d.getCreateDate());
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder()
                .append("Demand: demandId: ").append(demandId).append("\n")
                .append("        company: ").append(company).append("\n")
                .append("        contactPerson: ").append(contactPerson).append("\n")
                .append("        responsiblePerson: ").append(responsiblePerson).append("\n")
                .append("        createdBy: ").append(createdBy).append("\n")
                .append("        createDate: ").append(createDate).append("\n")
                .append("        salary: ").append(salary).append("\n")
                .append("        position: ").append(position).append("\n")
                .append("        status: ").append(status).append("\n")
                .append("        description: ").append(description).append("\n");
        return result.toString();
    }

    @Override
    public String toCacheString() {
        StringBuilder sb = new StringBuilder();
        if (company != null)
            sb.append(company.getName()).append(' ');
        if (contactPerson != null)
            sb.append(contactPerson.getFio()).append(' ');
        if (responsiblePerson != null)
            sb.append(responsiblePerson.getFio()).append(' ');
        if (position != null)
            sb.append(position).append(' ');
        if (description != null)
            sb.append(description).append(' ');
        return sb.toString().trim();
    }
}