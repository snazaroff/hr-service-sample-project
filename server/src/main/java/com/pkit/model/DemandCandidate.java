package com.pkit.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Entity
@Table(name="demand_candidate")
public class DemandCandidate extends Jsonable implements Comparable<DemandCandidate>, Serializable {

    @JsonProperty("demand_candidate_id")
    private Long demandCandidateId;
    @JsonProperty("demand")
    private Demand demand;
    @JsonProperty("candidate")
    private Candidate candidate;

    @JsonProperty("create_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;

    @JsonProperty("created_by")
    private Person createdBy;

    @Enumerated(EnumType.ORDINAL)
    @JsonProperty("status")
    private DemandCandidateStatus status = DemandCandidateStatus.PICKED;

    @JsonProperty("meetings")
    private Set<DemandCandidateMeeting> meetings = new TreeSet<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "demand_candidate_id", unique = true, nullable = false)
    public Long getDemandCandidateId() {
        return demandCandidateId;
    }

    @ManyToOne(cascade={CascadeType.MERGE})
    @JoinColumn(name = "demand_id", nullable = false, updatable = false)
    public Demand getDemand() {
        return demand;
    }

    @ManyToOne(cascade={CascadeType.MERGE})
    @JoinColumn(name = "candidate_id", nullable = false, updatable = false)
    public Candidate getCandidate() {
        return candidate;
    }

    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    public Person getCreatedBy() {
        return createdBy;
    }

    @Column(name = "create_date", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME NOT NULL DEFAULT SYSTIMESTAMP")
    public LocalDateTime getCreateDate() {
        return createDate;
    }

//    @OrderBy("createDate DESC")
//    @OneToMany(mappedBy = "demandCandidate", fetch = FetchType.EAGER)//, orphanRemoval = true, cascade = {CascadeType.ALL})
//    public Set<DemandCandidateNote> getNotes() {
//        return notes;
//    }

    @OrderBy("meetingDateTime DESC")
    @OneToMany(mappedBy = "demandCandidate", fetch = FetchType.EAGER)
    public Set<DemandCandidateMeeting> getMeetings() {
        return meetings;
    }

    @Column(name = "status")
    public DemandCandidateStatus getStatus() {
        return status;
    }

    @PrePersist
    private void prePersist(){
        createDate = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return "DemandCandidate{" +
                "demandCandidateId=" + demandCandidateId +
                ", demand=" + demand +
                ", candidate=" + candidate +
                ", createDate=" + createDate +
                ", createdBy=" + createdBy +
                ", status=" + status +
                ", meetings=" + meetings +
                '}';
    }

    @Override
    public int compareTo(@NotNull DemandCandidate o) {

        if (Objects.equals(demand, o.demand)) {
            return candidate.compareTo(o.candidate);
        } else {
            return demand.compareTo(o.demand);
        }
    }
}