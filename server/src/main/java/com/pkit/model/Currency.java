package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@ToString
public enum Currency {

    RUB(new Status((short)0, "RUB"), "643"),
    USD(new Status((short)1, "USD"), "840"),
    EUR(new Status((short)3, "EUR"), "978");

    @JsonProperty
    private final Status status;
    private final String code;

    Currency(Status status, String code) {
        this.status = status;
        this.code = code;
    }

    public static Status getStatus(Short id) {

        if (id == null) {
            return null;
        }

        for (Currency status : Currency.values()) {
            if (id.equals(status.status.getStatusId())) {
                return status.status;
            }
        }
        throw new IllegalArgumentException("No matching type for id " + id);
    }

    public static Currency getByName(String name) {

        if (name == null || name.equals("")) {
            return Currency.RUB;
        }

        for (Currency currency : Currency.values()) {
            if (name.equals(currency.status.getName())) {
                return currency;
            }
        }
        return Currency.RUB;
    }

    public Short getStatusId() {
        return status.getStatusId();
    }

    public String getName() {
        return status.getName();
    }
}