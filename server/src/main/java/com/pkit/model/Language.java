package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@ToString
@Table(name="language")
public class Language implements Serializable {

    private Language() {}

    @JsonProperty("language_id")
    private Long languageId;
    private String name;

    @Id
    @Column(name = "language_id")
    public Long getLanguageId() {
        return languageId;
    }
}
