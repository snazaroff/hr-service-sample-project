package com.pkit.model;

import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
@ToString
@Entity
@Table(name="town")
public class Town implements java.io.Serializable {

    @JsonProperty("town_id")
    private Long townId;
    @JsonProperty("region_id")
    private Long regionId;
    @JsonProperty("country_id")
    private Long countryId;
    @JsonProperty("name")
    private String name;

    @Id
    @Column(name = "town_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getTownId() {
        return townId;
    }

    @Column(name = "region_id")
    public Long getRegionId() {
        return regionId;
    }

    @Column(name = "country_id")
    public Long getCountryId() {
        return countryId;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }
}
