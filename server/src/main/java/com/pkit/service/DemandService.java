package com.pkit.service;

import com.pkit.model.*;
import com.pkit.model.filter.DemandFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DemandService extends BaseService<Demand, Long> {

    List<Demand> getAll();

    Page<Demand> getAll(Pageable pageable);

    List<Demand> getAll(Long personId);

    List<Demand> findByFilter(DemandFilter demandFilter);

    Page<Demand> findByFilter(DemandFilter demandFilter, Pageable pageable);

    List<Demand> getByCompanyId(Long companyId);

    Page<Demand> getByCompanyId(Long companyId, Pageable pageable);

    List<Demand> getByCompany(Company company);

    Page<Demand> getByCompany(Company company, Pageable pageable);

    List<Demand> getByDepartmentId(Long departmentId);

    Page<Demand> getByDepartmentId(Long departmentId, Pageable pageable);

    List<Demand> getByDepartment(Department department);

    Page<Demand> getByDepartment(Department department, Pageable pageable);

    List<Demand> search(String term);
}