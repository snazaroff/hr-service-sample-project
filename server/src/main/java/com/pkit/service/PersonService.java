package com.pkit.service;

import com.pkit.model.Person;
import com.pkit.model.filter.PersonFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PersonService extends BaseService<Person, Long> {

    List<Person> getAll();

    Page<Person> getAll(Pageable pageable);

    List<Person> findByFilter(PersonFilter personFilter);

    Page<Person> findByFilter(PersonFilter personFilter, Pageable pageable);

    List<Person> getByCompanyId(Long companyId);

    Page<Person> getByCompanyId(Long companyId, Pageable pageable);

    List<Person> getByDepartmentId(Long departmentId);

    Page<Person> getByDepartmentId(Long departmentId, Pageable pageable);
}