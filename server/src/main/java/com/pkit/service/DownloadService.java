package com.pkit.service;

import com.pkit.exception.ReportTypeException;
import com.pkit.model.*;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.*;
import java.util.*;

@Slf4j
@Service
@Transactional
public class DownloadService {

    @Autowired
    private DataSource dataSource;

    @Setter
    @Getter
    public static class Result {
        ResumeOutputType resumeOutputType;
        byte[] file;
    }

    public Result download(Long candidateId, String type) throws Exception {
        log.debug("download: candidateId: {}; type: {}", candidateId, type);
        ResumeOutputType resumeOutputType = ResumeOutputType.getOutputType(type);
        log.debug("resumeOutputType: {}", resumeOutputType);
        if (resumeOutputType == null) {
            throw new Exception("Wrong resume output type: " + type);
        }

        byte[] jasperXml = JRLoader.loadBytesFromResource("resume.jrxml");
        log.debug("jasperXml: {}", jasperXml.length);

        JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(jasperXml));

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("CANDIDATE_ID", candidateId);

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource.getConnection());

        Exporter exporter;

        if (resumeOutputType == ResumeOutputType.PDF)
            exporter = new JRPdfExporter();
        else if (resumeOutputType == ResumeOutputType.DOCX)
            exporter = new JRDocxExporter();
        else if (resumeOutputType == ResumeOutputType.TXT)
            exporter = new JRTextExporter();
        else if (resumeOutputType == ResumeOutputType.HTML)
            exporter = new HtmlExporter();
        else
            throw new ReportTypeException();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(baos));
        exporter.exportReport();

        Result result = new Result();
        result.setFile(baos.toByteArray());
        result.setResumeOutputType(resumeOutputType);
        return result;
    }
}