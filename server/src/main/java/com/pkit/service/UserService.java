package com.pkit.service;

import com.pkit.model.Person;
import com.pkit.model.User;
import com.pkit.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.debug("loadUserByUsername( username: {})", username);

        User user = userRepository.findByLogin(username);
        return new UserDetailsEx(user);
    }

    public User add(User user) {
        log.debug("add( user: {})", user);
        return userRepository.save(user);
    }

    public List<User> getAll() {
        log.debug("getAll()");

        return userRepository.findAll(new Sort("login"));
    }

    public User getUserByPerson(Person person) {
        log.debug("getUserByPerson(person: {})", person);

        return userRepository.findByPerson(person);
    }

    public static class UserDetailsEx implements UserDetails {

        private final User user;

        public UserDetailsEx(User user) {
            this.user = user;
        }

        public User getUser() {
            return user;
        }

        public Person getPerson() {
            return user.getPerson();
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
//            log.debug("getAuthorities()");

            if (user.getRoles() == null || user.getRoles().size() == 0) {
                return Collections.EMPTY_LIST;
            } else {
//                List roles = user.getRoles().stream().map(m -> (GrantedAuthority) () -> m.getRoleId()).collect(Collectors.toList());
                List<SimpleGrantedAuthority> roles = user.getRoles().stream().map(m -> new SimpleGrantedAuthority(m.getRoleId())).collect(Collectors.toList());
//                log.debug("Roles: {}", user.getRoles());
//                log.debug("Roles: {}", roles.size());
//                log.debug("Roles: {}", roles);
//                if(roles.size() > 0)
//                    log.debug("Roles: {}", roles.get(0).getAuthority());
//                log.debug("Roles: {}", roles.get(0).getClass().getName());
//                return user.getRoles().stream().map(m -> (GrantedAuthority) () -> m.getRoleId()).collect(Collectors.toList());
                return user.getRoles().stream().map(m -> new SimpleGrantedAuthority(m.getRoleId())).collect(Collectors.toList());
            }
        }

        @Override
        public String getPassword() {
            return user == null ? null : user.getPassword();
        }

        @Override
        public String getUsername() {
            return user.getLogin();
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }

        public String toString() {

            if (user == null)
                return "UserDetails: null";
            else {
                StringBuilder sb = new StringBuilder();
                sb.append("UserDetails: userId: ").append(user.getUserId()).append("\n");
                sb.append("       person: ").append(user.getPerson()).append("\n");
                sb.append("       username: ").append(this.getUsername()).append("\n");
                sb.append("       password: ").append(user.getPassword()).append("\n");
                sb.append("       isAccountNonExpired: ").append(this.isAccountNonExpired()).append("\n");
                sb.append("       isAccountNonLocked: ").append(this.isAccountNonLocked()).append("\n");
                sb.append("       isCredentialsNonExpired: ").append(this.isCredentialsNonExpired()).append("\n");
                sb.append("       isEnabled: ").append(this.isEnabled()).append("\n");

                sb.append("Roles:\n");
                for (GrantedAuthority grantedAuthority : this.getAuthorities()) {
                    sb.append(grantedAuthority.getAuthority()).append("\n");
                }

                return sb.toString();
            }
        }
    }
}