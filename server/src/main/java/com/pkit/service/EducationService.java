package com.pkit.service;

import com.pkit.model.Education;

public interface EducationService extends BaseService<Education, Long> { }