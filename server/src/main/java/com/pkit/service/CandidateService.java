package com.pkit.service;

import com.pkit.model.*;
import com.pkit.model.filter.CandidateFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CandidateService extends BaseService<Candidate, Long> {

    List<Candidate> getAll();

    Page<Candidate> getAll(Pageable pageable);

    List<Candidate> findByFilter(CandidateFilter candidateFilter);

    //todo: поправить фильтр с учетом зависимости кандидата от компании
    Page<Candidate> findByFilter(CandidateFilter candidateFilter, Pageable pageable);

    List<Category> getCategoryList(Long candidateId);

    List<Category> getCategoryListWithTopCategory(Long candidateId);

    List<Category> deleteCategory(Long candidateId, Long categoryId);

    Category addCategory(Long candidateId, Long categoryId);

    List<Category> addBatchCategory(Long candidateId, List<Long> categoryIds);

    List<Candidate> search(String term);

    List<Candidate> getByCompanyId(Long companyId);

    Page<Candidate> getByCompanyId(Long companyId, Pageable pageable);

    List<Candidate> getByCompany(Company company);

    Page<Candidate> getByCompany(Company company, Pageable pageable);
}