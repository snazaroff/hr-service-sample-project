package com.pkit.service;

import com.pkit.model.*;
import com.pkit.repository.PhotoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;

@Slf4j
@Service
@Transactional
public class PhotoService {

    final private PhotoRepository photoRepository;
    final private FileStoreService fileStoreService;

    public PhotoService(FileStoreService fileStoreService, PhotoRepository photoRepository) {
        this.fileStoreService = fileStoreService;
        this.photoRepository = photoRepository;
    }

    public List<Photo> getByCandidate(Long candidateId) {
        log.debug("getByCandidate(candidateId: {})", candidateId);
        return photoRepository.findByCandidateId(candidateId);
    }

    public Photo get(Long photoId) {
        log.debug("get(photoId: {})", photoId);
        return photoRepository.findOne(photoId);
    }

    public byte[] getPhotoContent(Long photoId) throws IOException {
        log.debug("getPhotoContent(photoId: {})", photoId);
        return fileStoreService.getFileContent(photoId);
    }

    public Photo add(Photo photo, String fileName, byte[] photoContent) throws IOException {
        log.debug("add(candidateId: {})", photo.getCandidateId());

        FileInfo fileInfo = new FileInfo();
        fileInfo.setName(fileName);
        fileInfo.setContentType("image/jpeg");
        fileInfo.setOwnerId(photo.getCandidateId());

        fileInfo = fileStoreService.addFile(fileInfo, photoContent);
        photo.setPhotoId(fileInfo.getFileId());
        return photoRepository.save(photo);
    }

    public void delete(Long photoId) {
        log.debug("delete(photoId: {})", photoId);
        photoRepository.delete(photoId);
        fileStoreService.deleteByFileId(photoId);
    }

    public void deleteAll(Long candidateId) {
        log.debug("deleteAll(candidateId: {})", candidateId);
        photoRepository.deleteByCandidateId(candidateId);
        fileStoreService.deleteByOwnerId(candidateId);
    }

    public Photo setMain(Long photoId) {
        Photo photo = photoRepository.findOne(photoId);
        List<Photo> photos = photoRepository.findByCandidateId(photo.getCandidateId());
        for (Photo p : photos) {
            if (p.isMain()) {
                p.setMain(false);
                photoRepository.save(p);
            }
        }
        photo.setMain(true);
        photo = photoRepository.save(photo);
        return photo;
    }
}