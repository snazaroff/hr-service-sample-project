package com.pkit.service;

import com.pkit.model.*;

public interface DemandCandidateMeetingNoteService extends BaseService<DemandCandidateMeetingNote, Long> {
}