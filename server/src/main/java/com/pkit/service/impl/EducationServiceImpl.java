package com.pkit.service.impl;

import com.pkit.model.Education;
import com.pkit.repository.EducationRepository;
import com.pkit.service.EducationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EducationServiceImpl extends BaseServiceImpl<Education, Long> implements EducationService {

    private final EducationRepository educationRepository;

    public EducationServiceImpl(EducationRepository educationRepository) {
        this.educationRepository = educationRepository;
    }

    public List<Education> getByCandidateId(Long candidateId) {
        return educationRepository.findByCandidateId(candidateId);
    }
}