package com.pkit.service.impl;

import com.pkit.model.event.Event;
import com.pkit.service.HistoryService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class HistoryServiceImpl implements HistoryService {
	public List<Event> get(Long id) {
		return new ArrayList<>();
	}

	public List<Event> get(Long id, Short count) {
		return new ArrayList<>();
	}

	public List<Event> getLast(Long id) {
		return new ArrayList<>();
	}

	public List<Event> getLast(Long id, Short count) {
		return new ArrayList<>();
	}
}