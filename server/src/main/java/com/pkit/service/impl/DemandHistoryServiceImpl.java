package com.pkit.service.impl;

import com.pkit.exception.DemandNotFoundException;
import com.pkit.model.*;
import com.pkit.model.event.*;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DemandHistoryServiceImpl implements DemandHistoryService {

    private final static short MAX_RECORD = 10000;

    private final DemandService demandService;
    private final DemandNoteService demandNoteService;
    private final DemandCandidateService demandCandidateService;
    private final DemandCandidateNoteService demandCandidateNoteService;

    public DemandHistoryServiceImpl(
            DemandService demandService,
            DemandNoteService demandNoteService,
            DemandCandidateService demandCandidateService,
            DemandCandidateNoteService demandCandidateNoteService
    ) {
        this.demandService = demandService;
        this.demandNoteService = demandNoteService;
        this.demandCandidateService = demandCandidateService;
        this.demandCandidateNoteService = demandCandidateNoteService;
    }

    public List<Event> get(Long demandId) {
        log.debug("get(demandId: {})", demandId);

        return get(demandId, (short) 0);
    }

    public List<Event> get(Long demandId, Short count) {
        log.debug("getLast(demandId: {}, count: {})", demandId, count);

        List<Event> result = getBasic(demandId, count, true);
        Collections.sort(result);
        return (count == 0) ? result : result.stream().limit(count).collect(Collectors.toList());
    }

    public List<Event> getLast(Long demandId) {
        log.debug("getLast(demandId: {})", demandId);

        return getLast(demandId, (short) 0);
    }

    public List<Event> getLast(Long demandId, Short count) {
        log.debug("getLast(demandId: {}, count: {})", demandId, count);

        List<Event> result = getBasic(demandId, count, false);
        Collections.reverse(result);
        return (count == 0) ? result : result.stream().limit(count).collect(Collectors.toList());
    }

    private List<Event> getBasic(Long demandId, Short count, boolean forwardDirection) {
        log.debug("getBasic(demandId: {}, count: {}, forwardDirection: {})", demandId, count, forwardDirection);

        Demand demand = demandService.get(demandId);

        if (demand == null) throw new DemandNotFoundException(demandId);

        List<Event> result = new ArrayList<>();
        result.add(new CreateDemandEvent(demand));

        List<DemandCandidate> demandCandidates = demandCandidateService.getByDemandIdAndLimitAndSort(
                demandId, (count > 0) ? count : MAX_RECORD, forwardDirection);
        log.debug("demandCandidates: {}", demandCandidates);

        for (DemandCandidate demandCandidate : demandCandidates) {
            result.add(new AddDemandCandidateEvent(demandCandidate));
            result.addAll(demandCandidateNoteService.findByDemandCandidate(demandCandidate).stream().map(demandCandidateNote -> new AddDemandCandidateNoteEvent(demandCandidate, demandCandidateNote)).collect(Collectors.toList()));
            for (DemandCandidateMeeting demandCandidateMeeting : demandCandidate.getMeetings()) {
                result.add(new AddDemandCandidateMeetingEvent(demandCandidate, demandCandidateMeeting));
                result.addAll(demandCandidateMeeting.getNotes().stream().map(demandCandidateMeetingNote -> new AddDemandCandidateMeetingNoteEvent(demandCandidate, demandCandidateMeeting, demandCandidateMeetingNote)).collect(Collectors.toList()));
            }
        }

        List<DemandNote> demandNotes = demandNoteService.getByDemandIdAndLimitAndSort(
                demandId, (count > 0) ? count : MAX_RECORD, forwardDirection);
        log.debug("demandNotes: {}", demandNotes);

        result.addAll(demandNotes.stream().map(AddDemandNoteEvent::new).collect(Collectors.toList()));
        return result;
    }
}