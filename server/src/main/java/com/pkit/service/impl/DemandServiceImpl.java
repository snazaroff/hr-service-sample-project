package com.pkit.service.impl;

import com.pkit.model.*;
import com.pkit.model.filter.DemandFilter;
import com.pkit.repository.DemandRepository;
import com.pkit.service.*;
import com.querydsl.core.BooleanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.*;

@Slf4j
@Service
@Transactional
public class DemandServiceImpl extends BaseServiceImpl<Demand, Long> implements DemandService {

    private final CompanyService companyService;
    private final DemandRepository demandRepository;
    private final DepartmentService departmentService;

    public DemandServiceImpl(
            CompanyService companyService,
            DemandRepository demandRepository,
            DepartmentService departmentService
    ) {
        this.companyService = companyService;
        this.demandRepository = demandRepository;
        this.departmentService = departmentService;
    }

    @Override
    public List<Demand> getAll() {
        log.debug("getAll()");

        return demandRepository.findAll();
    }

    @Override
    public Page<Demand> getAll(Pageable pageable) {
        log.debug("getAll(pageable: {})", pageable);

        return demandRepository.findAll(pageable);
    }

    @Override
    public List<Demand> getAll(Long personId) {
        log.debug("getAll(personId: {})", personId);
        return demandRepository.getAll(personId);
    }

    public List<Demand> findByFilter(DemandFilter demandFilter) {
        log.debug("findByFilter: {}", demandFilter);

        QDemand demand = QDemand.demand;
        BooleanBuilder builder = new BooleanBuilder();

        if (demandFilter.getPosition() != null && !demandFilter.getPosition().trim().equals("")) {
            builder.and(demand.position.like('%' + demandFilter.getPosition().trim() + '%'));
        }
        if (demandFilter.getCustomerId() != null) {
            builder.and(demand.company.companyId.eq(demandFilter.getCustomerId()));
        }
        return demandRepository.findAll(builder.getValue(), new QPageRequest(0, 1000)).getContent();
    }

    @Override
    public Page<Demand> findByFilter(DemandFilter demandFilter, Pageable pageable) {
        QDemand demand = QDemand.demand;
        BooleanBuilder builder = new BooleanBuilder();

        if (demandFilter.getPosition() != null && !demandFilter.getPosition().trim().equals("")) {
            builder.and(demand.position.like('%' + demandFilter.getPosition().trim() + '%'));
        }
        if (demandFilter.getCustomerId() != null) {
            builder.and(demand.company.companyId.eq(demandFilter.getCustomerId()));
        }
        return demandRepository.findAll(builder.getValue(), pageable);
    }

    @Override
    public List<Demand> getByCompanyId(Long companyId) {
        return getByCompany(companyService.get(companyId));
    }

    @Override
    public Page<Demand> getByCompanyId(Long companyId, Pageable pageable) {
        return getByCompany(companyService.get(companyId), pageable);
    }

    @Override
    public List<Demand> getByCompany(Company company) {
        return demandRepository.findByCompany(company);
    }

    @Override
    public Page<Demand> getByCompany(Company company, Pageable pageable) {
        return demandRepository.findByCompany(company, pageable);
    }

    @Override
    public List<Demand> getByDepartmentId(Long departmentId) {
        return getByDepartment(departmentService.get(departmentId));
    }

    @Override
    public Page<Demand> getByDepartmentId(Long departmentId, Pageable pageable) {
        return getByDepartment(departmentService.get(departmentId), pageable);
    }

    @Override
    public List<Demand> getByDepartment(Department department) {
        log.debug("getByDepartment( department: {})", department);
        return demandRepository.findByDepartmentId(department.getDepartmentId());
    }

    @Override
    public Page<Demand> getByDepartment(Department department, Pageable pageable) {
        log.debug("getByDepartment( department: {})", department);
        return demandRepository.findByDepartmentId(department.getDepartmentId(), pageable);
    }

    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Demand> search(String searchString) {
        log.debug("search({})" + searchString);

        if (searchString == null || searchString.trim().equals(""))
            return Collections.EMPTY_LIST;

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Demand> q = cb.createQuery(Demand.class);
        Root<Demand> demand = q.from(Demand.class);

        Path<String> position = demand.get("position");

        String[] terms = searchString.split(" ");

        List<Predicate> predicates = new ArrayList<>();

        for (String term : terms) {
            predicates.add(cb.like(cb.lower(position), cb.lower(cb.literal("%" + term + "%"))));
        }

        q.select(demand).where(
                cb.or(predicates.toArray(new Predicate[predicates.size()]))
        );
        return entityManager.createQuery(q).getResultList();
    }
}