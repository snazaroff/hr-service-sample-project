package com.pkit.service.impl;

import com.pkit.model.Employment;
import com.pkit.repository.EmploymentRepository;
import com.pkit.service.EmploymentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EmploymentServiceImpl extends BaseServiceImpl<Employment, Long> implements EmploymentService {

    private final EmploymentRepository employmentRepository;

    public EmploymentServiceImpl(EmploymentRepository employmentRepository) {
        this.employmentRepository = employmentRepository;
    }

    public List<Employment> getByCandidateId(Long candidateId) {
        return employmentRepository.findByCandidateId(candidateId);
    }
}