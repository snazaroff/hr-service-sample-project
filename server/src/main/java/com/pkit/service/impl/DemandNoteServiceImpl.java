package com.pkit.service.impl;

import com.pkit.model.DemandNote;
import com.pkit.repository.DemandNoteRepository;
import com.pkit.service.DemandNoteService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DemandNoteServiceImpl extends BaseServiceImpl<DemandNote, Long> implements DemandNoteService {

    private final DemandNoteRepository demandNoteRepository;

    public DemandNoteServiceImpl(DemandNoteRepository demandNoteRepository) {
        this.demandNoteRepository = demandNoteRepository;
    }

    public List<DemandNote> getByDemandId(Long demandId) {
        return demandNoteRepository.findByDemandId(demandId);
    }

    @Override
    public List<DemandNote> getByDemandIdAndLimit(Long demandId, Short limit) {
        return demandNoteRepository.findByDemandIdAndLimit(demandId, limit);
    }

    @Override
    public List<DemandNote> getByDemandIdAndLimitAndSort(Long demandId, Short limit, boolean forwardDirection) {
        if (forwardDirection) {
            return demandNoteRepository.findByDemandIdAndLimitAndSortAsc(demandId, limit);
        } else {
            return demandNoteRepository.findByDemandIdAndLimitAndSortDesc(demandId, limit);
        }
    }
}