package com.pkit.service.impl;

import com.pkit.exception.CompanyNotFoundException;
import com.pkit.model.*;
import com.pkit.model.filter.CompanyFilter;
import com.pkit.repository.CompanyRepository;
import com.pkit.repository.DemandRepository;
import com.pkit.service.CompanyService;
import com.querydsl.core.BooleanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Slf4j
@Service
@Transactional
public class CompanyServiceImpl extends BaseServiceImpl<Company, Long> implements CompanyService {

    private final CompanyRepository companyRepository;
    private final DemandRepository demandRepository;

    public CompanyServiceImpl(CompanyRepository customerRepository, DemandRepository demandRepository) {
        this.companyRepository = customerRepository;
        this.demandRepository = demandRepository;
    }

    @Override
    public List<Company> getAll() {
        log.debug("getAll()");

        return companyRepository.findAll();
    }

    @Override
    public Page<Company> getAll(Pageable pageable) {
        log.debug("getAll(pageable: " + pageable + ")");

        return companyRepository.findAll(pageable);
    }

    public List<Company> findByFilter(CompanyFilter companyFilter) {
        log.debug("findByFilter( companyFilter: " + companyFilter + ")");

        QCompany company = QCompany.company;
        BooleanBuilder builder = new BooleanBuilder();

        if (companyFilter.getName() != null && !companyFilter.getName().trim().equals("")) {
            builder.and(company.name.like('%' + companyFilter.getName().trim() + '%'));
        }
        return companyRepository.findAll(builder.getValue(), new QPageRequest(0, 1000)).getContent();
    }

    public Page<Company> findByFilter(CompanyFilter companyFilter, Pageable pageable) {
        log.debug("findByFilter( companyFilter: {}, pageable: {})", companyFilter, pageable);

        QCompany company = QCompany.company;
        BooleanBuilder builder = new BooleanBuilder();

        if (companyFilter.getName() != null && !companyFilter.getName().trim().equals("")) {
            builder.and(company.name.like('%' + companyFilter.getName().trim() + '%'));
        }
        return companyRepository.findAll(builder.getValue(), pageable);
    }

    @Override
    public List<Demand> getDemands(Long customerId) {
        log.debug("getDemands(customerId: {})", customerId);
        Company company = companyRepository.findOne(customerId);
        if (company == null) throw new CompanyNotFoundException(customerId);
        return demandRepository.findByCompany(company);
    }

    @Override
    public List<Company> getCustomers() {
        log.debug("getCustomers()");
        return companyRepository.findByCompanyType(CompanyType.CUSTOMER);
    }

    @Override
    public Page<Company> getCustomers(Pageable pageable) {
        log.debug("getCustomers()");
        return companyRepository.findByCompanyType(CompanyType.CUSTOMER, pageable);
    }

    @Override
    public Page<Company> getCustomerByManagerCompanyId(Long managerCompanyId, Pageable pageable) {
        log.debug("getCustomersByCompanyId(managerCompanyId: {})", managerCompanyId);
        return companyRepository.getCustomerByManagerCompanyId( managerCompanyId, pageable);
    }

    @Override
    public List<Company> getEmploymentAgencies() {
        log.debug("getEmploymentAgencies()");
        return companyRepository.findByCompanyType(CompanyType.EMPLOYMENT_AGENCY);
    }

    @Override
    public List<Company> getAvailableCustomers(Long personId) {
        return companyRepository.getAvailableCompanies(personId);
    }
}