package com.pkit.service.impl;

import com.pkit.model.Person;
import com.pkit.model.QCandidate;
import com.pkit.model.QPerson;
import com.pkit.model.filter.PersonFilter;
import com.pkit.repository.PersonRepository;
import com.pkit.service.PersonService;
import com.querydsl.core.BooleanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Slf4j
@Service
@Transactional
public class PersonServiceImpl extends BaseServiceImpl<Person, Long> implements PersonService {

    private final PersonRepository personRepository;

    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getAll() {
        return personRepository.findAll(new Sort("lastName", "firstName", "middleName"));
    }

    @Override
    public Page<Person> getAll(Pageable pageable) {
        log.debug("getAll(pageable: " + pageable + ")");

        return personRepository.findAll(pageable);
    }

    public List<Person> findByFilter(PersonFilter personFilter) {
        log.debug("findByFilter( personFilter: {})", personFilter);

        QCandidate candidate = QCandidate.candidate;
        BooleanBuilder builder = new BooleanBuilder();

        if (personFilter.getLastName() != null && !personFilter.getLastName().trim().equals("")) {
            builder.and(candidate.lastName.eq(personFilter.getLastName().trim()));
        }
        if (personFilter.getFirstName() != null && !personFilter.getFirstName().trim().equals("")) {
            builder.and(candidate.firstName.eq(personFilter.getFirstName().trim()));
        }
        if (personFilter.getMiddleName() != null && !personFilter.getMiddleName().trim().equals("")) {
            builder.and(candidate.middleName.eq(personFilter.getMiddleName().trim()));
        }
        return Collections.EMPTY_LIST;
//        return candidateRepository.findAll(builder.getValue());
    }

    public Page<Person> findByFilter(PersonFilter personFilter, Pageable pageable) {
        log.debug("findByFilter( personFilter: {}, pageable: {})", personFilter, pageable);

        QPerson person = QPerson.person;
        BooleanBuilder builder = new BooleanBuilder();

        if (personFilter.getLastName() != null && !"".equals(personFilter.getLastName().trim())) {
            builder.and(person.lastName.eq(personFilter.getLastName().trim()));
        }
        if (personFilter.getFirstName() != null && !"".equals(personFilter.getFirstName().trim())) {
            builder.and(person.firstName.eq(personFilter.getFirstName().trim()));
        }
        if (personFilter.getMiddleName() != null && !"".equals(personFilter.getMiddleName().trim())) {
            builder.and(person.middleName.eq(personFilter.getMiddleName().trim()));
        }

        return personRepository.findAll(builder.getValue(), pageable);
    }

    public List<Person> getByCompanyId(Long companyId) {
        log.debug("getByCompanyId(companyId: {})", companyId);
        return personRepository.findByCompanyId(companyId);
    }

    public Page<Person> getByCompanyId(Long companyId, Pageable pageable) {
        log.debug("getByCompanyId(companyId: {})", companyId);
        return personRepository.findByCompanyId(companyId, pageable);
    }

    public List<Person> getByDepartmentId(Long departmentId) {
        log.debug("getByDepartmentId(departmentId: {})", departmentId);
        return personRepository.findByDepartmentId(departmentId);
    }

    public Page<Person> getByDepartmentId(Long departmentId, Pageable pageable) {
        log.debug("getByDepartmentId(departmentId: {})", departmentId);
        return personRepository.findByDepartmentId(departmentId, pageable);
    }
}