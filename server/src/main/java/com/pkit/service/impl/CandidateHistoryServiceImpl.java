package com.pkit.service.impl;

import com.pkit.exception.CandidateNotFoundException;
import com.pkit.model.*;
import com.pkit.model.event.*;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CandidateHistoryServiceImpl implements CandidateHistoryService {

    private final static short MAX_RECORD = 10000;

    private final CandidateService candidateService;
    private final CandidateNoteService candidateNoteService;
    private final DemandCandidateService demandCandidateService;
    private final DemandCandidateNoteService demandCandidateNoteService;

    public CandidateHistoryServiceImpl(
            CandidateService candidateService,
            CandidateNoteService candidateNoteService,
            DemandCandidateService demandCandidateService,
            DemandCandidateNoteService demandCandidateNoteService
    ) {
        this.candidateService = candidateService;
        this.candidateNoteService = candidateNoteService;
        this.demandCandidateService = demandCandidateService;
        this.demandCandidateNoteService = demandCandidateNoteService;
    }

    public List<Event> get(Long candidateId) {
        log.debug("get( candidateId: {})", candidateId);
        return get(candidateId, (short) 0);
    }

    public List<Event> get(Long candidateId, Short count) {
        log.debug("get(candidateId: {}, count: {})", candidateId, count);

        List<Event> result = getBasic(candidateId, count, true);
        Collections.sort(result);
        log.debug("result: {}", result);
        return (count == 0) ? result : result.stream().limit(count).collect(Collectors.toList());
    }

    public List<Event> getLast(Long candidateId) {
        log.debug("getLast( candidateId: {})", candidateId);

        return getLast(candidateId, (short) 0);
    }

    public List<Event> getLast(Long candidateId, Short count) {
        log.debug("getLast(candidateId: {}, count: {})", candidateId, count);

        List<Event> result = getBasic(candidateId, count, false);
        Collections.reverse(result);
        log.debug("result: {}", result);
        return (count > 0 && result.size() > count) ? result.subList(0, count) : result;
    }

    private List<Event> getBasic(Long candidateId, Short count, boolean forwardDirection) {
        log.debug("getBasic(candidateId: {}, count: {}, forwardDirection: {})", candidateId, count, forwardDirection);

        Candidate candidate = candidateService.get(candidateId);

        if (candidate == null) throw new CandidateNotFoundException(candidateId);

        log.debug("candidate: {}", candidate);

        List<Event> result = new ArrayList<>();
        result.add(new CreateCandidateEvent(candidate));

        List<DemandCandidate> demandCandidates = demandCandidateService.getByCandidateIdAndLimitAndSort(candidateId, (count == 0) ? MAX_RECORD : count, forwardDirection);
        for (DemandCandidate demandCandidate : demandCandidates) {
            result.add(new AddDemandCandidateEvent(demandCandidate));
            result.addAll(demandCandidateNoteService.findByDemandCandidate(demandCandidate).stream().map(demandCandidateNote -> new AddDemandCandidateNoteEvent(demandCandidate, demandCandidateNote)).collect(Collectors.toList()));
            for (DemandCandidateMeeting demandCandidateMeeting : demandCandidate.getMeetings()) {
                result.add(new AddDemandCandidateMeetingEvent(demandCandidate, demandCandidateMeeting));
                result.addAll(demandCandidateMeeting.getNotes().stream().map(demandCandidateMeetingNote -> new AddDemandCandidateMeetingNoteEvent(demandCandidate, demandCandidateMeeting, demandCandidateMeetingNote)).collect(Collectors.toList()));
            }
        }
        List<CandidateNote> candidateNotes = candidateNoteService.getByCandidateIdAndLimitAndSort(candidateId, (count == 0)? MAX_RECORD: count, forwardDirection);
        result.addAll(candidateNotes.stream().map(CommentCandidateEvent::new).collect(Collectors.toList()));
        return result;
    }
}