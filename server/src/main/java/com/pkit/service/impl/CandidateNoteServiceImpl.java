package com.pkit.service.impl;

import com.pkit.model.CandidateNote;
import com.pkit.repository.CandidateNoteRepository;
import com.pkit.service.CandidateNoteService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CandidateNoteServiceImpl
        extends BaseServiceImpl<CandidateNote, Long> implements CandidateNoteService {

    private final CandidateNoteRepository candidateNoteRepository;

    public CandidateNoteServiceImpl(CandidateNoteRepository candidateNoteRepository) {
        this.candidateNoteRepository = candidateNoteRepository;
    }

    public List<CandidateNote> getByCandidateId(Long candidateId) {
        return candidateNoteRepository.findByCandidateId(candidateId);
    }

    @Override
    public List<CandidateNote> getByCandidateIdAndLimit(Long demandId, Short limit) {
        return candidateNoteRepository.findByCandidateIdAndLimit(demandId, limit);
    }

    @Override
    public List<CandidateNote> getByCandidateIdAndLimitAndSort(Long demandId, Short limit, boolean forwardDirection) {
        if (forwardDirection) {
            return candidateNoteRepository.findByCandidateIdAndLimitAndSortAsc(demandId, limit);
        } else {
            return candidateNoteRepository.findByCandidateIdAndLimitAndSortDesc(demandId, limit);
        }
    }
}