package com.pkit.service.impl;

import com.pkit.service.BaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;

@Slf4j
@Transactional
public class BaseServiceImpl<T, E extends Serializable> implements BaseService<T, E> {

    @Autowired
    protected JpaRepository<T, E> repository;

    public BaseServiceImpl() {
        log.debug("In Constructor BaseServiceImpl: {}", this.getClass().getSimpleName());
    }

    @Override
    public Collection<T> getAll() {
        return repository.findAll();
    }

    @Override
    public T get(E id) {
        log.debug("get: {}", id);

        T item = repository.findOne(id);

        log.debug("get: {}; class.name: {}", id, (item == null) ? "Not defined" : item.getClass().getName());
        return item;
    }

    @Override
    @Transactional
    public T save(T item) {
        log.debug("save: {}", item);

        return repository.saveAndFlush(item);
    }

    @Override
    @Transactional
    public T add(T item) {
        log.debug("> create: {}", item);
        item = repository.saveAndFlush(item);
        log.debug("< create: {}", item);
        return item;
    }

    @Override
    @Transactional
    public T update(T item) {
        log.debug("> update: item: {}", item);
        item = repository.saveAndFlush(item);
        log.debug("< update: item: {}", item);
        return item;
    }

    @Override
    @Transactional
    public void delete(E id) {
        log.debug("> delete id: {}", id);
        repository.delete(id);
        log.debug("< delete id: {}", id);
    }

    @Override
    @Transactional
    public void delete(T item) {
        log.debug("> delete id: {}", item);
        repository.delete(item);
        log.debug("< delete id: {}", item);
    }

    @Override
    public boolean exists(E id) {
        return !repository.exists(id);
    }
}