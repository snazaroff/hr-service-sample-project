package com.pkit.service.impl;

import com.pkit.model.DemandCandidate;
import com.pkit.repository.DemandCandidateRepository;
import com.pkit.service.DemandCandidateService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DemandCandidateServiceImpl
        extends BaseServiceImpl<DemandCandidate, Long>
        implements DemandCandidateService {

    private final DemandCandidateRepository demandCandidateRepository;

    public DemandCandidateServiceImpl(DemandCandidateRepository demandCandidateRepository) {
        this.demandCandidateRepository = demandCandidateRepository;
    }

    public List<DemandCandidate> getByDemandId(Long demandId) {
        return demandCandidateRepository.findByDemandId(demandId);
    }

    @Override
    public List<DemandCandidate> getByDemandIdAndLimit(Long demandId, Short limit) {
        return demandCandidateRepository.findByDemandIdAndLimit(demandId, limit);
    }

    @Override
    public List<DemandCandidate> getByDemandIdAndLimitAndSort(Long demandId, Short limit, boolean forwardDirection) {
        if (forwardDirection) {
            return demandCandidateRepository.findByDemandIdAndLimitAndSortAsc(demandId, limit);
        } else {
            return demandCandidateRepository.findByDemandIdAndLimitAndSortDesc(demandId, limit);
        }
    }

    public List<DemandCandidate> getByCandidateId(Long candidateId) {
        return demandCandidateRepository.findByCandidateId(candidateId);
    }

    @Override
    public List<DemandCandidate> getByCandidateIdAndLimit(Long candidateId, Short limit) {
        return demandCandidateRepository.findByCandidateIdAndLimit(candidateId, limit);
    }

    @Override
    public List<DemandCandidate> getByCandidateIdAndLimitAndSort(Long candidateId, Short limit, boolean forwardDirection) {
        if (forwardDirection) {
            return demandCandidateRepository.findByCandidateIdAndLimitAndSortAsc(candidateId, limit);
        } else {
            return demandCandidateRepository.findByCandidateIdAndLimitAndSortDesc(candidateId, limit);
        }
    }
}