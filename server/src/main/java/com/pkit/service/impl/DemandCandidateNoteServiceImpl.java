package com.pkit.service.impl;

import com.pkit.model.DemandCandidate;
import com.pkit.model.DemandCandidateNote;
import com.pkit.repository.DemandCandidateNoteRepository;
import com.pkit.service.DemandCandidateNoteService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DemandCandidateNoteServiceImpl extends BaseServiceImpl<DemandCandidateNote, Long>
        implements DemandCandidateNoteService {

  private final DemandCandidateNoteRepository demandCandidateNoteRepository;

  public DemandCandidateNoteServiceImpl(DemandCandidateNoteRepository demandCandidateNoteRepository) {
    this.demandCandidateNoteRepository = demandCandidateNoteRepository;
  }

  @Override
  public List<DemandCandidateNote> findByDemandCandidate(DemandCandidate demandCandidate) {
    return demandCandidateNoteRepository.findByDemandCandidate(demandCandidate);
  }

  @Override
  public List<DemandCandidateNote> findByDemandCandidateIn(List<DemandCandidate> demandCandidates) {
    return demandCandidateNoteRepository.findByDemandCandidateIn(demandCandidates);
  }
}