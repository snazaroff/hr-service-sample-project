package com.pkit.service.impl;

import com.pkit.data.TestData;
import com.pkit.model.*;
import com.pkit.model.filter.CandidateFilter;
import com.pkit.repository.*;
import com.pkit.service.*;
import com.querydsl.core.BooleanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class CandidateServiceImpl
        extends BaseServiceImpl<Candidate, Long> implements CandidateService {

    @Autowired(required = false)
    private TestData testData;

    private final CandidateRepository candidateRepository;
    private final CandidateCategoryRepository candidateCategoryRepository;
    private final CandidateLanguageRepository candidateLanguageRepository;
    private final CategoryService categoryService;
    private final CompanyService companyService;
    private final DemandRepository demandRepository;
    private final EducationRepository educationRepository;
    private final EmploymentRepository employmentRepository;
    private final PhotoRepository photoRepository;
    private final PhotoService photoService;

    private final CandidateIndexRepository candidateIndexRepository;
    private final DemandIndexRepository demandIndexRepository;
    private final ElasticsearchTemplate elasticsearchTemplate;

    public CandidateServiceImpl(
            CandidateRepository candidateRepository,
            CandidateCategoryRepository candidateCategoryRepository,
            CandidateLanguageRepository candidateLanguageRepository,
            CategoryService categoryService,
            CompanyService companyService,
            DemandRepository demandRepository,
            EducationRepository educationRepository,
            EmploymentRepository employmentRepository,
            PhotoRepository photoRepository,
            PhotoService photoService,
            CandidateIndexRepository candidateIndexRepository,
            DemandIndexRepository demandIndexRepository,
            ElasticsearchTemplate elasticsearchTemplate) {
        this.candidateRepository = candidateRepository;
        this.candidateCategoryRepository = candidateCategoryRepository;
        this.candidateLanguageRepository = candidateLanguageRepository;
        this.categoryService = categoryService;
        this.companyService = companyService;
        this.demandRepository = demandRepository;
        this.educationRepository = educationRepository;
        this.employmentRepository = employmentRepository;
        this.photoRepository = photoRepository;
        this.photoService = photoService;
        this.candidateIndexRepository = candidateIndexRepository;
        this.demandIndexRepository = demandIndexRepository;
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @PostConstruct
    public void init() {

        if (testData != null) {
            testData.init();
        } else {
            log.debug("testData: null");
        }

        //Load test candidates to elasticsearch
        elasticsearchTemplate.deleteIndex(CandidateIndex.class);
        elasticsearchTemplate.createIndex(CandidateIndex.class);
        elasticsearchTemplate.putMapping(CandidateIndex.class);
        elasticsearchTemplate.refresh(CandidateIndex.class);

        List<Candidate> candidates = candidateRepository.findAll();
        for (Candidate candidate : candidates) {
            CandidateIndex candidateIndex = new CandidateIndex();
            candidateIndex.setCandidateId(Long.toString(candidate.getCandidateId()));
            candidateIndex.setData(candidate.toCacheString());
            candidateIndexRepository.save(candidateIndex);
        }

        //Load test demand to elasticsearch
//        elasticsearchTemplate.deleteIndex(DemandIndex.class);
        elasticsearchTemplate.createIndex(DemandIndex.class);
        elasticsearchTemplate.putMapping(DemandIndex.class);
        elasticsearchTemplate.refresh(DemandIndex.class);

        List<Demand> demands = demandRepository.findAll();
        for (Demand demand : demands) {
            DemandIndex demandIndex = new DemandIndex();
            demandIndex.setDemandId(Long.toString(demand.getDemandId()));
            demandIndex.setData(demand.toCacheString());
            demandIndexRepository.save(demandIndex);
        }

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        for (int i = 0; i < candidates.size(); i++) {
            String fileName = "boys_" + (i % 9 + 1) + ".jpg";
            try (InputStream is = classloader.getResourceAsStream("images/" + fileName)) {
                byte[] image = IOUtils.toByteArray(Objects.requireNonNull(is));

                Photo photo = new Photo();
                photo.setCandidateId(candidates.get(i).getCandidateId());
                photo.setMain(true);
                photoRepository.save(photo);
                photoService.add(photo, fileName, image);
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public List<Candidate> getAll() {
        log.debug("getAll()");

        return candidateRepository.findAll();
    }

    @Override
    public Page<Candidate> getAll(Pageable pageable) {
        log.debug("getAll(pageable: " + pageable + ")");

        return candidateRepository.findAll(pageable);
    }

    public List<Candidate> findByFilter(CandidateFilter candidateFilter) {
        log.debug("findByFilter( candidateFilter: " + candidateFilter + ")");

        QCandidate candidate = QCandidate.candidate;
        BooleanBuilder builder = new BooleanBuilder();

        if (candidateFilter.getLastName() != null && !candidateFilter.getLastName().trim().equals("")) {
            builder.and(candidate.lastName.eq(candidateFilter.getLastName().trim()));
        }
        if (candidateFilter.getFirstName() != null && !candidateFilter.getFirstName().trim().equals("")) {
            builder.and(candidate.firstName.eq(candidateFilter.getFirstName().trim()));
        }
        if (candidateFilter.getMiddleName() != null && !candidateFilter.getMiddleName().trim().equals("")) {
            builder.and(candidate.middleName.eq(candidateFilter.getMiddleName().trim()));
        }
        return Collections.EMPTY_LIST;
//        return candidateRepository.findAll(builder.getValue());
    }

    public Page<Candidate> findByFilter(CandidateFilter candidateFilter, Pageable pageable) {
        log.debug("findByFilter( candidateFilter: " + candidateFilter + ", pageable: " + pageable + ")");

        QCandidate candidate = QCandidate.candidate;
        BooleanBuilder builder = new BooleanBuilder();

        if (candidateFilter.getLastName() != null && !candidateFilter.getLastName().trim().equals("")) {
            builder.and(candidate.lastName.eq(candidateFilter.getLastName().trim()));
        }
        if (candidateFilter.getFirstName() != null && !candidateFilter.getFirstName().trim().equals("")) {
            builder.and(candidate.firstName.eq(candidateFilter.getFirstName().trim()));
        }
        if (candidateFilter.getMiddleName() != null && !candidateFilter.getMiddleName().trim().equals("")) {
            builder.and(candidate.middleName.eq(candidateFilter.getMiddleName().trim()));
        }
        return candidateRepository.findAll(builder.getValue(), pageable);
    }

    @Override
    @Transactional
    public Candidate add(Candidate candidate) {
        log.debug("add: " + candidate);

        Set<Education> educations = new HashSet<>(candidate.getEducations());
        candidate.getEducations().clear();

        Set<CandidateLanguage> languages = new HashSet<>(candidate.getLanguages());
        candidate.getLanguages().clear();

        log.debug("educations: " + educations.size());
//        for (Education education : educations) {
//            log.debug("education: " + education);
//            log.debug("education.candidate: " + education.getCandidate());
//        }

        Set<Employment> employments = new HashSet<>(candidate.getEmployments());
        candidate.getEmployments().clear();

        log.debug("employments: " + employments.size());
//        for (Employment employment : employments) {
//            log.debug("employment: " + employment);
//        }

        languages.addAll(candidate.getLanguages());
        candidate.getLanguages().clear();

        log.debug("candidate: " + candidate);
        candidateRepository.save(candidate);

        log.debug("languages.size(): " + languages.size());
        for (CandidateLanguage candidateLanguage : languages) {
//            log.debug("candidateLanguage: " + candidateLanguage);
            candidateLanguage.setCandidateId(candidate.getCandidateId());
        }

        educationRepository.save(educations);
        employmentRepository.save(employments);
        candidateLanguageRepository.save(languages);

        candidate.getEducations().addAll(educations);
        educations.clear();

        candidate.getEmployments().addAll(employments);
        employments.clear();

        candidate.getLanguages().addAll(languages);
        languages.clear();

        log.debug("add: " + candidate);

        return candidate;
    }

    @Override
    public Candidate update(Candidate candidate) {
        log.debug("update: " + candidate);

        Set<Education> educations = new HashSet(candidate.getEducations());
        candidate.getEducations().clear();

        Set<CandidateLanguage> languages = new HashSet(candidate.getLanguages());
        candidate.getLanguages().clear();

        log.debug("educations: " + educations.size());
        for (Education education : educations) {
            log.debug("education: " + education);
            log.debug("education.candidate: " + education.getCandidate());
        }

        Set<Employment> employments = new HashSet(candidate.getEmployments());
        candidate.getEmployments().clear();

        log.debug("employments: " + employments.size());
//        for (Employment employment : employments) {
//            log.debug("employment: " + employment);
//        }

        languages.addAll(candidate.getLanguages());
        candidate.getLanguages().clear();

        log.debug("candidate: " + candidate);
        candidateRepository.save(candidate);

        log.debug("languages.size(): " + languages.size());
        for (CandidateLanguage candidateLanguage : languages) {
            log.debug("candidateLanguage: " + candidateLanguage);
            candidateLanguage.setCandidateId(candidate.getCandidateId());
        }

        educationRepository.save(educations);
        employmentRepository.save(employments);
        candidateLanguageRepository.save(languages);

        candidate.getEducations().addAll(educations);
        educations.clear();

        candidate.getEmployments().addAll(employments);
        employments.clear();

        candidate.getLanguages().addAll(languages);
        languages.clear();

        log.debug("update: " + candidate);

        return candidate;
    }

    @Override
    @Transactional
    public List<Category> deleteCategory(Long candidateId, Long categoryId) {
        log.debug("deleteCategory( candidateId: " + candidateId + ", categoryId: " + categoryId + ")");

        List<Category> deleted = new ArrayList<>();
        List<Category> forDelete = new ArrayList<>();
        Candidate candidate = candidateRepository.getOne(candidateId);
        Category category = categoryService.getCategory(categoryId);
        log.debug("category.getParentId(): " + category.getParentId());
        if (category.getParentId() == null) {
            log.debug("category.getParentId() == null");
            forDelete.addAll(categoryService.getSubCategory(categoryId));
            List<Category> categories = candidate.getCategories().stream().map(CandidateCategory::getCategory).collect(Collectors.toList());
            log.debug("categories.size(): " + categories.size());
            forDelete.retainAll(categories);
            deleted.addAll(forDelete);
            deleted.add(category);
        } else {
            log.debug("category.getParentId() != null");
            forDelete.add(category);
            deleted.add(category);
            deleted.add(categoryService.getCategory(category.getParentId()));
        }
        log.debug("forDelete: " + forDelete);

        for (Category c : forDelete) {
            log.debug("delete: candidateId: " + candidate.getCandidateId() + "; categoryId: " + c.getCategoryId());
            CandidateCategory candidateCategory = candidateCategoryRepository.getOne(new CandidateCategoryPK(candidate.getCandidateId(), c.getCategoryId()));
            log.debug("candidateCategory: " + candidateCategory);
            candidateCategoryRepository.deleteItem(candidate.getCandidateId(), c.getCategoryId());
        }
        log.debug("deleted: {}", deleted);
        return deleted;
    }

    @Override
    @Transactional
    public Category addCategory(Long candidateId, Long categoryId) {
        Candidate candidate = candidateRepository.getOne(candidateId);
        Category category = categoryService.getCategory(categoryId);
        CandidateCategory candidateCategory = new CandidateCategory(candidate, category);
        return candidateCategoryRepository.save(candidateCategory).getCategory();
    }

    @Override
    @Transactional
    public List<Category> addBatchCategory(Long candidateId, List<Long> categoryIds) {
        Candidate candidate = candidateRepository.getOne(candidateId);
        return candidateCategoryRepository.save(
                categoryService.getCategories(categoryIds)
                        .stream()
                        .map(category -> new CandidateCategory(candidate, category))
                        .collect(Collectors.toList())
        ).stream()
                .map(CandidateCategory::getCategory)
                .collect(Collectors.toList());
    }

    @Override
    public List<Category> getCategoryList(Long candidateId) {
        return candidateCategoryRepository
                .findByCandidateId(candidateId)
                .stream()
                .map(CandidateCategory::getCategory)
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public List<Category> getCategoryListWithTopCategory(Long candidateId) {
        List<Category> categories = getCategoryList(candidateId);
        return categories
                .stream()
                .filter(category -> category.getParentId() != null)
                .sorted()
                .collect(Collectors.toList());
    }

    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Candidate> search(String searchString) {
        log.debug("search({})" + searchString);

        if (searchString == null || searchString.trim().equals(""))
            return Collections.EMPTY_LIST;

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Candidate> q = cb.createQuery(Candidate.class);
        Root<Candidate> candidate = q.from(Candidate.class);

        Path<String> firstName = candidate.get("firstName");
        Path<String> lastName = candidate.get("lastName");

        String[] terms = searchString.split(" ");

        List<Predicate> predicates = new ArrayList<>();

        for (String term : terms) {
            predicates.add(cb.like(cb.lower(firstName), cb.lower(cb.literal("%" + term + "%"))));
            predicates.add(cb.like(cb.lower(lastName), cb.lower(cb.literal("%" + term + "%"))));
        }

        q.select(candidate).where(
                cb.or(predicates.toArray(new Predicate[predicates.size()]))
        );
        return entityManager.createQuery(q).getResultList();
    }

    public List<Candidate> getByCompany(Company company) {
        log.debug("getByCompany(company: {})", company);

        return candidateRepository.findByCompany(company);
    }

    public List<Candidate> getByCompanyId(Long companyId) {
        log.debug("getByCompanyId(companyId: {})", companyId);

        Company company = companyService.get(companyId);
        return getByCompany(company);
    }

    public Page<Candidate> getByCompany(Company company, Pageable pageable) {
        log.debug("getByCompany(companyId: {})", company);

        return candidateRepository.findByCompany(company, pageable);
    }

    public Page<Candidate> getByCompanyId(Long companyId, Pageable pageable) {
        log.debug("getByCompanyId(companyId: {})", companyId);

        Company company = companyService.get(companyId);
        return getByCompany(company, pageable);
    }

}