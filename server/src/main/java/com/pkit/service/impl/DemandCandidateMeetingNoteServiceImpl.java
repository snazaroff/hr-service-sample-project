package com.pkit.service.impl;

import com.pkit.model.*;
import com.pkit.service.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DemandCandidateMeetingNoteServiceImpl
        extends BaseServiceImpl<DemandCandidateMeetingNote, Long>
        implements DemandCandidateMeetingNoteService {
}
