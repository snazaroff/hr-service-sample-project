package com.pkit.service.impl;

import com.pkit.model.DemandCandidateMeeting;
import com.pkit.service.DemandCandidateMeetingService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DemandCandidateMeetingServiceImpl extends BaseServiceImpl<DemandCandidateMeeting, Long> implements DemandCandidateMeetingService { }