package com.pkit.service.impl;

import com.pkit.model.FileInfo;
import com.pkit.repository.FileInfoRepository;
import com.pkit.service.FileStoreService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.file.*;
import java.util.*;

@Slf4j
@Service
public class FileStoreServiceImpl implements FileStoreService {

    private final String storePath = "/store/";

    private final FileInfoRepository fileInfoRepository;

    public FileStoreServiceImpl(FileInfoRepository fileInfoRepository) {
        this.fileInfoRepository = fileInfoRepository;
    }

    @PostConstruct
    public void init() {
        File storePathFolder = new File(storePath);
        if (!storePathFolder.exists()) {
            storePathFolder.mkdirs();
        }
    }

    @Override
    public FileInfo getByFileId(Long fileId) {
        return fileInfoRepository.findOne(fileId);
    }

    @Override
    public List<FileInfo> getByOwnerId(Long ownerId) {
        return fileInfoRepository.findByOwnerId(ownerId);
    }

    public FileInfo addFile(FileInfo fileInfo, byte[] fileContent) throws IOException {
        log.debug("addFile(fileInfo: {})", fileInfo);

        fileInfo = fileInfoRepository.save(fileInfo);

        try (OutputStream os = new FileOutputStream(new File(storePath + "//" + fileInfo.getStorePath()))) {
            IOUtils.write(fileContent, os);
        }
        return fileInfo;
    }

    @Override
    public void deleteByFileId(long fileId) {
        FileInfo fileInfo = getByFileId(fileId);
        fileInfoRepository.delete(fileId);
        Path filePath = Paths.get(storePath + "//" + fileInfo.getStorePath());
        File file = filePath.toFile();
        log.debug("file.exists(): {}", file.exists());
        try {
            Files.deleteIfExists(filePath);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    @Transactional
    public void deleteByOwnerId(long ownerId) {
        fileInfoRepository.findByOwnerId(ownerId).forEach(fileInfo -> deleteByFileId(fileInfo.getFileId()));
    }

    public byte[] getFileContent(long fileId) throws IOException {
        log.debug("getFileContent(fileId: {})", fileId);

        FileInfo fileInfo = getByFileId(fileId);
        try (InputStream is = new FileInputStream(new File(storePath + "//" + fileInfo.getStorePath()))) {
            return IOUtils.toByteArray(is);
        }
    }
}