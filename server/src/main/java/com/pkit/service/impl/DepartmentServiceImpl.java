package com.pkit.service.impl;

import com.pkit.model.Company;
import com.pkit.model.Department;
import com.pkit.repository.DepartmentRepository;
import com.pkit.service.DepartmentService;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Transactional
public class DepartmentServiceImpl extends BaseServiceImpl<Department, Long> implements DepartmentService {

    private final DepartmentRepository departmentRepository;

    public DepartmentServiceImpl(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @Override
    public List<Department> getDepartments() {
        return departmentRepository.findAll(new Sort("name"));
    }

    @Override
    public List<Department> findByCompany(Company company) {
        return departmentRepository.findByCompany(company);
    }

    @Override
    public List<Department> getAll(Function<? super Department, ? extends String> function) {
        return getAll()
                .stream()
                .sorted(Comparator.comparing(function))
                .collect(Collectors.toList());
    }
}