package com.pkit.service;

import com.pkit.model.*;
import com.pkit.repository.CategoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public List<Category> getTopCategory() {
        return categoryRepository.findByParentIdOrderByNameAsc(null);
    }

    public List<Category> getSubCategory(Long categoryId) {
        return categoryRepository.findByParentIdOrderByNameAsc(categoryId);
    }

    public Category getCategory(Long categoryId) {
        return categoryRepository.findOne(categoryId);
    }

    public List<Category> getCategories(List<Long> categoryIds) {
        return categoryRepository.findByCategoryIdIn(categoryIds);
    }

    public void delete(Category category) {
        log.debug("CategoryService.delete( category: {})", category);
        categoryRepository.delete(category);
    }
}