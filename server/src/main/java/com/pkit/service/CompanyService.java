package com.pkit.service;

import com.pkit.model.*;
import com.pkit.model.filter.CompanyFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CompanyService extends BaseService<Company, Long> {

    List<Company> getAll();

    Page<Company> getAll(Pageable pageable);

    List<Company> findByFilter(CompanyFilter companyFilter);

    Page<Company> findByFilter(CompanyFilter companyFilter, Pageable pageable);

    List<Demand> getDemands(Long customerId);

    List<Company> getCustomers();

    Page<Company> getCustomers(Pageable pageable);

    Page<Company> getCustomerByManagerCompanyId(Long managerCompanyId, Pageable pageable);

    List<Company> getEmploymentAgencies();

    List<Company> getAvailableCustomers(Long personId);
}