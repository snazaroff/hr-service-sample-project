package com.pkit.service;

import com.pkit.model.DemandNote;

import java.util.List;

public interface DemandNoteService extends BaseService<DemandNote, Long> {

    List<DemandNote> getByDemandId(Long demandId);
    List<DemandNote> getByDemandIdAndLimit(Long demandId, Short limit);
    List<DemandNote> getByDemandIdAndLimitAndSort(Long demandId, Short limit, boolean forwardDirection);

}