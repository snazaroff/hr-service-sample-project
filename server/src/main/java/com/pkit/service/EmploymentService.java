package com.pkit.service;

import com.pkit.model.Employment;

public interface EmploymentService extends BaseService<Employment, Long> { }