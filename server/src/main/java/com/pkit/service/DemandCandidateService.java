package com.pkit.service;

import com.pkit.model.DemandCandidate;

import java.util.List;

public interface DemandCandidateService extends BaseService<DemandCandidate, Long> {

    List<DemandCandidate> getByDemandId(Long demandId);
    List<DemandCandidate> getByDemandIdAndLimit(Long demandId, Short limit);
    List<DemandCandidate> getByDemandIdAndLimitAndSort(Long demandId, Short limit, boolean forwardDirection);

    List<DemandCandidate> getByCandidateId(Long candidateId);
    List<DemandCandidate> getByCandidateIdAndLimit(Long candidateId, Short limit);
    List<DemandCandidate> getByCandidateIdAndLimitAndSort(Long candidateId, Short limit, boolean forwardDirection);
}