package com.pkit.service;

import com.pkit.model.DemandCandidateMeeting;

public interface DemandCandidateMeetingService extends BaseService<DemandCandidateMeeting, Long> { }