package com.pkit.service;

import com.pkit.model.Country;
import com.pkit.model.Metro;
import com.pkit.model.Region;
import com.pkit.model.Town;
import com.pkit.repository.CountryRepository;
import com.pkit.repository.MetroRepository;
import com.pkit.repository.RegionRepository;
import com.pkit.repository.TownRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DictionaryService {

    public final Town MOSCOW;
    public final Country RUSSIA;

    private final CountryRepository countryRepository;
    private final RegionRepository regionRepository;
    private final MetroRepository metroRepository;
    private final TownRepository townRepository;

    public DictionaryService(CountryRepository countryRepository, RegionRepository regionRepository, TownRepository townRepository, MetroRepository metroRepository) {
        this.countryRepository = countryRepository;
        this.regionRepository = regionRepository;
        this.townRepository = townRepository;
        this.metroRepository = metroRepository;

        MOSCOW = townRepository.findOne(2L);
        RUSSIA = countryRepository.findOne(1L);
    }

    public List<Country> getPrimaryCountries() {
        return countryRepository.findPrimaryCountries();
    }

    public List<Country> getAllCountries() {
        return countryRepository.findAll(new Sort("name"));
    }

    public List<Region> getRegionByCountryId(long countryId) {
        return regionRepository.findByCountryIdOrderByNameAsc(countryId);
    }

    public List<Town> getTownByCountryId(long countryId) {
        return townRepository.findByCountryIdOrderByNameAsc(countryId);
    }

    public List<Town> getTownByRegionId(long regionId) {
        return townRepository.findByRegionIdOrderByNameAsc(regionId);
    }

    @Cacheable("DictionaryService.getMetroByTown")
    public List<Metro> getMetroByTown(Long townId) {
        return metroRepository.findByTownIdOrderByNameAsc(townId);
    }
}