package com.pkit.service;

import com.pkit.model.CandidateNote;

import java.util.List;

public interface CandidateNoteService extends BaseService<CandidateNote, Long> {

    List<CandidateNote> getByCandidateId(Long candidateId);
    List<CandidateNote> getByCandidateIdAndLimit(Long demandId, Short limit);
    List<CandidateNote> getByCandidateIdAndLimitAndSort(Long demandId, Short limit, boolean forwardDirection);
}