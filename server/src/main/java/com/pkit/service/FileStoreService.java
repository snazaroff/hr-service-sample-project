package com.pkit.service;

import com.pkit.model.FileInfo;

import java.io.IOException;
import java.util.*;

public interface FileStoreService {

  FileInfo getByFileId(Long fileId);

  List<FileInfo> getByOwnerId(Long ownerId);

  FileInfo addFile(FileInfo fileInfo, byte[] fileContent) throws IOException;

  void deleteByFileId(long fileId);

  void deleteByOwnerId(long ownerId);

  byte[] getFileContent(long fileId) throws IOException;
}
