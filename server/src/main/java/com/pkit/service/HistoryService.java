package com.pkit.service;

import com.pkit.model.event.Event;

import java.util.List;

public interface HistoryService {
	List<Event> get(Long id);
	List<Event> get(Long id, Short count);
	List<Event> getLast(Long id);
	List<Event> getLast(Long id, Short count);
}