package com.pkit.service;

import com.pkit.model.Company;
import com.pkit.model.Department;

import java.util.List;

public interface DepartmentService extends BaseService<Department, Long>, AllSortedService<Department>{

    List<Department> getDepartments();

    List<Department> findByCompany(Company company);
}