package com.pkit.service;

import java.io.Serializable;
import java.util.List;
import java.util.function.Function;

public interface AllSortedService<T extends Serializable> {

    List<T> getAll(Function<? super T, ? extends String> function);
}