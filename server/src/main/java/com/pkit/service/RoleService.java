package com.pkit.service;

import com.pkit.model.*;
import com.pkit.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class RoleService {

    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public List<Role> getAll() {
        return roleRepository.findAll();
    }

    public Page<Role> getAll(Pageable pageable) {
        log.debug("getAll(pageable: " + pageable + ")");

        return roleRepository.findAll(pageable);
    }

    public Role get(String roleId) {
        return roleRepository.findOne(roleId);
    }

    public Role add(Role role) {
        return roleRepository.save(role);
    }

    public void remove(String roleId) {
        roleRepository.delete(roleId);
    }
}