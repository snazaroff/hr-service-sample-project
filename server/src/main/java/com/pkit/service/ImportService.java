package com.pkit.service;

//import com.fasterxml.jackson.core.JsonProcessingException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.data.Languages;
import com.pkit.file_store.service.ConvertService;
import com.pkit.model.*;
import com.pkit.model.Currency;
import com.pkit.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;

@Slf4j
@Service
public class ImportService {

    @Autowired
    private Languages allLanguages;

    private final ObjectMapper om;

    private final ConvertService convertService;

    private final CandidateCategoryRepository candidateCategoryRepository;

    private final CandidateLanguageRepository candidateLanguageRepository;
    private final CandidateRepository candidateRepository;
    private final CategoryRepository categoryRepository;

    private final EducationRepository educationRepository;
    private final EmploymentRepository employmentRepository;

    private final MetroRepository metroRepository;
    //    private final PhotoRepository photoRepository;
    private final PhotoService photoService;

    public ImportService(
            ObjectMapper om,
//            ConvertService convertService,
            CandidateCategoryRepository candidateCategoryRepository,
            CandidateLanguageRepository candidateLanguageRepository,
            CandidateRepository candidateRepository,
            CategoryRepository categoryRepository,
            EducationRepository educationRepository,
            EmploymentRepository employmentRepository,
            MetroRepository metroRepository,
            PhotoService photoService
    ) {
        log.debug("ImportService()");

        this.om = om;

        this.convertService = new ConvertService();
        this.candidateCategoryRepository = candidateCategoryRepository;
        this.candidateLanguageRepository = candidateLanguageRepository;
        this.candidateRepository = candidateRepository;
        this.categoryRepository = categoryRepository;
        this.educationRepository = educationRepository;
        this.employmentRepository = employmentRepository;
        this.metroRepository = metroRepository;
        this.photoService = photoService;
    }

    public Candidate importResume(byte[] file, String fileName, Person person, Company company) throws Exception {

        Map<String, Object> candidateMap = convertService.importResume(file, fileName);

        return save(candidateMap, person, company);
    }

    @Transactional
    public Candidate save(Map<String, Object> candidateMap, Person person, Company company) throws IOException {
        log.debug("save: ");
        List<String> photos = (List<String>) candidateMap.get("photos");
        candidateMap.remove("photos");

        List<Candidate> candidates = candidateRepository.findAll();
        log.debug("candidates: {}", candidates.size());

        Candidate c = candidates.get(0);

//        try {
        log.debug("candidate: {}", om.writerWithDefaultPrettyPrinter().writeValueAsString(c));
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }

        //Add created person
        candidateMap.put("created_by", new HashMap() {{
            put("person_id", person.getPersonId());
        }});

        //Преобразуем данные, приводим к конктетным значениям в базе
        //Метро
        if (candidateMap.containsKey("metro")) {
            String metroName = (String) candidateMap.get("metro");
            Metro metro = metroRepository.findByName(metroName);
            if (metro != null) {
                candidateMap.put("metro", metro);
            } else {
                log.error("Metro '{}' not found", metroName);
                candidateMap.remove("metro");
            }
        }

        //Пол
        if (candidateMap.containsKey("sex")) {
            String sex = (String) candidateMap.get("sex");
            candidateMap.put("sex", SexStatus.getByName(sex));
        }

        //Зарплата
        Salary salary = null;

        if (candidateMap.containsKey("salary")) {
            Map salaryOld = (Map) candidateMap.get("salary");
            Number value = (Number) salaryOld.get("value");
            String currency = (String) salaryOld.get("currency");

            salary = new Salary();
            salary.setAmount(value.longValue());
            salary.setCurrency(Currency.valueOf(currency));
        }

        log.debug("candidate:2: {}", om.writerWithDefaultPrettyPrinter().writeValueAsString(candidateMap));

        Candidate candidate = om.readValue(om.writeValueAsString(candidateMap), Candidate.class);

        log.debug("candidateMap.containsKey(\"categories\"): {}", candidateMap.containsKey("categories"));

        log.debug("candidate.createdBy: {}", candidate.getCreatedBy());
//        candidate.setCompany(company);
        candidate.setStatus(CandidateStatus.REVIERED);

        if (salary != null) {
            candidate.setSalary(salary);
        }
        candidate = candidateRepository.save(candidate);
        log.debug("candidate: {}", candidate.getCandidateId());

        //Языки
        if (candidateMap.containsKey("languages")) {

            List<Map> languages = (List<Map>) candidateMap.get("languages");

            for (Map<String, String> language : languages) {
                log.debug("language: {}", language);

                String languageName = language.get("name");
                String languageStatus = language.get("status");

                log.debug("languageName: {}; languageStatus: {}", languageName, languageStatus);

                CandidateLanguage candidateLanguage = new CandidateLanguage(candidate, allLanguages.findByName(languageName));

                candidateLanguage.setStatus(LanguageStatus.getByName(languageStatus));
                candidate.getLanguages().add(candidateLanguage);
            }
            candidateLanguageRepository.save(candidate.getLanguages());
        }

        //Работа
        if (candidateMap.containsKey("employments")) {

            List<Map> employments = (List<Map>) candidateMap.get("employments");

            for (Map<String, String> employmentMap : employments) {
                Employment employment = om.readValue(om.writeValueAsString(employmentMap), Employment.class);
                employment.setCandidate(candidate);
                employment.setCreatedBy(person);
                employmentRepository.save(employment);
            }
        }

        //Образование
        if (candidateMap.containsKey("educations")) {

            List<Map> educations = (List<Map>) candidateMap.get("educations");
            for (Map<String, Object> educationMap : educations) {
                log.debug(": {}", om.writeValueAsString(educationMap));
                String educationType = (String) educationMap.get("education_type");
                educationMap.put("education_type", EducationType.getByName(educationType));
                log.debug(": {}", om.writeValueAsString(educationMap));
                Education education = om.readValue(om.writeValueAsString(educationMap), Education.class);
                education.setCandidate(candidate);
                education.setCreatedBy(person);
                educationRepository.save(education);
            }
        }

        if (candidateMap.containsKey("courses")) {

            List<Map> courses = (List<Map>) candidateMap.get("courses");
            for (Map<String, Object> educationMap : courses) {
                log.debug(": {}", om.writeValueAsString(educationMap));
                String educationType = (String) educationMap.get("education_type");
                educationMap.put("education_type", EducationType.getByName(educationType));
                log.debug(": {}", om.writeValueAsString(educationMap));

                Education education = om.readValue(om.writeValueAsString(educationMap), Education.class);
                education.setCandidate(candidate);
                education.setCreatedBy(person);
                educationRepository.save(education);
            }
        }

        //Категории
        if (candidateMap.containsKey("categories")) {
            List<String> categories = (List<String>) candidateMap.get("categories");
            candidateMap.remove("categories");

            Category parent = null;
            for (String categoryName : categories) {
                log.debug("category: {}", categoryName);

                Category category;
                if (parent == null)
                    category = categoryRepository.findByNameAndParentId(categoryName, null);
                else
                    category = categoryRepository.findByNameAndParentId(categoryName, parent.getCategoryId());
                if (category.getHasChild()) {
                    parent = category;
                }
                log.debug("category: {}", category);
                CandidateCategory candidateCategory = new CandidateCategory(candidate, category);

                candidateCategoryRepository.save(candidateCategory);
            }
        }

        //Сохраняем фотографии
        if (photos != null && photos.size() > 0) {
            Base64.Decoder decoder = Base64.getDecoder();
            for (String photoBody : photos) {
                Photo photo = new Photo();
                photo.setCandidateId(candidate.getCandidateId());
                photoService.add(photo, "image.jpeg", decoder.decode(photoBody));
            }
        }
        return candidate;
    }
}