package com.pkit.service;

import com.pkit.model.DemandCandidate;
import com.pkit.model.DemandCandidateNote;

import java.util.List;

public interface DemandCandidateNoteService extends BaseService<DemandCandidateNote, Long> {
  List<DemandCandidateNote> findByDemandCandidate(DemandCandidate demandCandidate);
  List<DemandCandidateNote> findByDemandCandidateIn(List<DemandCandidate> demandCandidates);
}