package com.pkit;

import com.pkit.model.Person;
import com.pkit.model.User;
import com.pkit.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;

import java.util.Collection;

@Slf4j
public class Utils {

    public static String yearToText(int year) {
        String yearString = Integer.toString(year);
        if (year % 100 > 10 && year % 100 < 20) {
            return yearString + " лет";
        } else {
            switch (year % 10) {
                case 1:
                    return yearString + " год";
                case 2:
                case 3:
                case 4:
                    return yearString + " года";
                default:
                    return yearString + " лет";
            }
        }
    }

    public static String toNormalize(Collection<? extends Cacheable> list) {
        StringBuilder sb = new StringBuilder();
        for (Cacheable n : list) {
            sb.append(n.toCacheString()).append(' ');
        }
        return sb.toString().trim();
    }

    public static String getDBException(DataIntegrityViolationException ex) {
        ConstraintViolationException constraintViolationException = (ConstraintViolationException) ex.getCause();
        log.debug("sqlException.getConstraintName(): {}", constraintViolationException.getConstraintName());
        log.debug("sqlException.getMessage(): {}", constraintViolationException.getMessage());
        log.debug("sqlException.getErrorCode(): {}", constraintViolationException.getErrorCode());
        log.debug("sqlException.getSQLState(): {}", constraintViolationException.getSQLState());
        log.debug("sqlException.getSQL(): {}", constraintViolationException.getSQL());

        String constraintName = constraintViolationException.getConstraintName();
        constraintName = constraintName.split(":")[0].substring(1);
        log.debug("constraintName: {}", constraintName);
        String message;
        if ("CANDIDATE_NOTE_FK_CREATED_BY".equals(constraintName))
            message = "У сотрудника есть комментарии.";
        else if ("DEMAND_FK_CONTACT_PERSON_ID".equals(constraintName))
            message = "Сотрудник используется, как контактное лици в заказе.";
        else if ("PERSON_FK_CREATED_BY".equals(constraintName))
            message = "Сотрудник является создателем других пользователей.";
        else if ("DEMAND_FK_COMPANY_ID".equals(constraintName))
            message = "У заказчика есть заказы.";
        else if ("PERSON_FK_DEPARTMENT_ID".equals(constraintName))
            message = "В отделе есть сотрудники.";
        else if ("DEPARTMENT_FK_COMPANY_ID".equals(constraintName))
            message = "В компании есть отделы.";
        else
            message = constraintName;
        message += " Удаление запрещено";
        return message;
    }

    public static Long getPersonId(Authentication authentication) {
        return getPerson(authentication).getPersonId();
    }

    public static Person getPerson(Authentication authentication) {
        UserService.UserDetailsEx userDetailsEx = (UserService.UserDetailsEx) authentication.getPrincipal();

        return userDetailsEx.getPerson();
    }


    public static User getUser(Authentication authentication) {
        UserService.UserDetailsEx userDetailsEx = (UserService.UserDetailsEx) authentication.getPrincipal();

        return userDetailsEx.getUser();
    }
}