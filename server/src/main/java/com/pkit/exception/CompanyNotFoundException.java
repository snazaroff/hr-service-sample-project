package com.pkit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Company")
public class CompanyNotFoundException extends RuntimeException {
  public CompanyNotFoundException(Long id) {
    super(String.format("{\"type\":\"сompany\", \"id\":%d}", id));
  }
}