package com.pkit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Demand Candidate")
public class DemandCandidateNotFoundException extends RuntimeException {
  public DemandCandidateNotFoundException(Long id) {
    super(String.format("{\"type\":\"demandCandidate\", \"id\":%d}", id));
  }
}