package com.pkit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY, reason = "Person bind to department")
public class PersonBindToDepartmentException extends RuntimeException {
    public PersonBindToDepartmentException(String message) {
        super("Сотрудник привязан к отделу");
    }
}
