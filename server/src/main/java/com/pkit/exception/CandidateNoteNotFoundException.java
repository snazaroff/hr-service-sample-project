package com.pkit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Candidate Note")
public class CandidateNoteNotFoundException extends RuntimeException {
  public CandidateNoteNotFoundException(Long id) {
    super(String.format("{\"type\":\"сandidateNote\", \"id\":%d}", id));
  }
}