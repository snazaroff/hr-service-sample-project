package com.pkit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Education")
public class EducationNotFoundException extends RuntimeException {
  public EducationNotFoundException(Long id) {
    super(String.format("{\"type\":\"education\", \"id\":%d}", id));
  }
}