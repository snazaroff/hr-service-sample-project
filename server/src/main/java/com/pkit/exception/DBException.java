package com.pkit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Problem with DB")
public class DBException extends RuntimeException {
    public DBException(String message) {
        super(message);
    }
}
