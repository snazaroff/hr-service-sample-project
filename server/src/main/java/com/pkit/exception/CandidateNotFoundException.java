package com.pkit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Candidate")
public class CandidateNotFoundException extends RuntimeException {
  public CandidateNotFoundException(Long id) {
    super(String.format("{\"type\":\"сandidate\", \"id\":%d}", id));
  }
}