package com.pkit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Demand Candidate Meeting")
public class DemandCandidateMeetingNotFoundException extends RuntimeException {
  public DemandCandidateMeetingNotFoundException(Long id) {
    super(String.format("{\"type\":\"demandCandidateMeeting\", \"id\":%d}", id));
  }
}