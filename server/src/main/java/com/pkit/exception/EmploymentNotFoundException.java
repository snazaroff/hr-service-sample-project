package com.pkit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Employment")
public class EmploymentNotFoundException extends RuntimeException {
  public EmploymentNotFoundException(Long id) {
    super(String.format("{\"type\":\"employment\", \"id\":%d}", id));
  }
}