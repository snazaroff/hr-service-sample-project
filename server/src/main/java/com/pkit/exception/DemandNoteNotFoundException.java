package com.pkit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Demand Note")
public class DemandNoteNotFoundException extends RuntimeException {
  public DemandNoteNotFoundException(Long id) {
    super(String.format("{\"type\":\"demandNote\", \"id\":%d}", id));
  }
}