package com.pkit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Department")
public class DepartmentNotFoundException extends RuntimeException {
  public DepartmentNotFoundException(Long id) {
    super(String.format("{\"type\":\"department\", \"id\":%d}", id));
  }
}
