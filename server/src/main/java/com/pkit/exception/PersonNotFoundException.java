package com.pkit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Person")
public class PersonNotFoundException extends RuntimeException {
  public PersonNotFoundException(Long id) {
    super(String.format("{\"type\":\"person\", \"id\":%d}", id));
  }
}