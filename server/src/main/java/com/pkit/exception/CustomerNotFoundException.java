package com.pkit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Customer")
public class CustomerNotFoundException extends RuntimeException {
  public CustomerNotFoundException(Long id) {
    super(String.format("{\"type\":\"сustomer\", \"id\":%d}", id));
  }
}