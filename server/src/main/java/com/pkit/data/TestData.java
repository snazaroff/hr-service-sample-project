package com.pkit.data;

/**
 * Загрузка тестовых данных, если необходимо
 */
public interface TestData {

    void init();
}
