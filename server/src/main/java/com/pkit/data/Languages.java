package com.pkit.data;

import com.pkit.model.Language;
import com.pkit.repository.LanguageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

@Slf4j
@Component
public class Languages {

    private final LanguageRepository languageRepository;

    private List<Language> languages;

    public Language RUSSIAN;
    public Language ENGLISH;

    public Languages(LanguageRepository languageRepository) {
        this.languageRepository = languageRepository;
    }

    @PostConstruct
    private void init() {
        //todo: пересмотреть
        log.debug("languageRepository: {}", languageRepository);
        languages = languageRepository.findAll();
        log.debug("init(): {}", languages.size());

        RUSSIAN = languageRepository.findByName("Русский");
        ENGLISH = languageRepository.findByName("Английский");
    }

    public List<Language> getLanguages() {
        log.debug("getLanguages(): {}", languages.size());

        return languages;
    }

    public Language findByName(String name) {
        log.debug("findByName('{}')", name);

        return languages.stream()
                .filter(a -> Objects.equals(a.getName(), name))
                .findAny().orElse(null);
    }

    public Language findById(Long languageId) {
        log.debug("findById('{}')", languageId);

        return languages.stream()
                .filter(a -> Objects.equals(a.getLanguageId(), languageId))
                .findAny().orElse(null);
    }
}