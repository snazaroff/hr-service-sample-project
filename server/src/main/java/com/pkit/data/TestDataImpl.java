package com.pkit.data;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.generator.*;
import com.pkit.model.*;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Slf4j
@Component
@ConditionalOnProperty(
        value = "test.data.load",
        havingValue = "true",
        matchIfMissing = false)
public class TestDataImpl implements TestData {

    @Autowired
    ObjectMapper om;

    @Autowired
    CandidateGenerator candidateGenerator;

    @Autowired
    EmailGenerator emailGenerator;

    @Autowired
    PersonGenerator personGenerator;

    @Autowired
    DepartmentGenerator departmentGenerator;

    @Autowired
    DemandGenerator demandGenerator;

    @Autowired
    PhoneGenerator phoneGenerator;

    @Autowired
    CompanyGenerator companyGenerator;

    private final CompanyService companyService;
    private final CandidateService candidateService;
    private final DemandService demandService;
    private final DemandCandidateService demandCandidateService;
    private final DepartmentService departmentService;
    private final PersonService personService;
    private final RoleService roleService;
    private final UserService userService;

    private AtomicInteger userIndex = new AtomicInteger(100);

    public TestDataImpl(
            CandidateService candidateService,
            CompanyService companyService,
            DemandService demandService,
            DemandCandidateService demandCandidateService,
            DepartmentService departmentService,
            PersonService personService,
            RoleService roleService,
            UserService userService) {
        this.candidateService = candidateService;
        this.companyService = companyService;
        this.demandService = demandService;
        this.demandCandidateService = demandCandidateService;
        this.departmentService = departmentService;
        this.personService = personService;
        this.roleService = roleService;
        this.userService = userService;
    }

    @Override
    public void init() {
        log.debug("init()");

        Person mainPerson = personService.get(0L);
        Company adminCompany = companyService.getAll().stream()
                .filter(company -> "Главная фирма".equals(company.getName()))
                .findAny().orElse(null);

        log.debug("adminCompany: {}", adminCompany);

        Department adminDepartment = departmentService.getAll().stream()
                .filter(department -> "Отдел главной фирмы".equals(department.getName()))
                .findAny().orElse(null);
        log.debug("adminDepartment: {}", adminDepartment);

        //Добавляем администраторов сайта
        Person siteAdmin = new Person();
        siteAdmin.setCreatedBy(mainPerson);
        siteAdmin.setDepartment(adminDepartment);
        siteAdmin.setLastName("Патефонов");
        siteAdmin.setFirstName("Прокопий");
        siteAdmin.setMiddleName("Трофимович");
        siteAdmin.setBirthDate(LocalDate.of(1962, 10, 12));
        siteAdmin.setPhone("+7 (495) 101-77-77");
        siteAdmin.setEmail(emailGenerator.generate(siteAdmin));
        siteAdmin.setAddress("д. Париж");
//        siteAdmin.setStartDate(LocalDate.of(1962, 10, 12));
        siteAdmin.setIsActive(true);
        siteAdmin.setPosition("Админ сайта №10");
        siteAdmin.setSex(SexStatus.MALE);

        Person siteAdmin1 = personService.add(siteAdmin);
        createUser(siteAdmin1, RoleType.ROLE_OWNER_ADMIN);

        siteAdmin = new Person();
        siteAdmin.setCreatedBy(mainPerson);
        siteAdmin.setDepartment(adminDepartment);
        siteAdmin.setLastName("Пережопкин");
        siteAdmin.setFirstName("Зигмунд");
        siteAdmin.setMiddleName("Гавриилович");
        siteAdmin.setBirthDate(LocalDate.of(1962, 10, 12));
        siteAdmin.setPhone("+7 (495) 101-77-77");
        siteAdmin.setEmail(emailGenerator.generate(siteAdmin));
        siteAdmin.setAddress("д. Париж");
//        siteAdmin.setStartDate(LocalDate.of(1962, 10, 12));
        siteAdmin.setIsActive(true);
        siteAdmin.setPosition("Админ сайта №20");
        siteAdmin.setSex(SexStatus.MALE);

        Person siteAdmin2 = personService.add(siteAdmin);
        createUser(siteAdmin2, RoleType.ROLE_OWNER_ADMIN);

        Arrays.asList(1, 2).forEach(num -> {
            Company employmentAgency = createEmploymentAgency(siteAdmin1, num, "г. Москва", "+7 (495) 201-00-00");
            List<Person> persons = personService.getByCompanyId(employmentAgency.getCompanyId());
            User userManager = persons
                    .stream()
                    .map(userService::getUserByPerson)
                    .filter(user -> user != null && user.getRoles().contains(new Role(RoleType.ROLE_EA_MANAGER.name())))
                    .findAny().orElse(null);
            log.debug("userManager: {}", userManager);
            if (userManager != null && userManager.getPerson() != null) {
                Person eaManager = userManager.getPerson();
                log.debug("eaManager: {}", eaManager);
                Arrays.asList(1, 2, 3, 4, 5).forEach(customerNum -> {
                    Company customer = createCustomer(eaManager, customerNum, "г. Москва", "+7 (495) 301-00-00");
                    List<Person> customerPersons = personService.getByCompanyId(customer.getCompanyId());
                    log.debug("customerPersons: {}", customerPersons.size());
                    User customerUserManager = customerPersons
                            .stream()
                            .map(userService::getUserByPerson)
                            .filter(user -> user != null && user.getRoles().contains(new Role(RoleType.ROLE_MANAGER.name())))
                            .findAny().orElse(null);
                    Person customerManager = customerUserManager.getPerson();
                    User customerUserUser = customerPersons
                            .stream()
                            .map(userService::getUserByPerson)
                            .filter(user -> user != null && user.getRoles().contains(new Role(RoleType.ROLE_USER.name())))
                            .findAny().orElse(null);
                    Person customerUser = customerUserUser.getPerson();
                    log.debug("customerManager: {}", customerManager);
                    log.debug("customerUser: {}", customerUser);
                    Arrays.asList(1, 2, 3, 4, 5).forEach(demandNum -> {
                        Set<Candidate> candidates = new HashSet<>();
                        Arrays.asList(1, 2, 3, 4).forEach(candidateNum -> {
                            Candidate candidate = candidateGenerator.getCandidate();
                            candidate.setCreatedBy(eaManager);
                            candidate.setCompany(employmentAgency);
                            candidate.setStatus(CandidateStatus.REVIERED);
                            candidate.setEmail(emailGenerator.generate(candidate));
                            candidateService.save(candidate);
                            candidates.add(candidate);
                        });
                        Demand demand = demandGenerator.generate();
                        try {
                            log.debug("demand: " + om.writeValueAsString(demand));
                        } catch (JsonProcessingException e) {
                            log.error(e.getMessage(), e);
                        }
                        demand.setCreatedBy(eaManager);
                        demand.setCompany(customer);
                        demand.setContactPerson(customerUser);
                        demand.setResponsiblePerson(customerUser);
//                        demand.setCandidates(candidates.stream().map(m -> new DemandCandidate()).collect(Collectors.toSet()));
                        demandService.save(demand);

                        candidates.forEach(candidate -> {
                            DemandCandidate demandCandidate = new DemandCandidate();
                            demandCandidate.setDemand(demand);
                            demandCandidate.setCandidate(candidate);
                            demandCandidate.setCreatedBy(eaManager);
                            demandCandidate.setCreateDate(LocalDateTime.now());
                            demandCandidate.setStatus(DemandCandidateStatus.PICKED);
                            demandCandidateService.save(demandCandidate);
                        });
                    });
                });
            }
        });
    }

    private Company createEmploymentAgency(Person createdBy, int employmentAgencyNum, String address, String phonePattern) {

        String companyPhone = phoneGenerator.getCompanyPhone(phonePattern, employmentAgencyNum);
        Company employmentAgency = new Company();
        employmentAgency.setCreatedBy(createdBy);
        employmentAgency.setName("ООО Кадровое агентство №" + employmentAgencyNum);
        employmentAgency.setCompanyType(CompanyType.EMPLOYMENT_AGENCY);
        employmentAgency.setAddress(address);
        employmentAgency.setPhone(companyPhone);
        employmentAgency.setHeadPerson(createdBy);

        employmentAgency.setEmail(emailGenerator.generate(createdBy));

        employmentAgency.setIsActive(true);

        companyService.add(employmentAgency);

        //Добавляем отделы в кадровые агентства
        Arrays.asList(1, 2).forEach(
                num -> createDepartment(createdBy, employmentAgency, num, companyPhone, true)
        );

        return employmentAgency;
    }

    //Добавляем отделы в кадровые агентства
    private Department createDepartment(Person createdBy, Company company, int departmentNum, String phonePattern, boolean isEa) {

        return createDepartment(createdBy, company, "Отдел №" + departmentNum, departmentNum, phonePattern, isEa);
    }

    private Department createDepartment(Person createdBy, Company company, String name, int departmentNum, String phonePattern, boolean isEa) {

        String departmentPhone = phoneGenerator.getDepartmentPhone(phonePattern, departmentNum);

        Department department = new Department();
        department.setCreatedBy(createdBy);
        department.setName(name);
        department.setHead(null);
        department.setContactPerson(null);
        department.setPhone(departmentPhone);
        department.setEmail(emailGenerator.generate(createdBy));
        department.setIsActive(true);
        department.setCompany(company);

        departmentService.add(department);

        //Создаем руководителя отдела
        Person manager = createPerson(createdBy, department, 1, departmentPhone, isEa ? RoleType.ROLE_EA_MANAGER : RoleType.ROLE_MANAGER);
        department.setHead(manager);
        departmentService.add(department);

        //Создаем сотрудника отдела
        Arrays.asList(2, 3, 4).stream().forEach(num -> {
                    Person eaUser = createPerson(manager, department, num, departmentPhone, isEa ? RoleType.ROLE_EA_USER : RoleType.ROLE_USER);
                }
        );
        return department;
    }

    //Добавляем сотрудников в кадровые агентства
    private Person createPerson(Person createdBy, Department department, int personNum, String phonePattern, RoleType roleType) {

        String personPhone = phoneGenerator.getPersonPhone(phonePattern, personNum);

        Person person = personGenerator.generate();
        person.setCreatedBy(createdBy);
        person.setDepartment(department);
        person.setPhone(personPhone);
        person.setEmail(emailGenerator.generate(person));

        personService.add(person);

        createUser(person, roleType);

        return person;
    }

    private Company createCustomer(Person createdBy, int customerNum, String address, String phonePattern) {

        String companyPhone = phoneGenerator.getCompanyPhone(phonePattern, customerNum);

        Company customer = companyGenerator.generate(createdBy);
        customer.setCompanyType(CompanyType.CUSTOMER);
        customer.setPhone(companyPhone);
        customer.setAddress(address);
        customer.setEmail(emailGenerator.generate(createdBy));

        companyService.add(customer);

        Arrays.asList(1, 2, 3, 4, 5).forEach(
                departmentNum -> {
                    Department department = createDepartment(createdBy, customer, departmentGenerator.getDepartmentName(), departmentNum, companyPhone, false);
                    List<Person> persons = personService.getByDepartmentId(department.getDepartmentId());
                    User userManager = persons
                            .stream()
                            .map(userService::getUserByPerson)
                            .filter(user -> user != null && (
                                    user.getRoles().contains(new Role(RoleType.ROLE_EA_MANAGER)) ||
                                            user.getRoles().contains(new Role(RoleType.ROLE_MANAGER))))
                            .findAny().orElse(null);
                    log.debug("userManager: {}", userManager);
                    if (userManager != null && userManager.getPerson() != null) {
                        Person manager = userManager.getPerson();
                        log.debug("manager: {}", manager);
                        department.setHead(manager);
                        departmentService.save(department);
                    }
                }
        );
        return customer;
    }

    private User createUser(Person person, RoleType roleType) {

        Role role = roleService.get(roleType.name());
        List<Role> roles = roleService.getAll();
        log.debug("roleService.getAll(): {}", roles);
        User user = new User();
        user.setIsActive(true);
        user.setLogin("user" + userIndex.getAndIncrement());
        user.setPassword("asdf");
        user.setPerson(person);
        user.setRoles(Arrays.asList(role).stream().collect(Collectors.toSet()));
        userService.add(user);
        return user;
    }
}
