package com.pkit.generator;

import com.pkit.configuration.GeneratorDataConfig;
import org.springframework.stereotype.Component;

@Component
public class MetroGenerator {

    private final GeneratorDataConfig generatorDataConfig;

    public MetroGenerator(GeneratorDataConfig generatorDataConfig) {
        this.generatorDataConfig = generatorDataConfig;
    }

    public String generate() {
        return generatorDataConfig.getPositions().get((int)Math.floor(Math.random()*generatorDataConfig.getPositions().size()));
    }
}