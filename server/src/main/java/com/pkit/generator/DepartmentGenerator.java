package com.pkit.generator;

import com.pkit.configuration.GeneratorDataConfig;
import com.pkit.model.Department;
import com.pkit.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DepartmentGenerator {

    @Autowired
    private PersonGenerator testPersonGenerator;

    @Autowired
    private PhoneGenerator phoneGenerator;

    private final GeneratorDataConfig generatorDataConfig;

    public DepartmentGenerator(GeneratorDataConfig generatorDataConfig) {
        this.generatorDataConfig = generatorDataConfig;
    }

    public Department generate() {
        Department department = new Department();

        department.setName(getDepartmentName());
        department.setPhone(phoneGenerator.generate());
        return department;
    }

    public Department getDepartment() {

        Department department = new Department();

        Person head = testPersonGenerator.getPerson("test");
        head.setDepartment(department);
        Person contactPerson = testPersonGenerator.getPerson("test");
        contactPerson.setDepartment(department);

        department.setName(getDepartmentName());
        department.setPhone(phoneGenerator.generate());
        return department;
    }

    public String getDepartmentName() {
        List<String> departments = generatorDataConfig.getDepartments();
        if (departments.size() == 0) return "Отдел игрушек";
        return departments.get((int)Math.floor(Math.random()*departments.size()));
    }
}