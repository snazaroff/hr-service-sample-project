package com.pkit.generator;

import com.pkit.configuration.GeneratorDataConfig;
import com.pkit.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CandidateStatusGenerator {

    private final GeneratorDataConfig generatorDataConfig;

    public CandidateStatusGenerator(GeneratorDataConfig generatorDataConfig) {
        this.generatorDataConfig = generatorDataConfig;
    }

    public CandidateStatus generate() {
        return CandidateStatus.values()[(int) Math.floor(Math.random() * CandidateStatus.values().length)];
    }
}