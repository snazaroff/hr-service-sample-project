package com.pkit.generator;

import com.pkit.configuration.GeneratorDataConfig;
import com.pkit.model.Candidate;
import com.pkit.model.Employment;
import com.pkit.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Slf4j
@Component
public class EmploymentGenerator {

    private final GeneratorDataConfig generatorDataConfig;

    private final CompanyGenerator companyGenerator;
    private final PositionGenerator positionGenerator;

    private final PersonService personService;

    public EmploymentGenerator(
            GeneratorDataConfig generatorDataConfig,

            CompanyGenerator companyGenerator,
            PositionGenerator positionGenerator,

            PersonService personService
    ) {
        this.generatorDataConfig = generatorDataConfig;

        this.companyGenerator = companyGenerator;
        this.positionGenerator = positionGenerator;

        this.personService = personService;
    }

    public Set<Employment> generate(Candidate candidate) {

        Set<Employment> employments = new HashSet<>();

        LocalDate endDate = candidate.getBirthDate().plusYears(25);

        endDate = endDate.plusDays((int)Math.floor(Math.random()*120));

        while (LocalDate.now().isAfter(endDate)) {

            Employment employment = new Employment();
            employment.setCreatedBy(personService.get(0L));
            employment.setCandidate(candidate);
            employment.setPosition(positionGenerator.generate());
            employment.setFirmName(companyGenerator.getFirmName());
            employment.setStartDate(Date.from(endDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));// new Date(endDate.toEpochDay()));

            endDate = endDate.plusDays(300 + (int)Math.floor(Math.random()*200));

            if (LocalDate.now().isAfter(endDate))
                employment.setEndDate(Date.from(endDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));// new Date(endDate.toEpochDay()));
            employment.setDescription("Тестовый вариант");

            employments.add(employment);
        }
        return employments;
    }
}