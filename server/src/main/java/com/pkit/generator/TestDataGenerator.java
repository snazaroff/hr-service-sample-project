package com.pkit.generator;

import com.ibm.icu.text.Transliterator;
import com.pkit.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Component
public class TestDataGenerator {

    @Autowired
    CompanyGenerator testFirmGenerator;

    private static final String CONVERT_ID = "Any-Latin; NFD; [^\\p{Alnum}] Remove";
    private static final Transliterator cyrillicToLatin = Transliterator.getInstance(CONVERT_ID);

    final String[] position = {"Дворник", "Завхоз", "Главбух", "Менагер", "Директор", "Охранник", "Старший курьер", "Конюх", "Сомелье"};

    public String getPosition() {
        return position[(int)Math.floor(Math.random()*position.length)];
    }

    public String getPhone() {
        ThreadLocalRandom tlr = ThreadLocalRandom.current();
        return String.format("(%3d) %3d-%2d-%2d", tlr.nextInt(1, 999), tlr.nextInt(1, 99), tlr.nextInt(1, 99), tlr.nextInt(1, 99) );
    }

    public String getEMail(String firstName, String lastName, String firmName) {
        return cyrillicToLatin.transliterate(firstName.toLowerCase().charAt(0) + "." + lastName.toLowerCase()) + "@" + cyrillicToLatin.transliterate(firmName.toLowerCase()) + ".ru";
    }

    public Education getSchool(LocalDate date) {

        Random random = new Random();

        Education education = new Education();
        education.setEducationName("Средняя школа №" + random.nextInt(32));
        education.setStartDate(LocalDate.of(date.getYear(), 9, 1));
        education.setEndDate(LocalDate.of(date.getYear() + 10, 6, 1));
        education.setEducationType(EducationType.HIGH_SCHOOL);
        return education;
    }

    private final String[] instituteNames = {
            "Институт народного хозяйства", "Институт культуры", "Московский технологический институт",
            "Московская высшая партийная школа", "Российский православный университет", "Высшая школа экономики"};

    public Education getInstitute(LocalDate date) {

        Education education = new Education();
        education.setEducationName(instituteNames[(int)Math.floor(Math.random()*instituteNames.length)]);
        education.setStartDate(LocalDate.of(date.getYear(), 9, 1));
        education.setEndDate(LocalDate.of(date.getYear() + 5, 9, 1));
        education.setEducationType(EducationType.HIGH_SCHOOL);
        return education;
    }

    public Employment getEmployment(Date startDate, Date endDate) {
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);

        Employment employment = new Employment();
        employment.setFirmName(testFirmGenerator.getFirmName());
        employment.setStartDate(startDate);
        employment.setEndDate(endDate);
        employment.setPosition(getPosition());

        return employment;
    }
}