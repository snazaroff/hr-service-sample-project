package com.pkit.generator;

import com.pkit.configuration.GeneratorDataConfig;
import com.pkit.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SalaryGenerator {

    final long[] salaries = {100L, 200L, 300L, 400L, 500L, 600L, 700L, 800L, 900L, 1000L};

    private final GeneratorDataConfig generatorDataConfig;

    public SalaryGenerator(GeneratorDataConfig generatorDataConfig) {
        this.generatorDataConfig = generatorDataConfig;
    }

    public Salary generate() {
        return new Salary( salaries[(int)Math.floor(Math.random()*salaries.length)], Currency.values()[(int)Math.floor(Math.random()*Currency.values().length)]);
    }
}