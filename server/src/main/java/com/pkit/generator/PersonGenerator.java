package com.pkit.generator;

import com.pkit.configuration.GeneratorDataConfig;
import com.pkit.model.Candidate;
import com.pkit.model.Person;
import com.pkit.model.SexStatus;
import com.pkit.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
@Component
public class PersonGenerator {

    private final GeneratorDataConfig generatorDataConfig;

    private static final LocalDate[] birthDates;

    static {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        birthDates = new LocalDate[]{
                LocalDate.parse("1980-12-01", formatter),
                LocalDate.parse("1955-10-21", formatter),
                LocalDate.parse("1999-05-13", formatter)
        };
    }

    private final AddressGenerator addressGenerator;
    private final BirthDateGenerator birthDateGenerator;
    private final PhoneGenerator phoneGenerator;
    private final TestDataGenerator testDataGenerator;

    public PersonGenerator(
            GeneratorDataConfig generatorDataConfig,

            AddressGenerator addressGenerator,
            BirthDateGenerator birthDateGenerator,
            PhoneGenerator phoneGenerator,
            TestDataGenerator testDataGenerator

    ) {
        this.generatorDataConfig = generatorDataConfig;

        this.addressGenerator = addressGenerator;
                this.birthDateGenerator = birthDateGenerator;
                this.phoneGenerator = phoneGenerator;
                this.testDataGenerator = testDataGenerator;
    }

    public Person generate() {
        Person person = new Person();
        person.setFirstName(getFirstName());
        person.setMiddleName(getMiddleName());
        person.setLastName(getLastName());
        person.setBirthDate(birthDateGenerator.generate());
        person.setEmail(testDataGenerator.getEMail(person.getFirstName(), person.getLastName(), "erwerwerwerwr"));

        person.setIsActive(true);
        person.setSex(SexStatus.MALE);

        person.setPhone(phoneGenerator.generate());
        person.setCell(phoneGenerator.generate());

        person.setAddress(addressGenerator.generate());
        return person;
    }

    public Person getPerson(String firmName) {
        Person person = new Person();
        person.setFirstName(getFirstName());
        person.setLastName(getLastName());
        person.setEmail(testDataGenerator.getEMail(person.getFirstName(), person.getLastName(), firmName));

//        Department department = testDepartmentGenerator.getDepartment();
//        log.debug("department: " + department);
//        log.debug("departmentService: " + departmentService);
//        department = departmentService.add(department);
//        createdBy.setDepartment(department);
        return person;
    }

    public String getPersonName() {
        return getFirstName() + " " + getLastName();
    }

    public Candidate getCandidate() {
        Candidate candidate = new Candidate();
        candidate.setFirstName(getFirstName());
        candidate.setLastName(getLastName());
        candidate.setEmail(testDataGenerator.getEMail(candidate.getFirstName(), candidate.getLastName(), "candidate"));
        candidate.setSex(SexStatus.MALE);
        candidate.setBirthDate(birthDates[(int)Math.floor(Math.random()*birthDates.length)]);
        return candidate;
    }

    public User getUser(String login) {
        User user = new User();
        user.setLogin(login);
        user.setPassword("asdf");
//        user.setFirstName(getFirstName());
//        user.setLastName(getLastName());
        return user;
    }

    public String getFirstName() {
        List<String> firstNames = generatorDataConfig.getFirstNames();
        if (firstNames.size() == 0) return "Иван";
        return firstNames.get((int)Math.floor(Math.random()*firstNames.size()));
    }

    public String getMiddleName() {
        List<String> middleNames = generatorDataConfig.getMiddleNames();
        if (middleNames.size() == 0) return "Иваныч";
        return middleNames.get((int)Math.floor(Math.random()*middleNames.size()));
    }

    public String getLastName() {
        List<String> lastNames = generatorDataConfig.getLastNames();
        if (lastNames.size() == 0) return "Иванов";
        return lastNames.get((int)Math.floor(Math.random()*lastNames.size()));
    }
}