package com.pkit.generator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

@Slf4j
@Component
public class PhoneGenerator {

    public String generate() {
        ThreadLocalRandom tlr = ThreadLocalRandom.current();
        return String.format("+7(%3d) %3d-%2d-%2d",
                tlr.nextInt(100, 999),
                tlr.nextInt(100, 800),
                tlr.nextInt(1, 99),
                tlr.nextInt(1, 99) );
    }

    /**
     * Предполагаем, что все телефоны внутри организации связаны между собой
     * 123-45-67 - первые три цифры одинаковы внутри фирмы
     * вторые две цифры одинаковы для департамента
     * последнии две цифры разные для сотрудников внутри департамента
     * @param phonePattern
     * @param companyNum
     * @return
     */
    public String getCompanyPhone(String phonePattern, int companyNum) {
        log.debug("getCompanyPhone( phonePattern: {}, companyNum: {})", phonePattern, companyNum);

        String[] parts = phonePattern.split("-");
        log.debug("parts.length: {}, parts: {}", parts.length, Arrays.asList(parts));

        parts[0] = parts[0].substring(0, parts[0].length() - 1) + companyNum;
        log.debug("parts: {}", String.join("-", parts));
        return String.join("-", parts);
    }

    public String getDepartmentPhone(String phonePattern, int departmentNum) {
        log.debug("getDepartmentPhone( phonePattern: {}, departmentNum: {})", phonePattern, departmentNum);

        String[] parts = phonePattern.split("-");
        log.debug("parts.length: {}, parts: {}", parts.length, Arrays.asList(parts));

        parts[1] = String.format("%02d", departmentNum);
        log.debug("parts: {}", String.join("-", parts));
        return String.join("-", parts);
    }

    public String getPersonPhone(String phonePattern, int personNum) {
        log.debug("getPersonPhone( phonePattern: {}, personNum: {})", phonePattern, personNum);

        String[] parts = phonePattern.split("-");
        log.debug("parts.length: {}, parts: {}", parts.length, Arrays.asList(parts));

        parts[2] = String.format("%02d", personNum);
        log.debug("parts: {}", String.join("-", parts));
        return String.join("-", parts);
    }
}