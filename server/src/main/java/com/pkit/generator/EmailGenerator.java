package com.pkit.generator;

import com.ibm.icu.text.Transliterator;
import com.pkit.model.Human;
import org.springframework.stereotype.Component;

@Component
public class EmailGenerator {

    private static final String CONVERT_ID = "Any-Latin; NFD; [^\\p{Alnum}] Remove";
    private static final Transliterator cyrillicToLatin = Transliterator.getInstance(CONVERT_ID);

    private final String[] emailServers = {"gmail", "mail", "yandex", "rambler", "yahoo", "pochta"};

    public String generate(String firstName, String lastName, String firmName) {
        return cyrillicToLatin.transliterate(firstName.toLowerCase().charAt(0) + "." + lastName.toLowerCase()) + "@" + cyrillicToLatin.transliterate(firmName.toLowerCase()) + ".ru";
    }

    public String generate(Human human, String firmName) {
        return cyrillicToLatin.transliterate(human.getFirstName().toLowerCase().charAt(0) + "." + human.getLastName().toLowerCase()).replaceAll("ʹ", "") + "@" + cyrillicToLatin.transliterate(firmName.toLowerCase()).replaceAll("ʹ", "") + ".ru";
    }

    public String generate(Human human) {
        String firmName = emailServers[(int)Math.floor(Math.random()*emailServers.length)];
        return generate(human, firmName);
    }
}