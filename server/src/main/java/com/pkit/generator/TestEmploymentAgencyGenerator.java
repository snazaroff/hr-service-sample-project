package com.pkit.generator;

import com.pkit.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TestEmploymentAgencyGenerator {

    @Autowired
    private TestDataGenerator testDataGenerator;

    @Autowired
    private PersonGenerator testPersonGenerator;

    @Autowired
    private CompanyGenerator testFirmGenerator;

    public Company getEmploymentAgency() {
        Company employmentAgency = new Company();
        String firmName = testFirmGenerator.getFirmName();
        employmentAgency.setCompanyType(CompanyType.EMPLOYMENT_AGENCY);
        employmentAgency.setName(firmName);
        Person headPerson = testPersonGenerator.getPerson(firmName);
        employmentAgency.setHeadPerson(headPerson);
        Person contactPerson = testPersonGenerator.getPerson(firmName);
        employmentAgency.setContactPerson(contactPerson);

        Person responsiblePerson = testPersonGenerator.getPerson(firmName);
//        employmentAgency.setResponsiblePerson(responsiblePerson.getFirstName() + " " + responsiblePerson.getLastName());
        employmentAgency.setPhone(testDataGenerator.getPhone());
        employmentAgency.setCell(testDataGenerator.getPhone());
        employmentAgency.setEmail(responsiblePerson.getEmail());

//        employmentAgency.setContractNumber(testDataGenerator.getPhone());
//        employmentAgency.setAgencyType(1L);
//        employmentAgency.setContractType(1L);
//        employmentAgency.setStatus(1L);
//        employmentAgency.setPaymentType("1L");

        return employmentAgency;
    }
}