package com.pkit.generator;

import com.pkit.model.*;
import com.pkit.model.Currency;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.*;

@Slf4j
@Component
public class DemandGenerator {

    private final CompanyGenerator testFirmGenerator;
    private final PositionGenerator positionGenerator;
    private final SalaryGenerator salaryGenerator;
    private final TestDataGenerator testDataGenerator;

    private final CompanyService customerService;
    private final DemandService demandService;
    private final PersonService personService;

    public DemandGenerator(
            CompanyService customerService,
            DemandService demandService,
            PersonService personService,

            CompanyGenerator testFirmGenerator,
            PositionGenerator positionGenerator,
            SalaryGenerator salaryGenerator,
            TestDataGenerator testDataGenerator
    ) {
        this.customerService = customerService;
        this.demandService = demandService;
        this.personService = personService;

        this.positionGenerator = positionGenerator;
        this.salaryGenerator = salaryGenerator;
        this.testFirmGenerator = testFirmGenerator;
        this.testDataGenerator = testDataGenerator;
    }

    public Demand generate() {

        Demand demand = new Demand();
        demand.setCreateDate(LocalDateTime.now());
        demand.setSalary(salaryGenerator.generate());
        demand.setPosition(testDataGenerator.getPosition());
        demand.setStatus(DemandStatus.CREATE);
        demand.setDescription("Demand description");
        demand.setPosition(positionGenerator.generate());
        return demand;
    }

    public Demand createDemand() {

        Map<String, Object> firm = testFirmGenerator.getFirm(2);

        Company customer = (Company)firm.get("firm");

        List<Person> persons = (List<Person>)firm.get("persons");

        Person contactPerson = persons.get(0);
        Person responsiblePerson = persons.get(1);

        Demand demand = new Demand();
        demand.setCompany(customer);
        demand.setContactPerson(contactPerson);
        demand.setResponsiblePerson(responsiblePerson);
        demand.setCreateDate(LocalDateTime.now());
        demand.setSalary(new Salary(Math.round(Math.random() * 1000), Currency.RUB));
        demand.setPosition(testDataGenerator.getPosition());
        demand.setStatus(DemandStatus.CREATE);
        demand.setDescription("Demand description");

        customer = customerService.add(customer);
        contactPerson = personService.add(contactPerson);
        demand.setContactPerson(contactPerson);
        responsiblePerson = personService.add(responsiblePerson);
        demand.setResponsiblePerson(responsiblePerson);
        demand = demandService.add(demand);

        return demand;
    }

    public void destroyDemand(Demand demand) {
        log.debug("destroyDemand( demand: {})", demand);

        demandService.delete(demand);

        Person person = demand.getContactPerson();
        if (person == null)
            throw new RuntimeException("demand.getContactPerson() == null");

        personService.delete(person);

        person = demand.getResponsiblePerson();
        if (person == null)
            throw new RuntimeException("demand.getResponsiblePerson() == null");

        personService.delete(person);

        Company customer = demand.getCompany();
        if (customer == null)
            throw new RuntimeException("demand.getCompany() == null");

        customerService.delete(customer);
    }
}