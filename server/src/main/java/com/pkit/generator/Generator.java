package com.pkit.generator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.configuration.GeneratorDataConfig;
import com.pkit.model.*;
import com.pkit.model.Currency;
import com.pkit.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
public class Generator {

    @Autowired
    private ObjectMapper om;

    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private final CandidateGenerator candidateGenerator;
    private final CompanyGenerator companyGenerator;
    private final DemandGenerator demandGenerator;
    private final DepartmentGenerator departmentGenerator;
    private final EmailGenerator emailGenerator;
    private final PersonGenerator personGenerator;
    private final GeneratorDataConfig generatorDataConfig;

    private final CandidateService candidateService;
    private final CompanyService companyService;
    private final DemandCandidateMeetingService demandCandidateMeetingService;
    private final DemandCandidateNoteService demandCandidateNoteService;
    private final DemandCandidateService demandCandidateService;
    private final DemandNoteService demandNoteService;
    private final DemandService demandService;
    private final DepartmentService departmentService;
    private final PersonService personService;
    private final RoleService roleService;
    private final UserService userService;

    private final Map<String, Person> persons = new HashMap<>();
    private final Map<String, Candidate> candidates = new HashMap<>();

    public Generator(
            CandidateGenerator candidateGenerator,
            CompanyGenerator companyGenerator,
            GeneratorDataConfig generatorDataConfig,
            DemandGenerator demandGenerator,
            DepartmentGenerator departmentGenerator,
            EmailGenerator emailGenerator,
            PersonGenerator personGenerator,

            CandidateService candidateService,
            CompanyService companyService,
            DemandCandidateMeetingService demandCandidateMeetingService,
            DemandCandidateNoteService demandCandidateNoteService,
            DemandCandidateService demandCandidateService,
            DemandNoteService demandNoteService,
            DemandService demandService,
            DepartmentService departmentService,
            PersonService personService,
            RoleService roleService,
            UserService userService
    ) {
        this.generatorDataConfig = generatorDataConfig;

        this.candidateGenerator = candidateGenerator;
        this.companyGenerator   = companyGenerator;
        this.demandGenerator    = demandGenerator;
        this.departmentGenerator = departmentGenerator;
        this.emailGenerator     = emailGenerator;
        this.personGenerator    = personGenerator;

        this.candidateService = candidateService;
        this.companyService = companyService;
        this.demandCandidateMeetingService = demandCandidateMeetingService;
        this.demandCandidateNoteService = demandCandidateNoteService;
        this.demandCandidateService = demandCandidateService;
        this.demandNoteService = demandNoteService;
        this.demandService = demandService;
        this.departmentService = departmentService;
        this.personService = personService;
        this.roleService = roleService;
        this.userService = userService;
    }

    @PostConstruct
    private void init() throws IOException {
        log.debug("init()");
        log.debug("generatorDataConfig.getFirstNames(): {}", generatorDataConfig.getFirstNames());
        log.debug("generatorDataConfig: {}", om.writeValueAsString(generatorDataConfig));

        generate();
    }

    private void generate() {
        log.debug("generate()");

        for (int i = 0; i < 10; i++) {
            Person person = personGenerator.generate();
            person.setDepartment(departmentGenerator.getDepartment());
            log.debug("person: {}", person);
        }

        Person person = personService.get(0L);
        try {
            log.debug("structure: {}", om.writeValueAsString(generatorDataConfig.getStructure()));

            Map<String, Object> structure = generatorDataConfig.getStructure();

            List<Company> companies = null;
            if (structure.containsKey("companies")) {
                Map<String, Map> mapCompanies = (Map) structure.get("companies");

                companies = mapCompanies.entrySet()
                        .stream()
                        .map( stringMapEntry -> {
                            try {
                                return generateCompany(stringMapEntry.getValue(), person);
                            } catch (JsonProcessingException e) {
                                log.error(e.getMessage(), e);
                                return null;
                            }
                        })
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());
//                for (Map.Entry<String, Map> entry : companies.entrySet()) {
//                    generateCompany(entry.getValue(), person);
//                }
            }
            Company company = (companies != null && companies.size() > 0) ? companies.get(0) : null;
            if (structure.containsKey("candidates")) {
                Map<String, Map> candidates = (Map) structure.get("candidates");

                for (Map.Entry<String, Map> entry : candidates.entrySet()) {
                    candidateService.add(generateCandidate(entry.getValue(), company, person));
                }
            }
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
    }

    private Company generateCompany(Map map, Person person) throws JsonProcessingException {
        log.debug("generateCompany(map: {}, person: {})", map, person);

        Boolean isGenerated = (Boolean) map.getOrDefault("isGenerated", Boolean.FALSE);
        if (isGenerated) {
            Company company = companyGenerator.generate(person);
            log.debug("company: {}", om.writeValueAsString(company));
            company = companyService.add(company);
            Map<String, Map> departments = (Map) map.get("departments");
            log.debug(departments.getClass().getName());
            for (Map.Entry<String, Map> entry : departments.entrySet()) {
                Map value = entry.getValue();
                log.debug("entry.getValue(): {}", entry.getValue());
                log.debug("entry.getValue(): {}", entry.getValue().getClass().getName());
                generateDepartment(value, person, company);
            }

            if (map.containsKey("demands")) {
                Map<String, Map> demands = (Map) map.get("demands");

                if (demands != null) {
                    for (Map demand : demands.values()) {
                        generateDemand(demand, company);
                    }
                }
            }

            if (map.containsKey("candidates")) {
                Map<String, Map> candidates = (Map) map.get("candidates");

                for (Map candidate : candidates.values()) {
                    candidateService.add(generateCandidate(candidate, company, person));
                }
            }

//            if(structure.containsKey("candidates")) {
//                Map<String, Map> candidates = (Map) structure.get("candidates");
//                for (Map candidate : candidates.values()) {
//                    candidateService.add(generateCandidate(candidate, company, person));
//                }
//            }
            return company;
        }
        return null;
    }

    private void generateDepartment(Map map, Person person, Company company) {
        log.debug("generateDepartment(map: {}, person: {}, company: {})", map, person, company);

        Boolean isGenerated = (Boolean) map.getOrDefault("isGenerated", Boolean.FALSE);
        if (isGenerated) {
            Department department = departmentGenerator.generate();
            department.setCompany(company);
            department.setCreatedBy(person);
            department.setEmail(emailGenerator.generate(person));
            department = departmentService.add(department);
            Map<String, Map> persons = (Map) map.get("persons");
            for (Map.Entry<String, Map> entry : persons.entrySet()) {
                Map value = entry.getValue();
                log.debug("entry.getValue(): {}", entry.getValue());
                log.debug("entry.getValue(): {}", entry.getValue().getClass().getName());
                generatePerson(value, department);
            }
        }
    }

    private void generatePerson(Map map, Department department) {
        log.debug("generatePerson( map: {}, department: {})", map, department);

        Boolean isGenerated = (Boolean) map.getOrDefault("isGenerated", Boolean.FALSE);
        if (isGenerated) {

            Person person = personGenerator.generate();
            person.setCreatedBy(personService.get(0L));
            person.setDepartment(department);
            person.setEmail(emailGenerator.generate(person));
            personService.add(person);
            Map<String, Map> user = (Map) map.get("user");
            if (user != null) {
                generateUser(user, person);
            }
            if (map.containsKey("id")) {
                persons.put(map.get("id").toString(), person);
            }
        }
    }

    private void generateUser(Map map, Person person) {
        log.debug("generateUser( map: {}, person: {})", map, person);

        String login = map.getOrDefault("login", "simple").toString();
        String password = map.getOrDefault("password", "12345").toString();
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        user.setPerson(person);
        user.setIsActive(true);
        if (map.containsKey("roles")) {
            log.debug("roles: {}", map.get("roles"));
            log.debug("roles: {}", map.get("roles").getClass().getName());
            Map<String, String> roles = (Map) map.get("roles");
            log.debug("roles: {}", roles);
            for (String roleId : roles.values()) {
//                RoleType roleType = RoleType.valueOf(roleId);
                Role role = roleService.get(roleId);
                log.debug("role: {}", role);
                if (role == null) {
                    role = new Role(roleId);
                    user.getRoles().add(role);
                }
            }
        }
        userService.add(user);
    }

    private void generateDemand(Map map, Company company) throws JsonProcessingException {
        log.debug("generateDemand(map: {}, company: {})", map, company);

        Boolean isGenerated = (Boolean) map.getOrDefault("isGenerated", Boolean.FALSE);
        if (isGenerated) {
            Demand demand = demandGenerator.generate();
            log.debug("demand: {}", om.writeValueAsString(demand));
            demand.setCompany(company);
            if (map.containsKey("createdBy")) {
                String personId = map.get("createdBy").toString();
                log.debug("personId: {}", personId);
                Person createdBy = persons.get(personId);
                log.debug("createdBy: {}", createdBy);
                demand.setCreatedBy(createdBy);
            } else
                demand.setCreatedBy(company.getCreatedBy());
            demand.setCreateDate(LocalDateTime.now());
            demand.setStatus(DemandStatus.CREATE);
            if (map.containsKey("description"))
                demand.setDescription((String) map.get("description"));
            if (map.containsKey("position"))
                demand.setPosition((String) map.get("position"));
            log.debug("map: {}", map);
//            log.debug("map.get(\"salary\"): {}", map.get("salary"));
            demand.setSalary(new Salary((Integer) map.get("salary"), Currency.RUB));
            demand.setFinishDate(LocalDate.now().plusDays(45));

            if (map.containsKey("contactPerson")) {
                String contactPersonId = map.get("contactPerson").toString();
                log.debug("contactPersonId: {}", contactPersonId);
                Person contactPerson = persons.get(contactPersonId);
                log.debug("contactPerson: {}", contactPerson);
                demand.setContactPerson(contactPerson);
            }
            if (map.containsKey("responsiblePerson")) {
                String responsiblePersonId = map.get("responsiblePerson").toString();
                log.debug("responsiblePersonId: {}", responsiblePersonId);
                Person responsiblePerson = persons.get(responsiblePersonId);
                log.debug("responsiblePerson: {}", responsiblePerson);
                demand.setResponsiblePerson(responsiblePerson);
            }
            demand = demandService.add(demand);

            if (map.containsKey("notes")) {
                Map<String, Map> notes = (Map) map.get("notes");
                for (Map note : notes.values()) {
                    log.debug("note: {}", note);
                    DemandNote demandNote = new DemandNote();
                    demandNote.setNote(note.get("note").toString());
                    demandNote.setDemand(demand);
                    demandNote.setCreatedBy(getCreatedBy(note, company.getCreatedBy()));
//                  demandNote.setCreateDate(LocalDateTime.now());
                    demandNoteService.add(demandNote);
                }
            }
            if (map.containsKey("candidates")) {
                Map<String, Map> candidates = (Map) map.get("candidates");
                for (Map candidate : candidates.values()) {
                    log.debug("candidate: {}", candidate);
                    DemandCandidate demandCandidate = new DemandCandidate();
                    demandCandidate.setDemand(demand);
                    demandCandidate.setCandidate(candidateService.add(generateCandidate(candidate, company, company.getCreatedBy())));
                    demandCandidate.setCreateDate(LocalDateTime.now());
                    demandCandidate.setCreatedBy(company.getCreatedBy());
                    demandCandidate.setStatus(DemandCandidateStatus.PICKED);
                    demandCandidate = demandCandidateService.add(demandCandidate);
                    log.debug("candidate.containsKey(\"notes\"): {}", candidate.containsKey("notes"));
                    if (candidate.containsKey("notes")) {
                        for (Map note : ((Map<String, Map>) candidate.get("notes")).values()) {
                            DemandCandidateNote demandCandidateNote = new DemandCandidateNote();
                            demandCandidateNote.setDemandCandidate(demandCandidate);
                            demandCandidateNote.setCreateDate(LocalDateTime.now());
                            demandCandidateNote.setCreatedBy(getCreatedBy(note, company.getCreatedBy()));
                            demandCandidateNote.setNote((String) note.get("note"));
                            demandCandidateNote = demandCandidateNoteService.add(demandCandidateNote);
                        }
                    }
                    if (candidate.containsKey("meetings")) {
                        for (Map meeting : ((Map<String, Map>) candidate.get("meetings")).values()) {
                            DemandCandidateMeeting demandCandidateMeeting = new DemandCandidateMeeting();
                            demandCandidateMeeting.setDemandCandidate(demandCandidate);
                            demandCandidateMeeting.setCreateDate(LocalDateTime.now());
                            demandCandidateMeeting.setCreatedBy(getCreatedBy(meeting, company.getCreatedBy()));
                            if (meeting.containsKey("type"))
                                demandCandidateMeeting.setType(MeetingType.valueOf(meeting.get("type").toString()));
                            else
                                demandCandidateMeeting.setType(MeetingType.HEAD_TO_HEAD);
                            if (meeting.containsKey("meetingDateTime"))
                                demandCandidateMeeting.setMeetingDateTime(LocalDateTime.parse(meeting.get("meetingDateTime").toString(), dateTimeFormatter));
                            else
                                demandCandidateMeeting.setMeetingDateTime(LocalDateTime.now().plusDays(5).withHour(15).withMinute(15).withSecond(0));
                            log.debug("demandCandidateMeeting: {}", demandCandidateMeeting);
                            demandCandidateMeeting = demandCandidateMeetingService.add(demandCandidateMeeting);
                        }
                    }
                }
            }
        }
    }

    private Candidate generateCandidate(Map map, Company company, Person person) {
        log.debug("generateCandidate( map: {}, company: {}, person: {})", map, company, person);

        Boolean isGenerated = (Boolean) map.getOrDefault("isGenerated", Boolean.FALSE);
        if (isGenerated) {
            Candidate candidate = candidateGenerator.generate();
            candidate.setCreatedBy(getCreatedBy(map, person));
            candidate.setCreateDate(LocalDateTime.now());
            candidate.setCompany(company);
            if (map.containsKey("id")) {
                candidates.put(map.get("id").toString(), candidate);
            }
            return candidate;
        }
        return null;
    }

    private Person getCreatedBy(Map map, Person defaultPerson) {
        if (map.containsKey("createdBy")) {
            String personId = map.get("createdBy").toString();
            Person createdBy = persons.get(personId);
            log.debug("personId: {}, createdBy: {}", personId, createdBy);
            return createdBy;
        } else {
            return defaultPerson;
        }
    }
}