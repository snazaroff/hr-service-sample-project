package com.pkit.generator;

import com.pkit.configuration.GeneratorDataConfig;
import com.pkit.data.Languages;
import com.pkit.model.Candidate;
import com.pkit.model.CandidateLanguage;
import com.pkit.model.Language;
import com.pkit.model.LanguageStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class LanguageGenerator {

    private final GeneratorDataConfig generatorDataConfig;

    private final List<Language> languages;// = Languages.getLanguages();

    public LanguageGenerator(GeneratorDataConfig generatorDataConfig, Languages languages) {
        this.generatorDataConfig = generatorDataConfig;
        this.languages = languages.getLanguages();
    }

    public CandidateLanguage generate(Candidate candidate) {

        CandidateLanguage candidateLanguage = new CandidateLanguage(candidate, languages.get((int)Math.floor(Math.random()*languages.size())));
        candidateLanguage.setStatus(LanguageStatus.values()[(int) Math.floor(Math.random() * LanguageStatus.values().length)]);

        return candidateLanguage;
    }
}