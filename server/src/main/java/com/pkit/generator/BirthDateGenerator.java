package com.pkit.generator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Slf4j
@Component
public class BirthDateGenerator {

    private final int year = LocalDate.now().getYear();

    public LocalDate generate() {

        int age = 18 + (int)Math.floor(Math.random()*50);

        return LocalDate.of(year - age, 1 + (int)Math.floor(Math.random()*12), 1 + (int)Math.floor(Math.random()*28));
    }
}