package com.pkit.generator;

import com.pkit.configuration.GeneratorDataConfig;
import org.springframework.stereotype.Component;

@Component
public class DegreeGenerator {

    private final GeneratorDataConfig generatorDataConfig;

    private final String[] degrees = {"Бакалавр", "Магистр", "Кандитат гуманитарных наук"};

    public DegreeGenerator(GeneratorDataConfig generatorDataConfig) {
        this.generatorDataConfig = generatorDataConfig;
    }

    public String generate() {
        return degrees[(int)Math.floor(Math.random()*degrees.length)];

    }
}