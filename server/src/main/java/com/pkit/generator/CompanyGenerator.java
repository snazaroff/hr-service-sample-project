package com.pkit.generator;

import com.pkit.configuration.GeneratorDataConfig;
import com.pkit.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.*;

@Slf4j
@Component
public class CompanyGenerator {

    private final AddressGenerator addressGenerator;
    private final EmailGenerator emailGenerator;
    private final PhoneGenerator phoneGenerator;

    private final GeneratorDataConfig generatorDataConfig;

    public CompanyGenerator(
            GeneratorDataConfig generatorDataConfig,
            AddressGenerator addressGenerator,
            EmailGenerator emailGenerator,
            PhoneGenerator phoneGenerator
    ) {
        this.generatorDataConfig = generatorDataConfig;

        this.addressGenerator = addressGenerator;
        this.emailGenerator = emailGenerator;
        this.phoneGenerator = phoneGenerator;
    }

    public Company generate(Person person) {
        Company company = new Company();
        company.setCreateDate(LocalDateTime.now());
        company.setCreatedBy(person);
        company.setName(getFirmName());
        company.setAddress(addressGenerator.generate());
        company.setPhone(phoneGenerator.generate());
        company.setEmail(emailGenerator.generate(person, company.getName()));
        company.setCompanyType(CompanyType.CUSTOMER);
        company.setIsActive(true);
        return company;
    }

    public Map<String, Object> getFirm(int personCount) {
        log.debug("getFirm( personCount: {})", personCount);

        Map<String, Object> result = new HashMap<>();

        String firmName = getFirmName();
        String firmType = getFirmType();

        Company customer = new Company();
        customer.setName(firmType + " " + firmName);
        customer.setAddress(addressGenerator.generate());

        result.put("firm", customer);

        return result;
    }

    public String getFirmName() {
        List<String> firmNames = generatorDataConfig.getFirmNames();
        if (firmNames.size() == 0) return "Рога и копыта";
        return getFirmType() + " " + firmNames.get((int) Math.floor(Math.random() * firmNames.size()));
    }

    private String getFirmType() {
        List<String> firmTypes = generatorDataConfig.getFirmTypes();
        if (firmTypes.size() == 0) return "ЫЫЫ";
        return firmTypes.get((int) Math.floor(Math.random() * firmTypes.size()));
    }
}