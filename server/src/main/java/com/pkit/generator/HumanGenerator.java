package com.pkit.generator;

import com.pkit.configuration.GeneratorDataConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Slf4j
@Component
public class HumanGenerator {

    private final GeneratorDataConfig generatorDataConfig;

    private final AddressGenerator addressGenerator;
    private final BirthDateGenerator birthDateGenerator;
    private final PhoneGenerator phoneGenerator;

    public HumanGenerator(
            GeneratorDataConfig generatorDataConfig,
            AddressGenerator addressGenerator,
            BirthDateGenerator birthDateGenerator,
            PhoneGenerator phoneGenerator
    ) {
        this.generatorDataConfig = generatorDataConfig;

        this.addressGenerator = addressGenerator;
        this.birthDateGenerator = birthDateGenerator;
        this.phoneGenerator = phoneGenerator;
    }

    public String getFirstName() {
        List<String> firstNames = generatorDataConfig.getFirstNames();
        if (firstNames.size() == 0) return "Иван";
        return firstNames.get((int)Math.floor(Math.random()*firstNames.size()));
    }

    public String getMiddleName() {
        List<String> middleNames = generatorDataConfig.getMiddleNames();
        if (middleNames.size() == 0) return "Иваныч";
        return middleNames.get((int)Math.floor(Math.random()*middleNames.size()));
    }

    public String getLastName() {
        List<String> lastNames = generatorDataConfig.getLastNames();
        if (lastNames.size() == 0) return "Иванов";
        return lastNames.get((int)Math.floor(Math.random()*lastNames.size()));
    }

    public LocalDate getBirthDate() {
        return birthDateGenerator.generate();
    }

    public String getAddress() {
        return addressGenerator.generate();
    }

    public String getPhone() {
        return phoneGenerator.generate();
    }

    public String getCell() {
        return phoneGenerator.generate();
    }
}