package com.pkit.generator;

import com.pkit.configuration.GeneratorDataConfig;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AddressGenerator {

    private final GeneratorDataConfig generatorDataConfig;

    public AddressGenerator(GeneratorDataConfig generatorDataConfig) {
        this.generatorDataConfig = generatorDataConfig;
    }

    public String generate() {
        String city = getCity();
        String street = getStreet();
        int home = getHome();
        return String.format("%s %s д.%s", city, street, home);
    }

    private String getCity() {
        List<String> cities = generatorDataConfig.getCities();
        if (cities.size() == 0) return "г. Нерезиновск";
        return cities.get((int)Math.floor(Math.random()*cities.size()));
    }

    private String getStreet() {
        List<String> streets = generatorDataConfig.getStreets();
        if (streets.size() == 0) return "тупик Перспективный";
        return streets.get((int)Math.floor(Math.random()*streets.size()));
    }

    private int getHome() {
        return 1 + (int)Math.floor(Math.random()*99);
    }
}