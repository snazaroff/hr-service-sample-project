package com.pkit.generator;

import com.pkit.configuration.GeneratorDataConfig;
import com.pkit.model.Candidate;
import com.pkit.model.Education;
import com.pkit.model.EducationType;
import com.pkit.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Component
public class EducationGenerator {

    private final GeneratorDataConfig generatorDataConfig;

    private final PositionGenerator positionGenerator;

    private final PersonService personService;

    public EducationGenerator(
            GeneratorDataConfig generatorDataConfig,
            PositionGenerator positionGenerator,
            PersonService personService
    ) {
        this.generatorDataConfig = generatorDataConfig;
        this.positionGenerator = positionGenerator;
        this.personService = personService;
    }

    public Set<Education> generate(Candidate candidate) {

        Set<Education> educations = new HashSet<>();

        LocalDate endDate = candidate.getBirthDate().plusYears(7);

        //Школа
        Education education = new Education();
        education.setCreatedBy(personService.get(0L));
        education.setCandidate(candidate);
        education.setSpecialization(positionGenerator.generate());
        education.setEducationName("Школа №" + (int)Math.floor(Math.random()*2000));
        education.setStartDate(endDate.withMonth(9).withDayOfMonth(1));
        endDate = endDate.plusYears(10).withMonth(7).withDayOfMonth(1);
        education.setEndDate(endDate);
        education.setEducationType(EducationType.HIGH_SCHOOL);
        educations.add(education);

        //Институт
        education = new Education();
        education.setCreatedBy(personService.get(0L));
        education.setCandidate(candidate);
        education.setSpecialization(positionGenerator.generate());
        education.setEducationName("Школа №" + (int)Math.floor(Math.random()*2000));
        endDate = endDate.withMonth(9).withDayOfMonth(1);
        education.setStartDate(endDate);
        education.setEndDate(endDate.plusYears(5).withMonth(7).withDayOfMonth(1));
        education.setEducationType(EducationType.HIGH_SCHOOL);
        educations.add(education);

//        endDate = endDate.plusDays(300 + (int)Math.floor(Math.random()*200));
//
//        if (LocalDate.now().isAfter(endDate))
//            education.setEndDate(endDate);
//
//
//        while (LocalDate.now().isAfter(endDate)) {
//
//            Education education = new Education();
//            education.setCandidate(candidate);
//            education.setPosition(positionGenerator.generate());
//            education.setEducationName(companyGenerator.getFirmName());
//            education.setStartDate(endDate);
//
//            endDate = endDate.plusDays(300 + (int)Math.floor(Math.random()*200));
//
//            if (LocalDate.now().isAfter(endDate))
//                education.setEndDate(endDate);
//            education.setEducationType();
//
//            educations.add(education);
//        }
        return educations;
    }
}