package com.pkit.generator;

import com.pkit.configuration.GeneratorDataConfig;
import com.pkit.model.*;
import com.pkit.service.DictionaryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
@Component
public class CandidateGenerator extends HumanGenerator {

    private final DictionaryService dictionaryService;

    private static final LocalDate[] birthDates;

    static {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        birthDates = new LocalDate[]{
                LocalDate.parse("1980-12-01", formatter),
                LocalDate.parse("1955-10-21", formatter),
                LocalDate.parse("1999-05-13", formatter)
        };
    }

    @Autowired
    TestDataGenerator testDataGenerator;

    @Autowired
    EmailGenerator emailGenerator;

    private final EducationGenerator educationGenerator;
    private final EmploymentGenerator employmentGenerator;
    private final DegreeGenerator degreeGenerator;
    private final LanguageGenerator languageGenerator;
    private final PositionGenerator positionGenerator;

    @Autowired
    SalaryGenerator salaryGenerator;

    @Autowired
    CandidateStatusGenerator candidateStatusGenerator;

    public CandidateGenerator(
            GeneratorDataConfig generatorDataConfig,

            AddressGenerator addressGenerator,
            BirthDateGenerator birthDateGenerator,
            EducationGenerator educationGenerator,
            EmploymentGenerator employmentGenerator,
            DegreeGenerator degreeGenerator,
            LanguageGenerator languageGenerator,
            PhoneGenerator phoneGenerator,
            PositionGenerator positionGenerator,

            DictionaryService dictionaryService
    ) {
        super(generatorDataConfig, addressGenerator, birthDateGenerator, phoneGenerator);

        this.educationGenerator = educationGenerator;
        this.employmentGenerator = employmentGenerator;
        this.degreeGenerator    = degreeGenerator;
        this.languageGenerator  = languageGenerator;
        this.positionGenerator  = positionGenerator;

        this.dictionaryService = dictionaryService;
    }

    public Candidate generate() {
        Candidate candidate = new Candidate();
        candidate.setFirstName(getFirstName());
        candidate.setMiddleName(getMiddleName());
        candidate.setLastName(getLastName());
        candidate.setBirthDate(getBirthDate());
        candidate.setEmail(emailGenerator.generate(candidate));

        candidate.setSex(SexStatus.MALE);

        candidate.setPhone(getPhone());
        candidate.setCell(getPhone());

        candidate.setAddress(getAddress());
        candidate.setMetro(getMetro());
        candidate.setStatus(candidateStatusGenerator.generate());
        candidate.setPosition(positionGenerator.generate());

        candidate.setEmployments(employmentGenerator.generate(candidate));
        candidate.setEducations(educationGenerator.generate(candidate));

        candidate.getLanguages().add(languageGenerator.generate(candidate));
        candidate.getLanguages().add(languageGenerator.generate(candidate));
        candidate.getLanguages().add(languageGenerator.generate(candidate));

        candidate.setDegree(degreeGenerator.generate());
        candidate.setSalary(salaryGenerator.generate());

        return candidate;
    }

    public Person getPerson(String firmName) {
        Person person = new Person();
        person.setFirstName(getFirstName());
        person.setLastName(getLastName());
        person.setEmail(testDataGenerator.getEMail(person.getFirstName(), person.getLastName(), firmName));

        return person;
    }

    public String getPersonName() {
        return getFirstName() + " " + getLastName();
    }

    public Candidate getCandidate() {
        Candidate candidate = new Candidate();
        candidate.setFirstName(getFirstName());
        candidate.setLastName(getLastName());
        candidate.setEmail(testDataGenerator.getEMail(candidate.getFirstName(), candidate.getLastName(), "candidate"));
        candidate.setSex(SexStatus.MALE);
        candidate.setBirthDate(birthDates[(int)Math.floor(Math.random()*birthDates.length)]);
        candidate.setPosition(positionGenerator.generate());
        return candidate;
    }

    private Metro getMetro() {
        List<Metro> metros = dictionaryService.getMetroByTown(dictionaryService.MOSCOW.getTownId());
        return metros.get((int)Math.floor(Math.random()*metros.size()));
    }
}