package com.pkit.security.policy;

import java.util.List;

public interface PolicyDefinition {
	List<PolicyRule> getAllPolicyRules();
}