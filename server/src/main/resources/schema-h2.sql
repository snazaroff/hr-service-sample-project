/*
  DROP
*/

SET SCHEMA public;

DROP TABLE IF EXISTS employment_agency;

DROP TABLE IF EXISTS user_role;

DROP TABLE IF EXISTS users;

DROP TABLE IF EXISTS roles;

DROP TABLE IF EXISTS picked_candidate;

DROP TABLE IF EXISTS picked_company;

DROP TABLE IF EXISTS picked_demand;

DROP TABLE IF EXISTS person;

DROP TABLE IF EXISTS company;

DROP TABLE IF EXISTS company_type;

DROP TABLE IF EXISTS department;

DROP TABLE IF EXISTS demand_candidate_meeting_note;

DROP TABLE IF EXISTS demand_candidate_meeting;

DROP TABLE IF EXISTS meeting_type;

DROP TABLE IF EXISTS demand_candidate_note;

DROP TABLE IF EXISTS demand_candidate;

DROP TABLE IF EXISTS candidate_language;

DROP TABLE IF EXISTS person_candidate;

DROP TABLE IF EXISTS candidate_note;

DROP TABLE IF EXISTS candidate_category;

DROP TABLE IF EXISTS candidate_photo;

DROP TABLE IF EXISTS category;

DROP TABLE IF EXISTS education;

DROP TABLE IF EXISTS employment;

DROP TABLE IF EXISTS candidate;

DROP TABLE IF EXISTS demand_note;

DROP TABLE IF EXISTS demand;

DROP TABLE IF EXISTS person;

DROP TABLE IF EXISTS language;

DROP TABLE IF EXISTS metro;

DROP TABLE IF EXISTS town;

DROP TABLE IF EXISTS region;

DROP TABLE IF EXISTS country;

DROP TABLE IF EXISTS education_type;

CREATE TABLE country (
  country_id INTEGER PRIMARY KEY,
  name VARCHAR2(100),
  status INTEGER DEFAULT 0
);

CREATE TABLE region (
  region_id INTEGER PRIMARY KEY,
  country_id INTEGER NOT NULL,
  name VARCHAR2(100),
  status SMALLINT DEFAULT 0
);

ALTER TABLE region
  ADD CONSTRAINT region_fk_country_id FOREIGN KEY (country_id)
  REFERENCES country (country_id);

CREATE TABLE town (
  town_id INTEGER IDENTITY PRIMARY KEY,
  country_id INTEGER,
  region_id INTEGER,
  name VARCHAR2(100) NOT NULL,
  status SMALLINT DEFAULT 0
);

ALTER TABLE town
  ADD CONSTRAINT town_fk_country_id FOREIGN KEY (country_id)
  REFERENCES country (country_id);

ALTER TABLE town
  ADD CONSTRAINT town_fk_region_id FOREIGN KEY (region_id)
  REFERENCES region (region_id);

CREATE TABLE metro (
  metro_id INTEGER IDENTITY PRIMARY KEY,
  town_id INTEGER NOT NULL,
  line_id INTEGER NOT NULL,
  name VARCHAR2(100) NOT NULL
);

ALTER TABLE metro
  ADD CONSTRAINT metro_fk_town_id FOREIGN KEY (town_id)
  REFERENCES town (town_id);

/*
  Company type
1 - заказчик
2 - кадровое агентство
3 - владелец
*/

CREATE TABLE company_type (
  company_type_id INTEGER PRIMARY KEY,
  name VARCHAR2(45) NOT NULL
);

/*
  Company
*/

CREATE TABLE company (
  company_id IDENTITY PRIMARY KEY,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  name VARCHAR2(45) NOT NULL,
  address VARCHAR(255) NOT NULL,
  head_person_id BIGINT,
  contact_person_id BIGINT,
  phone VARCHAR2(100),
  cell VARCHAR2(100),
  email VARCHAR2(100),
  company_type_id INTEGER NOT NULL,
  is_active BOOLEAN NOT NULL DEFAULT TRUE
);

ALTER TABLE company
  ADD CONSTRAINT company_fk_company_type_id FOREIGN KEY (company_type_id)
  REFERENCES company_type (company_type_id);

--ALTER TABLE company
--  ADD CONSTRAINT company_fk_created_by FOREIGN KEY (created_by)
--  REFERENCES person(person_id);

/*
  Person
*/

CREATE TABLE person (
  person_id IDENTITY PRIMARY KEY,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
--  company_id BIGINT NOT NULL,
  first_name VARCHAR2(100) NOT NULL,
  middle_name VARCHAR2(100),
  last_name VARCHAR2(100) NOT NULL,
  birth_date DATE NOT NULL,
  phone VARCHAR2(100),
  cell VARCHAR2(100),
  email VARCHAR2(100),
  address VARCHAR(255),
  position VARCHAR(255),
  start_date DATE,
  end_date DATE,
  department_id BIGINT,
  sex INTEGER NOT NULL DEFAULT 0,
  is_active BOOLEAN
);

--ALTER TABLE person
--  ADD CONSTRAINT person_fk_company_id FOREIGN KEY (company_id)
--  REFERENCES company (company_ID);

ALTER TABLE person
  ADD CONSTRAINT person_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

--ALTER TABLE company
--  ADD CONSTRAINT company_fk_head_person_id FOREIGN KEY (HEAD_PERSON_ID)
--  REFERENCES person (person_id);

--ALTER TABLE company
--  ADD CONSTRAINT company_fk_contact_person_id FOREIGN KEY (contact_person_id)
--  REFERENCES person (person_id);

/*
  Employment Agency
*/

/*
CREATE TABLE employment_agency (
  employment_agency_id IDENTITY NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  name VARCHAR2(300) NOT NULL,
  head_person VARCHAR2(100),
  contact_person VARCHAR2(100),
  responsible_person VARCHAR2(100),
  phone VARCHAR2(100),
  cell VARCHAR2(100),
  email VARCHAR2(100),
  contract_bigint VARCHAR2(100),
  start_date DATE,
  end_date DATE,
  agency_type INTEGER,
  contract_type INTEGER,
  status INTEGER,
  payment_type VARCHAR2(100)
);

ALTER TABLE employment_agency
  ADD CONSTRAINT employment_agency_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);
*/

/*
  Department
*/

CREATE TABLE department (
  department_id IDENTITY PRIMARY KEY,
  company_id BIGINT NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  name VARCHAR2(45) NOT NULL,
  head_id BIGINT,
  contact_person_id BIGINT,
  phone VARCHAR2(100),
  email VARCHAR2(100),
  is_active BOOLEAN DEFAULT TRUE
);

ALTER TABLE department
  ADD CONSTRAINT department_fk_company_id FOREIGN KEY (company_id)
  REFERENCES company (company_id);

ALTER TABLE department
  ADD CONSTRAINT department_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

ALTER TABLE department
  ADD CONSTRAINT department_fk_head_id FOREIGN KEY (head_id)
  REFERENCES person (person_id) ON DELETE SET NULL;

ALTER TABLE department
  ADD CONSTRAINT department_fk_contact_person_id FOREIGN KEY (contact_person_id)
  REFERENCES person (person_id) ON DELETE SET NULL;

ALTER TABLE person
  ADD CONSTRAINT person_fk_department_id FOREIGN KEY (department_id)
  REFERENCES department (department_id);

/*
  Users
*/

CREATE TABLE users (
  user_id IDENTITY PRIMARY KEY,
  login VARCHAR2(100) NOT NULL,
  password VARCHAR2(100),
  start_date DATE,
  person_id BIGINT NOT NULL,
  is_active BOOLEAN
);

ALTER TABLE users
  ADD CONSTRAINT users_fk_person_id FOREIGN KEY (person_id)
  REFERENCES person (person_id) ON DELETE CASCADE;

CREATE UNIQUE INDEX IF NOT EXISTS indx_users_person_id ON users(person_id);

/*
  Roles
*/

CREATE TABLE roles (
  role_id VARCHAR2(20) PRIMARY KEY,
  parent VARCHAR2(20)
);

CREATE TABLE user_role (
  user_id INTEGER NOT NULL,
  role_id VARCHAR2(20) NOT NULL
);

ALTER TABLE user_role
  ADD CONSTRAINT user_role_fk_user_id FOREIGN KEY (user_id)
  REFERENCES users (user_id) ON DELETE CASCADE;

ALTER TABLE user_role
  ADD CONSTRAINT user_role_fk_role_id FOREIGN KEY (role_id)
  REFERENCES roles (role_id) ON DELETE CASCADE;

/*
  Company
*/

/*
CREATE TABLE company (
  CUSTOMER_ID IDENTITY NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  name VARCHAR2(45) NOT NULL,
  address VARCHAR(255) NOT NULL,
  head_id BIGINT,
  contact_person_id BIGINT,
  phone VARCHAR2(100),
  email VARCHAR2(100),
  is_active BOOLEAN
);

ALTER TABLE company
  ADD CONSTRAINT customer_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

//ALTER TABLE CUSTOMER
//  ADD CONSTRAINT PK_CUSTOMER primary key (CUSTOMER_ID);

ALTER TABLE company
  ADD CONSTRAINT customer_fk_head_id FOREIGN KEY (head_id)
  REFERENCES person (person_id);

ALTER TABLE company
  ADD CONSTRAINT customer_fk_contact_person_id FOREIGN KEY (contact_person_id)
  REFERENCES person (person_id);
*/

/*
  Category
*/

CREATE TABLE category (
  category_id IDENTITY(100) PRIMARY KEY,
  parent_id BIGINT NULL,
  name VARCHAR2(100) NOT NULL,
  has_child BOOLEAN DEFAULT FALSE);

--ALTER TABLE CATEGORY
--  ADD CONSTRAINT PK_CATEGORY primary key (category_id);

ALTER TABLE category
  ADD CONSTRAINT category_fk_parent_id FOREIGN KEY (parent_id)
  REFERENCES category (category_id);

/*
  Candidate
*/

CREATE TABLE candidate (
  candidate_id IDENTITY NOT NULL,
  company_id BIGINT NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  first_name VARCHAR2(100) NOT NULL,
  middle_name VARCHAR2(100),
  last_name VARCHAR2(100) NOT NULL,
  address VARCHAR2(1000),
--  company_id BIGINT NOT NULL,
  metro_id BIGINT,
  birth_date DATE NOT NULL,
  salary_amount BIGINT,
  salary_currency INTEGER DEFAULT 0,
  phone VARCHAR2(100),
  cell VARCHAR2(100),
  email VARCHAR2(100),
  position VARCHAR2(100),
  status INTEGER NOT NULL DEFAULT 0,
  sex INTEGER NOT NULL DEFAULT 0,
  degree VARCHAR2(100)
);

ALTER TABLE candidate
  ADD CONSTRAINT candidate_fk_company_id FOREIGN KEY (company_id)
  REFERENCES company (company_id);

ALTER TABLE candidate
  ADD CONSTRAINT candidate_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

ALTER TABLE candidate
  ADD CONSTRAINT candidate_fk_metro_id FOREIGN KEY (metro_id)
  REFERENCES metro (metro_id);

--ALTER TABLE candidate
--  ADD CONSTRAINT candidate_fk_company_id FOREIGN KEY (company_id)
--  REFERENCES company (company_id);

/*
  Candidate images
*/

CREATE TABLE candidate_photo (
  photo_id IDENTITY NOT NULL,
  candidate_id BIGINT NOT NULL,
--  image BLOB NOT NULL,
  is_main BOOLEAN
);

ALTER TABLE candidate_photo
  ADD CONSTRAINT candidate_photo_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id) ON DELETE CASCADE;

/*
  Candidate category
*/

CREATE TABLE candidate_category (
  candidate_id BIGINT NOT NULL,
  category_id BIGINT NOT NULL
);

ALTER TABLE candidate_category
  ADD CONSTRAINT candidate_category_fk_category_id FOREIGN KEY (category_id)
  REFERENCES category (category_id);

ALTER TABLE candidate_category
  ADD CONSTRAINT candidate_category_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id) ON DELETE CASCADE;

ALTER TABLE candidate_category
  ADD CONSTRAINT candidate_category_pk PRIMARY KEY (candidate_id, category_id);

/*
  candidate_note
*/

CREATE TABLE candidate_note (
  candidate_note_id IDENTITY NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  candidate_id BIGINT NOT NULL,
  note VARCHAR2(4000)
);

ALTER TABLE candidate_note
  ADD CONSTRAINT candidate_note_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id) ON DELETE CASCADE;

ALTER TABLE candidate_note
  ADD CONSTRAINT candidate_note_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

/*
  Employment
*/

CREATE TABLE employment (
  employment_id IDENTITY NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  candidate_id INTEGER NOT NULL,
  firm_name VARCHAR2(300) NOT NULL,
  position VARCHAR2(300) NOT NULL,
  description VARCHAR2(10000),
  start_date DATE NOT NULL,
  end_date DATE
);

ALTER TABLE employment
  ADD CONSTRAINT employment_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

ALTER TABLE employment
  ADD CONSTRAINT employment_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id) ON DELETE CASCADE;

/*
  Education type
*/

CREATE TABLE education_type (
  education_type_id IDENTITY NOT NULL,
  education_type_name VARCHAR2(100) NOT NULL
);

/*
  Education
*/

CREATE TABLE education (
  education_id IDENTITY NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  candidate_id INTEGER NOT NULL,
  education_name VARCHAR2(300) NOT NULL,
  faculty_name VARCHAR2(300),
  education_type_id INTEGER NOT NULL,
  start_date DATE,
  end_date DATE
);

ALTER TABLE education
  ADD CONSTRAINT education_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

ALTER TABLE education
  ADD CONSTRAINT education_fk_education_type_id FOREIGN KEY (education_type_id)
  REFERENCES education_type (education_type_id);

ALTER TABLE education
  ADD CONSTRAINT candidate_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id) ON DELETE CASCADE;

/*
  Demand
*/

CREATE TABLE demand (
  demand_id IDENTITY NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  company_id BIGINT NOT NULL,
  contact_person_id BIGINT NOT NULL,
  responsible_person_id BIGINT NOT NULL,
  finish_date DATE,
  salary_amount NUMBER,
  salary_currency INTEGER DEFAULT 0,
  position VARCHAR2(100),
  status SMALLINT NOT NULL DEFAULT 0,
  description VARCHAR2(4000)
);

ALTER TABLE demand
  ADD CONSTRAINT demand_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

ALTER TABLE demand
  ADD CONSTRAINT demand_fk_contact_person_id FOREIGN KEY (contact_person_id)
  REFERENCES person (person_id);

ALTER TABLE demand
  ADD CONSTRAINT demand_fk_resp_person_id FOREIGN KEY (responsible_person_id)
  REFERENCES person (person_id);

ALTER TABLE demand
  ADD CONSTRAINT demand_fk_company_id FOREIGN KEY (company_id)
  REFERENCES company (company_id);

/*
  Demand_note
*/

CREATE TABLE demand_note (
  demand_note_id IDENTITY NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  demand_id BIGINT NOT NULL,
  note VARCHAR2(4000)
);

ALTER TABLE demand_note
  ADD CONSTRAINT demand_note_fk_demand_id FOREIGN KEY (demand_id)
  REFERENCES demand (demand_id) ON DELETE CASCADE;

ALTER TABLE demand_note
  ADD CONSTRAINT demand_note_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

CREATE TABLE person_candidate (
  person_id BIGINT NOT NULL,
  candidate_id BIGINT NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  status SMALLINT NOT NULL DEFAULT 0,
  note VARCHAR2(4000)
);

ALTER TABLE person_candidate
  ADD CONSTRAINT person_candidate_pk primary key (person_id, candidate_id);

ALTER TABLE person_candidate
  ADD CONSTRAINT person_candidate_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id);

ALTER TABLE person_candidate
  ADD CONSTRAINT person_candidate_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

CREATE TABLE demand_candidate (
  demand_candidate_id IDENTITY NOT NULL,
  created_by BIGINT NOT NULL,
  demand_id BIGINT NOT NULL,
  candidate_id BIGINT NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  status SMALLINT NOT NULL DEFAULT 0
);

ALTER TABLE demand_candidate
  ADD CONSTRAINT demand_candidate_fk_demand_id FOREIGN KEY (demand_id)
  REFERENCES demand (demand_id) ON DELETE CASCADE;

ALTER TABLE demand_candidate
  ADD CONSTRAINT demand_candidate_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id);

ALTER TABLE demand_candidate
  ADD CONSTRAINT demand_candidate_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

CREATE TABLE demand_candidate_note (
  demand_candidate_note_id IDENTITY NOT NULL,
  demand_candidate_id  BIGINT NOT NULL,
  created_by BIGINT NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  note VARCHAR2(4000)
);

ALTER TABLE demand_candidate_note
  ADD CONSTRAINT demand_candidate_note_fk_demand_candidate_id FOREIGN KEY (demand_candidate_id)
  REFERENCES demand_candidate (demand_candidate_id) ON DELETE CASCADE;

/*
  Meeting type
  0 - "Телефонное интервью"
  1 - "Встреча"
  2 - "Тестирование"
*/

CREATE TABLE meeting_type (
  meeting_type_id INTEGER NOT NULL,
  name VARCHAR2(45) NOT NULL
);

CREATE TABLE demand_candidate_meeting (
  demand_candidate_meeting_id IDENTITY NOT NULL,
  demand_candidate_id BIGINT NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  meeting_datetime DATETIME NOT NULL,
  meeting_type_id INTEGER NOT NULL
);

ALTER TABLE demand_candidate_meeting
  ADD CONSTRAINT demand_candidate_meeting_fk_demand_candidate_id FOREIGN KEY (demand_candidate_id)
  REFERENCES demand_candidate (demand_candidate_id) ON DELETE CASCADE;

ALTER TABLE demand_candidate_meeting
  ADD CONSTRAINT demand_candidate_meeting_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

ALTER TABLE demand_candidate_meeting
  ADD CONSTRAINT demand_candidate_meeting_fk_meeting_type_id FOREIGN KEY (meeting_type_id)
  REFERENCES meeting_type (meeting_type_id);

ALTER TABLE demand_candidate_meeting
  ADD CONSTRAINT demand_candidate_meeting_UQ_meeting_datetime UNIQUE (demand_candidate_id, meeting_datetime);

CREATE TABLE demand_candidate_meeting_note (
  demand_candidate_meeting_note_id IDENTITY NOT NULL,
  demand_candidate_meeting_id BIGINT NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  note VARCHAR2(4000)
);

ALTER TABLE demand_candidate_meeting_note
  ADD CONSTRAINT demand_candidate_meeting_note_fk_demand_candidate_meeting_id FOREIGN KEY (demand_candidate_meeting_id)
  REFERENCES demand_candidate_meeting (demand_candidate_meeting_id) ON DELETE CASCADE;

ALTER TABLE demand_candidate_meeting_note
  ADD CONSTRAINT demand_candidate_meeting_note_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

CREATE TABLE language (
  language_id INTEGER IDENTITY PRIMARY KEY,
  name VARCHAR2(100)
);

CREATE TABLE candidate_language (
  candidate_id BIGINT NOT NULL,
  language_id BIGINT NOT NULL,
  status INTEGER NOT NULL
);

ALTER TABLE candidate_language
  ADD CONSTRAINT candidate_language_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id) ON DELETE CASCADE;

ALTER TABLE candidate_language
  ADD CONSTRAINT candidate_language_fk_language_id FOREIGN KEY (language_id)
  REFERENCES language (language_id);

ALTER TABLE candidate_language
  ADD CONSTRAINT candidate_language_pk PRIMARY KEY (candidate_id, language_id);

CREATE TABLE picked_candidate (
  candidate_id BIGINT NOT NULL,
  person_id BIGINT NOT NULL
);

ALTER TABLE picked_candidate
  ADD CONSTRAINT picked_candidate_pk PRIMARY KEY (candidate_id, person_id);

ALTER TABLE picked_candidate
  ADD CONSTRAINT picked_candidate_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id) ON DELETE CASCADE;

ALTER TABLE picked_candidate
  ADD CONSTRAINT picked_candidate_fk_person_id FOREIGN KEY (person_id)
  REFERENCES person (person_id) ON DELETE CASCADE;

CREATE TABLE picked_company (
  company_id BIGINT NOT NULL,
  person_id BIGINT NOT NULL
);

ALTER TABLE picked_company
  ADD CONSTRAINT picked_company_fk_company_id FOREIGN KEY (company_id)
  REFERENCES company (company_id) ON DELETE CASCADE;

ALTER TABLE picked_company
  ADD CONSTRAINT picked_company_fk_person_id FOREIGN KEY (person_id)
  REFERENCES person (person_id) ON DELETE CASCADE;

ALTER TABLE picked_company
  ADD CONSTRAINT picked_company_pk PRIMARY KEY (company_id, person_id);

CREATE TABLE picked_demand (
  demand_id BIGINT NOT NULL,
  person_id BIGINT NOT NULL
);

ALTER TABLE picked_demand
  ADD CONSTRAINT picked_demand_fk_demand_id FOREIGN KEY (demand_id)
  REFERENCES demand (demand_id) ON DELETE CASCADE;

ALTER TABLE picked_demand
  ADD CONSTRAINT picked_demand_fk_person_id FOREIGN KEY (person_id)
  REFERENCES person (person_id) ON DELETE CASCADE;

ALTER TABLE picked_demand
  ADD CONSTRAINT picked_demand_pk PRIMARY KEY (demand_id, person_id);

DROP TABLE IF EXISTS file_store.file;

DROP SCHEMA IF EXISTS file_store;

CREATE SCHEMA file_store;

SET SCHEMA file_store;

CREATE TABLE file (
  file_id IDENTITY PRIMARY KEY,
  owner_id BIGINT,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  name VARCHAR2(100),
  content_type VARCHAR2(100),
  status INTEGER DEFAULT 0
);
