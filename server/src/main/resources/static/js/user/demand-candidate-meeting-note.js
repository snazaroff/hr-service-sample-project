var demand_candidate_meeting_note_form_fields = ["note"];

function addDemandCandidateMeetingNote(demandCandidateMeetingId) {
    "use strict"
    console.log("addDemandCandidateMeetingNote(demandCandidateMeetingId: %s)", demandCandidateMeetingId);

    $("#tr-demand-candidate-meeting-note-" + demandCandidateMeetingId + "-new").show();
    $("#btn-add-demand-candidate-meeting-note-" + demandCandidateMeetingId).attr("disabled", true);
}

function deleteDemandCandidateMeetingNote(demandId, demandCandidateMeetingNoteId) {
    "use strict"
    console.log("deleteDemandCandidateMeetingNote( demandId: %s, demandCandidateMeetingNoteId: %s)", demandId, demandCandidateMeetingNoteId);

    if (confirm('Удалить комментарий?')) {
        doDeleteDemandCandidateMeetingNote(demandId, demandCandidateMeetingNoteId);
    }
}

function editDemandCandidateMeetingNote(demandCandidateMeetingNoteId) {
    "use strict"
    console.log("editDemandCandidateMeetingNote( demandCandidateMeetingNoteId: %s)", demandCandidateMeetingNoteId);

    let tr = $("#tr-demand-candidate-meeting-note-" + demandCandidateMeetingNoteId);
    let td;
    let span;
    let input;
    for (const field of demand_candidate_meeting_note_form_fields) {
        console.log("field: %s", field);
        td = tr.find("#td-demand-candidate-meeting-note-" + field + "-" + demandCandidateMeetingNoteId);
        span = tr.find("#span-demand-candidate-meeting-note-" + field + "-" + demandCandidateMeetingNoteId);
        console.log("#span-demand-candidate-meeting-note-" + field + "-" + demandCandidateMeetingNoteId)
        console.log("span: " + span);
        input = $("<input type=\"text\"/>");
        input.val(span.text());
        input.appendTo(td);
        span.hide();
    }

    $("#btn-demand-candidate-meeting-note-edit-" + demandCandidateMeetingNoteId).hide();
    $("#btn-demand-candidate-meeting-note-post-" + demandCandidateMeetingNoteId).show();
    $("#btn-demand-candidate-meeting-note-delete-" + demandCandidateMeetingNoteId).hide();
    $("#btn-demand-candidate-meeting-note-cancel-" + demandCandidateMeetingNoteId).show();
}

function cancelAddDemandCandidateMeetingNote(demandCandidateMeetingId) {
    "use strict"
    console.log("cancelAddDemandCandidateMeetingNote(demandCandidateMeetingId: %s)", demandCandidateMeetingId);

    $("#tr-demand-candidate-meeting-note-" + demandCandidateMeetingId + "-new").hide();
    $("#btn-add-demand-candidate-meeting-note-" + demandCandidateMeetingId).attr("disabled", false);
    $("#input-demand-candidate-meeting-note-" + demandCandidateMeetingId).val("");
}

function cancelEditDemandCandidateMeetingNote(demandCandidateMeetingNoteId) {
    "use strict"
    console.log("cancelEditDemandCandidateMeetingNote( demandCandidateMeetingNoteId: %s)", demandCandidateMeetingNoteId);

    var tr = $("#tr-demand-candidate-meeting-note-" + demandCandidateMeetingNoteId);
    for (let field of demand_candidate_meeting_note_form_fields) {
        console.log("field: %s", field);
        tr.find("#span-demand-candidate-meeting-note-" + field + "-" + demandCandidateMeetingNoteId).show();
        tr.find("#td-demand-candidate-meeting-note-" + field + "-" + demandCandidateMeetingNoteId + " input").remove();
    }

    $("#btn-demand-candidate-meeting-note-edit-" + demandCandidateMeetingNoteId).show();
    $("#btn-demand-candidate-meeting-note-post-" + demandCandidateMeetingNoteId).hide();
    $("#btn-demand-candidate-meeting-note-delete-" + demandCandidateMeetingNoteId).show();
    $("#btn-demand-candidate-meeting-note-cancel-" + demandCandidateMeetingNoteId).hide();
}

function postAddDemandCandidateMeetingNote(demandCandidateMeetingId) {
    "use strict";
    var note = $("#tr-demand-candidate-meeting-note-" + demandCandidateMeetingId + "-new input").val();

    if (note == null || note == "") {
        alert('Комментарий должен быть заполнен')
    } else {
        console.log("postAddDemandCandidateMeetingNote(demandCandidateMeetingId: %s, note: %s)", demandCandidateMeetingId, note);
        doAddDemandCandidateMeetingNote(demandCandidateMeetingId, note);
    }
}

function postUpdateDemandCandidateMeetingNote(demandCandidateMeetingId, demandCandidateMeetingNoteId) {
    "use strict";
    let tr = $("#tr-demand-candidate-meeting-note-" + demandCandidateMeetingNoteId);
    let note = tr.find("#td-demand-candidate-meeting-note-note" + "-" + demandCandidateMeetingNoteId + " input").val();

    if (note == null || note == "") {
        alert('Комментарий должен быть заполнен')
    } else {
        console.log("postUpdateDemandCandidateMeetingNote( demandCandidateMeetingId: %s, demandCandidateMeetingNoteId: %s, note: %s)", demandCandidateMeetingId, demandCandidateMeetingNoteId, note);
        doUpdateDemandCandidateMeetingNote(demandCandidateMeetingId, demandCandidateMeetingNoteId, note);
    }
}

function doAddDemandCandidateMeetingNote(demandCandidateMeetingId, note) {
    "use strict"
    console.log("doAddDemandCandidateMeetingNote( demandCandidateMeetingId: %s, note: %s)", demandCandidateMeetingId, note);

    let demandCandidateMeetingNote = {
        demand_candidate_meeting: {
            demand_candidate_meeting_id: demandCandidateMeetingId,
            type: {status_id: 0},
            meeting_datetime: '1970-01-01 00:00:00'
        },
        note: note
    }
    console.log("demandCandidateMeetingNote: " + JSON.stringify(demandCandidateMeetingNote));

    jQuery.ajax({
        type: "POST",
        url: "/demand/candidate/meeting/note",
        data: JSON.stringify(demandCandidateMeetingNote),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.groupCollapsed("doAddDemandCandidateMeetingNote.success:( demandCandidateMeetingId: %s, note: %s)", demandCandidateMeetingId, note);
            console.dir(data);
            data.demand_candidate_meeting = {demand_candidate_meeting_id: demandCandidateMeetingId};
            console.dir(data);

            var formatedHtml = demandCandidateMeetingNoteTemplate(data);

            console.log(formatedHtml);

            let trHeader = $("#tr-demand-candidate-meeting-note-" + demandCandidateMeetingId + "-new");

            $(formatedHtml).insertAfter(trHeader);
            $("#tr-demand-candidate-meeting-note-" + demandCandidateMeetingId + "-new").hide();
            $("#tr-demand-candidate-meeting-note-" + demandCandidateMeetingId + "-new input").val("");
            incCounter("comment_count");
            console.groupEnd();
        },
        error: processError,
        failure: processFailure,
        complete: function (jqXHR, textStatus) {
            $("#btn-add-demand-candidate-meeting-note-" + demandCandidateMeetingId).attr("disabled", false);
        }
    });
}

function doUpdateDemandCandidateMeetingNote(demandCandidateMeetingId, demandCandidateMeetingNoteId, note) {
    "use strict"
    console.log("doUpdateDemandCandidateMeetingNote( demandCandidateMeetingId: %s, demandCandidateMeetingNoteId: %s, note: %s)", demandCandidateMeetingId, demandCandidateMeetingNoteId, note);

    let demandCandidateMeetingNote = {
        demand_candidate_meeting_note_id: demandCandidateMeetingNoteId,
        demand_candidate_meeting: {
            demand_candidate_meeting_id: demandCandidateMeetingId
        },
        note: note
    }
    console.log("demandCandidateMeetingNote: " + JSON.stringify(demandCandidateMeetingNote));

    jQuery.ajax({
        type: "POST",
        url: "/demand/candidate/meeting/note",
        data: JSON.stringify(demandCandidateMeetingNote),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.groupCollapsed("doUpdateDemandCandidateMeetingNote.success:( demandCandidateMeetingNoteId: " + demandCandidateMeetingNoteId + ", note: " + note + ")");
            console.dir(data);
            data.demand_candidate_meeting = {demand_candidate_meeting_id: demandCandidateMeetingId};

            console.dir(data);

            var tr = $("#tr-demand-candidate-meeting-note-" + demandCandidateMeetingNoteId);
            for (const field of demand_candidate_meeting_note_form_fields) {
                console.log("field: %s, data[field]: %s, typeof(data[field]): %s", field, data[field], typeof (data[field]));
                let span = tr.find("#span-demand-candidate-meeting-note-" + field + "-" + demandCandidateMeetingNoteId);
                span.text((data[field] === null) ? "" : data[field]);
                span.show();
                tr.find("#td-demand-candidate-meeting-note-" + field + "-" + demandCandidateMeetingNoteId + " input").remove();
            }

            $("#btn-demand-candidate-meeting-note-edit-" + demandCandidateMeetingNoteId).attr("onclick", "editDemandCandidateMeetingNote(" + data.demand_note_id + ");")

            $("#btn-demand-candidate-meeting-note-edit-" + demandCandidateMeetingNoteId).show();
            $("#btn-demand-candidate-meeting-note-post-" + demandCandidateMeetingNoteId).hide();
            $("#btn-demand-candidate-meeting-note-delete-" + demandCandidateMeetingNoteId).show();
            $("#btn-demand-candidate-meeting-note-cancel-" + demandCandidateMeetingNoteId).hide();
            console.groupEnd();
        },
        error: processError,
        failure: processFailure
    });
}

function doDeleteDemandCandidateMeetingNote(demandCandidateMeetingNoteId) {
    "use strict"
    console.log("doDeleteDemandCandidateMeetingNote( demandCandidateMeetingNoteId: %s)", demandCandidateMeetingNoteId);

    jQuery.ajax({
        type: "DELETE",
        url: "/demand/candidate/meeting/note/" + demandCandidateMeetingNoteId,
        success: function (data) {
            console.log("doDeleteDemandCandidateMeetingNote.success: demandCandidateMeetingNoteId: %s, result: %s", demandCandidateMeetingNoteId, data);

            $("#tr-demand-candidate-meeting-note-" + demandCandidateMeetingNoteId).remove();
            decCounter("comment_count");
        },
        error: processError,
        failure: processFailure
    });
}