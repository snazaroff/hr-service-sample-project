function filter() {
    "use strict";
    console.log("filter()");
    $("#demand-filter-panel").toggle();
}

function doClear() {
    "use strict";
    console.log("doClear()");
    $("form input").val('');
    $("select option:selected").each(function () {
        $(this).removeAttr("selected");
    });
}

function doAddDemand() {
    "use strict";
    console.log("doAddDemand!");
    location.href = "/demand/-1";
}

function deleteDemand(event, demandId) {
    "use strict";
    console.log("deleteDemand(demandId: %s)", demandId);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble=true);

    $( function() {
        $( "#dialog-confirm-delete-demand" ).dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Удалить запись": function() {
                    $( this ).dialog( "close" );
                    doDeleteDemand(demandId);
                },
                "Отменить": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    } );
}

function doDeleteDemand(demandId) {
    "use strict";
    console.log("doDeleteDemand(demandId: %s)", demandId);

    doDeleteObject(demandId, "demand");
}

function setDemandPicked(event, demandId, aObj) {
    "use strict";
    console.log("setDemandPicked( demandId: %s, aObj: %s)", demandId, aObj);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble=true);

    jQuery.ajax({
        type: "POST",
        url: "/picked/demand/" + demandId,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.groupCollapsed("setDemandPicked.success: demandId: " + demandId);
            console.dir(data);
            aObj.title = "Удалить из избранных";
            aObj.onclick = function (event) {
                setDemandUnpicked(event, demandId, aObj);
            };
            aObj.innerHTML = "<i class=\"fa fa-star\"/>";
            console.groupEnd();
        },
        error: processError,
        failure: processFailure
    });
}

function setDemandUnpicked(event, demandId, aObj) {
    "use strict";
    console.log("setDemandUnpicked( demandId: %s, aObj: %s)", demandId, aObj);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble=true);

    jQuery.ajax({
        type: "DELETE",
        url: "/picked/demand/" + demandId,
        success: function (data) {
            console.groupCollapsed("setDemandUnpicked.success: demandId: " + demandId);
            console.dir(data);
            aObj.title = "Добавить в избранные";
            aObj.onclick = function (event) {
                setDemandPicked(event, demandId, aObj);
            };
            aObj.innerHTML = "<i class=\"fa fa-star-o\"/>";
            console.groupEnd();
        },
        error: processError,
        failure: processFailure
    });
}

function loadCompanyPersons() {
    "use strict";
    console.log("loadCompanyPersons()");

    console.log("%s (%s)", $("#companies option:selected").text(), $("#companies option:selected").val());

    var companyId = $("#companies option:selected").val();

    if(companyId) {
        jQuery.ajax({
            type: "GET",
            url: "/company/" + companyId + "/person",
            success: function (data) {
                console.log("loadPerson.success: (companyId: %s)", companyId);
                console.dir(data);
                $('#contact_persons option').remove();
                $('#responsible_persons option').remove();
                $('#contact_persons').append($('<option>', {value: null, text: null}));
                $('#responsible_persons').append($('<option>', {value: null, text: null}));
                for (var index in data) {
                    var item = data[index];
                    console.log(item);
                    $('#contact_persons').append($('<option>', {value: item.person_id, text: item.fio}));
                    $('#responsible_persons').append($('<option>', {value: item.person_id, text: item.fio}));
                }
            },
            error: processError,
            failure: processFailure
        });
    }
}