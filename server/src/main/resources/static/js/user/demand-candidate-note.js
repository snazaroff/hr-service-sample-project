var demand_candidate_note_form_fields = ["note"];

function addDemandCandidateNote() {
    "use strict"
    console.log("addDemandCandidateNote()");

    $("#tr-demand-candidate-note-new").show();
    $("#btn-add-demand-candidate-note").attr("disabled", true);
}

function deleteDemandCandidateNote(demandId, demandCandidateNoteId) {
    "use strict"
    console.log("deleteDemandCandidateNote( demandId: %s, demandCandidateNoteId: %s)", demandId, demandCandidateNoteId);

    if (confirm('Удалить комментарий?')) {
        doDeleteDemandCandidateNote(demandId, demandCandidateNoteId);
    }
}

function editDemandCandidateNote(demandCandidateNoteId) {
    "use strict"
    console.log("editDemandCandidateNote( demandCandidateNoteId: %s)", demandCandidateNoteId);

    let tr = $("#tr-demand-candidate-note-" + demandCandidateNoteId);
    let td;
    let span;
    let input;
    for (const field of demand_candidate_note_form_fields) {
        console.log("field: %s", field);
        td = tr.find("#td-demand-candidate-note-" + field + "-" + demandCandidateNoteId);
        span = tr.find("#span-demand-candidate-note-" + field + "-" + demandCandidateNoteId);
        console.log("#span-demand-candidate-note-" + field + "-" + demandCandidateNoteId)
        console.log("span: " + span);
        input = $("<input type=\"text\"/>");
        input.val(span.text());
        input.appendTo(td);
        span.hide();
    }

    $("#btn-demand-candidate-note-edit-" + demandCandidateNoteId).hide();
    $("#btn-demand-candidate-note-post-" + demandCandidateNoteId).show();
    $("#btn-demand-candidate-note-delete-" + demandCandidateNoteId).hide();
    $("#btn-demand-candidate-note-cancel-" + demandCandidateNoteId).show();
}

function cancelAddDemandCandidateNote() {
    "use strict"
    console.log("cancelAddDemandCandidateNote()");

    $("#tr-demand-candidate-note-new").hide();
    $("#btn-add-demand-candidate-note").attr("disabled", false);
    $("#input-demand-candidate-note-note").val("");
}

function cancelEditDemandCandidateNote(demandCandidateNoteId) {
    "use strict"
    console.log("cancelEditDemandCandidateNote( demandCandidateNoteId: %s)", demandCandidateNoteId);

    var tr = $("#tr-demand-candidate-note-" + demandCandidateNoteId);
    for (let field of demand_candidate_note_form_fields) {
        console.log("field: %s", field);
        tr.find("#span-demand-candidate-note-" + field + "-" + demandCandidateNoteId).show();
        tr.find("#td-demand-candidate-note-" + field + "-" + demandCandidateNoteId + " input").remove();
    }

    $("#btn-demand-candidate-note-edit-" + demandCandidateNoteId).show();
    $("#btn-demand-candidate-note-post-" + demandCandidateNoteId).hide();
    $("#btn-demand-candidate-note-delete-" + demandCandidateNoteId).show();
    $("#btn-demand-candidate-note-cancel-" + demandCandidateNoteId).hide();
}

function postAddDemandCandidateNote(demandId, demandCandidateId) {
    "use strict";
    var note = $("#tr-demand-candidate-note-new #input-demand-candidate-note").val();

    if (note == null || note == "") {
        alert('Комментарий должен быть заполнен')
    } else {
        console.log("postAddDemandCandidateNote(demandId: %s, demandCandidateId: %s, note: %s)", demandId, demandCandidateId, note);
        doAddDemandCandidateNote(demandId, demandCandidateId, note);
    }
}

function postUpdateDemandCandidateNote(demandId, demandCandidateId, demandCandidateNoteId) {
    "use strict";
    let tr = $("#tr-demand-candidate-note-" + demandCandidateNoteId);
    let note = tr.find("#td-demand-candidate-note-note" + "-" + demandCandidateNoteId + " input").val();

    if (note == null || note == "") {
        alert('Комментарий должен быть заполнен')
    } else {
        console.log("postUpdateDemandCandidateNote( demandId: %s, demandCandidateId: %s, demandCandidateNoteId: %s, note: %s)", demandId, demandCandidateId, demandCandidateNoteId, note);
        doUpdateDemandCandidateNote(demandId, demandCandidateId, demandCandidateNoteId, note);
    }
}

function doAddDemandCandidateNote(demandId, demandCandidateId, note) {
    "use strict"
    console.log("doAddDemandCandidateNote( demandId: %s, note: %s)", demandId, note);

    let demandCandidateNote = {
        demand_candidate: {
            demand_candidate_id: demandCandidateId
        },
        note: note
    }
    console.log("demandCandidateNote: " + JSON.stringify(demandCandidateNote));

    jQuery.ajax({
        type: "POST",
        url: "/demand/" + demandId + "/candidate/note/",
        data: JSON.stringify(demandCandidateNote),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.groupCollapsed("doAddDemandCandidateNote.success:( demandId: " + demandId + ", note: " + note + ")");
            console.dir(data);
            data.demand = {demand_id: demandId};
            data.demand_candidate = {demand_candidate_id: demandCandidateId};
            console.dir(data);

            var formatedHtml = demandCandidateNoteTemplate(data);

            console.log(formatedHtml);

            let trHeader = $("#tr-demand-candidate-note-new");

            $(formatedHtml).insertAfter(trHeader);
            $("#tr-demand-candidate-note-new").hide();
            $("#tr-demand-candidate-note-new #input-demand-candidate-note").val("");
            incCounter("comment_count");
            console.groupEnd();
        },
        error: processError,
        failure: processFailure,
        complete: function (jqXHR, textStatus) {
            $("#btn-add-demand-candidate-note").attr("disabled", false);
        }
    });
}

function doUpdateDemandCandidateNote(demandId, demandCandidateId, demandCandidateNoteId, note) {
    "use strict"
    console.log("doUpdateDemandCandidateNote( demandId: %s, demandCandidateId: %s, demandCandidateNoteId: %s, note: %s)", demandId, demandCandidateId, demandCandidateNoteId, note);

    let demandCandidateNote = {
        demand_candidate_note_id: demandCandidateNoteId,
        demand_candidate: {
            demand_candidate_id: demandCandidateId,
            demand: {demand_id: demandId}
        },
        note: note
    }
    console.log("demandCandidateNote: " + JSON.stringify(demandCandidateNote));

    jQuery.ajax({
        type: "POST",
        url: "/demand/" + demandId + "/candidate/note/",
        data: JSON.stringify(demandCandidateNote),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.groupCollapsed("doUpdateDemandCandidateNote.success:( demandCandidateNoteId: " + demandCandidateNoteId + ", note: " + note + ")");
            console.dir(data);
            data.demand = {demand_id: demandId};
            data.demand_candidate = {demand_candidate_id: demandCandidateId};

            console.dir(data);

            var tr = $("#tr-demand-candidate-note-" + demandCandidateNoteId);
            for (const field of demand_candidate_note_form_fields) {
                console.log("field: %s, data[field]: %s, typeof(data[field]): %s", field, data[field], typeof (data[field]));
                let span = tr.find("#span-demand-candidate-note-" + field + "-" + demandCandidateNoteId);
                span.text((data[field] === null) ? "" : data[field]);
                span.show();
                tr.find("#td-demand-candidate-note-" + field + "-" + demandCandidateNoteId + " input").remove();
            }

            $("#btn-demand-candidate-note-edit-" + demandCandidateNoteId).attr("onclick", "editDemandCandidateNote(" + data.demand_note_id + ");")

            $("#btn-demand-candidate-note-edit-" + demandCandidateNoteId).show();
            $("#btn-demand-candidate-note-post-" + demandCandidateNoteId).hide();
            $("#btn-demand-candidate-note-delete-" + demandCandidateNoteId).show();
            $("#btn-demand-candidate-note-cancel-" + demandCandidateNoteId).hide();
            console.groupEnd();
        },
        error: processError,
        failure: processFailure
    });
}

function doDeleteDemandCandidateNote(demandId, demandCandidateNoteId) {
    "use strict"
    console.log("doDeleteDemandCandidateNote( demandId: %s, demandCandidateNoteId: %s)", demandId, demandCandidateNoteId);

    jQuery.ajax({
        type: "DELETE",
        url: "/demand/" + demandId + "/candidate/note/" + demandCandidateNoteId,
        success: function (data) {
            console.log("doDeleteDemandCandidateNote.success: demandId: %s, demandCandidateNoteId: %s, result: %s", demandId, demandCandidateNoteId, data);

            $("#tr-demand-candidate-note-" + demandCandidateNoteId).remove();
            decCounter("comment_count");
        },
        error: processError,
        failure: processFailure
    });
}