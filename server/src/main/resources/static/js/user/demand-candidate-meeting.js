var demand_candidate_meeting_form_fields = ["meeting_datetime"];

function addDemandCandidateMeeting() {
    "use strict";
    console.log("addDemandCandidateMeeting()");

    $("#tr-demand-candidate-meeting-new").show();
    $("#btn-add-demand-candidate-meeting").attr("disabled", true);
}

function cancelAddDemandCandidateMeeting() {
    "use strict";
    console.log("cancelAddDemandCandidateMeeting()");

    closeDemandCandidateMeetingForm();
}

function closeDemandCandidateMeetingForm() {
    $("#tr-demand-candidate-meeting-new").hide();

    $("#new-demand-candidate-meeting-meeting_date").val("");
    $("#new-demand-candidate-meeting-meeting_time").val("");

    $("#new-demand-candidate-meeting-meeting_type :selected").prop("selected", false);
    $("#btn-add-demand-candidate-meeting").attr("disabled", false);
}

function cancelUpdateDemandCandidateMeeting() {
    "use strict";
    console.log("cancelUpdateDemandCandidateMeeting()");

    closeDemandCandidateMeetingForm();
}

function editDemandCandidateMeeting(demandCandidateId, demandCandidateMeetingId, meetingTypeId) {
    "use strict";
    console.log("editDemandCandidateMeeting( demandCandidateId: %s,  demandCandidateMeetingId: %s, meetingTypeId: %s)",
        demandCandidateId, demandCandidateMeetingId, meetingTypeId);

    var oldTr = $("#tr-demand-candidate-meeting-" + demandCandidateMeetingId);

    var tr = $("#tr-demand-candidate-meeting-new").clone();
    tr.attr("id", "edit-demand-candidate-meeting-" + demandCandidateMeetingId);

    var btnSave = tr.find("#save");
    btnSave.removeAttr("onclick");
    btnSave.on("click",
        function(){
            postUpdateDemandCandidateMeeting(demandCandidateId, demandCandidateMeetingId);
        }
    );
    var btnCancel = tr.find("#cancel");
    btnCancel.removeAttr("onclick");
    btnCancel.on("click",
        function(){
            cancelEditDemandCandidateMeeting(demandCandidateMeetingId);
        }
    );

    var meetingDateTime = oldTr.find("#td-demand-candidate-meeting-meeting_datetime-" + demandCandidateMeetingId).html().trim();
//    var meetingTypeId = oldTr.find("input").val();
    var meetingTypeName = oldTr.find("#td-demand-candidate-meeting-meeting_type-" + demandCandidateMeetingId).html().trim();
    console.log("meetingDateTime: %s; index: %s", meetingDateTime, meetingDateTime.indexOf(" "));
    console.log("meetingTypeId: %s, meetingTypeName: %s", meetingTypeId, meetingTypeName);

    var meetingDate = meetingDateTime.substring(0, meetingDateTime.indexOf(" "));
    var meetingTime = meetingDateTime.substring(meetingDateTime.indexOf(" ") + 1);
    console.log("meetingDate: %s; meetingTime: %s", meetingDate, meetingTime);

    tr.insertAfter(oldTr);
    tr.find("#new-demand-candidate-meeting-meeting_date").val(meetingDate);
    tr.find("#new-demand-candidate-meeting-meeting_time").val(meetingTime);
    tr.find("#new-demand-candidate-meeting-meeting_type").val(meetingTypeId);
    oldTr.hide();
    tr.show();
}

function cancelEditDemandCandidateMeeting(demandCandidateMeetingId) {
    "use strict";
    console.log("cancelEditDemandCandidateMeeting(demandCandidateMeetingId: %s)", demandCandidateMeetingId);

    var tr = $("#tr-demand-candidate-meeting-" + demandCandidateMeetingId);
    tr.show();

     $("#edit-demand-candidate-meeting-" + demandCandidateMeetingId).remove();
}

function postAddDemandCandidateMeeting(demandCandidateId) {
    "use strict";
    console.log("postAddDemandCandidateMeeting( postAddDemandCandidateMeeting: %s)", demandCandidateId);

    var tr = $("#tr-demand-candidate-meeting-new");
    var meetingDate = tr.find("#new-demand-candidate-meeting-meeting_date").val();
    var meetingTime = tr.find("#new-demand-candidate-meeting-meeting_time").val();

    console.log("%s %s", meetingDate, meetingTime);

    if(meetingDate === "") {
        alert("Введите дату встречи");
        return;
    }
    if(meetingTime === "") {
        alert("Введите время встречи");
        return;
    }

    var meetingDatetime = meetingDate + " " + meetingTime + ":00";

    var meetingType = tr.find("#new-demand-candidate-meeting-meeting_type").val();

    if(meetingDatetime.trim() === "") {
        alert("Введите дату и время встречи");
        return;
    }

    console.log("postDemandCandidateMeeting( demandCandidateId: %s, meetingDatetime: %s, meetingType: %s)",
        demandCandidateId, meetingDatetime, meetingType);

    doAddDemandCandidateMeeting(demandCandidateId, meetingDatetime, meetingType);
}

function postUpdateDemandCandidateMeeting(demandCandidateId, demandCandidateMeetingId) {
    "use strict";
    console.log("postUpdateDemandCandidateMeeting(demandCandidateId: %s, demandCandidateMeetingId: %s)",
        demandCandidateId, demandCandidateMeetingId);

    var tr = $("#edit-demand-candidate-meeting-" + demandCandidateMeetingId);

    var meetingDate = tr.find("#new-demand-candidate-meeting-meeting_date").val();
    var meetingTime = tr.find("#new-demand-candidate-meeting-meeting_time").val();

    console.log("meetingDate: %s, meetingTime: %s", meetingDate, meetingTime);

    var meetingType = {
        id: tr.find("#new-demand-candidate-meeting-meeting_type").val(),
        text: tr.find("#new-demand-candidate-meeting-meeting_type option:selected").text()
    };

    console.log("meetingType: " + JSON.stringify(meetingType));

    if(meetingDate === "") {
        alert("Введите дату встречи");
        return;
    }
    if (meetingTime === "") {
        alert("Введите время встречи");
        return;
    }
    if (meetingTime.length === 5) {
        meetingTime = meetingTime + ":00";
    }

    var meetingDateTime = meetingDate + " " + meetingTime;

    console.log("postUpdateDemandCandidateMeeting( demandCandidateId: %s, demandCandidateMeetingId: %s, meetingDatetime: %s, meetingType: %s)",
        demandCandidateId, demandCandidateMeetingId, meetingDateTime, meetingType);

    doUpdateDemandCandidateMeeting(demandCandidateId, demandCandidateMeetingId, meetingDateTime, meetingType);
}

function deleteDemandCandidateMeeting(demandCandidateMeetingId) {
    "use strict";
    console.log("deleteDemandCandidateMeeting( demandCandidateMeetingId: %s)", demandCandidateMeetingId);

    if(confirm('Удалить встречу?')) {
        doDeleteDemandCandidateMeeting(demandCandidateMeetingId);
    }
}

function doUpdateDemandCandidateMeeting(demandCandidateId, demandCandidateMeetingId, meetingDatetime, meetingType) {
    "use strict";
    console.log("doUpdateDemandCandidateMeeting( demandCandidateId: %s, demandCandidateMeetingId: %s, meetingDatetime: %s, meetingType: %s",
        demandCandidateId, demandCandidateMeetingId, meetingDatetime, meetingType);

    var demandCandidateMeeting = {
        demand_candidate_meeting_id: demandCandidateMeetingId,
        demand_candidate: {
            demand_candidate_id: demandCandidateId
        },
        meeting_datetime: meetingDatetime,
        type: {
            status_id: meetingType.id
        }
    };

    console.log("demandCandidateMeeting: " + JSON.stringify(demandCandidateMeeting));

    jQuery.ajax({
        type: "PUT",
        url: "/demand/candidate/meeting/",
        data: JSON.stringify(demandCandidateMeeting),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data){
            console.log("doUpdateDemandCandidateMeeting( demandCandidateId: %s", demandCandidateId);
            console.dir(data);
            var tr = $("#tr-demand-candidate-meeting-" + demandCandidateMeetingId);

            var td = tr.find("#td-demand-candidate-meeting-meeting_datetime-" + demandCandidateMeetingId);
            td.html(meetingDatetime);

            td = tr.find("#td-demand-candidate-meeting-meeting_type-" + demandCandidateMeetingId);
            td.html(meetingType.text);

            var editTr = $("#edit-demand-candidate-meeting-" + demandCandidateMeetingId);

            editTr.find("#new-demand-candidate-meeting-meeting_type").val(meetingTypeId);

            editTr.hide();
            tr.show();
        },
        error: processError,
        failure: processFailure
    });
}

function doDeleteDemandCandidateMeeting(demandCandidateMeetingId) {
    "use strict";
    console.log("doDeleteDemandCandidateMeeting( demandCandidateMeetingId: %s)", demandCandidateMeetingId);

    jQuery.ajax({
        type: "DELETE",
        url: "/demand/candidate/meeting/" + demandCandidateMeetingId,
        success: function(data){
            console.log("doDeleteDemandCandidateMeeting.success: (demandCandidateMeetingId: %s)", demandCandidateMeetingId);
            console.dir(data);
            $("#tr-demand-candidate-meeting-" + demandCandidateMeetingId).remove();
        },
        error: processError,
        failure: processFailure
    });
}

function doAddDemandCandidateMeeting(demandCandidateId, meetingDateTime, meetingType) {
    "use strict";
    console.log("doAddDemandCandidateMeeting( demandCandidateId: %s, meetingDateTime: %s, meetingType: %s)",
        demandCandidateId, meetingDateTime, meetingType);

    var demandCandidateMeeting = {
        demand_candidate: {
            demand_candidate_id: demandCandidateId
        },
        meeting_datetime: meetingDateTime,
        type: {
            status_id: meetingType
        }
    };

    console.log("demandCandidateMeeting: %s", JSON.stringify(demandCandidateMeeting));

    jQuery.ajax({
        type: "POST",
        url: "/demand/candidate/meeting/",
        data: JSON.stringify(demandCandidateMeeting),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data){
            console.groupCollapsed("doAddDemandCandidateMeeting.success: ( demandCandidateId: " + demandCandidateId + ")");
            data.demand_candidate = { demand_candidate_id: demandCandidateId };
            console.dir(data);

            var formatedHtml = demandCandidateMeetingTemplate(data);

            console.log("formatedHtml: " + formatedHtml);

            $(formatedHtml).appendTo($("#tbl-demand-candidate-meeting"));

            closeDemandCandidateMeetingForm();
            console.groupEnd();
        },
        error: processError,
        failure: processFailure
    });
}

function showDemandCandidateMeetingInfo(obj, demandCandidateMeetingId) {
    "use strict";
    console.log("showDemandCandidateMeetingInfo( demandCandidateMeetingId: %s)", demandCandidateMeetingId);

    for (let child of obj.childNodes) {
        if(child.className && child.className.includes("glyphicon-chevron-right")) {
            console.log("chevron-right");
            $(child).removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");

            $("#tr-demand-candidate-meeting-notes-" + demandCandidateMeetingId).show();

        } else if(child.className && child.className.includes("glyphicon-chevron-down")) {
            console.log("chevron-down");
            $(child).removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");

            $("#tr-demand-candidate-meeting-notes-" + demandCandidateMeetingId).hide();
        }
    }
}

function loadDemandCandidateMeetingInfo(demandCandidateMeetingId) {
  console.log("loadDemandCandidateMeetingInfo(demandCandidateMeetingId: %s)", demandCandidateMeetingId);
}

function loadCandidateHistory(candidateId, count) {
    "use strict";
    console.log("loadCandidateHistory( candidateId: %s, count: %s)", candidateId, count);

    jQuery.ajax({
        type: "GET",
        url: "/candidate/" + candidateId + "/history/last/" + count,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            console.groupCollapsed("loadCandidateHistory.success: ( candidateId: " + candidateId + ", count: " + count + ")");
            console.dir(data);

            var infoDataHtml = "";
            for(const item of data) {
                let type = item.type;
                console.log("type: %s", type);
                var formatedHtml = null;

                if (type === "add-candidate-note") {
                    formatedHtml = commentCandidateEventTemplate(item);
                } else if (type === "add-demand-candidate") {
                    formatedHtml = demandCandidateEventTemplate(item);
                } else if (type === "add-demand-candidate-note") {
                    formatedHtml = demandCandidateNoteEventTemplate(item);
                } else if (type === "add-demand-candidate-meeting") {
                    formatedHtml = demandCandidateMeetingEventTemplate(item);
                } else if (type === "add-demand-candidate-meeting-note") {
                    formatedHtml = demandCandidateMeetingNoteEventTemplate(item);
                } else if (type === "add-candidate") {
                    formatedHtml = createCandidateEventTemplate(item);
                } else {
                    formatedHtml = "<span>" + type + "</span><br/>";
                }
                if(formatedHtml) {
                    console.log(formatedHtml);
                    infoDataHtml = infoDataHtml + formatedHtml;
                }
            }
            $("#row-info-" + candidateId + " td").html(infoDataHtml);
            console.groupEnd();
        },
        error: processError,
        failure: processFailure
    });
}