function doAddCandidateNote(candidateId, newCandidateNote, okPostProcessing) {
    "use strict";
    console.log("doAddCandidateNote(candidateId: %s, newCandidateNote: %s)", candidateId, JSON.stringify(newCandidateNote));

    jQuery.ajax({
        type: "POST",
        url: "/candidate/" + candidateId + "/note/",
        data: JSON.stringify(newCandidateNote),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log("doAddCandidateNote.success: newCandidateNote: %s", JSON.stringify(newCandidateNote));
            console.dir(data);
            okPostProcessing(data);
            incCounter("comment_count");
        },
        error: processError,
        failure: processFailure
    });
}

function doDeleteCandidateNote(candidateId, candidateNoteId) {
    "use strict";
    console.log("doDeleteCandidateNote( candidateId: %s, candidateNoteId: %s)", candidateId, candidateNoteId);

    if (candidateNoteId) {
        jQuery.ajax({
            type: "DELETE",
            url: "/candidate/" + candidateId + "/note/" + candidateNoteId,
            success: function (data) {
                console.log("doDeleteCandidateNote.success: deleteObject: %s", JSON.stringify(deleteObject));
                console.dir(data);
                $("#candidate-note-row-" + candidateNoteId).remove();
                decCounter("comment_count");
            },
            error: processError,
            failure: processFailure
        });
    }
}