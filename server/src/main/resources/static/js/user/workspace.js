function setCustomerUnpicked(event, customerId, obj) {
    "use strict";
    console.log("setCustomerUnpicked(customerId: %s, obj: %s)", customerId, obj);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble=true);

    jQuery.ajax({
        type: "DELETE",
        url: "/picked/customer/" + customerId,
        success: function (data) {
            console.log("setCustomerUnpicked.success: (customerId: %s)", customerId);
            console.dir(data);
            $("#tr-company-" + customerId).remove();
        },
        error: processError,
        failure: processFailure
    });
}

function setCandidateUnpicked(event, candidateId, obj) {
    "use strict";
    console.log("setCandidateUnpicked(candidateId: %s, obj: %s)", candidateId, obj);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble=true);

    jQuery.ajax({
        type: "DELETE",
        url: "/picked/candidate/" + candidateId,
        success: function (data) {
            console.log("setCandidateUnpicked.success: (candidateId: %s)", candidateId);
            console.dir(data);

            $("#tr-candidate-" + candidateId).remove();
        },
        error: processError,
        failure: processFailure
    });
}

function setDemandUnpicked(event, demandId, obj) {
    "use strict";
    console.log("setDemandUnpicked(demandId: %s, obj: %s)", demandId, obj);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble=true);

    jQuery.ajax({
        type: "DELETE",
        url: "/picked/demand/" + demandId,
        success: function (data) {
            console.log("setDemandUnpicked.success: (demandId: %s)", demandId);
            console.dir(data);

            $("#tr-demand-" + demandId).remove();
        },
        error: processError,
        failure: processFailure
    });
}