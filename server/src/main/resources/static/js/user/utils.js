function processError(jqXHR, textStatus, errorThrown) {
    "use strict";
    console.error("status: %s, statusText: %s, responseXML: %s, responseText: %s", jqXHR.status, jqXHR.statusText, jqXHR.responseXML, jqXHR.responseText);

    if (jqXHR.status == 404) {
        var response = JSON.parse(jqXHR.responseText);
        var message = "";
        switch (response.type) {
            case "department":
                message = "Департамент id: " + response.id + " не найден";
                break;
            case "person":
                message = "Сотрудник id: " + response.id + " не найден";
                break;
            case "demand":
                message = "Заказ id: " + response.id + " не найдено";
                break;
            case "education":
                message = "Образование id: " + response.id + " не найдено";
                break;
            default:
                message = "Объект типа: " + response.type + " id: " + response.id + " не найден";
        }
        alert(message);
    } else {
        console.log("jqXHR: " + jqXHR);
        for (const item in jqXHR) {
            console.log(item + ": " + jqXHR[item]);
        }
//    for(const item in jqXHR.responseJSON) {
//        console.log(item + ": " + jqXHR.responseJSON[item]);
//    }

        console.log("textStatus: " + textStatus);
//    for(const item of textStatus) {
//        console.log(item);
//    }

        console.log("errorThrown: " + errorThrown);
        for (const item of errorThrown) {
            console.log(item);
        }

//      var response = JSON.parse(jqXHR.responseText);
        var response = jqXHR.responseText;

        console.error(response);

//      alert(response.message);
        alert(response);

    }
//    alert(response);

    console.error("textStatus: %s; errorThrown: %s", textStatus, errorThrown);
}

function processFailure(errorMsg) {
    "use strict";
    console.error("Ошибка: %s", errorMsg);
    alert("Ошибка: " + errorMsg);
}

function incCounter(id) {
    "use strict";
    var element = $("#" + id);
    var counter = element.text();
    //убираем скобки ()
    counter = counter.substr(1, counter.length - 2);
    counter = (counter === null || counter === "") ? 0 : parseInt(counter);
    console.log("incCounter(id: %s); counter: %s", id, counter);
//        element.addClass("badge");
    element.text("(" + (counter + 1) + ")");
}

function decCounter(id) {
    "use strict";
    var element = $("#" + id);
    var counter = element.text();
    //убираем скобки ()
    counter = counter.substr(1, counter.length - 2);
    counter = (counter === null || counter === "") ? 0 : parseInt(counter);
    console.log("decCounter(id: %s); counter: %s", id, counter);
    counter = counter - 1;
    element.text((counter === 0) ? "" : "(" + counter + ")");
//        element.removeClass("badge");
}

function doDeleteObject(objectId, objectType) {
    "use strict";
    console.log("doDeleteObject(objectId: %s, objectType: %s)", objectId, objectType);

    if (objectId) {
        jQuery.ajax({
            type: "DELETE",
            url: "/" + objectType + "/" + objectId,
            success: function (data) {
                console.groupCollapsed("delete " + objectType + ".success: objectId: " + objectId);

//            var p = $(deleteObject).parents("tr").first();
//            var p = $(deleteObject).closest("tr");
                var p = $("#tr-" + objectType + "-" + objectId).closest("tr");
                console.log("p: %s, %s", p.prop("tagName"), p.prop("id"));
                p.remove();
                decCounter(objectType + "_count")
                console.groupEnd();
            },
            error: processError,
            failure: processFailure,
            complete: function () {
                $("#deleteConfirmModal").modal("hide");
            }
        });
    }
}