function doAddPerson() {
    "use strict";
    console.log("doAddPerson!");
    location.href = "/person/-1";
}

function filter() {
    "use strict";
    console.log("filter()");
    $("#person-filter-panel").toggle();
}

function cancel() {
    "use strict";
    console.log("cancel!");
    location.href = "/person/";
}

function doClear() {
    "use strict";
    console.log("doClear()");
    $("form input").val('');
}

function deletePerson(event, personId) {
    "use strict";
    console.log("deletePerson(personId: %s)", personId);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble = true);

    $(function () {
        $("#dialog-confirm-delete-person").dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Удалить запись": function () {
                    $(this).dialog("close");
                    doDeletePerson(personId);
                },
                "Отменить": function () {
                    $(this).dialog("close");
                }
            }
        });
    });
}

function doDeletePerson(personId) {
    "use strict";
    console.log("deletePerson(personId: %s)", personId);

    doDeleteObject(personId, "person");
}