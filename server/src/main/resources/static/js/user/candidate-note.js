
function addCandidateNote() {
    "use strict";
    console.log("addCandidateNote()");

    $("#candidate-note-row-0 #candidate-note-id").val("0");
    $("#candidate-note-row-0").parent().children(".row").hide();
    $("#candidate-note-row-0").show();
    $("#add-candidate-note").hide();
}

function cancelAddCandidateNote() {
    "use strict";
    console.log("cancelAddCandidateNote()");

    showCandidateNoteList();
    cleanCandidateNoteForm();
}

function updateCandidateNote(candidateNoteId) {
    "use strict";
    console.log("updateCandidateNote(candidateNoteId: %s)", candidateNoteId);

    $("#candidate-note-row-0 #candidate-note-id").val(candidateNoteId);

    var note = $("#candidate-note-row-" + candidateNoteId).find("div textarea").val();
    $("#candidate-note-row-0").find("div textarea").val(note);
    $("#candidate-note-row-0").parent().children(".row").hide();
    $("#candidate-note-row-0").show();
    $("#add-candidate-note").hide();
}

function saveCandidateNote(candidateId) {
    "use strict";
    console.log("saveCandidateNote(candidateId: %s)", candidateId);

    var candidateNoteId = $("#candidate-note-row-0 #candidate-note-id").val();

    console.log("candidateNoteId: %s", candidateNoteId);
    console.log("candidateNoteId: %s", typeof(candidateNoteId));

    var noteText = $("#candidate-note-row-0 textarea").val();
	console.log("noteText : " + noteText);

    var candidateNote = {
        candidate: {
            candidate_id: candidateId
        },
        person: {
            person_id: 1
        },
        note: noteText
    };

    console.log("candidateNote: %s", JSON.stringify(candidateNote));

    console.log("candidateNote: %s", JSON.stringify(candidateNote));

    if (candidateNoteId === '0') {
        //Новая запись
        doAddCandidateNote(candidateId, candidateNote, postAdd);

    } else {
        //Редактируем запись
        console.log("candidateNoteId !== 0");
        candidateNote.candidate_note_id = candidateNoteId;

        doAddCandidateNote(candidateId, candidateNote, postUpdate);
    }
}

function showCandidateNoteList() {
    "use strict";
    console.log("showCandidateNoteList()");

    $("#candidate-note-row-0").parent().children(".row").show();
    $("#candidate-note-row-0").hide();
    $("#candidate-note-row-new").hide();
    $("#add-candidate-note").show();
}

function cleanCandidateNoteForm() {
    "use strict";
    console.log("cleanCandidateNoteForm()");

    $("#candidate-note-row-0").find("div input[type='text']").val("");
    $("#candidate-note-row-0").find("div textarea").val("");
    $("#candidate-note-row-0").find("div select").val("");
    $("div input[type='hidden']").val("0");
}

function postAdd(candidateNote) {
    "use strict";
    console.log("postAdd: %s", JSON.stringify(candidateNote));

    var candidateNoteRow = $("#candidate-note-row-new").clone();

    candidateNoteRow.attr("id", "candidate-note-row-" + candidateNote.candidate_note_id);
    candidateNoteRow.find("div textarea").val(candidateNote.note);
    var timeLabel = candidateNoteRow.find("div label:contains('Время')");
    timeLabel.text(timeLabel.text() + candidateNote.create_date);

    var authorLabal = candidateNoteRow.find("div label:contains('Автор')");
    authorLabal.text(authorLabal.text() + candidateNote.created_by.fio);

    var button = candidateNoteRow.find("div div button[onclick='edit_button']");
    button.removeProp("onclick");
    button.click(
        function (event) {
            updateCandidateNote(candidateNote.candidate_note_id);
        }
    );

    button = candidateNoteRow.find("div div button[onclick='delete_button']");
    button.removeProp("onclick");
    button.click(
        function (event) {
            setDeleteCandidateNote(candidateNote.candidate_note_id, this);
        }
    );

    var parent = $("#candidate-note-row-new").parent();

    candidateNoteRow.appendTo(parent);

    showCandidateNoteList();
    cleanCandidateNoteForm();
}

function postUpdate(candidateNote) {
    "use strict";
    console.log("postUpdate: %s", JSON.stringify(candidateNote));

    //Редактируем запись
    var candidateNoteRow = $("#candidate-note-row-" + candidateNote.candidate_note_id);

    var note = $("#candidate-note-row-0").find("div textarea").val();

    candidateNoteRow.find("div textarea").val(candidateNote.note);

    showCandidateNoteList();
    cleanCandidateNoteForm();
}

function deleteCandidateNote(candidateId, candidateNoteId) {
    "use strict";
    console.log("deleteCandidateNote(candidateId: %s, candidateNoteId: %s)", candidateId, candidateNoteId);

    if (confirm("Удалить запись?")) {
        doDeleteCandidateNote(candidateId, candidateNoteId);
    }
}

function editCandidateNote(id, obj) {
    "use strict";
    console.log("editCandidateNote(id: %s)", id);

    editCandidateNoteId = id;
    doUpdateCandidateNote();
}