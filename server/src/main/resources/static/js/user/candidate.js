function loadCandidateHistory(candidateId, count) {
    "use strict";
    console.log("loadCandidateHistory(candidateId: %s, count: %s)", candidateId, count)

    jQuery.ajax({
        type: "GET",
        url: "/candidate/" + candidateId + "/history/last/" + count,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.groupCollapsed("loadCandidateHistory: success: data: " + data.length);
            console.dir(data);
            console.groupEnd();
            var infoDataHtml = "";
            console.groupCollapsed("formatedHtml");
            for (let item of data) {
                console.log(item);
                var type = item.type;
                console.log(type);
                var formatedHtml = null;

                if (type === "add-candidate-note") {
                    formatedHtml = commentCandidateEventTemplate(item);
                } else if (type === "add-demand-candidate") {
                    formatedHtml = demandCandidateEventTemplate(item);
                } else if (type === "add-demand-candidate-note") {
                    formatedHtml = demandCandidateNoteEventTemplate(item);
                } else if (type === "add-demand-candidate-meeting") {
                    formatedHtml = demandCandidateMeetingEventTemplate(item);
                } else if (type === "add-demand-candidate-meeting-note") {
                    formatedHtml = demandCandidateMeetingNoteEventTemplate(item);
                } else if (type === "add-candidate") {
                    formatedHtml = createCandidateEventTemplate(item);
                } else {
                    formatedHtml = "<span>" + type + "</span><br/>";
                }
                if (formatedHtml) {
                    console.log(formatedHtml);
                    infoDataHtml = infoDataHtml + formatedHtml;
                }
            }
            console.groupEnd();
            console.log("html: " + $("#tr-candidate-info-" + candidateId + " td").html());
            $("#tr-candidate-info-" + candidateId + " td").html(infoDataHtml);
        },
        error: processError,
        failure: processFailure
    });
}

function doAddCandidate() {
    "use strict";
    console.log("doAddCandidate!");
    location.href = "/candidate/-1";
}

function doImportResume() {
    "use strict";
    console.log("doImportResume");
    location.href = "/import";
}

var deleteObject;

function deleteCandidate(event, candidateId) {
    "use strict";
    console.log("deleteCandidate(candidateId: %s)", candidateId);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble = true);

    $(function () {
        $("#dialog-confirm-delete-candidate").dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Удалить запись": function () {
                    $(this).dialog("close");
                    doDeleteCandidate(candidateId);
                },
                "Отменить": function () {
                    $(this).dialog("close");
                }
            }
        });
    });
}

function deleteCandidateCategory(event, candidateId, categoryId, obj) {
    "use strict"
    console.log("deleteCandidateCategory( candidateId: %s, categoryId: %s)", candidateId, categoryId);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble = true);

    if (candidateId && categoryId) {
        if (confirm("Удалить запись?")) {
            jQuery.ajax({
                type: "DELETE",
                url: "/candidate/" + candidateId + "/category/" + categoryId,
                success: function (data) {
                    console.groupCollapsed("deleteCandidateCategory.success: " + JSON.stringify(deleteObject));
                    console.dir(data);

                    for (item in data) {
                        console.log(item.category_id);
                    }
                    console.groupEnd();
                    getCategoryWithTop(candidateId);
                },
                error: processError,
                failure: processFailure
            });
        }
    }
}

function filter() {
    "use strict";
    console.log("filter()");
    $("#candidate-filter-panel").toggle();
}

function showCandidateInfo(event, obj, candidateId) {
    "use strict";
    console.groupCollapsed("showCandidateInfo(candidateId: %s)", candidateId);
    console.log("obj: " + obj);
    console.log("obj.hasChildNodes: " + obj.hasChildNodes());

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble = true);

    console.log("obj.parent(): " + $(obj).parent());
    console.log("obj.parent(): " + $(obj).parent().prop("tagName"));
    console.log("obj.parent(): " + $(obj).parent().parent().prop("tagName"));

    console.log("arguments: " + arguments);
    for (const arg of arguments)
        console.log("arguments: " + arg);

    var tr = $("#tr-candidate-" + candidateId);
    console.log("tr.children(): " + tr.children());
    console.log("tr.children().length: " + tr.children().length);
    var tdCount = tr.children().length;

    for (let child of obj.childNodes) {

        if (child.className && child.className.includes("glyphicon-chevron-right")) {
            console.log("chevron-right");
            $(child).removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
//            var tr = $( "TR" ).insertAfter( $(obj).parent().parent() );
            $("<tr id=\"tr-candidate-info-" + candidateId + "\"><td colspan=\"" + tdCount + "\" align=\"left\" style=\"padding-left: 30px;border: 1px solid grey;border-radius: 8px;\">Loading...</td></tr>").insertAfter(tr);

            loadCandidateHistory(candidateId, 5);
        } else if (child.className && child.className.includes("glyphicon-chevron-down")) {
            console.log("chevron-down");
            $(child).removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
            $("#tr-candidate-info-" + candidateId).remove();
        }
    }
    console.groupEnd();
}

function getCategoryWithTop(candidateId) {
    "use strict"
    console.log("getCategoryWithTop( candidateId: %s)", candidateId);

    jQuery.ajax({
        type: "GET",
        url: "/candidate/" + candidateId + "/category",
        success: function (data) {
            console.dir(data);
            $("#category_list").empty();
            for (index in data) {
                var item = data[index];
                console.log(item);
                var $tr = $("<tr style=\"border:1px\"/>").appendTo("#category_list");
                var tdString = "<td style=\"align:right;width:60px\">";
                if (item.parent_id === null)
                    tdString = tdString + "<span class=\"glyphicon glyphicon-chevron-down\"></span>";
                else {
                    tdString = tdString + "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                    tdString = tdString + "<span class=\"glyphicon glyphicon-chevron-right\"></span>";
                }
                tdString = tdString + "<span>&nbsp;</span>";
                tdString = tdString + "<span>" + item.name + "</span>";
                tdString = tdString + "</td>";
                $(tdString).appendTo($tr);

                tdString = "<td style=\"width:50px;text-align:right\" id=\"td-category-" + item.category_id + "\">";
                tdString = tdString + "<button type=\"button\" class=\"btn btn-default btn-xs\" title=\"Удалить запись\"";
                tdString = tdString + " onclick=\"deleteCandidateCategory(" + candidateId + ", " + item.category_id + ", this);\">";
                tdString = tdString + "<i class=\"fa fa-remove\"/></button></td>";
                $(tdString).appendTo($tr);
            }
        },
        error: processError,
        failure: processFailure
    });
}

function loadSubCategory(categoryId) {
    "use strict"
    console.log("loadSubCategory(categoryId: %s)", categoryId);

    jQuery.ajax({
        type: "GET",
        url: "/category/" + categoryId + "/item",
        success: function (data) {
            console.log("loadSubCategory.success: (categoryId: %s)", categoryId);
            console.dir(data);
            for (index in data) {
                var item = data[index];
                var div = $("<div id=\"category-id-" + item.category_id + "\" style=\"border:1px\">").appendTo("#category-id-" + categoryId);
                $("<span>&nbsp;&nbsp;&nbsp;</span>").appendTo(div);
                $("<input id=\"check-category-id-" + item.category_id + "\" type=\"checkbox\" class=\"form-check-input\">").appendTo(div);
                $("<span>&nbsp;&nbsp;&nbsp;</span>").appendTo(div);
                $("<span>" + item.name + "</span>").appendTo(div);
            }
        },
        error: processError,
        failure: processFailure
    });
}

function chooseCategory(candidateId) {
    "use strict"
    console.log("chooseCategory( candidateId: %s)", candidateId);

    var list = $("input[id^='check-category-id-']").filter("input:checked");
    console.log("list.length: " + list.length);
    var ids = [];
    for (var i = 0; i < list.length; i++) {
        ids.push(list[i].id.substring("check-category-id-".length));
    }
    console.log("ids: " + ids);

    jQuery.ajax({
        type: "POST",
        url: "/candidate/" + candidateId + "/category/batch",
        data: JSON.stringify(ids),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log("chooseCategory.success: ( candidateId: %s)", candidateId);
            console.dir(data);

            for (index in data) {
                console.log("item.category_id: " + data[index].category_id);
            }
            getCategoryWithTop(candidateId);

            var divList = $("div[id^='category-id-']");
            console.log("divList.length: " + divList.length);
            for (var i = 0; i < divList.length; i++) {
                console.log("id: " + divList[i].id.substring("category-id-".length));
                var id = parseInt(divList[i].id.substring("category-id-".length));
                console.log("id: " + id);
                if (id >= 100) {
                    console.log("Remove: " + "#" + divList[i].id);
                    $("#" + divList[i].id).remove();
                }
            }
        },
        error: processError,
        failure: processFailure,
        complete: function () {
            $("#categoryChoice").modal("hide");
        }
    });
}

function chooseFilterCategory() {
    "use strict"
    console.groupCollapsed("chooseFilterCategory()");

    var list = $("input[id^='check-category-id-']").filter("input:checked");
    console.log("list.length: " + list.length);
    var items = [];
    //todo: можно сделать покрасивее
    for (var i = 0; i < list.length; i++) {
        var item = list[i];
        var obj = new Object();
        obj.id = item.id.substring("check-category-id-".length);
        obj.text = $("#" + item.id).next().next().html();
        items.push(obj);
    }

    console.log("items.length: " + items.length);
    console.dir(items)

    if (items.length > 0) {
        $("#category_id").val(items[0].id);
        $("#category_name").text(items[0].text);
    }
    $("#categoryChoice").modal("hide");
    console.groupEnd();
}

function clearFilterCategory() {
    "use strict"
    console.groupCollapsed("clearFilterCategory()");
    console.log($("#category_id").val());
    console.log($("#category_name").html());
    console.log($("#category_id").attr("value"));
    console.groupEnd();
    $("#category_name").html("");

    $("#category_id").attr("value", "");
}

function setCandidatePicked(event, candidateId, obj) {
    "use strict"
    console.log("setCandidatePicked(candidateId: %s, obj: %s)", candidateId, obj);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble = true);

    jQuery.ajax({
        type: "POST",
        url: "/picked/candidate/" + candidateId,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.groupCollapsed("setCandidateUnpicked.success: candidateId: " + candidateId);
            console.dir(data);
            obj.title = "Удалить из избранных";
            obj.onclick = function (event) {
                setCandidateUnpicked(event, candidateId, obj);
            };
            obj.innerHTML = "<i class=\"fa fa-star\"/>";
            console.groupEnd();
        },
        error: processError,
        failure: processFailure
    });
}

function setCandidateUnpicked(event, candidateId, obj) {
    "use strict";
    console.log("setCandidateUnpicked(candidateId: %s, obj: %s)", candidateId, obj);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble = true);

    jQuery.ajax({
        type: "DELETE",
        url: "/picked/candidate/" + candidateId,
        success: function (data) {
            console.groupCollapsed("setCandidateUnpicked.success: candidateId: " + candidateId);
            console.dir(data);
            obj.title = "Добавить в избранные";
            obj.onclick = function (event) {
                setCandidatePicked(event, candidateId, obj);
            };
            obj.innerHTML = "<i class=\"fa fa-star-o\"/>";
            console.groupEnd();
        },
        error: processError,
        failure: processFailure
    });
}

function doClear() {
    "use strict";
    console.log("doClear()");
    $("form input").val('');
}