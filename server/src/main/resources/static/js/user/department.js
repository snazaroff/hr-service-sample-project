function addDepartment() {
    "use strict";
    console.log("addDepartment()");
    location.href = "/department/-1";
}

function deleteDepartment(event, departmentId) {
    "use strict";
    console.log("deleteDepartment(departmentId: %s)", departmentId);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble = true);

    $(function () {
        $("#dialog-confirm-delete-department").dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Удалить запись": function () {
                    $(this).dialog("close");
                    doDeleteDepartment(departmentId);
                },
                "Отменить": function () {
                    $(this).dialog("close");
                }
            }
        });
    });
}

function doDeleteDepartment(departmentId) {
    "use strict";
    console.log("doDeleteDepartment(departmentId: %s)", departmentId);

    doDeleteObject(departmentId, "department");
}