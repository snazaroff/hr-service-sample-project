function doDeleteCandidate(candidateId, deleteObject) {
    "use strict";
    console.log("doDeleteCandidate(candidateId: %s)", candidateId);

    doDeleteObject(candidateId, "candidate");
}