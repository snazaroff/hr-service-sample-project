function doAddEmploymentAgency() {
    "use strict";
    console.log("doAddEmploymentAgency");
    location.href = "/employment-agency/-1";
}

function filter() {
    "use strict";
    console.log("filter()");
    $("#employment-agency-filter-panel").toggle();
}

function doClear() {
    "use strict";
    console.log("doClear()");
    $("form input").val('');
}

function deleteEmploymentAgency(event, employmentAgencyId, deleteObject) {
    "use strict";
    console.log("deleteEmploymentAgency(employmentAgencyId: %s, deleteObject: %s)", employmentAgencyId, deleteObject);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble=true);

    $( function() {
        $( "#dialog-confirm-delete-employment-agency" ).dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Удалить запись": function() {
                    $( this ).dialog( "close" );
                    doDeleteEmploymentAgency(employmentAgencyId, deleteObject);
                },
                "Отменить": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    } );
}

function doDeleteEmploymentAgency(employmentAgencyId, deleteObject) {
    "use strict";
    console.log("doDeleteEmploymentAgency( employmentAgencyId: %s, deleteObject: %s)", employmentAgencyId, deleteObject);

    if (deleteEmploymentAgency) {
        jQuery.ajax({
            type: "DELETE",
            url: "/employment-agency/" + employmentAgencyId,
            success: function (data) {
                console.log("deleteEmploymentAgency.success: ( employmentAgencyId: %s)", employmentAgencyId);
                console.dir(data);
                var p = $(deleteObject).parent().parent();
                console.log("p:" + p);
                p.hide();
            },
            error: processError,
            failure: processFailure,
            complete: function () {
                $("#deleteConfirmModal").modal("hide");
            }
        });
    }
}

function addEmploymentAgencyDepartment(employmentAgencyId) {
    "use strict";
    console.log("addEmploymentAgencyDepartment( employmentAgencyId: %s)", employmentAgencyId);
    location.href = "/employment-agency/" + employmentAgencyId + "/department/-1";
}

function addEmploymentAgencyPerson(employmentAgencyId) {
    "use strict";
    console.log("addEmploymentAgencyPerson( employmentAgencyId: %s)", employmentAgencyId);
    location.href = "/employment-agency/" + employmentAgencyId + "/person/-1";
}