// var source   = document.getElementById("entry-template").innerHTML;
// var template = Handlebars.compile(source);

var createCandidateEventTemplate = Handlebars.compile(
    '<div class="info">\n' +
    '    <table style="width:100%" cellspacing="25">\n' +
    '        <tr class="tr_comment">\n' +
    '            <td class="td_create_datetime">\n' +
    '                <div class="well well-sm" style="width:100%;height:50px;border-radius: 8px;">\n' +
    '    {{createDateTime}}\n' +
    '                </div>\n' +
    '            </td>\n' +
    '            <td class="td_comment">\n' +
    '                <div class="well well-sm" style="width:100%;height:50px;border-radius: 8px;">\n' +
    '                    <span><b>Комментарий по кандидату:</b> {{note}}</span><br/>\n' +
    '                    <span><b>Автор:</b> <a href="/person/{{createdBy.person_id}}">{{createdBy.fio}}</a></span>\n' +
    '                </div>\n' +
    '            </td>\n' +
    '        </tr>\n' +
    '    </table>\n' +
    '</div>'
);

var commentCandidateEventTemplate = Handlebars.compile(
    '<div class="info">\n' +
    '    <table style="width:100%" cellspacing="25">\n' +
    '        <tr class="tr_comment">\n' +
    '            <td class="td_create_datetime">\n' +
    '                <div class="well well-sm" style="width:100%;height:55px;border-radius: 8px;">\n' +
    '    {{createDateTime}}\n' +
    '                </div>\n' +
    '            </td>\n' +
    '            <td class="td_comment">\n' +
    '                <div class="well well-sm" style="width:100%;height:55px;border-radius: 8px;">\n' +
    '                    <span><b>Комментарий по кандидату:</b> {{note}}</span><br/>\n' +
    '                    <span><b>Автор:</b> <a href="/person/{{createdBy.person_id}}">{{createdBy.fio}}</a></span>\n' +
    '                </div>\n' +
    '            </td>\n' +
    '        </tr>\n' +
    '    </table>\n' +
    '</div>'
);

var demandCandidateEventTemplate = Handlebars.compile(
    '<div class="info">\n' +
    '    <table style="width:100%">\n' +
    '        <tr class="tr_comment">\n' +
    '            <td class="td_create_datetime">\n' +
    '                <div class="well well-sm" style="width:100%;height:50px;border-radius: 8px;">\n' +
    '    {{createDateTime}}\n' +
    '                </div>\n' +
    '            </td>\n' +
    '            <td class="td_comment">\n' +
    '                <div class="well well-sm" style="width:100%;height:50px;border-radius: 8px;">\n' +
    '                    <span><b>Присоединен к заказу:</b> <a href="/demand/{{demand.demand_id}}">{{demand.position}}</a></span><br/>\n' +
    '                    <span><b>Автор:</b> <a href="/person/{{createdBy.person_id}}">{{createdBy.fio}}</a></span>\n' +
    '                </div>\n' +
    '            </td>\n' +
    '        </tr>\n' +
    '    </table>\n' +
    '</div>'
);

var demandCandidateNoteEventTemplate = Handlebars.compile(
    '<div class="info">\n' +
    '    <table style="width:100%">\n' +
    '        <tr class="tr_comment">\n' +
    '            <td class="td_create_datetime">\n' +
    '                <div class="well well-sm" style="width:100%;height:75px;border-radius: 8px;">\n' +
    '    {{createDateTime}}\n' +
    '                </div>\n' +
    '            </td>\n' +
    '            <td class="td_comment">\n' +
    '                <div class="well well-sm" style="width:100%;height:75px;border-radius: 8px;">\n' +
    '                    <span><b>Комментарий по заказу:</b> <a href="/demand/{{demand.demand_id}}">{{demand.position}}</a></span><br/>\n' +
    '                    <span><b>Автор:</b> <a href="/person/{{createdBy.person_id}}">{{createdBy.fio}}</a></span><br/>\n' +
    '                    <span><b>Комментарий:</b> {{note}}</span>\n' +
    '                </div>\n' +
    '            </td>\n' +
    '        </tr>\n' +
    '    </table>\n' +
    '</div>'
);

var demandCandidateMeetingEventTemplate = Handlebars.compile(
    '<div class="info">\n' +
    '    <table style="width:100%">\n' +
    '        <tr class="tr_comment">\n' +
    '            <td class="td_create_datetime">\n' +
    '                <div class="well well-sm" style="width:100%;height:100px;border-radius: 8px;">\n' +
    '    {{createDateTime}}\n' +
    '                </div>\n' +
    '            </td>\n' +
    '            <td class="td_comment">\n' +
    '                <div class="well well-sm" style="width:100%;height:80px;border-radius: 8px;">\n' +
    '                    <span><b>Назначена встреча:</b></span>\n' +
    '                    <span>{{meetingDateTime}}</span><br/>\n' +
    '                    <span style="font-weight: bold">Заказ:</span>\n' +
    '                    <a href="/demand/{{demand.demand_id}}">{{demand.position}}</a><br/>\n' +
    '                    <span><b>Автор:</b> <a href="/person/{{createdBy.person_id}}">{{createdBy.fio}}</a></span>\n' +
    '                </div>\n' +
    '            </td>\n' +
    '        </tr>\n' +
    '    </table>\n' +
    '</div>'
);

var demandCandidateMeetingNoteEventTemplate = Handlebars.compile(
    '<div class="info">\n' +
    '    <table style="width:100%">\n' +
    '        <tr class="tr_comment" style="border-radius: 8px;">\n' +
    '            <td class="td_create_datetime">\n' +
    '                <div class="well well-sm" style="width:100%;height:100px;border-radius: 8px;">\n' +
    '    {{createDateTime}}\n' +
    '                </div>\n' +
    '            </td>\n' +
    '            <td class="td_comment">\n' +
    '                <div class="well well-sm" style="width:100%;height:100px;border-radius: 8px;">\n' +
    '                    <span style="font-weight: bold">Комментарий по встрече:</span>\n' +
    '                    <span>{{item.meetingDateTime}}</span><br/>\n' +
    '                    <span style="font-weight: bold">Заказ:</span>\n' +
    '                    <a href="/demand/{{demand.demand_id}}">{{demand.position}}</a><br/>\n' +
    '                    <span style="font-weight: bold">Комментарий:</span>\n' +
    '                    <span>{{note}}</span><br/>\n' +
    '                    <span><b>Автор:</b> <a href="/person/{{createdBy.person_id}}">{{createdBy.fio}}</a></span>\n' +
    '                </div>\n' +
    '            </td>\n' +
    '        </tr>\n' +
    '    </table>\n' +
    '</div>'
);