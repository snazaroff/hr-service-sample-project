function addLanguage() {
    "use strict"
    console.log("addLanguage()");

    $("#new-language").show();
    $("#btn-add-language").attr("disabled", true);
}

function cancelCandidateLanguage(obj) {
    "use strict"
    console.log("cancelCandidateLanguage()");

    $("#new-language").hide();
    $("#languageList :selected").prop("selected", false);
    $("#languageStatusList :selected").prop("selected", false);
    $("#btn-add-language").attr("disabled", false);
}

function postCandidateLanguage(candidateId, obj) {
    "use strict"
    var languageId = $("#languageList").val();
    var statusId = $("#languageStatusList").val();

    console.log("postCandidateLanguage( candidateId: %s, languageId: %s, statusId: %s, obj: %s )", candidateId, languageId, statusId, obj);
    doAddLanguage(candidateId, languageId, statusId);
}

function addCandidateLanguage(candidateId, languageId, statusId, obj) {
    "use strict"
    console.log("addCandidateLanguage( candidateId: %s, languageId: %s, statusId: %s, obj: %s )", candidateId, languageId, statusId, obj);

    $("#new-language").show();
}

function deleteCandidateLanguage(candidateId, languageId) {
    "use strict"
    console.log("deleteCandidateLanguage( candidateId: %s, languageId: %s)", candidateId, languageId);

    doDeleteLanguage(candidateId, languageId);
}

function doAddLanguage(candidateId, languageId, status) {
    "use strict"
    console.log("doAddLanguage( candidateId: %s, languageId: %s, status: %s", candidateId, languageId, status);

    jQuery.ajax({
        type: "POST",
        url: "/candidate/" + candidateId + "/language/" + languageId + "/status/" + status,
        success: function (data) {
            console.log("doAddLanguage.success: candidateId: %s, languageId: %s, status: %s", candidateId, languageId, status);
            console.dir(data);

            var formatedHtml = candidateLanguageTemplate(data);

            $(formatedHtml).appendTo($("#language-table"));
            $("#new-language").hide();
        },
        error: processError,
        failure: processFailure,
        complete: function (jqXHR, textStatus) {
            $("#btn-add-language").attr("disabled", false);
        }
    });
}

function doDeleteLanguage(candidateId, languageId) {
    "use strict"
    console.log("doDeleteLanguage( candidateId: %s, languageId: %s)", candidateId, languageId);

    jQuery.ajax({
        type: "DELETE",
        url: "/candidate/" + candidateId + "/language/" + languageId,
        success: function (data) {
            console.dir(data);
            $("#td-language-" + candidateId + "-" + languageId).parent().remove();
        },
        error: processError,
        failure: processFailure
    });
}