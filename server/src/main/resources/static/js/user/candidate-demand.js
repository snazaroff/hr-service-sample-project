function addCandidateDemand(demandId, candidateId) {
    "use strict";
    console.log("addCandidateDemand( demandId: %s, candidateId: %s)", demandId, candidateId);

    let demandCandidate = {
        demand: {
            demand_id: demandId
        },
        candidate: {
            candidate_id: candidateId
        }
    };
    console.log(JSON.stringify(demandCandidate));

    jQuery.ajax({
        type: "POST",
        url: "/demand/" + demandId + "/candidate",
        data: JSON.stringify(demandCandidate),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.groupCollapsed("addCandidateDemand.success:");
            console.dir(data);
            let formatedHtml = candidateDemandTemplate(data);

            $(formatedHtml).appendTo($("#tbl-candidate-demand"));
            incCounter("demand_count")
            console.groupEnd();
        },
        error: processError,
        failure: processFailure,
        complete: function () {
            $("#demandChoicePanel").modal("hide");
        }
    });
}

function deleteCandidateDemand(demandId, demandCandidateId, event) {
    "use strict"
    console.log("deleteCandidateDemand( demandId: %s, demandCandidateId: %s)", demandId, demandCandidateId);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble = true);

    jQuery.ajax({
        type: "DELETE",
        url: "/demand/" + demandId + "/candidate/" + demandCandidateId,
        success: function (data) {
            console.groupCollapsed("deleteCandidateDemand.success: demandCandidateId: " + demandCandidateId);
            console.dir(data);
            decCounter("demand_count")
            $("#tr-candidate-demand-" + demandCandidateId).remove();
            console.groupEnd();
        },
        error: processError,
        failure: processFailure
    });
}