//todo: надо разобраться
//var education_form_fields = ["education_name", "start_date", "end_date", "education_type"];
var education_form_fields = ["education_name", "start_date", "end_date"];

function addEducation() {
    "use strict";
    console.log("addEducation();");

    $("#new-education").show();
    $("#btn-add-education").attr("disabled", true);
}

function cancelCandidateEducation() {
    "use strict";
    console.log("cancelCandidateEducation()");

    $("#new-education").hide();

    $("#new-education-education-name").val("");
    $("#new-education-start-date").val("");
    $("#new-education-end-date").val("");

    $("#new-education-education-type :selected").prop("selected", false);
    $("#educationStatusList :selected").prop("selected", false);
    $("#btn-add-education").attr("disabled", false);
}

function editCandidateEducation(educationId, educationTypeId) {
    "use strict";
    console.log("editCandidateEducation(educationId: %s, educationTypeId: %s)", educationId, educationTypeId);

    var tr = $("#tr-education-" + educationId);
    var td;
    var span;
    var input;
    for (const field of education_form_fields) {
        console.log("field: " + field);
        td = tr.find("#td-education-" + field + "-" + educationId);
        span = tr.find("#span-education-" + field + "-" + educationId);
        input = $("<input type=\"text\"/>");
        input.val(span.text());
        input.appendTo(td);
        span.hide();
    }

    let field = "education_type";
    var educationType = $("#new-education-education-type").clone();
    educationType.attr("id", "education-type-" + educationId);
    educationType.val(educationTypeId).change();

    td = tr.find("#td-education-" + field + "-" + educationId);
    educationType.appendTo(td);

    span = tr.find("#span-education-" + field + "-" + educationId);
    span.hide();

    $("#btn-education-edit-" + educationId).hide();
    $("#btn-education-post-" + educationId).show();
    $("#btn-education-delete-" + educationId).hide();
    $("#btn-education-cancel-" + educationId).show();
}

function cancelEditEducation(educationId) {
    "use strict";
    console.log("cancelEditEducation(educationId: %s)", educationId);

    let tr = $("#tr-education-" + educationId);
    for (const field of education_form_fields) {
        console.log("field: " + field);
        tr.find("#span-education-" + field + "-" + educationId).show();
        tr.find("#td-education-" + field + "-" + educationId + " input").remove();
    }

    let field = "education_type";
    $("#education-type-" + educationId).remove();
    var span = tr.find("#span-education-" + field + "-" + educationId);
    span.show();

    $("#btn-education-edit-" + educationId).show();
    $("#btn-education-post-" + educationId).hide();
    $("#btn-education-delete-" + educationId).show();
    $("#btn-education-cancel-" + educationId).hide();
}

function postCandidateEducation(candidateId) {
    "use strict";
    var educationName = $("#new-education #new-education-education-name").val();
    var startDate = $("#new-education #new-education-start-date").val();
    var endDate = $("#new-education #new-education-end-date").val();
    var educationType = $("#new-education #new-education-education-type").val();

    if (educationName === "") {
        alert("Введите название образования");
        return;
    }

    if (startDate === "") {
        alert("Введите дату начала периода");
        return;
    }

    if (educationType === "") {
        alert("Введите тип образования");
        return;
    }

    console.log("postCandidateEducation(candidateId: %s, educationName: %s, startDate: %s, endDate: %s, educationType: %s)",
        candidateId, educationName, startDate, endDate, educationType);
    doAddEducation(candidateId, educationName, startDate, endDate, educationType);
}

function postUpdateEducation(candidateId, educationId) {
    "use strict";

    var tr = $("#tr-education-" + educationId);
    var educationName = tr.find("#td-education-education_name" + "-" + educationId + " input").val();
    var startDate = tr.find("#td-education-start_date" + "-" + educationId + " input").val();
    var endDate = tr.find("#td-education-end_date" + "-" + educationId + " input").val();
    var educationTypeId = $("#education-type-" + educationId).val();

    if (educationName === "") {
        alert("Введите название образования");
        return;
    }

    if (startDate === "") {
        alert("Введите дату начала периода");
        return;
    }

    if (educationTypeId === "") {
        alert("Введите тип образования");
        return;
    }

    console.log("postUpdateEducation(candidateId: %s, educationId: %s, educationName: %s, startDate: %s, endDate: %s, educationTypeId: %s)",
        candidateId, educationId, educationName, startDate, endDate, educationTypeId);

    doUpdateEducation(candidateId, educationId, educationName, startDate, endDate, educationTypeId);
}

function deleteCandidateEducation(candidateId, educationId) {
    "use strict";
    console.log("deleteCandidateEducation(candidateId: %s, educationId: %s)", candidateId, educationId);
    doDeleteEducation(candidateId, educationId);
}

function doAddEducation(candidateId, educationName, startDate, endDate, educationTypeId) {
    "use strict";
    console.log("doAddEducation(candidateId: %s, educationName: %s", candidateId, educationName);

    let education = {
        education_name: educationName,
        start_date: startDate,
        end_date: endDate,
        education_type: {
            education_type_id: educationTypeId
        }
    }
    console.log("emloyment: " + JSON.stringify(education));

    jQuery.ajax({
        type: "POST",
        url: "/candidate/" + candidateId + "/education",
        data: JSON.stringify(education),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log("doAddEducation.success: (candidateId: %s, educationName: %s", candidateId, educationName);

            data.candidate_id = candidateId;
            console.dir(data);

            let formatedHtml = candidateEducationTemplate(data);
            $(formatedHtml).appendTo($("#education-table"));

            $("#new-education").hide();
            $("#new-education #new-education-education-name").val("");
            $("#new-education #new-education-start-date").val("");
            $("#new-education #new-education-end-date").val("");
            $("#new-education #new-education-education-type").val("");
        },
        error: processError,
        failure: processFailure,
        complete: function () {
            $("#btn-add-education").attr("disabled", false);
        }
    });
}

function doUpdateEducation(candidateId, educationId, educationName, startDate, endDate, educationTypeId) {
    "use strict";
    console.log("doUpdateEducation(candidateId: %s, educationId: %s", candidateId, educationId);

    let education = {
        education_id: educationId,
        education_name: educationName,
        start_date: startDate,
        end_date: endDate,
        education_type: {
            education_type_id: educationTypeId
        }
    }
    console.log("emloyment: " + JSON.stringify(education));

    jQuery.ajax({
        type: "PUT",
        url: "/candidate/" + candidateId + "/education",
        data: JSON.stringify(education),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.groupCollapsed("doUpdateEducation.success:");
            console.dir(data);
            var tr = $("#tr-education-" + educationId);
            for (const field of education_form_fields) {
                console.log("field: %s, data[field]: %s, typeof(data[field]): %s", field, data[field], typeof (data[field]));
                tr.find("#span-education-" + field + "-" + educationId).show();
                if (data[field] === null) {
                    tr.find("#span-education-" + field + "-" + educationId).text("");
                } else {
                    tr.find("#span-education-" + field + "-" + educationId).text(data[field]);
                }
                tr.find("#td-education-" + field + "-" + educationId + " input").remove();
            }

            var field = "education_type";
            $("#education-type-" + educationId).remove();
            var span = tr.find("#span-education-" + field + "-" + educationId);
            span.text(data.education_type.name);
            span.show();
            $("#btn-education-edit-" + educationId).attr("onclick", "editCandidateEducation(" + data.education_id + ", " + data.education_type.education_type_id + ");")

            $("#btn-education-edit-" + educationId).show();
            $("#btn-education-post-" + educationId).hide();
            $("#btn-education-delete-" + educationId).show();
            $("#btn-education-cancel-" + educationId).hide();
            console.groupEnd();
        },
        error: processError,
        failure: processFailure
    });
}

function doDeleteEducation(candidateId, educationId) {
    "use strict";
    console.log("doDeleteEducation(candidateId: %s, educationId: %s)", candidateId, educationId);

    jQuery.ajax({
        type: "DELETE",
        url: "/candidate/" + candidateId + "/education/" + educationId,
        success: function (data) {
            console.log("doDeleteEducation.success: (candidateId: %s, educationId: %s)", candidateId, educationId);
            console.dir(data);
            $("#tr-education-" + educationId).remove();
        },
        error: processError,
        failure: processFailure
    });
}