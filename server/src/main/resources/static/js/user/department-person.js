function addDepartmentPerson(departmentId) {
    "use strict";
    console.log("addDepartmentPerson(departmentId: %s)", departmentId);
    location.href = "/department/" + departmentId + "/person/-1";
}
