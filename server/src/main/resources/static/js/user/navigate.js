function activateTab(pages, page) {
    console.log("utils.activateTab('%s'); pages: %s", page, JSON.stringify(pages));

    if (!pages.includes(page)) {
        return;
    }

    $("li").removeClass("active");
    $("#tab_" + page).addClass("active");

    pages.forEach((item, index, array) => {
        $(`#page_${item}`).hide();
    });
    $("#page_" + page).show();
    var lastPath = storage.getLast();
    if (lastPath.indexOf('#') > -1) {
        lastPath = lastPath.substr(0, lastPath.indexOf('#'));
    }
    lastPath = lastPath + '#' + page;
    storage.setLast(lastPath);
}

function setInitPage(pages, defaultPage) {
    console.log("setInitPage(defaultPage: '%s', '%s')", defaultPage, JSON.stringify(pages));

    activateTab(pages, defaultPage)
    //Если в hash прописана активная страница, переключаем на неё
    var hash = window.location.hash;
    if (hash !== null && hash.trim() !== '')
        activateTab(pages, hash.substring(1))
}

function forward() {
    "use strict";
    var path = location.href;
    var paths = storage.getPaths();
    console.log("forward( %s ); paths: %s", path, JSON.stringify(paths));
    storage.setLast(path);
}

let storage = new Paths();

function back() {
    "use strict";
    var paths = storage.getPaths();
    console.log("back(%s)", JSON.stringify(paths));

    storage.getLast();
    var path = storage.getLast();
    storage.setLast(path)

    location.href = path;
}

function getPaths() {
    "use strict";
    console.log("paths: %s; %s", window.paths, window.paths.length);
    return window.paths;
}

function clearPaths() {
    "use strict";
    storage.clear();
}