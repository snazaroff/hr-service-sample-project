var fields = ["firm_name", "start_date", "end_date", "position"];

function addEmployment() {
    "use strict"
    console.log("addEmployment()");

    $("#new-employment").show();
    $("#btn-add-employment").attr("disabled", true);
}

function cancelCandidateEmployment() {
    "use strict"
    console.log("cancelCandidateEmployment()");

    $("#new-employment").hide();
    $("#employmentList :selected").prop("selected", false);
    $("#employmentStatusList :selected").prop("selected", false);
    $("#btn-add-employment").attr("disabled", false);
}

function editCandidateEmployment(employmentId) {
    "use strict"
    console.log("editCandidateEmployment(employmentId: %s)", employmentId);

    var tr = $("#tr-employment-" + employmentId);
    for (const field of fields) {
        console.log("field: %s", field);
        var td = tr.find("#td-employment-" + field + "-" + employmentId);
        var span = tr.find("#span-employment-" + field + "-" + employmentId);
        var input = $("<input type=\"text\"/>");
        input.val(span.text());
        input.appendTo(td);
        span.hide();
    }
    $("#btn-employment-edit-" + employmentId).hide();
    $("#btn-employment-post-" + employmentId).show();
    $("#btn-employment-delete-" + employmentId).hide();
    $("#btn-employment-cancel-" + employmentId).show();
}

function cancelEditEmployment(employmentId) {
    "use strict"
    console.log("cancelEditEmployment(employmentId: %s)", employmentId);

    var tr = $("#tr-employment-" + employmentId);
    for (const field of fields) {
        console.log("field: %s", field);
        tr.find("#span-employment-" + field + "-" + employmentId).show();
        tr.find("#td-employment-" + field + "-" + employmentId + " input").remove();
    }
    $("#btn-employment-edit-" + employmentId).show();
    $("#btn-employment-post-" + employmentId).hide();
    $("#btn-employment-delete-" + employmentId).show();
    $("#btn-employment-cancel-" + employmentId).hide();
}


function postCandidateEmployment(candidateId) {
    "use strict"
    console.log("postCandidateEmployment(candidateId: %s)", candidateId);

    var firmName = $("#new-employment #new-employment-firm-name").val();
    var startDate = $("#new-employment #new-employment-start-date").val();
    var endDate = $("#new-employment #new-employment-end-date").val();
    var position = $("#new-employment #new-employment-position").val();

    if (firmName === "") {
        alert("Введите название фирмы");
        return;
    }

    if (startDate === "") {
        alert("Введите дату начала периода");
        return;
    }

    if (position === "") {
        alert("Введите должность");
        return;
    }

    console.log("postCandidateEmployment(candidateId: %s, firmName: %s, startDate: %s, endDate: %s, position: %s)",
        candidateId, firmName, startDate, endDate, position);

    doAddEmployment(candidateId, firmName, startDate, endDate, position);
}

function postUpdateEmployment(candidateId, employmentId) {
    "use strict"
    console.log("postUpdateEmployment(candidateId: %s, employmentId: %s)", candidateId, employmentId);

    var tr = $("#tr-employment-" + employmentId);
    var firmName = tr.find("#td-employment-firm_name" + "-" + employmentId + " input").val();
    var startDate = tr.find("#td-employment-start_date" + "-" + employmentId + " input").val();
    var endDate = tr.find("#td-employment-end_date" + "-" + employmentId + " input").val();
    var position = tr.find("#td-employment-position" + "-" + employmentId + " input").val();

    if (firmName === "") {
        alert("Введите название фирмы");
        return;
    }

    if (startDate === "") {
        alert("Введите дату начала периода");
        return;
    }

    if (position === "") {
        alert("Введите должность");
        return;
    }

    console.log("postUpdateEmployment(candidateId: %s, employmentId: %s, firmName: %s, startDate: %s, endDate: %s, position: %s)",
        candidateId, employmentId, firmName, startDate, endDate, position);

    doUpdateEmployment(candidateId, employmentId, firmName, startDate, endDate, position);
}

function deleteCandidateEmployment(candidateId, employmentId) {
    "use strict"
    console.log("deleteCandidateEmployment(candidateId: %s, employmentId: %s)", candidateId, employmentId);

    doDeleteEmployment(candidateId, employmentId);
}

function doAddEmployment(candidateId, firmName, startDate, endDate, position) {
    "use strict"
    console.log("doAddEmployment(candidateId: %s, firmName: %s)", candidateId, firmName);

    var employment = {
        firm_name: firmName,
        start_date: startDate,
        end_date: endDate,
        position: position
    }

    console.log("emloyment: " + JSON.stringify(employment));

    jQuery.ajax({
        type: "POST",
        url: "/candidate/" + candidateId + "/employment",
        data: JSON.stringify(employment),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log("doAddEmployment.success: (candidateId: %s, firmName: %s)", candidateId, firmName);

            data.candidate_id = candidateId;
            console.dir(data);

            var formatedHtml = candidateEmploymentTemplate(data);

            $(formatedHtml).appendTo($("#employment-table"));
            //todo: надо переделать
            $("#new-employment").hide();
            $("#new-employment #new-employment-firm-name").val("");
            $("#new-employment #new-employment-start-date").val("");
            $("#new-employment #new-employment-end-date").val("");
            $("#new-employment #new-employment-position").val("");
        },
        error: processError,
        failure: processFailure,
        complete: function (jqXHR, textStatus) {
            $("#btn-add-employment").attr("disabled", false);
        }
    });
}

function doUpdateEmployment(candidateId, employmentId, firmName, startDate, endDate, position) {
    "use strict"
    console.log("doUpdateEmployment(candidateId: %s, employmentId: %s)", candidateId, employmentId);

    var employment = {
        employment_id: employmentId,
        firm_name: firmName,
        start_date: startDate,
        end_date: endDate,
        position: position
    }

    console.log("emloyment: " + JSON.stringify(employment));

    jQuery.ajax({
        type: "PUT",
        url: "/candidate/" + candidateId + "/employment",
        data: JSON.stringify(employment),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log("doUpdateEmployment.success: (candidateId: %s, employmentId: %s)", candidateId, employmentId);
            console.dir(data);
            var tr = $("#tr-employment-" + employmentId);
            for (let field of fields) {
                console.log("field: %s, data[field]: %s, typeof(data[field]): %s", field, data[field], typeof (data[field]));
                let span = tr.find("#span-employment-" + field + "-" + employmentId)
                span.text((data[field] === null) ? "" : data[field]);
                span.show();
                tr.find("#td-employment-" + field + "-" + employmentId + " input").remove();
            }
            $("#btn-employment-edit-" + employmentId).show();
            $("#btn-employment-post-" + employmentId).hide();
            $("#btn-employment-delete-" + employmentId).show();
            $("#btn-employment-cancel-" + employmentId).hide();
        },
        error: processError,
        failure: processFailure
    });
}

function doDeleteEmployment(candidateId, employmentId) {
    "use strict"
    console.log("doDeleteEmployment(candidateId: %s, employmentId: %s)", candidateId, employmentId);

    jQuery.ajax({
        type: "DELETE",
        url: "/candidate/" + candidateId + "/employment/" + employmentId,
        success: function (data) {
            console.log("doDeleteEmployment.success: (candidateId: %s, employmentId: %s)", candidateId, employmentId);
            console.dir(data);
            $("#tr-employment-" + employmentId).remove();
        },
        error: processError,
        failure: processFailure
    });
}
