function doAddCustomer() {
    "use strict";
    console.log("doAddCustomer");
    location.href = "/customer/-1";
}

function filter() {
    "use strict";
    console.log("filter()");
    $("#customer-filter-panel").toggle();
}

function doClear() {
    "use strict";
    console.log("doClear()");
    $("form input").val('');
}

function cancel() {
    "use strict";
    console.log("cancel!");
    location.href = "/customer/";
}

function deleteCustomer(event, customerId, deleteObject) {
    "use strict";
    console.log("deleteCustomer(customerId: %s, deleteObject: %s)", customerId, deleteObject);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble = true);

    $(function () {
        $("#dialog-confirm-delete-customer").dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Удалить запись": function () {
                    $(this).dialog("close");
                    doDeleteCustomer(customerId, deleteObject);
                },
                "Отменить": function () {
                    $(this).dialog("close");
                }
            }
        });
    });
}

function doDeleteCustomer(customerId, deleteObject) {
    "use strict";
    console.log("doDeleteCustomer( customerId: %s, deleteObject: %s)", customerId, deleteObject);

    if (deleteCustomer) {
        jQuery.ajax({
            type: "DELETE",
            url: "/customer/" + customerId,
            success: function (data) {
                console.log("deleteCustomer.success: ( customerId: %s)", customerId);
                console.dir(data);
                var p = $(deleteObject).parent().parent();
                console.log("p:" + p);
                p.hide();
            },
            error: processError,
            failure: processFailure,
            complete: function () {
                $("#deleteConfirmModal").modal("hide");
            }
        });
    }
}

function setCustomerPicked(event, customerId, obj) {
    "use strict"
    console.log("setCustomerPicked( customerId: %s, obj: %s)", customerId, obj);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble = true);

    jQuery.ajax({
        type: "POST",
        url: "/picked/customer/" + customerId,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.groupCollapsed("setDemandUnpicked.success: customerId: " + customerId);
            console.dir(data);
            obj.title = "Удалить из избранных";
            obj.onclick = function (event) {
                setCustomerUnpicked(event, customerId, obj);
            };
            obj.innerHTML = "<i class=\"fa fa-star\"/>";
            console.groupEnd();
        },
        error: processError,
        failure: processFailure
    });
}

function setCustomerUnpicked(event, customerId, obj) {
    "use strict";
    console.log("setCustomerUnpicked( customerId: %s, obj: %s)", customerId, obj);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble = true);

    jQuery.ajax({
        type: "DELETE",
        url: "/picked/customer/" + customerId,
        success: function (data) {
            console.groupCollapsed("setCustomerUnpicked.success: customerId: " + customerId);
            console.dir(data);
            obj.title = "Добавить в избранные";
            obj.onclick = function (event) {
                setCustomerPicked(event, customerId, obj);
            };
            obj.innerHTML = "<i class=\"fa fa-star-o\"/>";
            console.groupEnd();
        },
        error: processError,
        failure: processFailure
    });
}

function addCustomerDepartment(customerId) {
    "use strict";
    console.log("addCustomerDepartment( customerId: %s)", customerId);
    location.href = "/company/" + customerId + "/department/-1";
}

function addCustomerPerson(customerId) {
    "use strict";
    console.log("addCustomerPerson( customerId: %s)", customerId);
    location.href = "/company/" + customerId + "/person/-1";
}

function addCustomerDemand(customerId) {
    "use strict";
    console.log("addCustomerDemand( customerId: %s)", customerId);
    location.href = "/company/" + customerId + "/demand/-1";
}