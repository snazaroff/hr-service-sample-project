var demand_note_form_fields = ["note"];

function addDemandNote() {
    "use strict"
    console.log("addDemandNote()");

    $("#new-demand-note").show();
    $("#btn-add-demand-note").attr("disabled", true);
}

function removeDemandNote() {
    "use strict"
    console.log("removeDemandNote()");
}

function deleteDemandNote(demandId, demandNoteId) {
    "use strict"
    console.log("deleteDemandNote( demandId: %s, demandNoteId: %s)", demandId, demandNoteId);

    if(confirm('Удалить комментарий?')) {
        doDeleteDemandNote(demandId, demandNoteId);
    }
}

function editDemandNote(demandNoteId) {
    "use strict"
    console.log("editDemandNote( demandNoteId: %s)", demandNoteId);

    let tr = $("#tr-demand-note-" + demandNoteId);
    let td;
    let span;
    let input;
    for(const field of demand_note_form_fields) {
        console.log("field: %s", field);
        td = tr.find("#td-demand-note-" + field + "-" + demandNoteId);
        span = tr.find("#span-demand-note-" + field + "-" + demandNoteId);
        input = $("<input type=\"text\"/>");
        input.val(span.text());
        input.appendTo(td);
        span.hide();
    }

    $("#btn-demand-note-edit-" + demandNoteId).hide();
    $("#btn-demand-note-post-" + demandNoteId).show();
    $("#btn-demand-note-delete-" + demandNoteId).hide();
    $("#btn-demand-note-cancel-" + demandNoteId).show();
}

function cancelDemandNote() {
    "use strict"
    console.log("cancelDemandNote()");

    $("#new-demand-note").hide();
    $("#btn-add-demand-note").attr("disabled", false);
    $("#new-demand-note-note").val("");
}

function cancelEditDemandNote(demandNoteId) {
    "use strict"
    console.log("cancelEditDemandNote( demandNoteId: %s)", demandNoteId);

    var tr = $("#tr-demand-note-" + demandNoteId);
    for(let field of demand_note_form_fields) {
        console.log("field: %s", field);
        tr.find("#span-demand-note-" + field + "-" + demandNoteId).show();
        tr.find("#td-demand-note-" + field + "-" + demandNoteId + " input").remove();
    }

    $("#btn-demand-note-edit-" + demandNoteId).show();
    $("#btn-demand-note-post-" + demandNoteId).hide();
    $("#btn-demand-note-delete-" + demandNoteId).show();
    $("#btn-demand-note-cancel-" + demandNoteId).hide();
}

function postDemandNote(demandId) {
    "use strict";
    var note = $("#new-demand-note #new-demand-note-note").val();

    if (note == null || note == "") {
        alert('Комментарий должен быть заполнен')
    } else {
        console.log("postDemandNote(demandId: %s, note: %s)", demandId, note);
        doAddDemandNote(demandId, note);
    }
}

function postUpdateDemandNote(demandId, demandNoteId) {
    "use strict";
    let tr = $("#tr-demand-note-" + demandNoteId);
    let note = tr.find("#td-demand-note-note" + "-" + demandNoteId + " input").val();

    console.log("postUpdateDemandNote( demandId: %s, demandNoteId: %s, note: %s)", demandId, demandNoteId, note);
    doUpdateDemandNote(demandId, demandNoteId, note);
}

function doAddDemandNote(demandId, note) {
    "use strict"
    console.log("doAddDemandNote( demandId: %s, note: %s)", demandId, note);

    let demandNote = {
        demand: {
            demand_id: demandId
        },
        note: note
    }
    console.log("demandNote: " + JSON.stringify(demandNote));

    jQuery.ajax({
        type: "POST",
        url: "/demand/" + demandId + "/note/",
        data: JSON.stringify(demandNote),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data){
            console.groupCollapsed("doAddDemandNote.success:( demandId: " + demandId + ", note: " + note + ")");
            data.demand_id = demandId;
            console.dir(data);

            var formatedHtml = demandNoteTemplate(data);

            console.log(formatedHtml);

            $(formatedHtml).appendTo($("#tbl-demand-note"));
            $("#new-demand-note").hide();
            $("#new-demand-note #new-demand-note-note").val("");
            incCommentCount();
            console.groupEnd();
        },
        error: processError,
        failure: processFailure,
        complete: function( jqXHR, textStatus) {
            $("#btn-add-demand-note").attr("disabled", false);
        }
    });
}

function doUpdateDemandNote(demandId, demandNoteId, note) {
    "use strict"
    console.log("doUpdateDemandNote( demandNoteId: %s, note: %s)", demandNoteId, note);

    var demandNote = {
        demand_note_id: demandNoteId,
        demand: {
            demand_id: demandId
        },
        note: note
    }
    console.log("demandNote: " + JSON.stringify(demandNote));

    jQuery.ajax({
        type: "PUT",
        url: "/demand/" + demandId + "/note/",
        data: JSON.stringify(demandNote),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data){
            console.groupCollapsed("doUpdateDemandNote.success:( demandNoteId: " + demandNoteId + ", note: " + note + ")");
            console.dir(data);

            var tr = $("#tr-demand-note-" + demandNoteId);
            for(const field of demand_note_form_fields) {
                console.log("field: %s, data[field]: %s, typeof(data[field]): %s", field, data[field], typeof(data[field]));
                let span = tr.find("#span-demand-note-" + field + "-" + demandNoteId);
                span.text((data[field] === null)?"":data[field]);
                span.show();
                tr.find("#td-demand-note-" + field + "-" + demandNoteId + " input").remove();
            }

            $("#btn-demand-note-edit-" + demandNoteId).attr("onclick", "editDemandNote(" + data.demand_note_id + ");")

            $("#btn-demand-note-edit-" + demandNoteId).show();
            $("#btn-demand-note-post-" + demandNoteId).hide();
            $("#btn-demand-note-delete-" + demandNoteId).show();
            $("#btn-demand-note-cancel-" + demandNoteId).hide();
            console.groupEnd();
        },
        error: processError,
        failure: processFailure
    });
}

function doDeleteDemandNote(demandId, demandNoteId) {
    "use strict"
    console.log("doDeleteDemandNote( demandId: %s, demandNoteId: %s)", demandId, demandNoteId);

    jQuery.ajax({
        type: "DELETE",
        url: "/demand/" + demandId + "/note/" + demandNoteId,
        success: function(data){
            console.log("doDeleteDemandNote.success: demandId: %s, demandNoteId: %s, result: %s", demandId, demandNoteId, data);

            $("#tr-demand-note-" + demandNoteId).remove();
            decCommentCount();
        },
        error: processError,
        failure: processFailure
    });
}

function incCommentCount() {
    "use strict"
    console.log("incCommentCount()");
    incCounter("comment_count");
}

function decCommentCount() {
    "use strict"
    decCounter("comment_count");
}