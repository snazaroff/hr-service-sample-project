class Paths {

    getPaths() {
        try {
            return JSON.parse(sessionStorage.getItem("paths")) || [];
        } catch (e) {
            console.error(e);
            return [];
        }
    }

    setPaths(paths) {
        console.log("setPaths('%s')", JSON.stringify(paths));
        sessionStorage.setItem("paths", JSON.stringify(paths))
    }

    getLast() {
        "use strict";
        var path = "/";

        var paths = this.getPaths();
        if (paths && typeof (paths) === "object" && paths.length && paths.length > 0) {
            path = paths.pop();
            this.setPaths(paths);
        }
        return path;
    }

    setLast(value) {
        "use strict";
        console.log("setLast('%s')", value);

        var paths = this.getPaths();
        if (paths[paths.length - 1] !== value) {
            paths.push(value);
            this.setPaths(paths);
        }
    }

    clear() {
        "use strict";
        sessionStorage.removeItem("paths");
    }
}