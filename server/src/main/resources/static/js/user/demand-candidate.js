function addDemandCandidate(demandId, candidateId) {
    "use strict"
    console.log("addDemandCandidate( demandId: %s, candidateId: %s)", demandId, candidateId);

    let demandCandidate = {
        demand: {demand_id: demandId},
        candidate: {candidate_id: candidateId}
    };

    console.log(JSON.stringify(demandCandidate));

    jQuery.ajax({
        type: "POST",
        url: "/demand/" + demandId + "/candidate",
        data: JSON.stringify(demandCandidate),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.groupCollapsed("addDemandCandidate.success:");
            console.dir(data);

            let formatedHtml = demandCandidateTemplate(data);
            console.log(formatedHtml);

            $(formatedHtml).appendTo($("#tbl-demand-candidate"));
//            $("#btn-add-language").attr("disabled", false);
            incCounter("candidate_count")
            console.groupEnd();
            clearCandidateSearchForm();
        },
        error: processError,
        failure: processFailure,
        complete: function (jqXHR, textStatus) {
            $("#candidateChoicePanel").modal("hide");
        }
    });
}

function deleteDemandCandidate(demandId, demandCandidateId, event) {
    "use strict"
    console.log("deleteDemandCandidate(demandId: %s, demandCandidateId: %s)", demandId, demandCandidateId);

    event = event || window.event;
    event.stopPropagation ? event.stopPropagation() : (event.cancelBubble = true);

    jQuery.ajax({
        type: "DELETE",
        url: "/demand/" + demandId + "/candidate/" + demandCandidateId,
        success: function (data) {
            console.groupCollapsed("deleteDemandCandidate.success: demandCandidateId: " + demandCandidateId);
            console.dir(data);
            decCounter("candidate_count")
            $("#tr-demand-candidate-" + demandCandidateId).remove();
            console.groupEnd();
        },
        error: processError,
        failure: processFailure
    });
}