package com.pkit.file_store.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.file_store.TikaParser;
import com.pkit.file_store.parser.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;

import java.io.*;
import java.util.*;

@Slf4j
public class ConvertService {

    private final ObjectMapper om = new ObjectMapper();

    public Map<String, Object> importResume(byte[] file, String fileName) throws IOException, TikaException, SAXException {
        log.debug("importResumeRtf( file.length: {}, fileName: {})", file.length, fileName);

        TikaParser tikaSample = new TikaParser();
        ResumeRawData resumeRawData = tikaSample.parse(file);
        List<String> nodes = resumeRawData.getData();
        List<byte[]> images = resumeRawData.getImages();
        log.debug("resumeRawData.getResumeType(): {}", resumeRawData.getResumeType().name());

        log.debug("images: {}", images.size());

        try (OutputStream os = new FileOutputStream(new File(fileName + ".lines.txt"))) {
            for (String node : nodes) {
                os.write(node.getBytes());
                os.write('\n');
            }
            os.flush();
        }

        Map<String, Object> candidate = null;

        if (resumeRawData.getResumeType().equals(ResumeType.HH_1) || resumeRawData.getResumeType().equals(ResumeType.HH_3)) {
            HeadHunterParse headHunterParse = new HeadHunterParse();

            candidate = headHunterParse.parse(nodes);
        } else if (resumeRawData.getResumeType().equals(ResumeType.HC)) {
            HumanCapitalParse humanCapitalParse = new HumanCapitalParse();

            candidate = humanCapitalParse.parse(nodes);
        } else if (resumeRawData.getResumeType().equals(ResumeType.HC)) {
            SimpleTxtParse simpleTxtParse = new SimpleTxtParse();

            candidate = simpleTxtParse.parse(nodes);
        }

//        try {
            log.debug("candidate: {}", om.writeValueAsString(candidate));
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }

        if (candidate != null) {
            if (images.size() > 0) {
                Base64.Encoder encoder = Base64.getEncoder();
                List<String> photos = new ArrayList<>();
                candidate.put("photos", photos);
                log.debug("photos: {}", photos);
                for (byte[] image : resumeRawData.getImages()) {
                    photos.add(encoder.encodeToString(image));
                }
            }
        }
        return candidate;
    }
}