package com.pkit.file_store;

import com.pkit.file_store.parser.ResumeRawData;
import com.pkit.file_store.parser.ResumeType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.tika.exception.TikaException;
import org.apache.tika.extractor.EmbeddedDocumentExtractor;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class TikaParser {

    private final Map<ResumeType, byte[]> icons = new HashMap<>();

    public TikaParser() {

        String[] iconNames = {"hc.png", "hh_1.png", "hh_2.png"};

        ClassLoader classLoader = getClass().getClassLoader();

        for (ResumeType rt : ResumeType.values()) {
            try (InputStream inputStream = classLoader.getResourceAsStream("icons/" + rt.name().toLowerCase() + ".png")) {
                if (inputStream != null) {
                    icons.put(rt, IOUtils.toByteArray(inputStream));
                }
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
    }

    public ResumeRawData parse(byte[] file) throws TikaException, SAXException, IOException {

        ResumeRawData resumeRawData = new ResumeRawData();

        Parser parser = new AutoDetectParser();

        ContentHandler handler = new ParseContentHandler();

        Metadata metadata = new Metadata();
        ParseContext context = new ParseContext();

        EmbeddedDocumentExtractor embeddedDocumentExtractor =
                new EmbeddedDocumentExtractor() {
                    @Override
                    public boolean shouldParseEmbedded(Metadata metadata) {
                        return true;
                    }

                    @Override
                    public void parseEmbedded(InputStream stream, ContentHandler handler, Metadata metadata, boolean outputHtml)
                            throws IOException {

                        log.debug("metadata: {}", metadata);

                        String contentType = metadata.get("Content-Type");

                        byte[] image = IOUtils.toByteArray(stream);
                        log.debug("image: {}", image.length);

                        for (Map.Entry<ResumeType, byte[]> icon : icons.entrySet()) {
                            if (Arrays.equals(icon.getValue(), image)) {
                                resumeRawData.setResumeType(icon.getKey());
                            }
                        }

                        if ("image/wmf".equals(contentType)) {

//                            log.debug("ImageIO.getReaderFormatNames(): {}", Arrays.asList(ImageIO.getReaderFormatNames()));
//                            log.debug("ImageIO.getReaderFileSuffixes(): {}", Arrays.asList(ImageIO.getReaderFileSuffixes()));
//                            log.debug("ImageIO.getReaderMIMETypes(): {}", Arrays.asList(ImageIO.getReaderMIMETypes()));
//
//                            ImageInputStream imageInput = ImageIO.createImageInputStream(new ByteArrayInputStream(image));
//
//                            Iterator<ImageReader> it = ImageIO.getImageReadersByFormatName("WMF");
//
////                            Iterator<ImageReader> it = ImageIO.getImageReaders(imageInput);
//                            while (it.hasNext()) {
//                                ImageReader ir = it.next();
//                                log.debug("ir: {}", ir);
//                                String imageFormatName = ir.getFormatName().toUpperCase();
//                                log.debug("imageFormatName: {}", imageFormatName);
//                                ir.setInput(imageInput, true);
//
//                                log.debug("ir.getMinIndex(): {}", ir.getMinIndex());
//
//
////                                ir.setInput(new ByteArrayInputStream(image));
//                                log.debug("num: {}", ir.getNumImages(false));
//                                log.debug("ir.getHeight(): {}", ir.getHeight(0));
//                                log.debug("ir.getWidth(): {}", ir.getWidth(0));
//
//                                BufferedImage img_ = ir.read(0);
//                                log.debug("img_: {}", img_);
//                            }
//                            BufferedImage img = ImageIO.read(ImageIO.createImageInputStream(new ByteArrayInputStream(image)));
//
////                            WMFImageReader reader = new WMFImageReader(new WMFImageReaderSpi());
////                            reader.setInput(ImageIO.createImageInputStream(new ByteArrayInputStream(image)));
////
////                            log.debug("num: {}", reader.getNumImages(false));
////
////                            BufferedImage img = reader.read(1, reader.getDefaultReadParam());
//                            log.debug("img: {}", img);

                        } else {

                            BufferedImage img = ImageIO.read(new ByteArrayInputStream(image));
                            log.debug("img: {}", img);
                            log.debug("img.getHeight(): {}", img.getHeight());
                            if (img.getHeight() > 100) {

//                                Path imagesDir = new File("images/").toPath();
//                                if (!Files.exists(imagesDir)) {
//                                    Files.createDirectory(imagesDir);
//                                }
//
//                                Path outputPath = new File("images/" + System.currentTimeMillis() + "_" + metadata.get(Metadata.RESOURCE_NAME_KEY)).toPath();
//                                Files.copy(new ByteArrayInputStream(image), outputPath);

                                resumeRawData.getImages().add(image);
                            }
                        }

//                        BufferedImage img = com.twelvemonkeys.imageio.spi.Imastream.util.IIOUtil.ImageIO.read(new ByteArrayInputStream(image));
//                        log.debug("img: {}", img);
//                        if (img.getHeight() > 100)
//                            images.add(image);

                    }
                };

        context.set(EmbeddedDocumentExtractor.class, embeddedDocumentExtractor);
        context.set(Parser.class, parser);

        parser.parse(new ByteArrayInputStream(file), handler, metadata, context);

        log.debug("metadata: {}", metadata);
        log.debug("metadata: {}", metadata.size());
        log.debug("metadata: {}", Arrays.asList(metadata.names()));

        resumeRawData.setData(((ParseContentHandler) handler).result);
        return resumeRawData;
    }

    static class ParseContentHandler extends DefaultHandler {

        private final List<String> result = new ArrayList();
        private StringBuilder tmpSb;

        public ParseContentHandler() {
            super();
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            super.startElement(uri, localName, qName, attributes);

            if ("p".equals(qName)) {
                tmpSb = new StringBuilder();
            } else if ("b".equals(qName) && tmpSb != null) {
                tmpSb.append('\n');
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            super.endElement(uri, localName, qName);

            if ("p".equals(qName)) {
                String pBody = tmpSb.toString().replaceAll(" ", " ");
                if (pBody != null)
                    result.addAll(Arrays.stream(pBody.split("\n")).filter(s -> s != null && s.trim().length() > 0).collect(Collectors.toList()));
                tmpSb = null;
            } else if ("b".equals(qName) && tmpSb != null) {
                tmpSb.append('\n');
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) {

            if (tmpSb != null)
                tmpSb.append(new String(ch, start, length));
        }

        private List<String> getResult() {
            return result;
        }
    }
}