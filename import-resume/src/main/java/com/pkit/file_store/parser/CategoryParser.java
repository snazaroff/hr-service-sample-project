package com.pkit.file_store.parser;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
public class CategoryParser {

    public CategoryParser() {
    }

    public int process(List<String> lines, int currentLineNumber, Map<String, Object> candidate) {
        log.debug("process: {}", currentLineNumber);

        List<String> categories = new ArrayList<>();

        int lineCount = lines.size();

        String line = getLine(lines, currentLineNumber);

        while (!isLastLine(line) && currentLineNumber < lineCount) {
            //Ищем категории
            if (line.startsWith("• "))
                line = line.substring(2);
            log.debug("category: {}", line);
            categories.add(line);
            line = getLine(lines, ++currentLineNumber);
        }
        candidate.put("categories", categories);
        return currentLineNumber;
    }

    private boolean isLastLine(String line) {
        log.debug("isLastLine({}): {}", line, line.contains("Занятость:"));
        return line.contains("Занятость:");
    }

    private String getLine(List<String> lines, int currentLineNumber) {
        return lines.get(currentLineNumber).replaceAll(" ", " "); //Убираем левый символ, котрый возникает при преобразовании файла
    }
}