package com.pkit.file_store.parser;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Slf4j
public class SkillsParser {

    private final String[] LAST_STRING = {"Дополнительная информация", "Опыт вождения"};

    private final List<String> lines;
    private int currentLineNumber;
    private final Map<String, Object> candidate;


    public SkillsParser(List<String> lines, int currentLineNumber, Map<String, Object> candidate) {
        this.lines = lines;
        this.currentLineNumber = currentLineNumber;
        this.candidate = candidate;
    }

    public int process() {
        log.debug("process: {}", currentLineNumber);

        int lineCount = lines.size();
        currentLineNumber++;//Пропускаем "Опыт работы"
        while (currentLineNumber < lineCount) {
            String line = getLine(currentLineNumber);
            log.debug("line: {}", line);
            if ("Знание языков".equals(line)) {
                LanguageParser languageParser = new LanguageParser(lines, currentLineNumber, candidate);
                currentLineNumber = languageParser.process();
            } else {
                log.debug("skill: {}", line);
                currentLineNumber++;
            }
            if (isLastLine(line)) {
                return --currentLineNumber;
            }
        }
        return currentLineNumber;
    }

    private boolean isLastLine(String line) {
        log.debug("isLastLine({}): {}: {}", line, (Arrays.binarySearch(LAST_STRING, line) >= 0), LAST_STRING);
        return Arrays.binarySearch(LAST_STRING, line) >= 0;
    }

    private String getLine(int currentLineNumber) {
        return lines.get(currentLineNumber).replaceAll(" ", " "); //Убираем левый символ, котрый возникает при преобразовании файла
    }
}