package com.pkit.file_store.parser;

import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public abstract class Parser {

    abstract public int process(List<String> lines, int currentLineNumber, Map<String, Object> candidate);

    abstract public String[] getLastStrings();

    protected boolean isLastLine(String line) {
//        log.debug("isLastLine({}): {}: {}", line, (Arrays.binarySearch(getLastStrings(), line) >= 0), getLastStrings());
        return Arrays.binarySearch(getLastStrings(), line) >= 0;
    }

    protected String getLine(List<String> lines, int currentLineNumber) {
        return lines.get(currentLineNumber).replaceAll(" ", " "); //Убираем левый символ, котрый возникает при преобразовании файла
    }
}