package com.pkit.file_store.parser;

import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.*;

@Slf4j
public class HeadHunterParse {

    private final CategoryParser categoryParser = new CategoryParser();

    public HeadHunterParse() { }

    public Map<String, Object> parse(List<String> lines) {
        log.debug("parse(lines.size: {})", lines.size());

        Map<String, Object> candidate = new HashMap<>();
        candidate.put("create_date", UtilsParser.formatSimpleDateTime(LocalDateTime.now()));

        int lineCount = lines.size();
        int currentLineNumber = 0;
        while (currentLineNumber < lineCount) {
            String line = lines.get(currentLineNumber);
            log.debug("line: {}", line);
            if (line.contains("Резюме обновлено")) {

                int pos = line.lastIndexOf("Резюме обновлено");
                log.debug("Last refresh: {}", line.substring(pos));
                currentLineNumber++;
                line = lines.get(currentLineNumber);
                if (line.contains("Резюме обновлено")) {
                    currentLineNumber++;
                    line = lines.get(currentLineNumber);
                }

                String[] fio = line.split(" ");
                log.debug("fio: {}", Arrays.asList(fio));
                if (fio.length > 0) candidate.put("last_name", fio[0]);
                if (fio.length > 1) candidate.put("first_name", fio[1]);
                if (fio.length > 2) candidate.put("middle_name", fio[2]);
                currentLineNumber++;
                line = lines.get(currentLineNumber);
            }
            if (line.contains("Женщина") || line.contains("Мужчина")) {
                if (line.contains("Мужчина")) candidate.put("sex", "Мужчина");
                else if (line.contains("Женщина")) candidate.put("sex", "Женщина");
                String[] parts = line.split(" ");
                log.debug("parts: {}", Arrays.asList(parts));
                log.debug("parts: {}", parts.length);
                if (parts.length >= 3) {
                    String birthDate = parts[parts.length - 3] + " " + parts[parts.length - 2] + " " + parts[parts.length - 1];
                    candidate.put("birth_date", UtilsParser.formatSimpleDate(UtilsParser.parseDateFullMonthDate(birthDate)));
                }
                currentLineNumber++;
                PhoneParser phoneParser = new PhoneParser(lines, currentLineNumber, candidate);
                currentLineNumber = phoneParser.process();

            } else if (line.contains("Проживает:")) {
                candidate.put("address", parseAddress(line));
                candidate.put("metro", parseMetro(line));
                currentLineNumber++;
            } else if (line.contains("Гражданство:")) {
                currentLineNumber++;
            } else if (line.contains("готов к переезду")) {
                currentLineNumber++;
            } else if (line.contains("Желаемая должность и зарплата")) {
                currentLineNumber++;
                line = lines.get(currentLineNumber);
                candidate.put("position", line);
                currentLineNumber++;
                currentLineNumber = categoryParser.process(lines, currentLineNumber, candidate);
            } else if (line.contains("Занятость:")) {
                currentLineNumber++;
            } else if (line.contains("График работы:")) {
                currentLineNumber++;
            } else if (line.contains("Желательное время в пути до работы:")) {
                SalaryParser salaryParser = new SalaryParser(lines, currentLineNumber, candidate);
                currentLineNumber = salaryParser.process();
            } else if (line.contains("Опыт работы")) {
                Parser employmentParser = new EmploymentParser();
                currentLineNumber = employmentParser.process(lines, currentLineNumber, candidate);
                log.debug("candidate: {}", candidate);
                currentLineNumber++;
            } else if (line.contains("Образование")) {
                Parser educationParser = new EducationParser();
                currentLineNumber = educationParser.process(lines, currentLineNumber, candidate);
                log.debug("candidate: {}", candidate);
            } else if (line.contains("Тесты, экзамены")) {
                Parser courseParser = new CourseParser();
                currentLineNumber = courseParser.process(lines, currentLineNumber, candidate);
                currentLineNumber++;
            } else if (line.contains("Ключевые навыки")) {
                SkillsParser skillsParser = new SkillsParser(lines, currentLineNumber, candidate);
                currentLineNumber = skillsParser.process();
                currentLineNumber++;
            } else if (line.contains("Знание языков")) {
                currentLineNumber++;
            } else if (line.contains("Опыт вождения")) {
                currentLineNumber++;
            } else if (line.contains("Дополнительная информация")) {
                currentLineNumber++;
            } else if (line.contains("Рекомендации")) {
                currentLineNumber++;
            } else if (line.contains("Навыки")) {
                currentLineNumber++;
            } else if (line.contains("Обо мне")) {
                AboutParser skillsParser = new AboutParser(lines, currentLineNumber, candidate);
                currentLineNumber = skillsParser.process();
                currentLineNumber++;
            } else {
                currentLineNumber++;
            }
        }
        return candidate;
    }

    private String parseAddress(String line) {
        line = line.replace("Проживает:", "").trim();
        String[] parts = line.split(",");
        return parts[0];
    }

    private String parseMetro(String line) {
        log.debug("parseMetro( line: {})", line);

        line = line.replace("Проживает:", "").trim();
        String[] parts = line.split(",");
        if (parts.length > 1) {
            log.debug(parts[1].substring(4));
            return parts[1].substring(4);
        } else
            return null;
    }
}