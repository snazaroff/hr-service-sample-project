package com.pkit.file_store.parser;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.ToString;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@ToString
public enum ResumeType {

    HH_1,
    HH_2,
    HH_3,
    HC,
    NOT_DEFINED
}