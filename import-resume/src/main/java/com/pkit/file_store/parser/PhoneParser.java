package com.pkit.file_store.parser;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

@Slf4j
public class PhoneParser {

    private final String LAST_STRING = "Проживает:";

    private final List<String> lines;
    private int currentLineNumber;
    private final Map<String, Object> candidate;

    public PhoneParser(List<String> lines, int currentLineNumber, Map<String, Object> candidate) {
        this.lines = lines;
        this.currentLineNumber = currentLineNumber;
        this.candidate = candidate;
    }

    public int process() {
        log.debug("process: {}", currentLineNumber);

        int lineCount = lines.size();
//        currentLineNumber++;//Пропускаем "Опыт работы"
        while (currentLineNumber < lineCount) {
            String line = getLine(currentLineNumber);
            log.debug("line: {}", line);

            if (isLastLine(line)) {
                return --currentLineNumber;
            }

            if (line.contains("+7")) {
                candidate.put("phone", line.split(" — ")[0]);
            } else if (line.contains("@")) {
                candidate.put("email", line.split(" — ")[0]);
            }
            currentLineNumber++;
        }
        return currentLineNumber;
    }

    protected boolean isLastLine(String line) {
        return line.startsWith(LAST_STRING);
    }

    private String getLine(int currentLineNumber) {
        return lines.get(currentLineNumber).replaceAll(" ", " "); //Убираем левый символ, котрый возникает при преобразовании файла
    }
}