package com.pkit.file_store.parser;

import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.pkit.file_store.parser.UtilsParser.toDateArray;

@Slf4j
public class AboutParser {

    private final String[] LAST_STRING = {"Образование"};

    boolean newItem = false;
    StringBuilder sb = null;

    private final List<String> lines;
    private int currentLineNumber;
    private final Map<String, Object> candidate;

    public AboutParser(List<String> lines, int currentLineNumber, Map<String, Object> candidate) {
        this.lines = lines;
        this.currentLineNumber = currentLineNumber;
        this.candidate = candidate;
    }

    public int process() {
        log.debug("process: {}", currentLineNumber);
        int lineCount = lines.size();
        currentLineNumber++;//Пропускаем "Опыт работы"
        while (currentLineNumber < lineCount) {
            String line = getLine(currentLineNumber);
            log.debug("line: {}", line);
            if (isLastLine(line)) {
                saveEmployment();
                return --currentLineNumber;
            }

            LocalDate[] dates = toDateArray(line);
            LocalDate startDate = dates[0];
            LocalDate endDate = dates[0];
            newItem = startDate != null;
            if (newItem) {
            }
            currentLineNumber++;
        }
        return currentLineNumber;
    }

    private void saveEmployment() {
    }

    private boolean isLastLine(String line) {
        log.debug("isLastLine({}): {}: {}", line, (Arrays.binarySearch(LAST_STRING, line) >= 0), LAST_STRING);
        return Arrays.binarySearch(LAST_STRING, line) >= 0;
    }

    private String getLine(int currentLineNumber) {
        return lines.get(currentLineNumber).replaceAll(" ", " "); //Убираем левый символ, котрый возникает при преобразовании файла
    }
}