package com.pkit.file_store.parser;

import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class LanguageParser {

    private final String[] LAST_STRING = {"Дополнительная информация", "Навыки"};

    private final List<Map<String, String>> languages = new ArrayList<>();

    private final List<String> lines;
    private int currentLineNumber;
    private final Map<String, Object> candidate;

    public LanguageParser(List<String> lines, int currentLineNumber, Map<String, Object> candidate) {
        this.lines = lines;
        this.currentLineNumber = currentLineNumber;
        this.candidate = candidate;
    }

    public int process() {
        log.debug("process: {}", currentLineNumber);

        int lineCount = lines.size();
        currentLineNumber++;//Пропускаем "Знание языков"
        while (currentLineNumber < lineCount) {
            String line = getLine(currentLineNumber);
            log.debug("line: {}", line);

            if (isLastLine(line)) {
                --currentLineNumber;
//                candidate.put("languages", languages);
                break;
            }

            String[] parts = line.split("—");
            if (parts.length == 1) {
                currentLineNumber++;
                line += getLine(currentLineNumber);
                parts = line.split("—");
            }
            log.debug("parts.length: {}, parts: {}", parts.length, parts);
            Map<String, String> language = new HashMap<>();

            language.put("name", parts[0].trim());
            if (parts.length > 1) {
                language.put("status", parts[1].trim());
            }
            languages.add(language);

            currentLineNumber++;
        }
        log.debug("end");
        candidate.put("languages", languages);
        return currentLineNumber;
    }

    private boolean isLastLine(String line) {
        log.debug("isLastLine({}): {}", line, (Arrays.binarySearch(LAST_STRING, line) >= 0));
        return Arrays.binarySearch(LAST_STRING, line) >= 0;
    }

    private String getLine(int currentLineNumber) {
        return lines.get(currentLineNumber).replaceAll(" ", " "); //Убираем левый символ, котрый возникает при преобразовании файла
    }
}