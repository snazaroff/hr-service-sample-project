package com.pkit.file_store.parser;

import com.pkit.file_store.model.Currency;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class SalaryParser {

    private final List<String> lines;
    private int currentLineNumber;
    private final Map<String, Object> candidate;

    public SalaryParser(List<String> lines, int currentLineNumber, Map<String, Object> candidate) {
        this.lines = lines;
        this.currentLineNumber = currentLineNumber;
        this.candidate = candidate;
    }

    public int process() {
        log.debug("process: {}", currentLineNumber);

        currentLineNumber++;
        String line = lines.get(currentLineNumber);
        currentLineNumber++;
        String currency = lines.get(currentLineNumber);
        log.debug("Salary: {} {}", line, currency);
        try {
            Map<String, Object> salary = new HashMap<>();
            salary.put("value", Long.parseLong(line.replace(" ", "")));
            salary.put("currency", Currency.getByName(currency).getName());
            log.debug("salary: {}", salary);
            candidate.put("salary", salary);
        } catch (Exception e) {
            log.error("Not salary: {}", e.getMessage());
            return currentLineNumber - 1;
        }
        currentLineNumber++;
        return currentLineNumber;
    }
}