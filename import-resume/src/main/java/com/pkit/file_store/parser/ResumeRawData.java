package com.pkit.file_store.parser;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class ResumeRawData {
    private List<String> data;
    private List<byte[]> images = new ArrayList<>();
    private ResumeType resumeType;
}