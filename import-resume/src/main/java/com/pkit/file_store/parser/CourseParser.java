package com.pkit.file_store.parser;

import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class CourseParser extends Parser {

    private final String[] LAST_STRING = {"Ключевые навыки"};

    public int process(List<String> lines, int currentLineNumber, final Map<String, Object> candidate) {
        log.debug("process: {}", currentLineNumber);

        final List<Map<String, Object>> educations = new ArrayList<>();

        String educationType = "Курсы";
        int lineCount = lines.size();
        currentLineNumber++;//Пропускаем "Тесты, экзамены"
        Map<String, Object> education = null;
        List<String> description = new ArrayList<>();
        while (currentLineNumber < lineCount) {
            String line = getLine(lines, currentLineNumber);
            log.debug("line: {}", line);

            if (isLastLine(line)) {
                break;
            }

            if (UtilsParser.isYear(line)) {
                if (education != null) {
                    educations.add(saveEducation(education, description));
                }
                education = new HashMap<>();
                education.put("education_type", educationType);
                log.debug("endDate: {}", line + "-01-01");
                education.put("end_date", line + "-01-01");
                description = new ArrayList<>();
            } else {
                log.debug("add( {})", line);
                description.add(line.trim());
            }
            currentLineNumber++;
        }
        if (education != null) {
            education = saveEducation(education, description);
            educations.add(education);
        }

        candidate.put("courses", educations);

        return --currentLineNumber;
    }

    @Override
    public String[] getLastStrings() {
        return LAST_STRING;
    }

    private Map<String, Object> saveEducation(Map<String, Object> education, List<String> description) {

        String educationName = String.join(" ", description.subList(0, description.size() - 1));
        String specialization = description.get(description.size() - 1);
        education.put("education_name", educationName);
        education.put("specialization", specialization);
        log.debug("description: {}", description);
        return education;
    }
}