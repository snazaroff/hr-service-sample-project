package com.pkit.file_store.parser;

import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.util.*;

import static com.pkit.file_store.parser.UtilsParser.isCity;
import static com.pkit.file_store.parser.UtilsParser.isCountry;
import static com.pkit.file_store.parser.UtilsParser.toDateArray;

@Slf4j
public class EmploymentParser extends Parser {

    private final String[] LAST_STRING = {"Образование"};

    public int process(List<String> lines, int currentLineNumber, Map<String, Object> candidate) {
        log.debug("process: {}", currentLineNumber);

        boolean newItem;
        StringBuilder sb = null;
        Map<String, String> employment = null;

        final List<Map<String, String>> employments = new ArrayList<>();

        int lineCount = lines.size();
        currentLineNumber++;//Пропускаем "Опыт работы"
        while (currentLineNumber < lineCount) {
            String line = getLine(lines, currentLineNumber);
            log.debug("line: {}", line);
            if (isLastLine(line)) {
                employment.put("description", sb.toString());
                employments.add(employment);
                currentLineNumber--;
                break;
            }

            LocalDate[] dates = toDateArray(line);
            LocalDate startDate = dates[0];
            LocalDate endDate = dates[1];
            newItem = startDate != null;
            if (newItem) {
                if (employment != null) {
                    //Сохраняем уже набранные данные
                    employment.put("description", sb.toString());
                    employments.add(employment);
                }
                employment = new HashMap<>();
                employment.put("start_date", UtilsParser.formatSimpleDate(startDate));
                if (endDate != null)
                    employment.put("end_date", UtilsParser.formatSimpleDate(endDate));
                currentLineNumber++;
                currentLineNumber++;
                line = getLine(lines, currentLineNumber);
                employment.put("firm_name", line);
                currentLineNumber++;

                if (isCity(getLine(lines, currentLineNumber)) || isCountry(getLine(lines, currentLineNumber))) {
                    line = getLine(lines, currentLineNumber);
                    employment.put("firm_name", employment.get("firm_name") + "(" + line + ")");
                    currentLineNumber++;
                }

                line = getLine(lines, currentLineNumber);
                employment.put("position", line);
                sb = new StringBuilder();
                currentLineNumber++;
                line = getLine(lines, currentLineNumber);
            }

            if (!isLastLine(line)) {
                log.debug("line: {}", line);
                sb.append(line).append('\n');
                currentLineNumber++;
            }
        }
        candidate.put("employments", employments);
        log.debug("candidate: {}", candidate);
        return currentLineNumber;
    }

    @Override
    public String[] getLastStrings() {
        return LAST_STRING;
    }
}