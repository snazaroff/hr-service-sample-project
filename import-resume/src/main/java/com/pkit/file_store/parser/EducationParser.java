package com.pkit.file_store.parser;

import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class EducationParser extends Parser {

    private final String[] LAST_STRING = {"Знание языков", "Ключевые навыки", "Повышение квалификации, курсы", "Тесты, экзамены"};

    public int process(List<String> lines, int currentLineNumber, Map<String, Object> candidate) {
        log.debug("process: {}", currentLineNumber);

        int lineCount = lines.size();

        List<Map<String, String>> educations = new ArrayList<>();

        Map<String, String> education = null;

        String educationType = null;

        currentLineNumber++;//Пропускаем "Образование"
        while (currentLineNumber < lineCount) {

            if (education == null) {
                education = new HashMap<>();
                education.put("education_type", educationType);
            }

            String line = getLine(lines, currentLineNumber);
            log.debug("line: 0: {}", line);

            if (!UtilsParser.isYear(line)) {

                log.debug("line: _: {}", line);

                educationType = line;

                if (educationType != null) {
                    candidate.put("degree", line);
                    education.put("education_type", educationType);
                    currentLineNumber++;
                    line = getLine(lines, currentLineNumber);
                }
            }

            if (UtilsParser.isYear(line)) {
                log.debug("endDate: {}", line + "-01-01");
                education.put("end_date", line + "-01-01");
                currentLineNumber++;
            }

            line = getLine(lines, currentLineNumber);
            log.debug("line: 2: {}", line);
            education.put("education_name", line);

            currentLineNumber++;

            if (!"Средняя школа".equals(educationType)) {
                //Есть специализация
                String specialization = getLine(lines, currentLineNumber);
                log.debug("specialization: {}", specialization);

                currentLineNumber++;
                line = getLine(lines, currentLineNumber++);

                log.debug("currentLineNumber: {}, lineCount: {}, line: {}", currentLineNumber, lineCount, line);

                while (currentLineNumber < lineCount && !(isEducationType(line) || UtilsParser.isYear(line) || isLastLine(line))) {
                    log.debug("line:3: {}", line);
                    specialization += " " + line;
                    line = getLine(lines, currentLineNumber++);
                }
                log.debug("specialization: {}", specialization);
                education.put("specialization", specialization);

                currentLineNumber--;
            }

            line = getLine(lines, currentLineNumber);

            if (isEducationType(line) || UtilsParser.isYear(line) || isLastLine(line)) {
                log.debug("add education");
                educations.add(education);
                education = null;
            }

            log.debug("education: {}", education);
            if (isLastLine(line)) {
                --currentLineNumber;
                break;
            }
            if (UtilsParser.isYear(line)) {
                --currentLineNumber;
            }
        }
        log.debug("educations: {}", educations);
        log.debug("educations: {}", educations.size());
        candidate.put("educations", educations);
        return currentLineNumber;
    }

    @Override
    public String[] getLastStrings() {
        return LAST_STRING;
    }

    private boolean isEducationType(String line) {
        return line.startsWith("Высшее") ||
                line.startsWith("Среднее специальное") ||
                line.startsWith("Среднее");
    }
}