package com.pkit.file_store.parser;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

@Slf4j
public class UtilsParser {

    //    private static final DateTimeFormatter yearFormatter = DateTimeFormatter.ofPattern("yyyy").withZone(ZoneId.of("Europe/Moscow"));
    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneId.of("Europe/Moscow"));
    private static final DateTimeFormatter dateFullMonthFormatter = DateTimeFormatter.ofPattern("d MMMM yyyy").withLocale(new Locale("ru")).withZone(ZoneId.of("Europe/Moscow"));
    private static final DateTimeFormatter fullMonthFormatter = DateTimeFormatter.ofPattern("d LLLL yyyy").withLocale(new Locale("ru")).withZone(ZoneId.of("Europe/Moscow"));

    private static final DateTimeFormatter dateFullFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss").withLocale(new Locale("ru")).withZone(ZoneId.of("Europe/Moscow"));

    private static final Set<String> cities = new HashSet<>();

    private static final Set<String> countries = new HashSet<>();

    static {
        cities.add("Москва");
        countries.add("Россия");
    }

//    public static boolean isDate(String line) {
//        log.debug("pos: 1: {}, 2: {}, ", line.indexOf(" "), line.lastIndexOf(" "), line.substring(0, line.lastIndexOf(" ")));
//        line = line.substring(0, line.lastIndexOf(" ")).toLowerCase();
//        try {
//            LocalDate.parse(line, fullMonthFormatter);
//            return true;
//        } catch (DateTimeParseException e) {
//            return false;
//        }
//    }

    static boolean isYear(String line) {
        log.debug("isYear( line: {})", line);
        return line != null && line.length() == 4 && StringUtils.isNumeric(line);
    }

    static LocalDate getYear(String line) {
        log.debug("getYear( line: {})", line);

        return LocalDate.from(dateFormatter.parse(line + "-01-01"));
    }

    static String formatSimpleDate(LocalDate data) {
        return data.format(dateFormatter);
    }

    static String formatSimpleDateTime(LocalDateTime data) {
//        return data.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        return data.format(dateFullFormatter);
    }

    static LocalDate parseSimpleDate(String line) {
        return LocalDate.parse(line, dateFormatter);
    }

//    public static LocalDate parseFullDate(String line) {
//        return LocalDate.parse(line, dateFormatter);
//    }

    public static LocalDate parseDateFullMonthDate(String line) {
        return LocalDate.from(dateFullMonthFormatter.parse(line));
//        return LocalDate.parse(line, fullMonthFormatter);
    }


    static LocalDate toStartDate(String line) {
        log.debug("toStartDate( line: '{}')", line);
        try {
            return LocalDate.from(fullMonthFormatter.parse("01 " + line.trim()));
        } catch (DateTimeParseException e) {
            return null;
        }
    }

    static LocalDate toEndDate(String line) {
        log.debug("toEndDate( line: {})", line);
        int pos = line.trim().indexOf(" ");
        try {
            return LocalDate.from(fullMonthFormatter.parse("01 " + line.trim().substring(0, pos + 5)));
        } catch (DateTimeParseException e) {
            return null;
        }
    }

    static LocalDate[] toDateArray(String line) {
        log.debug("toDateArray( line: '{}')", line);
        String[] parts = line.split("—");
        if (parts.length > 1)
            return new LocalDate[]{toStartDate(parts[0]), toEndDate(parts[1])};
        else
            return new LocalDate[]{null, null};
    }

    static boolean isCity(String line) {

        String[] parts = line.split(",");
        return parts.length != 0 && cities.contains(parts[0]);
    }

    static boolean isCountry(String line) {

        String[] parts = line.split(",");
        return parts.length != 0 && countries.contains(parts[0]);
    }
}