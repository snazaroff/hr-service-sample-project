package com.pkit.file_store.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Status {

    private Short statusId;
    private String name;

    public Status(Short statusId, String name) {
        this.statusId = statusId;
        this.name = name;
    }
}
