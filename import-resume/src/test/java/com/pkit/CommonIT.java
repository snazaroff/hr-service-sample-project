package com.pkit;

import com.pkit.file_store.parser.UtilsParser;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Slf4j
public class CommonIT {

    @Test
    public void test() {
        log.debug("test():");

//        String line = "25 June 1969";
        String line = "25 июня 1969";
        LocalDate date = UtilsParser.parseDateFullMonthDate(line);
        log.debug("date: {}", date);
    }

    @Test
    public void test2() {
        log.debug("test2():");

//        final DateTimeFormatter dateFullMonthFormatter = DateTimeFormatter.ofPattern("dd MMMM yyyy").withLocale(new Locale("ru")).withZone(ZoneId.of("Europe/Moscow"));
        final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneId.of("Europe/Moscow"));
        final DateTimeFormatter dateFullMonthFormatter = DateTimeFormatter.ofPattern("dd MMMM yyyy").withLocale(Locale.ENGLISH).withZone(ZoneId.of("Europe/Moscow"));
        final DateTimeFormatter dateFullMonthFormatterRu = DateTimeFormatter.ofPattern("d MMMM yyyy").withLocale(new Locale("ru")).withZone(ZoneId.of("Europe/Moscow"));

        log.debug(LocalDate.now().format(dateFullMonthFormatter));
        log.debug(LocalDate.of(1969, 6, 25).format(dateFullMonthFormatterRu));

        log.debug("1: {}", dateFullMonthFormatter.parse("25 June 1969"));
        log.debug("2: {}", dateFullMonthFormatterRu.parse("15 октября 1989"));

        final DateTimeFormatter yearFormatter = DateTimeFormatter.ofPattern("yyyy").withZone(ZoneId.of("Europe/Moscow"));
        log.debug("3: {}", LocalDate.from(dateFormatter.parse("1969-01-01")));

        Locale localeRu = new Locale.Builder().setLanguage("ru").setScript("Cyrl").build();
        final DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("LLLL").withLocale(localeRu).withZone(ZoneId.of("Europe/Moscow"));
//        final DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("MMMM yyyy").withLocale(Locale.ENGLISH).withZone(ZoneId.of("Europe/Moscow"));

        log.debug(LocalDate.of(1969, 6, 25).format(formatter2));

//        log.debug("4: {}", LocalDate.from(formatter2.parse("Сентябрь".toLowerCase())));
        log.debug("4: {}", LocalDate.from(formatter2.parse("Сентябрь")));
    }
}
