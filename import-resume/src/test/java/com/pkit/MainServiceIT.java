package com.pkit;

import com.pkit.file_store.service.ConvertService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Ignore;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Slf4j
public class MainServiceIT {

    ConvertService mainService = new ConvertService();

    @Test
    public void testMainServiceRun() {
        log.debug("testMainServiceRun():");
//        mainService.importResume();
    }

    @Test
    @Ignore
    public void test2() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMMM yyyy", new Locale("RU"));

        String str = "18 октября 1994";
        log.debug(sdf.format(new Date()));

        try {
            log.debug("str: {}", sdf.parse(str));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
