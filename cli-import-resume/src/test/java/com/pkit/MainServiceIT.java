package com.pkit;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CliImportResumeApplication.class)
@Slf4j
@WebAppConfiguration
public class MainServiceIT {

    @Autowired
    private CliImportResumeApplication clr;

    @Test
    public void testMainServiceRun() {
        log.debug("testMainServiceRun():");
        this.clr.run("-folder docs");
//        this.clr.run("-file docs/Назаров Сергей Евгеньевич.doc");
    }
}