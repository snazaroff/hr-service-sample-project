INSERT INTO category(category_id, name)
VALUES(0, 'Все профессиональные области');

INSERT INTO category(category_id, name, has_child)
VALUES(1, 'Информационные технологии, интернет, телеком', true);

INSERT INTO category(parent_id, name) VALUES(1, 'CRM системы');
INSERT INTO category(parent_id, name) VALUES(1, 'CTO, CIO, Директор по IT');
INSERT INTO category(parent_id, name) VALUES(1, 'Web инженер');
INSERT INTO category(parent_id, name) VALUES(1, 'Web мастер');
INSERT INTO category(parent_id, name) VALUES(1, 'Администратор баз данных');
INSERT INTO category(parent_id, name) VALUES(1, 'Аналитик');
INSERT INTO category(parent_id, name) VALUES(1, 'Арт-директор');
INSERT INTO category(parent_id, name) VALUES(1, 'Банковское ПО');
INSERT INTO category(parent_id, name) VALUES(1, 'Игровое ПО');
INSERT INTO category(parent_id, name) VALUES(1, 'Инженер');
INSERT INTO category(parent_id, name) VALUES(1, 'Интернет');
INSERT INTO category(parent_id, name) VALUES(1, 'Компьютерная безопасность');
INSERT INTO category(parent_id, name) VALUES(1, 'Консалтинг, Аутсорсинг');
INSERT INTO category(parent_id, name) VALUES(1, 'Контент');
INSERT INTO category(parent_id, name) VALUES(1, 'Маркетинг');
INSERT INTO category(parent_id, name) VALUES(1, 'Мультимедиа');
INSERT INTO category(parent_id, name) VALUES(1, 'Начальный уровень, Мало опыта');
INSERT INTO category(parent_id, name) VALUES(1, 'Оптимизация сайта (SEO)');
INSERT INTO category(parent_id, name) VALUES(1, 'Передача данных и доступ в интернет');
INSERT INTO category(parent_id, name) VALUES(1, 'Поддержка, Helpdesk');
INSERT INTO category(parent_id, name) VALUES(1, 'Программирование, Разработка');
INSERT INTO category(parent_id, name) VALUES(1, 'Продажи');
INSERT INTO category(parent_id, name) VALUES(1, 'Продюсер');
INSERT INTO category(parent_id, name) VALUES(1, 'Развитие бизнеса');
INSERT INTO category(parent_id, name) VALUES(1, 'Сетевые технологии');
INSERT INTO category(parent_id, name) VALUES(1, 'Системная интеграция');
INSERT INTO category(parent_id, name) VALUES(1, 'Системный администратор');
INSERT INTO category(parent_id, name) VALUES(1, 'Системы автоматизированного проектирования');
INSERT INTO category(parent_id, name) VALUES(1, 'Системы управления предприятием (ERP)');
INSERT INTO category(parent_id, name) VALUES(1, 'Сотовые, Беспроводные технологии');
INSERT INTO category(parent_id, name) VALUES(1, 'Стартапы');
INSERT INTO category(parent_id, name) VALUES(1, 'Телекоммуникации');
INSERT INTO category(parent_id, name) VALUES(1, 'Тестирование');
INSERT INTO category(parent_id, name) VALUES(1, 'Технический писатель');
INSERT INTO category(parent_id, name) VALUES(1, 'Управление проектами');
INSERT INTO category(parent_id, name) VALUES(1, 'Электронная коммерция');

INSERT INTO category(category_id, name, has_child)
VALUES(2, 'Бухгалтерия, управленческий учет, финансы предприятия', true);

INSERT INTO category(parent_id, name) VALUES(2, 'ACCA');
INSERT INTO category(parent_id, name) VALUES(2, 'CIPA');
INSERT INTO category(parent_id, name) VALUES(2, 'GAAP');
INSERT INTO category(parent_id, name) VALUES(2, 'Аудит');
INSERT INTO category(parent_id, name) VALUES(2, 'Бухгалтер');
INSERT INTO category(parent_id, name) VALUES(2, 'Бухгалтер-калькулятор');
INSERT INTO category(parent_id, name) VALUES(2, 'Бюджетирование и планирование');
INSERT INTO category(parent_id, name) VALUES(2, 'Валютный контроль');
INSERT INTO category(parent_id, name) VALUES(2, 'Казначейство');
INSERT INTO category(parent_id, name) VALUES(2, 'Кассир, Инкассатор');
INSERT INTO category(parent_id, name) VALUES(2, 'Кредитный контроль');
INSERT INTO category(parent_id, name) VALUES(2, 'МСФО, IFRS');
INSERT INTO category(parent_id, name) VALUES(2, 'Налоги');
INSERT INTO category(parent_id, name) VALUES(2, 'Начальный уровень, Мало опыта');
INSERT INTO category(parent_id, name) VALUES(2, 'Основные средства');
INSERT INTO category(parent_id, name) VALUES(2, 'Оффшоры');
INSERT INTO category(parent_id, name) VALUES(2, 'Первичная документация');
INSERT INTO category(parent_id, name) VALUES(2, 'Планово-экономическое управление');
INSERT INTO category(parent_id, name) VALUES(2, 'Расчет себестоимости');
INSERT INTO category(parent_id, name) VALUES(2, 'Руководство бухгалтерией');
INSERT INTO category(parent_id, name) VALUES(2, 'ТМЦ');
INSERT INTO category(parent_id, name) VALUES(2, 'Учет заработной платы');
INSERT INTO category(parent_id, name) VALUES(2, 'Учет счетов и платежей');
INSERT INTO category(parent_id, name) VALUES(2, 'Финансовый анализ');
INSERT INTO category(parent_id, name) VALUES(2, 'Финансовый контроль');
INSERT INTO category(parent_id, name) VALUES(2, 'Финансовый менеджмент');
INSERT INTO category(parent_id, name) VALUES(2, 'Ценные бумаги');
INSERT INTO category(parent_id, name) VALUES(2, 'Экономист');

INSERT INTO category(category_id, name, has_child)
VALUES(3, 'Маркетинг, реклама, PR', true);

INSERT INTO category(parent_id, name) VALUES(3, 'Below The Line (BTL)');
INSERT INTO category(parent_id, name) VALUES(3, 'PR, Маркетинговые коммуникации');
INSERT INTO category(parent_id, name) VALUES(3, 'Аналитик');
INSERT INTO category(parent_id, name) VALUES(3, 'Арт директор');
INSERT INTO category(parent_id, name) VALUES(3, 'Ассистент');
INSERT INTO category(parent_id, name) VALUES(3, 'Бренд-менеджмент');
INSERT INTO category(parent_id, name) VALUES(3, 'Верстальщик');
INSERT INTO category(parent_id, name) VALUES(3, 'Дизайнер');
INSERT INTO category(parent_id, name) VALUES(3, 'Интернет-маркетинг');
INSERT INTO category(parent_id, name) VALUES(3, 'Исследования рынка');
INSERT INTO category(parent_id, name) VALUES(3, 'Консультант');
INSERT INTO category(parent_id, name) VALUES(3, 'Копирайтер');
INSERT INTO category(parent_id, name) VALUES(3, 'Менеджер по работе с клиентами');
INSERT INTO category(parent_id, name) VALUES(3, 'Менеджмент продукта (Product manager)');
INSERT INTO category(parent_id, name) VALUES(3, 'Мерчендайзинг');
INSERT INTO category(parent_id, name) VALUES(3, 'Наружная реклама');
INSERT INTO category(parent_id, name) VALUES(3, 'Начальный уровень/Мало опыта');
INSERT INTO category(parent_id, name) VALUES(3, 'Печатная реклама');
INSERT INTO category(parent_id, name) VALUES(3, 'Планирование, Размещение рекламы');
INSERT INTO category(parent_id, name) VALUES(3, 'Политический PR');
INSERT INTO category(parent_id, name) VALUES(3, 'Проведение опросов, Интервьюер');
INSERT INTO category(parent_id, name) VALUES(3, 'Продвижение, Специальные мероприятия');
INSERT INTO category(parent_id, name) VALUES(3, 'Производство рекламы');
INSERT INTO category(parent_id, name) VALUES(3, 'Промоутер');
INSERT INTO category(parent_id, name) VALUES(3, 'Радио реклама');
INSERT INTO category(parent_id, name) VALUES(3, 'Рекламный агент');
INSERT INTO category(parent_id, name) VALUES(3, 'Таинственный покупатель');
INSERT INTO category(parent_id, name) VALUES(3, 'Телевизионная реклама');
INSERT INTO category(parent_id, name) VALUES(3, 'Торговый маркетинг(Trade marketing)');
INSERT INTO category(parent_id, name) VALUES(3, 'Управление маркетингом');
INSERT INTO category(parent_id, name) VALUES(3, 'Управление проектами');

INSERT INTO category(category_id, name, has_child)
VALUES(4, 'Административный персонал', true);

INSERT INTO category(parent_id, name) VALUES(4, 'АХО');
INSERT INTO category(parent_id, name) VALUES(4, 'Архивист');
INSERT INTO category(parent_id, name) VALUES(4, 'Ввод данных');
INSERT INTO category(parent_id, name) VALUES(4, 'Вечерний секретарь');
INSERT INTO category(parent_id, name) VALUES(4, 'Водитель');
INSERT INTO category(parent_id, name) VALUES(4, 'Делопроизводство');
INSERT INTO category(parent_id, name) VALUES(4, 'Курьер');
INSERT INTO category(parent_id, name) VALUES(4, 'Начальный уровень, Мало опыта');
INSERT INTO category(parent_id, name) VALUES(4, 'Персональный ассистент');
INSERT INTO category(parent_id, name) VALUES(4, 'Письменный перевод');
INSERT INTO category(parent_id, name) VALUES(4, 'Последовательный перевод');
INSERT INTO category(parent_id, name) VALUES(4, 'Ресепшен');
INSERT INTO category(parent_id, name) VALUES(4, 'Секретарь');
INSERT INTO category(parent_id, name) VALUES(4, 'Синхронный перевод');
INSERT INTO category(parent_id, name) VALUES(4, 'Сотрудник call-центра');
INSERT INTO category(parent_id, name) VALUES(4, 'Уборщица');
INSERT INTO category(parent_id, name) VALUES(4, 'Управляющий офисом(Оffice manager)');
INSERT INTO category(parent_id, name) VALUES(4, 'Учет товарооборота');

INSERT INTO category(category_id, name, has_child)
VALUES(5, 'Банки, инвестиции, лизинг', true);

INSERT INTO category(parent_id, name) VALUES(5, 'Forex');
INSERT INTO category(parent_id, name) VALUES(5, 'Автокредитование');
INSERT INTO category(parent_id, name) VALUES(5, 'Акции, Ценные бумаги');
INSERT INTO category(parent_id, name) VALUES(5, 'Аналитик');
INSERT INTO category(parent_id, name) VALUES(5, 'Аудит, Внутренний контроль');
INSERT INTO category(parent_id, name) VALUES(5, 'Бумаги с фиксированной доходностью (fixed Income)');
INSERT INTO category(parent_id, name) VALUES(5, 'Бухгалтер');
INSERT INTO category(parent_id, name) VALUES(5, 'Бюджетирование');
INSERT INTO category(parent_id, name) VALUES(5, 'Валютный контроль');
INSERT INTO category(parent_id, name) VALUES(5, 'Внутренние операции (Back Office)');
INSERT INTO category(parent_id, name) VALUES(5, 'Денежный рынок (money market)');
INSERT INTO category(parent_id, name) VALUES(5, 'Инвестиционная компания');
INSERT INTO category(parent_id, name) VALUES(5, 'Ипотека, Ипотечное кредитование');
INSERT INTO category(parent_id, name) VALUES(5, 'Казначейство, Управление ликвидностью');
INSERT INTO category(parent_id, name) VALUES(5, 'Кассовое обслуживание, инкассация');
INSERT INTO category(parent_id, name) VALUES(5, 'Коммерческий банк');
INSERT INTO category(parent_id, name) VALUES(5, 'Корпоративное финансирование');
INSERT INTO category(parent_id, name) VALUES(5, 'Корреспондентские, Международные отношения');
INSERT INTO category(parent_id, name) VALUES(5, 'Кредитование малого и среднего бизнеса');
INSERT INTO category(parent_id, name) VALUES(5, 'Кредиты');
INSERT INTO category(parent_id, name) VALUES(5, 'Кредиты: розничные');
INSERT INTO category(parent_id, name) VALUES(5, 'Лизинг');
INSERT INTO category(parent_id, name) VALUES(5, 'Методология, Банковские технологии');
INSERT INTO category(parent_id, name) VALUES(5, 'Налоги');
INSERT INTO category(parent_id, name) VALUES(5, 'Начальный уровень, Мало опыта');
INSERT INTO category(parent_id, name) VALUES(5, 'ОПЕРУ');
INSERT INTO category(parent_id, name) VALUES(5, 'Обменные пункты, Банкоматы');
INSERT INTO category(parent_id, name) VALUES(5, 'Отчетность');
INSERT INTO category(parent_id, name) VALUES(5, 'Оценка залога, Стоимости имущества');
INSERT INTO category(parent_id, name) VALUES(5, 'Паевые фонды');
INSERT INTO category(parent_id, name) VALUES(5, 'Пластиковые карты');
INSERT INTO category(parent_id, name) VALUES(5, 'Портфельные инвестиции');
INSERT INTO category(parent_id, name) VALUES(5, 'Привлечение клиентов');
INSERT INTO category(parent_id, name) VALUES(5, 'Продажа финансовых продуктов');
INSERT INTO category(parent_id, name) VALUES(5, 'Проектное финансирование');
INSERT INTO category(parent_id, name) VALUES(5, 'Прямые инвестиции');
INSERT INTO category(parent_id, name) VALUES(5, 'Работа с проблемными заемщиками');
INSERT INTO category(parent_id, name) VALUES(5, 'Разработка новых продуктов, Маркетинг');
INSERT INTO category(parent_id, name) VALUES(5, 'Расчеты');
INSERT INTO category(parent_id, name) VALUES(5, 'Риски: кредитные');
INSERT INTO category(parent_id, name) VALUES(5, 'Риски: лизинговые');
INSERT INTO category(parent_id, name) VALUES(5, 'Риски: операционные');
INSERT INTO category(parent_id, name) VALUES(5, 'Риски: прочие');
INSERT INTO category(parent_id, name) VALUES(5, 'Риски: рыночные');
INSERT INTO category(parent_id, name) VALUES(5, 'Риски: финансовые');
INSERT INTO category(parent_id, name) VALUES(5, 'Розничный бизнес');
INSERT INTO category(parent_id, name) VALUES(5, 'Руководство бухгалтерией');
INSERT INTO category(parent_id, name) VALUES(5, 'Торговое финансирование');
INSERT INTO category(parent_id, name) VALUES(5, 'Трейдинг, Дилинг');
INSERT INTO category(parent_id, name) VALUES(5, 'Факторинг');
INSERT INTO category(parent_id, name) VALUES(5, 'Филиалы');
INSERT INTO category(parent_id, name) VALUES(5, 'Финансовый мониторинг');
INSERT INTO category(parent_id, name) VALUES(5, 'Экономист');
INSERT INTO category(parent_id, name) VALUES(5, 'Эмиссии');

INSERT INTO category(category_id, name, has_child)
VALUES(6, 'Управление персоналом, тренинги', true);

INSERT INTO category(parent_id, name) VALUES(6, 'Компенсации и льготы');
INSERT INTO category(parent_id, name) VALUES(6, 'Начальный уровень, Мало опыта');
INSERT INTO category(parent_id, name) VALUES(6, 'Развитие персонала');
INSERT INTO category(parent_id, name) VALUES(6, 'Рекрутмент');
INSERT INTO category(parent_id, name) VALUES(6, 'Тренинги');
INSERT INTO category(parent_id, name) VALUES(6, 'Управление персоналом');
INSERT INTO category(parent_id, name) VALUES(6, 'Учет кадров');

INSERT INTO category(category_id, name, has_child)
VALUES(7, 'Автомобильный бизнес', true);

INSERT INTO category(parent_id, name) VALUES(7, 'Автожестянщик');
INSERT INTO category(parent_id, name) VALUES(7, 'Автозапчасти');
INSERT INTO category(parent_id, name) VALUES(7, 'Автомойщик');
INSERT INTO category(parent_id, name) VALUES(7, 'Автослесарь');
INSERT INTO category(parent_id, name) VALUES(7, 'Начальный уровень /Мало опыта');
INSERT INTO category(parent_id, name) VALUES(7, 'Продажа');
INSERT INTO category(parent_id, name) VALUES(7, 'Производство');
INSERT INTO category(parent_id, name) VALUES(7, 'Прокат, лизинг');
INSERT INTO category(parent_id, name) VALUES(7, 'Сервисное обслуживание');
INSERT INTO category(parent_id, name) VALUES(7, 'Тонировщик');
INSERT INTO category(parent_id, name) VALUES(7, 'Шины, Диски');

INSERT INTO category(category_id, name, has_child)
VALUES(8, 'Безопасность', true);

INSERT INTO category(parent_id, name) VALUES(8, 'Взыскание задолженности, Коллекторская деятельность');
INSERT INTO category(parent_id, name) VALUES(8, 'Имущественная безопасность');
INSERT INTO category(parent_id, name) VALUES(8, 'Инкассатор');
INSERT INTO category(parent_id, name) VALUES(8, 'Личная безопасность');
INSERT INTO category(parent_id, name) VALUES(8, 'Охранник');
INSERT INTO category(parent_id, name) VALUES(8, 'Пожарная безопасность');
INSERT INTO category(parent_id, name) VALUES(8, 'Руководитель СБ');
INSERT INTO category(parent_id, name) VALUES(8, 'Системы видеонаблюдения');
INSERT INTO category(parent_id, name) VALUES(8, 'Экономическая и информационная безопасность');

INSERT INTO category(category_id, name, has_child)
VALUES(9, 'Высший менеджмент', true);

INSERT INTO category(parent_id, name) VALUES(9, 'Администрирование');
INSERT INTO category(parent_id, name) VALUES(9, 'Антикризисное управление');
INSERT INTO category(parent_id, name) VALUES(9, 'Добыча cырья');
INSERT INTO category(parent_id, name) VALUES(9, 'Инвестиции');
INSERT INTO category(parent_id, name) VALUES(9, 'Информационные технологии, Интернет, Мультимедиа');
INSERT INTO category(parent_id, name) VALUES(9, 'Искусство, Развлечения, Масс-медиа');
INSERT INTO category(parent_id, name) VALUES(9, 'Коммерческий Банк');
INSERT INTO category(parent_id, name) VALUES(9, 'Консультирование');
INSERT INTO category(parent_id, name) VALUES(9, 'Маркетинг, Реклама, PR');
INSERT INTO category(parent_id, name) VALUES(9, 'Медицина, Фармацевтика');
INSERT INTO category(parent_id, name) VALUES(9, 'Наука, Образование');
INSERT INTO category(parent_id, name) VALUES(9, 'Продажи');
INSERT INTO category(parent_id, name) VALUES(9, 'Производство, Технология');
INSERT INTO category(parent_id, name) VALUES(9, 'Спортивные клубы, Фитнес, Салоны красоты');
INSERT INTO category(parent_id, name) VALUES(9, 'Страхование');
INSERT INTO category(parent_id, name) VALUES(9, 'Строительство, Недвижимость');
INSERT INTO category(parent_id, name) VALUES(9, 'Транспорт, Логистика');
INSERT INTO category(parent_id, name) VALUES(9, 'Туризм, Гостиницы, Рестораны');
INSERT INTO category(parent_id, name) VALUES(9, 'Управление закупками');
INSERT INTO category(parent_id, name) VALUES(9, 'Управление малым бизнесом');
INSERT INTO category(parent_id, name) VALUES(9, 'Управление персоналом, Тренинги');
INSERT INTO category(parent_id, name) VALUES(9, 'Финансы');
INSERT INTO category(parent_id, name) VALUES(9, 'Юриспруденция');

INSERT INTO category(category_id, name, has_child)
VALUES(10, 'Добыча сырья', true);

INSERT INTO category(parent_id, name) VALUES(10, 'Бурение');
INSERT INTO category(parent_id, name) VALUES(10, 'Газ');
INSERT INTO category(parent_id, name) VALUES(10, 'Геологоразведка');
INSERT INTO category(parent_id, name) VALUES(10, 'Инженер');
INSERT INTO category(parent_id, name) VALUES(10, 'Маркшейдер');
INSERT INTO category(parent_id, name) VALUES(10, 'Начальный уровень, Мало опыта');
INSERT INTO category(parent_id, name) VALUES(10, 'Нефть');
INSERT INTO category(parent_id, name) VALUES(10, 'Руда');
INSERT INTO category(parent_id, name) VALUES(10, 'Уголь');
INSERT INTO category(parent_id, name) VALUES(10, 'Управление предприятием');

INSERT INTO category(category_id, name, has_child)
VALUES(11, 'Искусство, развлечения, масс-медиа', true);

INSERT INTO category(parent_id, name) VALUES(11, 'Дизайн, графика, живопись');
INSERT INTO category(parent_id, name) VALUES(11, 'Журналистика');
INSERT INTO category(parent_id, name) VALUES(11, 'Издательская деятельность');
INSERT INTO category(parent_id, name) VALUES(11, 'Казино и игорный бизнес');
INSERT INTO category(parent_id, name) VALUES(11, 'Кино');
INSERT INTO category(parent_id, name) VALUES(11, 'Литературная, Редакторская деятельность');
INSERT INTO category(parent_id, name) VALUES(11, 'Мода');
INSERT INTO category(parent_id, name) VALUES(11, 'Музыка');
INSERT INTO category(parent_id, name) VALUES(11, 'Начальный уровень, Мало опыта');
INSERT INTO category(parent_id, name) VALUES(11, 'Пресса');
INSERT INTO category(parent_id, name) VALUES(11, 'Прочее');
INSERT INTO category(parent_id, name) VALUES(11, 'Радио');
INSERT INTO category(parent_id, name) VALUES(11, 'Телевидение');
INSERT INTO category(parent_id, name) VALUES(11, 'Фотография');

INSERT INTO category(category_id, name, has_child)
VALUES(12, 'Консультирование', true);

INSERT INTO category(parent_id, name) VALUES(12, 'Internet, E-Commerce');
INSERT INTO category(parent_id, name) VALUES(12, 'Knowledge management');
INSERT INTO category(parent_id, name) VALUES(12, 'PR Consulting');
INSERT INTO category(parent_id, name) VALUES(12, 'Информационные технологии');
INSERT INTO category(parent_id, name) VALUES(12, 'Исследования рынка');
INSERT INTO category(parent_id, name) VALUES(12, 'Корпоративные финансы');
INSERT INTO category(parent_id, name) VALUES(12, 'Начальный уровень, Мало опыта');
INSERT INTO category(parent_id, name) VALUES(12, 'Недвижимость');
INSERT INTO category(parent_id, name) VALUES(12, 'Организационное консультирование');
INSERT INTO category(parent_id, name) VALUES(12, 'Реинжиниринг бизнес процессов');
INSERT INTO category(parent_id, name) VALUES(12, 'Реинжиниринг, Аутсорсинг финансовой функции');
INSERT INTO category(parent_id, name) VALUES(12, 'Стратегия');
INSERT INTO category(parent_id, name) VALUES(12, 'Управление практикой');
INSERT INTO category(parent_id, name) VALUES(12, 'Управление проектами');
INSERT INTO category(parent_id, name) VALUES(12, 'Управленческое консультирование');

INSERT INTO category(category_id, name, has_child)
VALUES(13, 'Медицина, фармацевтика', true);

INSERT INTO category(parent_id, name) VALUES(13, 'Ветеринария');
INSERT INTO category(parent_id, name) VALUES(13, 'Врач-эксперт');
INSERT INTO category(parent_id, name) VALUES(13, 'Дефектолог, Логопед');
INSERT INTO category(parent_id, name) VALUES(13, 'Клинические исследования');
INSERT INTO category(parent_id, name) VALUES(13, 'Лаборант');
INSERT INTO category(parent_id, name) VALUES(13, 'Лекарственные препараты');
INSERT INTO category(parent_id, name) VALUES(13, 'Лечащий врач');
INSERT INTO category(parent_id, name) VALUES(13, 'Маркетинг');
INSERT INTO category(parent_id, name) VALUES(13, 'Медицинский представитель');
INSERT INTO category(parent_id, name) VALUES(13, 'Медицинский советник');
INSERT INTO category(parent_id, name) VALUES(13, 'Медицинское оборудование');
INSERT INTO category(parent_id, name) VALUES(13, 'Младший и средний медперсонал');
INSERT INTO category(parent_id, name) VALUES(13, 'Начальный уровень, Мало опыта');
INSERT INTO category(parent_id, name) VALUES(13, 'Оптика');
INSERT INTO category(parent_id, name) VALUES(13, 'Провизор');
INSERT INTO category(parent_id, name) VALUES(13, 'Продажи');
INSERT INTO category(parent_id, name) VALUES(13, 'Производство');
INSERT INTO category(parent_id, name) VALUES(13, 'Психология');
INSERT INTO category(parent_id, name) VALUES(13, 'Регистратура');
INSERT INTO category(parent_id, name) VALUES(13, 'Сертификация');
INSERT INTO category(parent_id, name) VALUES(13, 'Фармацевт');

INSERT INTO category(category_id, name, has_child)
VALUES(14, 'Наука, образование', true);

INSERT INTO category(parent_id, name) VALUES(14, 'Биотехнологии');
INSERT INTO category(parent_id, name) VALUES(14, 'Гуманитарные науки');
INSERT INTO category(parent_id, name) VALUES(14, 'Инженерные науки');
INSERT INTO category(parent_id, name) VALUES(14, 'Информатика, Информационные системы');
INSERT INTO category(parent_id, name) VALUES(14, 'Математика');
INSERT INTO category(parent_id, name) VALUES(14, 'Науки о Земле');
INSERT INTO category(parent_id, name) VALUES(14, 'Начальный уровень, Мало опыта');
INSERT INTO category(parent_id, name) VALUES(14, 'Преподавание');
INSERT INTO category(parent_id, name) VALUES(14, 'Физика');
INSERT INTO category(parent_id, name) VALUES(14, 'Химия');
INSERT INTO category(parent_id, name) VALUES(14, 'Экономика, Менеджмент');
INSERT INTO category(parent_id, name) VALUES(14, 'Языки');

INSERT INTO category(category_id, name, has_child)
VALUES(15, 'Государственная служба, некоммерческие организации', true);

INSERT INTO category(parent_id, name) VALUES(15, 'Архивариус');
INSERT INTO category(parent_id, name) VALUES(15, 'Атташе');
INSERT INTO category(parent_id, name) VALUES(15, 'Библиотекарь');
INSERT INTO category(parent_id, name) VALUES(15, 'Благотворительность');
INSERT INTO category(parent_id, name) VALUES(15, 'НИИ');
INSERT INTO category(parent_id, name) VALUES(15, 'Общественные организации');
INSERT INTO category(parent_id, name) VALUES(15, 'Правительство');

INSERT INTO category(category_id, name, has_child)
VALUES(16, 'Продажи', true);

INSERT INTO category(parent_id, name) VALUES(16, 'FMCG, Товары народного потребления');
INSERT INTO category(parent_id, name) VALUES(16, 'Автомобили, Запчасти');
INSERT INTO category(parent_id, name) VALUES(16, 'Алкоголь');
INSERT INTO category(parent_id, name) VALUES(16, 'Бытовая техника');
INSERT INTO category(parent_id, name) VALUES(16, 'ГСМ, нефть, бензин');
INSERT INTO category(parent_id, name) VALUES(16, 'Дилерские сети');
INSERT INTO category(parent_id, name) VALUES(16, 'Дистрибуция');
INSERT INTO category(parent_id, name) VALUES(16, 'Клининговые услуги');
INSERT INTO category(parent_id, name) VALUES(16, 'Компьютерная техника');
INSERT INTO category(parent_id, name) VALUES(16, 'Компьютерные программы');
INSERT INTO category(parent_id, name) VALUES(16, 'Мебель');
INSERT INTO category(parent_id, name) VALUES(16, 'Медицина, Фармацевтика');
INSERT INTO category(parent_id, name) VALUES(16, 'Менеджер по работе с клиентами');
INSERT INTO category(parent_id, name) VALUES(16, 'Металлопрокат');
INSERT INTO category(parent_id, name) VALUES(16, 'Многоуровневый маркетинг');
INSERT INTO category(parent_id, name) VALUES(16, 'Начальный уровень, Мало опыта');
INSERT INTO category(parent_id, name) VALUES(16, 'Оптовая торговля');
INSERT INTO category(parent_id, name) VALUES(16, 'Продавец в магазине');
INSERT INTO category(parent_id, name) VALUES(16, 'Продажи по телефону, Телемаркетинг');
INSERT INTO category(parent_id, name) VALUES(16, 'Продукты питания');
INSERT INTO category(parent_id, name) VALUES(16, 'Прямые продажи');
INSERT INTO category(parent_id, name) VALUES(16, 'Решения по автоматизации процессов');
INSERT INTO category(parent_id, name) VALUES(16, 'Розничная торговля');
INSERT INTO category(parent_id, name) VALUES(16, 'Сантехника');
INSERT INTO category(parent_id, name) VALUES(16, 'Сельское хозяйство');
INSERT INTO category(parent_id, name) VALUES(16, 'Сертификация');
INSERT INTO category(parent_id, name) VALUES(16, 'Системы безопасности');
INSERT INTO category(parent_id, name) VALUES(16, 'Станки, тяжелое оборудование');
INSERT INTO category(parent_id, name) VALUES(16, 'Строительные материалы');
INSERT INTO category(parent_id, name) VALUES(16, 'Текстиль, Одежда, Обувь');
INSERT INTO category(parent_id, name) VALUES(16, 'Телекоммуникации, Сетевые решения');
INSERT INTO category(parent_id, name) VALUES(16, 'Тендеры');
INSERT INTO category(parent_id, name) VALUES(16, 'Товары для бизнеса');
INSERT INTO category(parent_id, name) VALUES(16, 'Торговля биржевыми товарами');
INSERT INTO category(parent_id, name) VALUES(16, 'Торговые сети');
INSERT INTO category(parent_id, name) VALUES(16, 'Торговый представитель');
INSERT INTO category(parent_id, name) VALUES(16, 'Управление продажами');
INSERT INTO category(parent_id, name) VALUES(16, 'Услуги для бизнеса');
INSERT INTO category(parent_id, name) VALUES(16, 'Услуги для населения');
INSERT INTO category(parent_id, name) VALUES(16, 'Финансовые услуги');
INSERT INTO category(parent_id, name) VALUES(16, 'Франчайзинг');
INSERT INTO category(parent_id, name) VALUES(16, 'Химическая продукция');
INSERT INTO category(parent_id, name) VALUES(16, 'Цветные металлы');
INSERT INTO category(parent_id, name) VALUES(16, 'Электроника, фото, видео');
INSERT INTO category(parent_id, name) VALUES(16, 'Электротехническое оборудование, Светотехника');

INSERT INTO category(category_id, name, has_child)
VALUES(17, 'Производство', true);

INSERT INTO category(parent_id, name) VALUES(17, 'Авиационная промышленность');
INSERT INTO category(parent_id, name) VALUES(17, 'Автомобильная промышленность');
INSERT INTO category(parent_id, name) VALUES(17, 'Атомная энергетика');
INSERT INTO category(parent_id, name) VALUES(17, 'Главный агроном');
INSERT INTO category(parent_id, name) VALUES(17, 'Главный инженер');
INSERT INTO category(parent_id, name) VALUES(17, 'Главный механик');
INSERT INTO category(parent_id, name) VALUES(17, 'Деревообработка, Лесная промышленность');
INSERT INTO category(parent_id, name) VALUES(17, 'Закупки и снабжение');
INSERT INTO category(parent_id, name) VALUES(17, 'Зоотехник');
INSERT INTO category(parent_id, name) VALUES(17, 'Инженер');
INSERT INTO category(parent_id, name) VALUES(17, 'Инженер, Мясо- и птицепереработка');
INSERT INTO category(parent_id, name) VALUES(17, 'Инженер, Производство и переработка зерновых');
INSERT INTO category(parent_id, name) VALUES(17, 'Инженер, Производство сахара');
INSERT INTO category(parent_id, name) VALUES(17, 'Конструктор');
INSERT INTO category(parent_id, name) VALUES(17, 'Контроль качества');
INSERT INTO category(parent_id, name) VALUES(17, 'Легкая промышленность');
INSERT INTO category(parent_id, name) VALUES(17, 'Машиностроение');
INSERT INTO category(parent_id, name) VALUES(17, 'Мебельное производство');
INSERT INTO category(parent_id, name) VALUES(17, 'Металлургия');
INSERT INTO category(parent_id, name) VALUES(17, 'Метролог');
INSERT INTO category(parent_id, name) VALUES(17, 'Начальный уровень, Мало опыта');
INSERT INTO category(parent_id, name) VALUES(17, 'Нефтепереработка');
INSERT INTO category(parent_id, name) VALUES(17, 'Охрана труда');
INSERT INTO category(parent_id, name) VALUES(17, 'Пищевая промышленность');
INSERT INTO category(parent_id, name) VALUES(17, 'Полиграфия');
INSERT INTO category(parent_id, name) VALUES(17, 'Радиоэлектронная промышленность');
INSERT INTO category(parent_id, name) VALUES(17, 'Руководство предприятием');
INSERT INTO category(parent_id, name) VALUES(17, 'Сельхозпроизводство');
INSERT INTO category(parent_id, name) VALUES(17, 'Сертификация');
INSERT INTO category(parent_id, name) VALUES(17, 'Стройматериалы');
INSERT INTO category(parent_id, name) VALUES(17, 'Судостроение');
INSERT INTO category(parent_id, name) VALUES(17, 'Табачная промышленность');
INSERT INTO category(parent_id, name) VALUES(17, 'Технолог');
INSERT INTO category(parent_id, name) VALUES(17, 'Технолог, Мясо- и птицепереработка');
INSERT INTO category(parent_id, name) VALUES(17, 'Технолог, Производство и переработка зерновых');
INSERT INTO category(parent_id, name) VALUES(17, 'Технолог, Производство сахара');
INSERT INTO category(parent_id, name) VALUES(17, 'Управление проектами');
INSERT INTO category(parent_id, name) VALUES(17, 'Управление цехом');
INSERT INTO category(parent_id, name) VALUES(17, 'Фармацевтическая промышленность');
INSERT INTO category(parent_id, name) VALUES(17, 'Химическая промышленность');
INSERT INTO category(parent_id, name) VALUES(17, 'Эколог');
INSERT INTO category(parent_id, name) VALUES(17, 'Электроэнергетика');
INSERT INTO category(parent_id, name) VALUES(17, 'Энергетик производства');
INSERT INTO category(parent_id, name) VALUES(17, 'Ювелирная промышленность');

INSERT INTO category(category_id, name, has_child)
VALUES(18, 'Страхование', true);

INSERT INTO category(parent_id, name) VALUES(18, 'Автострахование');
INSERT INTO category(parent_id, name) VALUES(18, 'Агент');
INSERT INTO category(parent_id, name) VALUES(18, 'Актуарий');
INSERT INTO category(parent_id, name) VALUES(18, 'Андеррайтер');
INSERT INTO category(parent_id, name) VALUES(18, 'Врач-эксперт');
INSERT INTO category(parent_id, name) VALUES(18, 'Комплексное страхование физических лиц');
INSERT INTO category(parent_id, name) VALUES(18, 'Комплексное страхование юридических лиц');
INSERT INTO category(parent_id, name) VALUES(18, 'Медицинское страхование');
INSERT INTO category(parent_id, name) VALUES(18, 'Начальный уровень, Мало опыта');
INSERT INTO category(parent_id, name) VALUES(18, 'Перестрахование');
INSERT INTO category(parent_id, name) VALUES(18, 'Руководитель направления');
INSERT INTO category(parent_id, name) VALUES(18, 'Страхование бизнеса');
INSERT INTO category(parent_id, name) VALUES(18, 'Страхование жизни');
INSERT INTO category(parent_id, name) VALUES(18, 'Страхование недвижимости');
INSERT INTO category(parent_id, name) VALUES(18, 'Страхование ответственности');
INSERT INTO category(parent_id, name) VALUES(18, 'Урегулирование убытков');
INSERT INTO category(parent_id, name) VALUES(18, 'Эксперт-оценщик');

INSERT INTO category(category_id, name, has_child)
VALUES(19, 'Строительство, недвижимость', true);

INSERT INTO category(parent_id, name) VALUES(19, 'Агент');
INSERT INTO category(parent_id, name) VALUES(19, 'Водоснабжение и канализация');
INSERT INTO category(parent_id, name) VALUES(19, 'Геодезия и картография');
INSERT INTO category(parent_id, name) VALUES(19, 'Гостиницы, Магазины');
INSERT INTO category(parent_id, name) VALUES(19, 'Девелопер');
INSERT INTO category(parent_id, name) VALUES(19, 'Дизайн/Оформление');
INSERT INTO category(parent_id, name) VALUES(19, 'ЖКХ');
INSERT INTO category(parent_id, name) VALUES(19, 'Жилье');
INSERT INTO category(parent_id, name) VALUES(19, 'Землеустройство');
INSERT INTO category(parent_id, name) VALUES(19, 'Инженер');
INSERT INTO category(parent_id, name) VALUES(19, 'Начальный уровень/Мало опыта');
INSERT INTO category(parent_id, name) VALUES(19, 'Нежилые помещения');
INSERT INTO category(parent_id, name) VALUES(19, 'Отопление, вентиляция и кондиционирование');
INSERT INTO category(parent_id, name) VALUES(19, 'Оценка');
INSERT INTO category(parent_id, name) VALUES(19, 'Проектирование, Архитектура');
INSERT INTO category(parent_id, name) VALUES(19, 'Прораб');
INSERT INTO category(parent_id, name) VALUES(19, 'Рабочие строительных специальностей');
INSERT INTO category(parent_id, name) VALUES(19, 'Строительство');
INSERT INTO category(parent_id, name) VALUES(19, 'Тендеры');
INSERT INTO category(parent_id, name) VALUES(19, 'Управление проектами');
INSERT INTO category(parent_id, name) VALUES(19, 'Эксплуатация');

INSERT INTO category(category_id, name, has_child)
VALUES(20, 'Транспорт, логистика', true);

INSERT INTO category(parent_id, name) VALUES(20, 'Авиаперевозки');
INSERT INTO category(parent_id, name) VALUES(20, 'Автоперевозки');
INSERT INTO category(parent_id, name) VALUES(20, 'Бизнес-авиация');
INSERT INTO category(parent_id, name) VALUES(20, 'ВЭД');
INSERT INTO category(parent_id, name) VALUES(20, 'Водитель');
INSERT INTO category(parent_id, name) VALUES(20, 'Гражданская авиация');
INSERT INTO category(parent_id, name) VALUES(20, 'Диспетчер');
INSERT INTO category(parent_id, name) VALUES(20, 'Железнодорожные перевозки');
INSERT INTO category(parent_id, name) VALUES(20, 'Закупки, Снабжение');
INSERT INTO category(parent_id, name) VALUES(20, 'Кладовщик');
INSERT INTO category(parent_id, name) VALUES(20, 'Контейнерные перевозки');
INSERT INTO category(parent_id, name) VALUES(20, 'Логистика');
INSERT INTO category(parent_id, name) VALUES(20, 'Морские/Речные перевозки');
INSERT INTO category(parent_id, name) VALUES(20, 'Начальный уровень, Мало опыта');
INSERT INTO category(parent_id, name) VALUES(20, 'Рабочий склада');
INSERT INTO category(parent_id, name) VALUES(20, 'Складское хозяйство');
INSERT INTO category(parent_id, name) VALUES(20, 'Таможенное оформление');
INSERT INTO category(parent_id, name) VALUES(20, 'Трубопроводы');
INSERT INTO category(parent_id, name) VALUES(20, 'Экспедитор');

INSERT INTO category(category_id, name, has_child)
VALUES(21, 'Туризм, гостиницы, рестораны', true);

INSERT INTO category(parent_id, name) VALUES(21, 'Авиабилеты');
INSERT INTO category(parent_id, name) VALUES(21, 'Анимация');
INSERT INTO category(parent_id, name) VALUES(21, 'Банкеты');
INSERT INTO category(parent_id, name) VALUES(21, 'Бронирование');
INSERT INTO category(parent_id, name) VALUES(21, 'Гид, Экскурсовод');
INSERT INTO category(parent_id, name) VALUES(21, 'Кейтеринг');
INSERT INTO category(parent_id, name) VALUES(21, 'Начальный уровень, Мало опыта');
INSERT INTO category(parent_id, name) VALUES(21, 'Организация встреч, Конференций');
INSERT INTO category(parent_id, name) VALUES(21, 'Организация туристических продуктов');
INSERT INTO category(parent_id, name) VALUES(21, 'Официант, Бармен');
INSERT INTO category(parent_id, name) VALUES(21, 'Оформление виз');
INSERT INTO category(parent_id, name) VALUES(21, 'Персонал кухни');
INSERT INTO category(parent_id, name) VALUES(21, 'Повар');
INSERT INTO category(parent_id, name) VALUES(21, 'Продажа туристических услуг');
INSERT INTO category(parent_id, name) VALUES(21, 'Размещение, Обслуживание гостей');
INSERT INTO category(parent_id, name) VALUES(21, 'Сомелье');
INSERT INTO category(parent_id, name) VALUES(21, 'Управление гостиницами');
INSERT INTO category(parent_id, name) VALUES(21, 'Управление ресторанами, Барами');
INSERT INTO category(parent_id, name) VALUES(21, 'Управление туристическим бизнесом');
INSERT INTO category(parent_id, name) VALUES(21, 'Хостес');
INSERT INTO category(parent_id, name) VALUES(21, 'Швейцар');
INSERT INTO category(parent_id, name) VALUES(21, 'Шеф-повар');

INSERT INTO category(category_id, name, has_child)
VALUES(22, 'Юристы', true);

INSERT INTO category(parent_id, name) VALUES(22, 'Compliance');
INSERT INTO category(parent_id, name) VALUES(22, 'Авторское право');
INSERT INTO category(parent_id, name) VALUES(22, 'Адвокат');
INSERT INTO category(parent_id, name) VALUES(22, 'Антимонопольное право');
INSERT INTO category(parent_id, name) VALUES(22, 'Арбитраж');
INSERT INTO category(parent_id, name) VALUES(22, 'Банковское право');
INSERT INTO category(parent_id, name) VALUES(22, 'Взыскание задолженности, Коллекторская деятельность');
INSERT INTO category(parent_id, name) VALUES(22, 'Договорное право');
INSERT INTO category(parent_id, name) VALUES(22, 'Законотворчество');
INSERT INTO category(parent_id, name) VALUES(22, 'Земельное право');
INSERT INTO category(parent_id, name) VALUES(22, 'Интеллектуальная собственность');
INSERT INTO category(parent_id, name) VALUES(22, 'Корпоративное право');
INSERT INTO category(parent_id, name) VALUES(22, 'Международное право');
INSERT INTO category(parent_id, name) VALUES(22, 'Морское право');
INSERT INTO category(parent_id, name) VALUES(22, 'Налоговое право');
INSERT INTO category(parent_id, name) VALUES(22, 'Начальный уровень, Мало опыта');
INSERT INTO category(parent_id, name) VALUES(22, 'Недвижимость');
INSERT INTO category(parent_id, name) VALUES(22, 'Недропользование');
INSERT INTO category(parent_id, name) VALUES(22, 'Помощник');
INSERT INTO category(parent_id, name) VALUES(22, 'Регистрация юридических лиц');
INSERT INTO category(parent_id, name) VALUES(22, 'Семейное право');
INSERT INTO category(parent_id, name) VALUES(22, 'Слияния и поглощения');
INSERT INTO category(parent_id, name) VALUES(22, 'Страховое право');
INSERT INTO category(parent_id, name) VALUES(22, 'Трудовое право');
INSERT INTO category(parent_id, name) VALUES(22, 'Уголовное право');
INSERT INTO category(parent_id, name) VALUES(22, 'Ценные бумаги, Рынки капитала');
INSERT INTO category(parent_id, name) VALUES(22, 'Юрисконсульт');

INSERT INTO category(category_id, name, has_child)
VALUES(23, 'Спортивные клубы, фитнес, салоны красоты', true);

INSERT INTO category(parent_id, name) VALUES(23, 'Администрация');
INSERT INTO category(parent_id, name) VALUES(23, 'Косметология');
INSERT INTO category(parent_id, name) VALUES(23, 'Массажист');
INSERT INTO category(parent_id, name) VALUES(23, 'Ногтевой сервис');
INSERT INTO category(parent_id, name) VALUES(23, 'Парикмахер');
INSERT INTO category(parent_id, name) VALUES(23, 'Продажи');
INSERT INTO category(parent_id, name) VALUES(23, 'Тренерский состав');

INSERT INTO category(category_id, name, has_child)
VALUES(24, 'Инсталляция и сервис', true);

INSERT INTO category(parent_id, name) VALUES(24, 'Инсталляция и настройка оборудования');
INSERT INTO category(parent_id, name) VALUES(24, 'Менеджер по сервису - промышленное оборудование');
INSERT INTO category(parent_id, name) VALUES(24, 'Менеджер по сервису - сетевые и телекоммуникационные технологии');
INSERT INTO category(parent_id, name) VALUES(24, 'Менеджер по сервису - транспорт');
INSERT INTO category(parent_id, name) VALUES(24, 'Руководитель сервисного центра');
INSERT INTO category(parent_id, name) VALUES(24, 'Сервисный инженер');

INSERT INTO category(category_id, name, has_child)
VALUES(25, 'Закупки', true);

INSERT INTO category(parent_id, name) VALUES(25, 'FMCG, Товары народного потребления');
INSERT INTO category(parent_id, name) VALUES(25, 'Автомобили, Запчасти');
INSERT INTO category(parent_id, name) VALUES(25, 'Алкоголь');
INSERT INTO category(parent_id, name) VALUES(25, 'ГСМ, нефть, бензин');
INSERT INTO category(parent_id, name) VALUES(25, 'Компьютерная техника');
INSERT INTO category(parent_id, name) VALUES(25, 'Металлопрокат');
INSERT INTO category(parent_id, name) VALUES(25, 'Начальный уровень, Мало опыта');
INSERT INTO category(parent_id, name) VALUES(25, 'Продукты питания');
INSERT INTO category(parent_id, name) VALUES(25, 'Сертификация');
INSERT INTO category(parent_id, name) VALUES(25, 'Станки, Тяжелое оборудование');
INSERT INTO category(parent_id, name) VALUES(25, 'Строительные материалы');
INSERT INTO category(parent_id, name) VALUES(25, 'Тендеры');
INSERT INTO category(parent_id, name) VALUES(25, 'Товары для бизнеса');
INSERT INTO category(parent_id, name) VALUES(25, 'Управление закупками');
INSERT INTO category(parent_id, name) VALUES(25, 'Фармацевтика');
INSERT INTO category(parent_id, name) VALUES(25, 'Химическая продукция');
INSERT INTO category(parent_id, name) VALUES(25, 'Электроника, фото, видео');
INSERT INTO category(parent_id, name) VALUES(25, 'Электротехническое оборудование/светотехника');

INSERT INTO category(category_id, name, has_child)
VALUES(26, 'Начало карьеры, студенты', true);

INSERT INTO category(parent_id, name) VALUES(26, 'Административный персонал');
INSERT INTO category(parent_id, name) VALUES(26, 'Автомобильный бизнес');
INSERT INTO category(parent_id, name) VALUES(26, 'Добыча сырья');
INSERT INTO category(parent_id, name) VALUES(26, 'Информационные технологии, Интернет, Мультимедиа');
INSERT INTO category(parent_id, name) VALUES(26, 'Искусство, Развлечения, Масс-медиа');
INSERT INTO category(parent_id, name) VALUES(26, 'Консультирование');
INSERT INTO category(parent_id, name) VALUES(26, 'Маркетинг, Реклама, PR');
INSERT INTO category(parent_id, name) VALUES(26, 'Медицина, Фармацевтика');
INSERT INTO category(parent_id, name) VALUES(26, 'Наука, Образование');
INSERT INTO category(parent_id, name) VALUES(26, 'Продажи');
INSERT INTO category(parent_id, name) VALUES(26, 'Производство, Технологии');
INSERT INTO category(parent_id, name) VALUES(26, 'Страхование');
INSERT INTO category(parent_id, name) VALUES(26, 'Строительство, Архитектура');
INSERT INTO category(parent_id, name) VALUES(26, 'Транспорт, Логистика');
INSERT INTO category(parent_id, name) VALUES(26, 'Туризм, Гостиницы, Рестораны');
INSERT INTO category(parent_id, name) VALUES(26, 'Управление персоналом');
INSERT INTO category(parent_id, name) VALUES(26, 'Финансы, Банки, Инвестиции');
INSERT INTO category(parent_id, name) VALUES(26, 'Юристы');

INSERT INTO category(category_id, name, has_child)
VALUES(27, 'Домашний персонал', true);

INSERT INTO category(parent_id, name) VALUES(27, 'Воспитатель, Гувернантка, Няня');
INSERT INTO category(parent_id, name) VALUES(27, 'Домработница, Горничная');
INSERT INTO category(parent_id, name) VALUES(27, 'Персональный водитель');
INSERT INTO category(parent_id, name) VALUES(27, 'Повар');
INSERT INTO category(parent_id, name) VALUES(27, 'Помощник по хозяйству, Управляющий');
INSERT INTO category(parent_id, name) VALUES(27, 'Репетитор');
INSERT INTO category(parent_id, name) VALUES(27, 'Садовник');
INSERT INTO category(parent_id, name) VALUES(27, 'Сиделка');

INSERT INTO category(category_id, name, has_child)
VALUES(28, 'Рабочий персонал', true);

INSERT INTO category(parent_id, name) VALUES(28, 'Гардеробщик');
INSERT INTO category(parent_id, name) VALUES(28, 'Грузчик');
INSERT INTO category(parent_id, name) VALUES(28, 'Дворник, Уборщица');
INSERT INTO category(parent_id, name) VALUES(28, 'Дорожные рабочие');
INSERT INTO category(parent_id, name) VALUES(28, 'Другое');
INSERT INTO category(parent_id, name) VALUES(28, 'Жестянщик');
INSERT INTO category(parent_id, name) VALUES(28, 'Заправщик картриджей');
INSERT INTO category(parent_id, name) VALUES(28, 'Комплектовщик, Укладчик-упаковщик');
INSERT INTO category(parent_id, name) VALUES(28, 'Краснодеревщик');
INSERT INTO category(parent_id, name) VALUES(28, 'Кузнец');
INSERT INTO category(parent_id, name) VALUES(28, 'Лифтер');
INSERT INTO category(parent_id, name) VALUES(28, 'Маляр');
INSERT INTO category(parent_id, name) VALUES(28, 'Машинист производства');
INSERT INTO category(parent_id, name) VALUES(28, 'Машинист сцены');
INSERT INTO category(parent_id, name) VALUES(28, 'Машинист экскаватора');
INSERT INTO category(parent_id, name) VALUES(28, 'Механик');
INSERT INTO category(parent_id, name) VALUES(28, 'Монтажник');
INSERT INTO category(parent_id, name) VALUES(28, 'Наладчик');
INSERT INTO category(parent_id, name) VALUES(28, 'Оператор станков');
INSERT INTO category(parent_id, name) VALUES(28, 'Пастух, Чабан');
INSERT INTO category(parent_id, name) VALUES(28, 'Перемотчик');
INSERT INTO category(parent_id, name) VALUES(28, 'Проводник');
INSERT INTO category(parent_id, name) VALUES(28, 'Разнорабочий');
INSERT INTO category(parent_id, name) VALUES(28, 'Сантехник');
INSERT INTO category(parent_id, name) VALUES(28, 'Сборщик');
INSERT INTO category(parent_id, name) VALUES(28, 'Сварщик');
INSERT INTO category(parent_id, name) VALUES(28, 'Слесарь');
INSERT INTO category(parent_id, name) VALUES(28, 'Столяр');
INSERT INTO category(parent_id, name) VALUES(28, 'Токарь, Фрезеровщик');
INSERT INTO category(parent_id, name) VALUES(28, 'Фасовщик');
INSERT INTO category(parent_id, name) VALUES(28, 'Швея');
INSERT INTO category(parent_id, name) VALUES(28, 'Шлифовщик');
INSERT INTO category(parent_id, name) VALUES(28, 'Штукатур');
INSERT INTO category(parent_id, name) VALUES(28, 'Электрик');
INSERT INTO category(parent_id, name) VALUES(28, 'Электромонтер, Кабельщик');
INSERT INTO category(parent_id, name) VALUES(28, 'Ювелир');

INSERT INTO meeting_type(MEETING_TYPE_ID, NAME)
VALUES(0, 'Телефонное интервью');

INSERT INTO meeting_type(MEETING_TYPE_ID, NAME)
VALUES(1, 'Встреча');

INSERT INTO meeting_type(MEETING_TYPE_ID, NAME)
VALUES(2, 'Тестирование');

INSERT INTO company_type(COMPANY_TYPE_ID, NAME)
VALUES(0, 'Заказчик');

INSERT INTO company_type(COMPANY_TYPE_ID, NAME)
VALUES(1, 'Кадровое агентство');

INSERT INTO company_type(COMPANY_TYPE_ID, NAME)
VALUES(2, 'Владелец');

ALTER TABLE company DROP CONSTRAINT IF EXISTS company_fk_created_by;

INSERT INTO company(company_id, name, address, company_type_id, is_active, created_by)
VALUES(0, 'Главная фирма', 'Адрес главной фирмы', 2, false, 0);
SELECT SET(@main_company, 0) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO person( PERSON_ID, created_by,FIRST_NAME, LAST_NAME, birth_date, START_DATE, IS_ACTIVE)
VALUES(0, 0, 'SYSTEM', 'SYSTEM', '1970-01-01', '1970-01-01', TRUE);
SELECT SET(@main_person, 0) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

ALTER TABLE company
  ADD CONSTRAINT company_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person(person_id);

INSERT INTO department(created_by,department_id, name, company_id, is_active)
VALUES(@main_person, 0, 'Отдел главной фирмы', @main_company, false);
SELECT SET(@main_department_company, 0) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

UPDATE person
SET department_id = @main_department_company
WHERE person_id = @main_person;

DELETE FROM language;

INSERT INTO language(name) VALUES('Английский');
INSERT INTO language(name) VALUES('Немецкий');
INSERT INTO language(name) VALUES('Французский');
INSERT INTO language(name) VALUES('Абазинский');
INSERT INTO language(name) VALUES('Абхазский');
INSERT INTO language(name) VALUES('Аварский');
INSERT INTO language(name) VALUES('Азербайджанский');
INSERT INTO language(name) VALUES('Албанский');
INSERT INTO language(name) VALUES('Амхарский');
INSERT INTO language(name) VALUES('Арабский');
INSERT INTO language(name) VALUES('Армянский');
INSERT INTO language(name) VALUES('Африкаанс');
INSERT INTO language(name) VALUES('Баскский');
INSERT INTO language(name) VALUES('Башкирский');
INSERT INTO language(name) VALUES('Белорусский');
INSERT INTO language(name) VALUES('Бенгальский');
INSERT INTO language(name) VALUES('Бирманский');
INSERT INTO language(name) VALUES('Болгарский');
INSERT INTO language(name) VALUES('Боснийский');
INSERT INTO language(name) VALUES('Бурятский');
INSERT INTO language(name) VALUES('Венгерский');
INSERT INTO language(name) VALUES('Вьетнамский');
INSERT INTO language(name) VALUES('Голландский');
INSERT INTO language(name) VALUES('Греческий');
INSERT INTO language(name) VALUES('Грузинский');
INSERT INTO language(name) VALUES('Дагестанский');
INSERT INTO language(name) VALUES('Даргинский');
INSERT INTO language(name) VALUES('Датский');
INSERT INTO language(name) VALUES('Иврит');
INSERT INTO language(name) VALUES('Ингушский');
INSERT INTO language(name) VALUES('Индонезийский');
INSERT INTO language(name) VALUES('Ирландский');
INSERT INTO language(name) VALUES('Исландский');
INSERT INTO language(name) VALUES('Испанский');
INSERT INTO language(name) VALUES('Итальянский');
INSERT INTO language(name) VALUES('Кабардино-черкесский');
INSERT INTO language(name) VALUES('Казахский');
INSERT INTO language(name) VALUES('Карачаево-балкарский');
INSERT INTO language(name) VALUES('Карельский');
INSERT INTO language(name) VALUES('Каталанский');
INSERT INTO language(name) VALUES('Кашмирский');
INSERT INTO language(name) VALUES('Китайский');
INSERT INTO language(name) VALUES('Коми');
INSERT INTO language(name) VALUES('Корейский');
INSERT INTO language(name) VALUES('Креольский (Сейшельские острова)');
INSERT INTO language(name) VALUES('Кумыкский');
INSERT INTO language(name) VALUES('Курдский');
INSERT INTO language(name) VALUES('Кхмерский (Камбоджийский)');
INSERT INTO language(name) VALUES('Кыргызский');
INSERT INTO language(name) VALUES('Лакский');
INSERT INTO language(name) VALUES('Лаосский');
INSERT INTO language(name) VALUES('Латинский');
INSERT INTO language(name) VALUES('Латышский');
INSERT INTO language(name) VALUES('Лезгинский');
INSERT INTO language(name) VALUES('Литовский');
INSERT INTO language(name) VALUES('Македонский');
INSERT INTO language(name) VALUES('Малазийский');
INSERT INTO language(name) VALUES('Мансийский');
INSERT INTO language(name) VALUES('Марийский');
INSERT INTO language(name) VALUES('Монгольский');
INSERT INTO language(name) VALUES('Непальский');
INSERT INTO language(name) VALUES('Ногайский');
INSERT INTO language(name) VALUES('Норвежский');
INSERT INTO language(name) VALUES('Осетинский');
INSERT INTO language(name) VALUES('Панджаби');
INSERT INTO language(name) VALUES('Персидский');
INSERT INTO language(name) VALUES('Польский');
INSERT INTO language(name) VALUES('Португальский');
INSERT INTO language(name) VALUES('Пушту');
INSERT INTO language(name) VALUES('Румынский');
INSERT INTO language(name) VALUES('Русский');
INSERT INTO language(name) VALUES('Санскрит');
INSERT INTO language(name) VALUES('Сербский');
INSERT INTO language(name) VALUES('Словацкий');
INSERT INTO language(name) VALUES('Словенский');
INSERT INTO language(name) VALUES('Сомалийский');
INSERT INTO language(name) VALUES('Суахили');
INSERT INTO language(name) VALUES('Тагальский');
INSERT INTO language(name) VALUES('Таджикский');
INSERT INTO language(name) VALUES('Тайский');
INSERT INTO language(name) VALUES('Талышский');
INSERT INTO language(name) VALUES('Тамильский');
INSERT INTO language(name) VALUES('Татарский');
INSERT INTO language(name) VALUES('Тибетский');
INSERT INTO language(name) VALUES('Тувинский');
INSERT INTO language(name) VALUES('Турецкий');
INSERT INTO language(name) VALUES('Туркменский');
INSERT INTO language(name) VALUES('Удмуртский');
INSERT INTO language(name) VALUES('Узбекский');
INSERT INTO language(name) VALUES('Уйгурский');
INSERT INTO language(name) VALUES('Украинский');
INSERT INTO language(name) VALUES('Урду');
INSERT INTO language(name) VALUES('Финский');
INSERT INTO language(name) VALUES('Фламандский');
INSERT INTO language(name) VALUES('Хинди');
INSERT INTO language(name) VALUES('Хорватский');
INSERT INTO language(name) VALUES('Чеченский');
INSERT INTO language(name) VALUES('Чешский');
INSERT INTO language(name) VALUES('Чувашский');
INSERT INTO language(name) VALUES('Шведский');
INSERT INTO language(name) VALUES('Эсперанто');
INSERT INTO language(name) VALUES('Эстонский');
INSERT INTO language(name) VALUES('Якутский');
INSERT INTO language(name) VALUES('Японский');

INSERT INTO COUNTRY(country_id, name, status) VALUES(1, 'Россия', 1);
INSERT INTO COUNTRY(country_id, name, status) VALUES(2, 'Украина', 2);
INSERT INTO COUNTRY(country_id, name, status) VALUES(3, 'Беларусь', 3);
INSERT INTO COUNTRY(country_id, name, status) VALUES(4, 'Казахстан', 4);
INSERT INTO COUNTRY(country_id, name, status) VALUES(5, 'Кыргызстан', 5);
INSERT INTO COUNTRY(country_id, name, status) VALUES(6, 'Узбекистан', 6);
INSERT INTO COUNTRY(country_id, name, status) VALUES(7, 'Молдавия', 7);
INSERT INTO COUNTRY(country_id, name, status) VALUES(8, 'Азербайджан', 8);
INSERT INTO COUNTRY(country_id, name, status) VALUES(9, 'Армения', 9);
INSERT INTO COUNTRY(country_id, name, status) VALUES(10, 'Грузия', 10);
INSERT INTO COUNTRY(country_id, name, status) VALUES(11, 'Таджикистан', 11);
INSERT INTO COUNTRY(country_id, name, status) VALUES(12, 'Туркменистан', 12);


INSERT INTO COUNTRY(country_id, name, status) VALUES( 13, 'Абхазия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 14, 'Австралия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 15, 'Австрия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 16, 'Албания', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 17, 'Алжир', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 18, 'Ангола', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 19, 'Андорра', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 20, 'Аргентина', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 21, 'Афганистан', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 22, 'Багамские Острова', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 23, 'Бангладеш', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 24, 'Бахрейн', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 25, 'Белиз', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 26, 'Бельгия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 27, 'Болгария', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 28, 'Боливия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 29, 'Босния и Герцеговина', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 30, 'Бразилия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 31, 'Буркина Фасо', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 32, 'Великобритания', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 33, 'Венгрия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 34, 'Венесуэла', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 35, 'Вьетнам', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 36, 'Габон', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 37, 'Гвинея', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 38, 'Германия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 39, 'Государство Палестина', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 40, 'Греция', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 41, 'Дания', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 42, 'Демократическая Республика Конго', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 43, 'Доминиканская Республика', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 44, 'Египет', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 45, 'Израиль', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 46, 'Индия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 47, 'Индонезия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 48, 'Иордания', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 49, 'Ирак', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 50, 'Иран', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 51, 'Ирландия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 52, 'Исландия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 53, 'Испания', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 54, 'Италия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 55, 'Йемен', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 56, 'Камбоджа', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 57, 'Канада', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 58, 'Катар', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 59, 'Кипр', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 60, 'Китай', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 61, 'Колумбия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 62, 'Кооперативная Республика Гайана', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 63, 'Королевство Саудовская Аравия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 64, 'Кувейт', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 65, 'Латвия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 66, 'Ливан', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 67, 'Ливия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 68, 'Литва', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 69, 'Лихтенштейн', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 70, 'Люксембург', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 71, 'Македония', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 72, 'Малайзия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 73, 'Мальдивская Республика', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 74, 'Мальта', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 75, 'Марокко', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 76, 'Мексика', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 77, 'Монако', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 78, 'Монголия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 79, 'Мьянма', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 80, 'Намибия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 81, 'Нигерия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 82, 'Нидерланды', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 83, 'Новая Зеландия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 84, 'Норвегия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 85, 'ОАЭ', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 86, 'Объединенная Республика Танзания', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 87, 'Оман', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 88, 'Пакистан', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 89, 'Панама', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 90, 'Перу', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 91, 'Польша', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 92, 'Португалия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 93, 'Республика Бенин', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 94, 'Республика Гана', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 95, 'Республика Гватемала', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 96, 'Республика Зимбабве', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 97, 'Республика Камерун', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 98, 'Республика Кения', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 99, 'Республика Конго', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 100, 'Республика Коста-Рика', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 101, 'Республика Кот-д’Ивуар', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 102, 'Республика Куба', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 103, 'Республика Либерия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 104, 'Республика Маврикий', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 105, 'Республика Мадагаскар', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 106, 'Республика Нигер', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 107, 'Республика Никарагуа', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 108, 'Республика Северный Судан', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 109, 'Республика Сейшельские острова', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 110, 'Республика Сенегал', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 111, 'Реюньон', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 112, 'Румыния', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 113, 'США', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 114, 'Сент-Винсент и Гренадины', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 115, 'Сербия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 116, 'Сингапур', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 117, 'Сирия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 118, 'Словакия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 119, 'Словения', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 120, 'Таиланд', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 121, 'Тайвань', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 122, 'Тунис', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 123, 'Турция', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 124, 'Уганда', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 125, 'Уругвай', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 126, 'Филиппины', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 127, 'Финляндия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 128, 'Франция', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 129, 'Хорватия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 130, 'Черногория', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 131, 'Чехия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 132, 'Чили', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 133, 'Швейцария', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 134, 'Швеция', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 135, 'Шотландия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 136, 'Шри-Ланка', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 137, 'Эквадор', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 138, 'Эстония', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 139, 'Эфиопия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 140, 'ЮАР', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 141, 'Южная Корея', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 142, 'Южная Осетия', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 143, 'Ямайка', 0);
INSERT INTO COUNTRY(country_id, name, status) VALUES( 144, 'Япония', 0);

INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 1, 'Алтайский край');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 2, 'Амурская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 3, 'Архангельская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 4, 'Астраханская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 5, 'Белгородская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 6, 'Брянская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 7, 'Владимирская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 8, 'Волгоградская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 9, 'Вологодская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 10, 'Воронежская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 11, 'Еврейская АО');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 12, 'Забайкальский край');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 13, 'Ивановская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 14, 'Иркутская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 15, 'Кабардино-Балкарская республика');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 16, 'Калининградская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 17, 'Калужская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 18, 'Камчатский край');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 19, 'Карачаево-Черкесская Республика');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 20, 'Кемеровская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 21, 'Кировская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 22, 'Костромская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 23, 'Краснодарский край');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 24, 'Красноярский край');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 25, 'Курганская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 26, 'Курская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 27, 'Ленинградская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 28, 'Липецкая область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 29, 'Магаданская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 30, 'Московская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 31, 'Мурманская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 32, 'Ненецкий АО');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 33, 'Нижегородская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 34, 'Новгородская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 35, 'Новосибирская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 36, 'Омская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 37, 'Оренбургская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 38, 'Орловская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 39, 'Пензенская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 40, 'Пермский край');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 41, 'Приморский край');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 42, 'Псковская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 43, 'Республика Адыгея');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 44, 'Республика Алтай');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 45, 'Республика Башкортостан');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 46, 'Республика Бурятия');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 47, 'Республика Дагестан');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 48, 'Республика Ингушетия');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 49, 'Республика Калмыкия');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 50, 'Республика Карелия');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 51, 'Республика Коми');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 52, 'Республика Крым');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 53, 'Республика Марий Эл');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 54, 'Республика Мордовия');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 55, 'Республика Саха (Якутия)');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 56, 'Республика Северная Осетия-Алания');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 57, 'Республика Татарстан');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 58, 'Республика Тыва');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 59, 'Республика Хакасия');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 60, 'Ростовская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 61, 'Рязанская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 62, 'Самарская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 63, 'Саратовская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 64, 'Сахалинская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 65, 'Свердловская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 66, 'Смоленская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 67, 'Ставропольский край');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 68, 'Тамбовская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 69, 'Тверская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 70, 'Томская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 71, 'Тульская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 72, 'Тюменская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 73, 'Удмуртская Республика');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 74, 'Ульяновская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 75, 'Хабаровский край');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 76, 'Ханты-Мансийский АО - Югра');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 77, 'Челябинская область');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 78, 'Чеченская республика');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 79, 'Чувашская Республика');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 80, 'Чукотский АО');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 81, 'Ямало-Ненецкий АО');
INSERT INTO region (COUNTRY_ID, REGION_ID, NAME) VALUES( 1, 82, 'Ярославская область');


INSERT INTO town (TOWN_ID, REGION_ID, COUNTRY_ID, NAME) VALUES( 1, 27, 1, 'Санкт-Петербург');
INSERT INTO town (TOWN_ID, REGION_ID, COUNTRY_ID, NAME) VALUES( 2, 30, 1, 'Москва');

INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Алейск');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Алтайское');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Баево');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Барнаул');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Белокуриха');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Белоярск');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Березовка (Алтайский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Бийск');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Благовещенка');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Боровиха');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Бурла');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Быстрый Исток');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Верх-Катунское');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Власиха (Алтайский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Волчиха');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Горняк');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Ельцовка');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Завьялово (Алтайский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Залесово');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Заринск');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Зелёная Дубрава');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Змеиногорск');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Зональное');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Зудилово');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Калманка');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Камень-на-Оби');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Ключи (Алтайский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Косиха');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Красногорское (Алтайский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Краснощёково');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Крутиха');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Кулунда');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Курья');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Кытманово');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Лебяжье (Алтайский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Леньки');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Малиновое Озеро');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Мамонтово');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Михайловское');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Налобиха');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Новичиха');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Новоалтайск');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Новоегорьевское');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Новые Зори');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Озерки');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Павловск');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Панкрушиха');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Первомайское (Алтайский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Петропавловское');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Поспелиха');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Ребриха');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Родино');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Романово');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Рубцовск');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Сибирский');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Славгород (Алтайский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Славгородское');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Смоленское');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Соколово');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Солонешное');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Солтон');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Сростки');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Староалейское');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Степное Озеро');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Табуны');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Тальменка');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Тогул');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Топчиха');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Троицкое (Алтайский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Тюменцево');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Угловское');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Усть-Калманка');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Усть-Чарышская Пристань');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Хабары');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Целинное');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Чарышское');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Черемное');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Шелаболиха');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Шипуново');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Южный (Алтайский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 1, 'Яровое');

INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Архара');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Белогорск');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Благовещенск (Амурская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Бурея (Амурская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Возжаевка');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Екатеринославка (Амурская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Ерофей Павлович');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Завитинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Зея');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Ивановка (Ивановский район, Амурская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Константиновка (Амурская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Магдагачи');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Новобурейский');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Поярково (Амурская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Прогресс (Амурская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Райчихинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Свободный');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Серышево');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Сковородино');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Талакан (Амурская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Тамбовка (Амурская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Тында');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Февральск (Амурская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Циолковский');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Чигири (Амурская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 2, 'Шимановск');

INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Архангельск');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Белушья Губа');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Березник');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Вельск');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Вычегодский');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Двинской');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Емца');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Ильинско-Подомское');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Каменка');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Каргополь');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Коноша');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Коряжма');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Котлас');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Красноборск');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Кулой');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Лешуконское');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Малошуйка');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Мезень');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Мирный (Архангельская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Новодвинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Няндома');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Обозерский');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Оксовский');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Октябрьский (Архангельская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Онега');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Плесецк');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Приводино');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Пуксоозера');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Савинский');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Самодед');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Северодвинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Североонежск');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Сольвычегодск');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Урдома');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Холмогоры');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Шенкурск');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Шипицыно');
INSERT INTO town (REGION_ID, NAME) VALUES( 3, 'Яренск');

INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Аксарайский');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Астрахань');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Ахтубинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Байбек');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Бахтемир');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Бирюковка (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Болхуны');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Буруны (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Верхний Баскунчак');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Волго-Каспийский');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Волжский (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Волжское (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Володарский');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Вольное (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Енотаевка');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Житное (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Зензели');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Знаменск');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Золотуха (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Икряное');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Ильинка (Икрянинский район)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Камызяк');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Капустин Яр');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Карагали');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Килинчи');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Кировский (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Кочковатка (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Красные Баррикады');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Красные Баррикады');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Красный Яр (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Лиман (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Марфино');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Мумра');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Нариманов');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Началово');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Нижний Баскунчак');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Никольское (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Ново-Николаевка (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Образцово-Травино');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Оранжереи');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Осыпной Бугор');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Прикаспийский');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Разночиновка');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Растопуловка');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Сасыколи');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Селитренное');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Солодники');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Солянка');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Солёное Займище (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Старица (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Старокучергановка');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Тамбовка (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Татарская Башмаковка');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Три Потока');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Трудфронт');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Тузуклей');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Тумак (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Ушаковка (Астраханская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Харабали');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Хошеутово');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Черный Яр');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Яксатово');
INSERT INTO town (REGION_ID, NAME) VALUES( 4, 'Яндыки');

INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Алексеевка');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Белгород');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Бессоновка');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Бирюч');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Борисовка');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Валуйки');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Вейделевка');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Волоконовка');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Головчино');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Грайворон');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Губкин');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Дубовое');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Ивня');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Короча');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Красная Яруга');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Ливенка');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Маслова Пристань');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Новый Оскол');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Пролетарский');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Прохоровка');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Разумное');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Ракитное (Белгородская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Северный');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Скородное');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Старый Оскол');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Стрелецкое');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Строитель (Белгородская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Томаровка');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Троицкий');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Уразово');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Чернянка');
INSERT INTO town (REGION_ID, NAME) VALUES( 5, 'Шебекино');

INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Алтухово (Брянская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Баклань (Брянская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Белая Берёзка');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Белые Берега');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Брянск');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Бытошь');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Выгоничи');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Вышков (Брянская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Глинищево');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Глоднево (Брянская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Гобики');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Гордеевка');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Десятуха');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Добрунь');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Дубровка');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Дятьково');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Жирятино');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Жуковка (Брянская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Злынка');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Ивот');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Карачев');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Клетня');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Климово');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Клинцы');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Кокино');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Кокоревка');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Комаричи');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Красная Гора');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Локоть');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Лопандино');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Любохна');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Мглин');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Меленск');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Мирный (Брянская область, Гордеевский район)');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Мичуринский (Брянская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Мишковка (Брянская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Навля');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Нивное (Брянская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Новозыбков');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Новый Ропск');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Овстуг');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Погар');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Почеп');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Ржаница');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Рогнедино');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Свень');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Севск');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Сельцо');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Сеща');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Синезерки');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Слободище (Брянская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Стародуб');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Старые Бобовичи');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Старь');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Суземка');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Супонево');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Сураж');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Троебортное');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Трубчевск');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Унеча');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Фокино (Брянская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 6, 'Чуровичи');

INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Александров');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Балакирево');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Владимир');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Вольгинский');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Вязники');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Гороховец');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Гусь-Хрустальный');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Камешково');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Карабаново');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Киржач');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Ковров');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Кольчугино');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Костерево');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Красная Горбатка');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Красный Октябрь');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Курлово');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Лакинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Меленки');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Муром');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Петушки');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Покров');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Радужный (Владимирская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Собинка');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Ставрово');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Струнино');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Судогда');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Суздаль');
INSERT INTO town (REGION_ID, NAME) VALUES( 7, 'Юрьев-Польский');

INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Александровка');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Алексеевская');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Антиповка');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Береславка');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Большие Чапурники');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Большой Лычак');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Быково');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Ветютнев');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Волгоград');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Волжский');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Городище');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Гуляевка (Волгоградская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Даниловка');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Дубовка');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Дудаченский');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Елань');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Ерзовка');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Жирновск');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Заплавное');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Зензеватка');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Иловля');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Калач-на-Дону');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Камышин');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Кирпичный (Волгоградская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Кислово');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Клетская');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Колобордов');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Котельниково');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Котово');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Краснооктябрьский (Волгоградская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Краснослободск (Волгоградская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Красные Липки');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Красный Яр');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Кумылженская');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Ленинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Линево');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Лог');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Лычак');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Малодельская');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Медведицкий');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Михайловка (Волгоградская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Нехаевская');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Нижний Чир');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Николаевск');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Новая Паника');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Новоаннинский');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Новониколаевский (Волгоградская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Новый Рогачик');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Образцы');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Октябрьский (Волгоградская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Ольховка');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Палласовка');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Петров Вал');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Писарёвка (Волгоградская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Преображенская');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Пригородный (Волгоградская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Приморск (Волгоградская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Рубёжный');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Рудня (Волгоградская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Рябовский');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Савинка');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Светлый Яр');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Себрово');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Серафимович');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Среднецарицынский');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Средняя Ахтуба');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Старая Полтавка');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Суровикино');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Урюпинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Фролово');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Чернышковский');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Школьный (Волгоградская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 8, 'Шуруповский');

INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Анненский Мост');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Бабаево');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Белозерск');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Великий Устюг');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Верховажье');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Вожега');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Вологда');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Вохтога');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Вытегра');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Грязовец');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Депо');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Кадников');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Кадниковский');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Кадуй');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Кириллов');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Кичменгский Городок');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Климовское');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Красавино');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Кубенское');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Липин Бор');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Малечкино');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Молочное (Вологодская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Никольск (Вологодская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Новатор');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Нюксеница');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Сокол');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Сямжа');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Тарногский Городок');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Тоншалово');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Тотьма');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Устюжна');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Федотово');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Харовск');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Чагода');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Череповец');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Шексна');
INSERT INTO town (REGION_ID, NAME) VALUES( 9, 'Шуйское');

INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Абрамовка');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Анна');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Бабяково');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Бобров');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Богучар');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Борисоглебск');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Бутурлиновка');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Верхний Мамон');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Верхняя Хава');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Воля');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Воробьёвка');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Воронеж');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Воронцовка');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Гремячье');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Грибановский');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Давыдовка');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Девица');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Елань-Колено');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Землянск');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Калач');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Каменка (Воронежская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Кантемировка');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Каширское');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Колодезный');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Латная');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Лиски');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Лосево');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Митрофановка');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Нижнедевицк');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Нижний Кисляй');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Никольское (Воронежская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Новая Усмань');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Нововоронеж');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Новохоперск');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Новохопёрск');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Ольховатка');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Орлово');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Острогожск');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Отрадное (Воронежская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Павловск (Воронежская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Панино');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Пески');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Петропавловка (Воронежская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Поворино');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Подгоренский');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Рамонь');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Репьевка');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Россошь');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Семилуки');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Солнечный (Воронежская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Сомово');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Средний Икорец');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Стрелица');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Таловая');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Тамбовка');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Терновка (Воронежская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Углянец');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Хохол');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Хохольский');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'Эртиль');
INSERT INTO town (REGION_ID, NAME) VALUES( 10, 'свх Масловский');

INSERT INTO town (REGION_ID, NAME) VALUES( 11, 'Амурзет');
INSERT INTO town (REGION_ID, NAME) VALUES( 11, 'Бабстово');
INSERT INTO town (REGION_ID, NAME) VALUES( 11, 'Биракан');
INSERT INTO town (REGION_ID, NAME) VALUES( 11, 'Биробиджан');
INSERT INTO town (REGION_ID, NAME) VALUES( 11, 'Ленинское (Еврейская АО)');
INSERT INTO town (REGION_ID, NAME) VALUES( 11, 'Николаевка (Еврейская АО)');
INSERT INTO town (REGION_ID, NAME) VALUES( 11, 'Облучье');
INSERT INTO town (REGION_ID, NAME) VALUES( 11, 'Приамурский');
INSERT INTO town (REGION_ID, NAME) VALUES( 11, 'Птичник (Еврейская АО)');
INSERT INTO town (REGION_ID, NAME) VALUES( 11, 'Смидович (Еврейская АО)');
INSERT INTO town (REGION_ID, NAME) VALUES( 11, 'Теплоозёрск');

INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Агинское (Забайкальский АО)');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Акша (Забайкальский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Александровский Завод');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Атамановка (Читинский район)');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Балей');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Борзя');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Верх-Усугли');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Вершино-Дарасунский');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Газимурский Завод');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Горный (Забайкальский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Дарасун (Забайкальский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Домна (Забайкальский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Дульдурга');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Забайкальск');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Калга');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Карымское');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Кокуй (Забайкальский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Краснокаменск');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Красный Чикой');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Кыра');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Могойтуй');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Могоча');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Нерчинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Нерчинский Завод');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Нижний Цасучей');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Новая Чара');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Новокручининский');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Оловянная');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Первомайский (Забайкальский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Петровск-Забайкальский');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Приаргунск');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Сретенск');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Улёты');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Хилок');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Холбон');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Чернышевск');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Чита');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Шелопугино (Забайкальский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Шерловая Гора');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Шилка');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Ясная (Забайкальский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 12, 'Ясногорск (Забайкальский край)');

INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Вичуга');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Выползово');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Гаврилов Посад');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Жарковский');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Заволжск');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Иваново (Ивановская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Ильинское-Хованское');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Калашниково');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Кинешма');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Комсомольск (Ивановская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Кохма');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Лежнево');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Лесное');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Лух');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Медное');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Наволоки');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Оленино');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Пено');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Пестяки');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Плес');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Приволжск');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Пучеж');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Редкино');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Родники');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Савино');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Селижарово');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Спирово');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Тейково');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Фурманов');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Шуя');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Южа');
INSERT INTO town (REGION_ID, NAME) VALUES( 13, 'Юрьевец');

INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Алексеевск');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Алзамай');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Ангарск');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Байкальск');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Балаганск');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Белореченский');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Бирюсинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Бодайбо');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Большой Луг');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Бохан');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Братск');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Вихоревка');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Гидростроитель');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Еланцы');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Железногорск-Илимский');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Железнодорожный (Иркутская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Жигалово');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Залари');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Зима');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Иркутск');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Качуг');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Квиток');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Киренск');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Куйтун');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Култук');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Кутулик');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Лесогорск');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Магистральный');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Мама');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Маркова');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Мегет');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Михайловка');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Мишелевка');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Нижнеудинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Новая Игирма');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Новобирюсинский');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Новонукутский');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Оек');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Октябрьский (Иркутская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Оса (Иркутская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Рудногорск');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Саянск');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Свирск');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Слюдянка');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Средний');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Тайтурка');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Тайшет');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Тельма');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Тулун');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Тулюшка');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Тыреть 1-я');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Улькан');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Усолье-Сибирское');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Усть-Илимск');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Усть-Кут');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Усть-Ордынский');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Усть-Уда');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Хомутово (Иркутская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Черемушки');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Черемхово');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Чунский');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Шелехов');
INSERT INTO town (REGION_ID, NAME) VALUES( 14, 'Юрты');

INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Алтуд');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Анзорей');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Аргудан');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Баксан');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Баксаненок');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Дугулубгей');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Екатериноградская');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Залукокоаже');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Заюково');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Исламей');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Каменка (Кабардино-Балкария)');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Карагач (Кабардино-Балкария)');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Кашхтау');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Кенже');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Кишпек');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Куба (Кабардино-Балкария)');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Майский');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Малка');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Нальчик');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Нартан');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Нарткала');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Прохладный');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Сармаково');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Солдатская');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Терек');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Тырныауз');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Хасанья');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Чегем');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Чегем Второй');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Шалушка');
INSERT INTO town (REGION_ID, NAME) VALUES( 15, 'Эльбрус (Кабардино-Балкария)');

INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Багратионовск');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Балтийск');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Гвардейск');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Гурьевск (Калининградская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Гусев');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Зеленоградск');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Калининград');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Краснознаменск (Калининградская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Ладушкин');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Мамоново');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Неман');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Нестеров');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Озерск (Калининградская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Пионерский');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Полесск');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Правдинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Приморск');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Светлогорск (Калининградская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Светлый');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Славск');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Советск (Калининградская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 16, 'Черняховск');

INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Бабынино (Калужская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Балабаново');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Барятино (Калужская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Белоусово');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Бетлица');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Боровск');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Воротынск');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Ворсино (Калужская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Детчино');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Думиничи');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Ермолино');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Жиздра');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Жилетово');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Жуков');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Износки');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Калуга');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Киров (Калужская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Козельск');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Кондрово');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Кременки');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Людиново');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Малоярославец');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Медынь');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Мещовск');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Мосальск');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Обнинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Перемышль');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Полотняный Завод');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Пятовский');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Сосенский');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Спас-Деменск');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Сухиничи');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Таруса');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Товарково (Калужская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Ульяново (Калужская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Ферзиково');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Хвастовичи (Калужская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 17, 'Юхнов');

INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Вилючинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Вулканный');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Елизово');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Ключи (Камчатский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Козыревск');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Коряки (Камчатский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Мильково (Камчатский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Нагорный (Камчатский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Николаевка (Камчатский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Новый (Камчатский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Озерновский');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Октябрьский (Камчатский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Оссора');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Палана');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Паратунка');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Петропавловск-Камчатский');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Пионерский (Камчатский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Раздольный (Камчатский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Соболево (Камчатский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Термальный');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Тигиль');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Тиличики');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Усть-Большерецк');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Усть-Камчатск');
INSERT INTO town (REGION_ID, NAME) VALUES( 18, 'Эссо');

INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Адыге-Хабль');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Али-Бердуковский');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Зеленчукская');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Икон-Халк');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Исправная');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Карачаевск');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Кардоникская');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Красный Курган (Карачаево-Черкесия)');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Кумыш');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Курджиново');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Медногорский');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Орджоникидзевский');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Первомайское (Карачаево-Черкесия)');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Преградная');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Псыж');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Сары-Тюз');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Сторожевая (Карачаево-Черкесия)');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Теберда');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Терезе');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Усть-Джегута');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Учкекен');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Хабез');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Чапаевское (Карачаево-Черкесия)');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Черкесск');
INSERT INTO town (REGION_ID, NAME) VALUES( 19, 'Эркин-Шахар');

INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Анжеро-Судженск');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Артышта');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Бачатский');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Белово');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Белогорск (Кемеровская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Березовский (Кемеровская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Верх-Чебула');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Грамотеино');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Гурьевск (Кемеровская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Зеленогорский');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Ижморский');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Инской');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Итатский');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Каз');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Калтан');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Карагайлинский');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Кедровка');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Кемерово');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Киселевск');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Крапивинский');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Краснобродский');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Красногорский (Кемеровская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Кузедеево');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Ленинск-Кузнецкий');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Малиновка');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Мариинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Междуреченск');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Мундыбаш');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Мыски');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Новокузнецк');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Новый Городок');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Осинники');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Плотниково');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Полысаево');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Прокопьевск');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Промышленная');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Салаир');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Солнечный (Кемеровская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Старобачаты');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Старопестерево');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Тайга');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Тайжина');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Таштагол');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Темиртау (Кемеровская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Тисуль');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Топки');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Трудармейский');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Тяжинский');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Чистогорский');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Шерегеш');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Юрга');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Ясногорский');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Яшкино');
INSERT INTO town (REGION_ID, NAME) VALUES( 20, 'Яя');

INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Арбаж');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Афанасьево (Кировская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Белая Холуница');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Богородское');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Вахруши');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Верхошижемье');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Восточный (Кировская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Вятские Поляны');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Даровской');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Демьяново');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Зуевка');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Кикнур');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Кильмезь (Кировская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Киров');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Кирово-Чепецк');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Кирс');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Котельнич');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Красная Поляна (Кировская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Кумены');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Лальск');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Лебяжье (Кировская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Ленинское');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Лесной (Кировская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Лойно');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Луза');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Малмыж');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Мирный');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Мураши');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Мурыгино');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Нагорск');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Нема');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Нолинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Омутнинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Опарино');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Оричи');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Орлов');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Первомайский (Кировская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Песковка');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Пижанка');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Подосиновец');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Рудничный');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Санчурск');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Светлополянск');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Свеча');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Слободской');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Советск (Кировская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Сосновка');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Стрижи');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Тужа');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Уни');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Уржум');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Фалёнки');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Юрья');
INSERT INTO town (REGION_ID, NAME) VALUES( 21, 'Яранск');

INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Антропово');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Боговарово');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Буй');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Ветлужский');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Волгореченск');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Вохма');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Галич (Костромская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Кадый');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Караваево');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Кологрив');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Кострома');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Красное-на-Волге');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Макарьев');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Мантурово');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Нерехта');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Нея');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Никольское (Костромская область)');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Островское');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Павино');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Парфентьево');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Поназырево');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Пыщуг');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Солигалич');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Судиславль');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Сусанино');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Чистые Боры');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Чухлома');
INSERT INTO town (REGION_ID, NAME) VALUES( 22, 'Шарья');

INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Абинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Абрау-Дюрсо');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Агой');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Агроном (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Адагум');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Адлер');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Азовская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Александровская (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Алексеевская (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Анапа');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Анапская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Анастасиевская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Андрюки');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Апшеронск');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Армавир');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Архангельская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Архипо-Осиповка');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Атаманская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Афипский');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Ахтанизовская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Ахтарский');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Ахтырский');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Баговская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Батуринская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Белая Глина');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Белозерный');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Белореченск');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Березанская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Берёзовый (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Бесскорбная');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Бриньковская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Брюховецкая станица');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Варениковская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Васюринская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Великовечное');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Венцы (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Верхнебаканский');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Весёлое (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Виноградный (Крымский район)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Виноградный (Темрюкский район)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Виноградный (муниципальное образование город-курорт Анапа)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Витязево');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Владимирская (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Вознесенская (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Воронежская (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Выселки');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Высокое (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Вышестеблиевская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Гайдук');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Геленджик');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Гирей');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Глебовка (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Глубокий (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Голубицкая');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Горное Лоо');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Горячий Ключ');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Гостагаевская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Гривенская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Губская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Гулькевичи');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Дагомыс');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Двубратский');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Джигинка');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Джубга');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Дивноморское');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Динская станица');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Дмитриевская (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Днепровская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Должанская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Дядьковская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Ейск');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Екатериновка (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Елизаветинская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Заветный');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Ивановская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Ильинская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Ильский');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Индустриальный (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Кабардинка');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Кавказская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Казанская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Калининская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Калниболотская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Калужская (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Камышеватская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Каневская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Киевское (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Кирпильская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Кисляковская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Ковалевское (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Коноково');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Константиновская (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Копанская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Кореновск');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Коржевский (Славянский район)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Красная Поляна');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Краснодар');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Красное (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Красносельский  (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Кропоткин');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Крыловская станица');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Крымск');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Кубанский (Новопокровский район)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Курганинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Курчанская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Кущевская станица');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Лабинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Ладожская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Лазаревское');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Ленинградская станица');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Львовское');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Майкопское');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Марьянская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Медведовская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Мингрельская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Мирской (Кавказский район)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Михайловская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Мостовской');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Мысхако');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Натухаевская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Небуг');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Незамаевская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Некрасовская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Нефтегорск (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Нижнебаканская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Нижняя Шиловка');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новоалексеевская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новобейсугская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Нововеличковская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новодеревянковская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новоджерелиевская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новодмитриевская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новокорсунская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новокубанск');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новолабинская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новолеушковская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новомалороссийская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новоминская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новомихайловский');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новомышастовская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новониколаевская (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новоплатнировская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новопокровская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новорождественская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новороссийск');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новотитаровская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новоукраинское');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Новощербиновская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Октябрьская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Ольгинская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Орел-Изумруд (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Отрадная');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Отрадо-Кубанское');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Отрадо-Ольгинское');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Павловская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Парковый');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Передовая (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Переправная');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Переясловская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Петровская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Петропавловская (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Пластуновская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Платнировская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Полтавская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Попутная');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Привольная (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Прикубанский');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Приморско-Ахтарск');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Прочноокопская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Псебай');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Пшехская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Раевская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Раздольная (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Роговская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Родниковская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Рязанская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Саратовская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Северо-Енисейский');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Северская станица');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Сенной (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Славянск-на-Кубани');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Смоленская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Советская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Совхозный (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Соколовское (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Сочи');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Спокойная');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Ставропольская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Старая Станица (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Старовеличковская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Стародеревянковская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Староджерелиевская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Старокорсунская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Старолеушковская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Староминская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Старомышастовская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Старонижестеблиевская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Старотитаровская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Старощербиновская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Стрелка');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Супсех');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Тамань');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Тбилисская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Темижбекская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Темиргоевская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Темрюк');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Терновская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Тимашевск');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Тихорецк');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Троицкая');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Трудобеликовский');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Туапсе');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Удобная');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Упорная');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Успенское');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Усть-Лабинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Фастовецкая');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Хадыженск');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Холмская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Цибанобалка');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Чамлыкская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Челбасская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Черноерковская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Черноморский');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Шедок');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Шепси');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Широчанка');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Шкуринская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Эстосадок');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Южный (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Юровка (Краснодарский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Ярославская');
INSERT INTO town (REGION_ID, NAME) VALUES( 23, 'Ясенская');

INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Абан');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Агинское');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Артемовск (Красноярский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Ачинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Байкит');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Балахта(Красноярский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Березовка');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Боготол');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Богучаны');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Большая Ирба');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Большая Мурта');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Большой Улуй');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Бородино');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Ванавара');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Дзержинское');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Дивногорск');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Диксон');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Дубинино');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Дудинка (Красноярский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Емельяново');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Енисейск');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Ермаковское');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Железногорск (Красноярский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Заозерный');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Зеленогорск (Красноярский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Зыково');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Игарка');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Идринское');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Иланский');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Ирбейское');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Казачинское');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Кайеркан');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Канск');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Каратузское');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Кедровый (Красноярский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Кодинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Козулька');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Кошурниково');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Краснокаменск (Красноярский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Краснотуранск');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Красноярск');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Курагино');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Лесосибирск');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Минусинск');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Мотыгино');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Назарово');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Нижний Ингаш');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Нижняя Пойма');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Новобирилюссы');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Новосёлово');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Новочернореченский');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Норильск');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Октябрьский (Красноярский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Партизанское');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Саянский');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Северо-Енисейский (Красноярский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Сосновоборск');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Сухобузимское');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Таежный');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Тасеево');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Тура (Красноярский край)');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Туруханск');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Тюхтет');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Ужур');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Уяр');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Хатанга');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Чунояр');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Шалинское');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Шарыпово');
INSERT INTO town (REGION_ID, NAME) VALUES( 24, 'Шушенское');

INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,29,'Автозаводская',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,101,'Академическая',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,54,'Александровский сад',4);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,91,'Алексеевская',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,540,'Алма-Атинская',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,129,'Алтуфьево',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,589,'Андроновка',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,163,'Аннино',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,44,'Арбатская',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,37,'Аэропорт',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,87,'Бабушкинская',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,61,'Багратионовская',4);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,604,'Балтийская',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,120,'Баррикадная',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,47,'Бауманская',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,122,'Беговая',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,585,'Белокаменная',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,35,'Белорусская',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,105,'Беляево',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,130,'Бибирево',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,11,'Библиотека им. Ленина',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,552,'Битцевский парк',12);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,536,'Борисово',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,140,'Боровицкая',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,89,'Ботанический сад',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,158,'Братиславская',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,193,'Бульвар адмирала Ушакова',12);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,164,'Бульвар Дмитрия Донского',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,1,'Бульвар Рокоссовского',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,195,'Бунинская аллея',12);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,578,'Бутырская',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,27,'Варшавская',11);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,90,'ВДНХ',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,594,'Верхние Котлы',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,610,'Верхние Лихоборы',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,132,'Владыкино',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,40,'Водный стадион',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,39,'Войковская',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,114,'Волгоградский проспект',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,156,'Волжская',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,524,'Волоколамская',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,16,'Воробьёвы горы',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,197,'Выставочная',4);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,513,'Выставочный центр',17);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,110,'Выхино',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,555,'Деловой центр',8);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,598,'Деловой центр (МЦК)',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,36,'Динамо',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,135,'Дмитровская',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,74,'Добрынинская',5);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,21,'Домодедовская',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,526,'Достоевская',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,161,'Дубровка',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,551,'Жулебино',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,593,'ЗИЛ',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,601,'Зорге',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,534,'Зябликово',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,587,'Измайлово',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,51,'Измайловская',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,104,'Калужская',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,24,'Кантемировская',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,26,'Каховская',11);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,25,'Каширская',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,42,'Киевская',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,96,'Китай-город',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,154,'Кожуховская',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,28,'Коломенская',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,6,'Комсомольская',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,106,'Коньково',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,605,'Коптево',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,571,'Котельники',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,20,'Красногвардейская',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,78,'Краснопресненская',5);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,5,'Красносельская',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,7,'Красные ворота',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,153,'Крестьянская застава',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,12,'Кропоткинская',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,66,'Крылатское',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,595,'Крымская',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,118,'Кузнецкий мост',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,112,'Кузьминки',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,64,'Кунцевская',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,46,'Курская',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,59,'Кутузовская',4);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,100,'Ленинский проспект',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,550,'Лермонтовский проспект',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,553,'Лесопарковая',12);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,606,'Лихоборы',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,586,'Локомотив',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,576,'Ломоносовский проспект',8);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,9,'Лубянка',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,597,'Лужники',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,157,'Люблино',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,84,'Марксистская',8);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,527,'Марьина роща',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,159,'Марьино',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,34,'Маяковская',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,86,'Медведково',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,196,'Международная',4);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,137,'Менделеевская',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,575,'Минская',8);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,525,'Митино',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,65,'Молодежная',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,523,'Мякинино',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,144,'Нагатинская',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,145,'Нагорная',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,146,'Нахимовский проспект',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,590,'Нижегородская',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,79,'Новогиреево',8);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,542,'Новокосино',8);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,31,'Новокузнецкая',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,68,'Новослободская',5);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,591,'Новохохловская',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,109,'Новоясеневская',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,103,'Новые Черемушки',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,583,'Окружная',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,98,'Октябрьская',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,124,'Октябрьское поле',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,22,'Орехово',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,131,'Отрадное',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,10,'Охотный ряд',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,30,'Павелецкая',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,602,'Панфиловская',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,13,'Парк Культуры',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,165,'Парк Победы',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,50,'Партизанская',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,52,'Первомайская',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,80,'Перово',8);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,582,'Петровский парк',45);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,133,'Петровско-Разумовская',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,155,'Печатники',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,63,'Пионерская',4);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,128,'Планерная',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,596,'Площадь Гагарина',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,83,'Площадь Ильича',8);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,45,'Площадь Революции',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,123,'Полежаевская',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,141,'Полянка',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,150,'Пражская',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,3,'Преображенская площадь',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,115,'Пролетарская',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,18,'Проспект Вернадского',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,93,'Проспект Мира',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,102,'Профсоюзная',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,119,'Пушкинская',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,541,'Пятницкое Шоссе',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,577,'Раменки',8);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,41,'Речной вокзал',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,92,'Рижская',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,152,'Римская',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,584,'Ростокино',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,572,'Румянцево',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,111,'Рязанский проспект',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,136,'Савеловская',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,574,'Саларьево',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,88,'Свиблово',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,147,'Севастопольская',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,611,'Селигерская',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,49,'Семеновская',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,142,'Серпуховская',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,201,'Славянский бульвар',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,43,'Смоленская',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,38,'Сокол',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,588,'Соколиная Гора',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,4,'Сокольники',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,554,'Спартак',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,15,'Спортивная',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,199,'Сретенский бульвар',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,603,'Стрешнево',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,198,'Строгино',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,58,'Студенческая',4);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,94,'Сухаревская',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,127,'Сходненская',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,72,'Таганская',5);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,33,'Тверская',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,32,'Театральная',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,113,'Текстильщики',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,511,'Телецентр',17);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,107,'Теплый Стан',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,573,'Технопарк',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,134,'Тимирязевская',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,85,'Третьяковская',8);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,570,'Тропарево',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,200,'Трубная',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,143,'Тульская',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,95,'Тургеневская',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,126,'Тушинская',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,592,'Угрешская',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,121,'Улица 1905 года',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,512,'Улица Академика Королева',17);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,162,'Улица Академика Янгеля',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,194,'Улица Горчакова',12);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,510,'Улица Милашенкова',17);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,514,'Улица Сергея Эйзенштейна',17);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,192,'Улица Скобелевская',12);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,191,'Улица Старокачаловская',12);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,17,'Университет',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,62,'Филевский парк',4);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,60,'Фили',4);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,579,'Фонвизинская',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,14,'Фрунзенская',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,608,'Ховрино',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,600,'Хорошёво',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,580,'Хорошёвская',45);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,23,'Царицыно',2);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,138,'Цветной бульвар',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,581,'ЦСКА',45);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,2,'Черкизовская',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,148,'Чертановская',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,139,'Чеховская',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,8,'Чистые пруды',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,151,'Чкаловская',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,99,'Шаболовская',6);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,599,'Шелепиха',44);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,535,'Шипиловская',10);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,81,'Шоссе Энтузиастов',8);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,53,'Щелковская',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,125,'Щукинская',7);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,48,'Электрозаводская',3);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,19,'Юго-Западная',1);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,149,'Южная',9);
INSERT INTO metro(TOWN_ID, METRO_ID, NAME, LINE_ID) VALUES(2,108,'Ясенево',6);


--Чистим базу
DELETE FROM demand_candidate;
DELETE FROM candidate;
DELETE FROM demand_note;
DELETE FROM demand;
DELETE FROM candidate_note;
UPDATE department
SET head_id = null;
DELETE FROM user_role;
DELETE FROM users;
DELETE FROM roles;
DELETE FROM person WHERE person_id <> 0;
DELETE FROM department WHERE department_id <> 0;
DELETE FROM company WHERE company_type_id = 0;

DELETE FROM employment;

DELETE FROM education;
DELETE FROM education_type;

--Типы образования
INSERT INTO education_type(education_type_id, education_type_name)
VALUES(0, 'Без образования');
INSERT INTO education_type(education_type_id, education_type_name)
VALUES(1, 'Начальная школа');
INSERT INTO education_type(education_type_id, education_type_name)
VALUES(2, 'Средняя школа');
SET @education_type_school = 0;
INSERT INTO education_type(education_type_id, education_type_name)
VALUES(3, 'Среднее специальное');
INSERT INTO education_type(education_type_id, education_type_name)
VALUES(4, 'Высшая школа');
SET @education_type_high_school = 1;
INSERT INTO education_type(education_type_id, education_type_name)
VALUES(5, 'Бакалавр');
INSERT INTO education_type(education_type_id, education_type_name)
VALUES(6, 'Магистр');
INSERT INTO education_type(education_type_id, education_type_name)
VALUES(7, 'Кандидат');
INSERT INTO education_type(education_type_id, education_type_name)
VALUES(8, 'Доктор');
INSERT INTO education_type(education_type_id, education_type_name)
VALUES(9, 'Курсы');
SET @education_type_course = 2;
INSERT INTO education_type(education_type_id, education_type_name)
VALUES(10, 'Неоконченное высшее');

--Добавляем компании
INSERT INTO company(name, address, company_type_id, phone, is_active, created_by)
VALUES('ООО Василек', 'д. Волобуйки', 0, '+7 (495) 201-11-11', true, 0);
SELECT SET(@company_1, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO company(name, address, company_type_id, phone, is_active, created_by)
VALUES('ОАО Лопушки', 'г. Усть-Пердюск ул. Узкая д.14', 0, '+7 (495) 201-22-22', true, 0);
SELECT SET(@company_2, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO company(name, address, company_type_id, phone, is_active, created_by)
VALUES('ГУП Связь', 'г. Москва ул. Тверская д.31', 0, '+7 (495) 201-33-33', true, 0);
SELECT SET(@company_3, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO company(name, address, company_type_id, phone, is_active, created_by)
VALUES('Колхоз Заветы Ильича', 'д. Красный луч', 0, '+7 (495) 201-44-44', true, 0);
SELECT SET(@company_4, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO company(name, address, company_type_id, phone, is_active, created_by)
VALUES('Ветеринарная клиника "Суки и козлы"', 'г. Ленинград ул. Фонтанка 11', 0, '+7 (495) 201-55-55', true, 0);
SELECT SET(@company_5, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO company(name, address, company_type_id, phone, is_active, created_by)
VALUES('Продуктовый магазин', 'г. Волобуевск ул. Узкая 17', 0, '+7 (495) 201-66-66', true, 0);
SELECT SET(@company_6, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO picked_company(COMPANY_ID, PERSON_ID)
VALUES(@company_1, 0);
INSERT INTO picked_company(COMPANY_ID, PERSON_ID)
VALUES(@company_2, 0);

--Добавляем отделы
INSERT INTO department(created_by,company_id, name, phone)
VALUES(@main_person, @company_1, 'Подсобное хозяйство', '+7 (925) 111-11-11');
SELECT SET(@department_1, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO department(created_by,company_id, name, phone)
VALUES(@main_person, @company_1, 'Департамент управления', '+7 (925) 222-22-22');
SELECT SET(@department_2, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO department(created_by,company_id, name, phone)
VALUES(@main_person, @company_2, 'Цех №1', '+7 (925) 333-33-33');
SELECT SET(@department_3, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO department(created_by,company_id, name, phone)
VALUES(@main_person, @company_2, 'Отдел кадров', '+7 (925) 444-44-44');
SELECT SET(@department_4, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO department(created_by,company_id, name, phone)
VALUES(@main_person, @company_3, 'Бухгалтерия', '+7 (925) 555-55-55');
SELECT SET(@department_5, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

--Добавляем сотрудников
--Департамент управления
INSERT INTO person(created_by,FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, PHONE, ADDRESS, START_DATE, END_DATE, IS_ACTIVE, DEPARTMENT_ID, POSITION)
VALUES(@main_person, 'Поликарп', 'Варфаламеевич', 'Перендюгин', DATE '1955-01-31', '+7 (495) 100-11-11', 'д.Малые Волобуйки', DATE '2001-01-31', NULL, TRUE, @department_2, 'Директор');
SELECT SET(@person_1, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO person(created_by,FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, PHONE, ADDRESS, START_DATE, END_DATE, IS_ACTIVE, DEPARTMENT_ID, POSITION)
VALUES(@main_person, 'Акакий', 'Владиславович', 'Жуйский', DATE '1958-04-27', '+7 (495) 101-22-22', 'г.Большие Волобуйки', DATE '1999-10-04', NULL, TRUE, @department_2, 'Главбух');
SELECT SET(@person_2, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
--Подсобное хозяйство
INSERT INTO person(created_by,FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, PHONE, ADDRESS, START_DATE, END_DATE, IS_ACTIVE, DEPARTMENT_ID, POSITION)
VALUES(@main_person, 'Карп', 'Трофимович', 'Лысухин', DATE '1962-10-12', '+7 (495) 101-77-77', 'д. Париж', DATE '1981-04-09', NULL, TRUE, @department_4, 'Главный конюх');
SELECT SET(@person_3, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO person(created_by,FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, PHONE, ADDRESS, START_DATE, END_DATE, IS_ACTIVE, DEPARTMENT_ID, POSITION)
VALUES(@main_person, 'Аристарх', 'Феофанович', 'Лаптев', DATE '1964-06-15', '+7 (495) 101-33-33', 'пгт. Ромашки', DATE '1998-11-21', NULL, TRUE, @department_1, 'Скотник');
SELECT SET(@person_4, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO person(created_by,FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, PHONE, ADDRESS, START_DATE, END_DATE, IS_ACTIVE, DEPARTMENT_ID, POSITION)
VALUES(@main_person, 'Захар', 'Васильевич', 'Выпердулькин', DATE '1969-07-15', '+7 (495) 101-44-44', 'пгт. Дальние ебеня', DATE '1991-11-27', NULL, TRUE, @department_1, 'Конюх');
SELECT SET(@person_5, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
--Цех №1
INSERT INTO person(created_by,FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, PHONE, ADDRESS, START_DATE, END_DATE, IS_ACTIVE, DEPARTMENT_ID)
VALUES(@main_person, 'Клавдия', 'Захаровна', 'Обойкина', DATE '1952-01-16', '+7 (495) 101-55-55', 'пгт. Ближние ебеня', DATE '1977-02-27', NULL, TRUE, @department_3);
SELECT SET(@person_6, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
--Отдел кадров
INSERT INTO person(created_by,FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, PHONE, ADDRESS, START_DATE, END_DATE, IS_ACTIVE, DEPARTMENT_ID, POSITION)
VALUES(@main_person, 'Сергей', 'Георгиевич', 'Бубукин', DATE '1957-11-21', '+7 (495) 101-66-66', 'д. Париж', DATE '1979-03-08', NULL, TRUE, @department_4, 'Начальник отдела кадров');
SELECT SET(@person_7, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO person(created_by,FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, PHONE, ADDRESS, START_DATE, END_DATE, IS_ACTIVE, DEPARTMENT_ID)
VALUES(@main_person, 'Михаил', 'Тимофеич', 'Пережопкин', DATE '1962-10-12', '+7 (495) 101-77-77', 'д. Париж', DATE '1981-04-09', NULL, TRUE, @department_4);
SELECT SET(@person_8, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO person(created_by,FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, PHONE, ADDRESS, START_DATE, END_DATE, IS_ACTIVE, DEPARTMENT_ID)
VALUES(@main_person, 'Серафим', 'Архангелович', 'Попов-Зазаборский', DATE '1962-10-12', '+7 (495) 101-77-77', 'д. Париж', DATE '1981-04-09', NULL, TRUE, @department_4);
SELECT SET(@person_9, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO person(created_by,FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, PHONE, ADDRESS, START_DATE, END_DATE, IS_ACTIVE, DEPARTMENT_ID)
VALUES(@main_person, 'Петр', 'Савельевич', 'Попадьин', DATE '1962-10-12', '+7 (495) 101-77-77', 'д. Париж', DATE '1981-04-09', NULL, TRUE, @department_4);
SELECT SET(@person_10, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO person(created_by,FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, PHONE, ADDRESS, START_DATE, END_DATE, IS_ACTIVE, DEPARTMENT_ID)
VALUES(@main_person, 'Христофор', 'Мамедович', 'Перепелкин', DATE '1962-10-12', '+7 (495) 101-77-77', 'д. Париж', DATE '1981-04-09', NULL, TRUE, @department_4);
SELECT SET(@person_11, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

UPDATE department
SET head_id = @person_1
WHERE department_id = @department_2;
UPDATE department
SET head_id = @person_11
WHERE department_id = @department_1;
UPDATE department
SET head_id = @person_3
WHERE department_id = @department_3;
UPDATE department
SET head_id = @person_4
WHERE department_id = @department_4;
UPDATE department
SET head_id = @person_5
WHERE department_id = @department_5;

UPDATE company
SET head_person_id = @person_2
WHERE company_id = @company_1;

INSERT INTO candidate(created_by,company_id, FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, SALARY_AMOUNT, POSITION, PHONE, CELL, EMAIL, ADDRESS, METRO_ID)
VALUES(@main_person, @company_1, 'Савелий', 'Васильевич', 'Вышивайкин', DATE '1988-01-31', 300, 'Курьер', '+7 (495) 101-12-34', '7(925)200-11-22', 'aaaaa@uu.ru', 'г.Волобуевск, ул. Маршала Наполеона, д.12. кв.111', 20);
SELECT SET(@candidate_1, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO candidate(created_by,company_id, FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, SALARY_AMOUNT, POSITION, PHONE, CELL, EMAIL, ADDRESS, METRO_ID)
VALUES(@main_person, @company_1, 'Феофан', 'Федорович', 'Дурдыкин', DATE '1976-02-04', 500, 'Дворник', '7(495)101-12-35', '7(925)200-11-23', 'bbbbb@uu.ru', 'г.Парижск, ул. Коня Буденого, д.2. кв.51', 35);
SELECT SET(@candidate_2, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO candidate(created_by,company_id, FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, SALARY_AMOUNT, POSITION, PHONE, CELL, EMAIL, ADDRESS, METRO_ID)
VALUES(@main_person, @company_1, 'Ипполит', 'Макарыч', 'Головешкин', DATE '1981-05-01', 600, 'Вахтер', '7(495)101-12-36', '7(925)200-11-24', 'ccccc@uu.ru', 'г.Зажопинск, ул. Тыловая, д.14. кв.10', 50);
SELECT SET(@candidate_3, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO candidate(created_by,company_id, FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, SALARY_AMOUNT, POSITION, PHONE, CELL, EMAIL, ADDRESS, METRO_ID)
VALUES(@main_person, @company_1, 'Васисуалий', 'Януарьевич', 'Тузиков', DATE '1985-06-21', 600, 'Слесарь', '7(495)101-12-61', '7(925)200-11-25', 'dddd@uu.ru', 'г.Зажопинск, ул. Тыловая, д.14. кв.11', 50);
SELECT SET(@candidate_4, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO candidate(created_by,company_id, FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, SALARY_AMOUNT, POSITION, PHONE, CELL, EMAIL, ADDRESS, METRO_ID)
VALUES(@main_person, @company_1, 'Феофан', 'Гавриилович', 'Канареечкин', DATE '1971-04-13', 600, 'Сантехник', '7(495)101-12-83', '7(925)200-11-26', 'dddd@uu.ru', 'г.Зажопинск, ул. Тыловая, д.14. кв.21', 50);
SELECT SET(@candidate_5, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);
INSERT INTO candidate(created_by,company_id, FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, SALARY_AMOUNT, POSITION, PHONE, CELL, EMAIL, ADDRESS, METRO_ID)
VALUES(@main_person, @company_1, 'Аристарх', 'Иванович', 'Патефонов', DATE '1951-08-11', 600, 'Трудовик', '7(495)101-12-77', '7(925)200-11-27', 'dddd@uu.ru', 'г.Зажопинск, ул. Тыловая, д.14. кв.44', 50);
SELECT SET(@candidate_6, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO picked_candidate( CANDIDATE_ID, PERSON_ID)
VALUES(@candidate_1, 0);
INSERT INTO picked_candidate( CANDIDATE_ID, PERSON_ID)
VALUES(@candidate_2, 0);

INSERT INTO candidate_category(candidate_id, category_id)
SELECT @candidate_1, category_id FROM category
WHERE parent_id = 2
LIMIT 2;

INSERT INTO candidate_category(candidate_id, category_id)
SELECT @candidate_1, category_id FROM category
WHERE parent_id = 3
LIMIT 2;

INSERT INTO candidate_category(candidate_id, category_id)
SELECT @candidate_1, category_id FROM category
WHERE parent_id = 4
LIMIT 2;

INSERT INTO candidate_category(candidate_id, category_id)
SELECT @candidate_2, category_id FROM category
WHERE parent_id = 5
LIMIT 2;

INSERT INTO candidate_category(candidate_id, category_id)
SELECT @candidate_3, category_id FROM category
WHERE parent_id = 6
LIMIT 2;

--INSERT INTO candidate_photo(CANDIDATE_ID, IMAGE, IS_MAIN) VALUES(@candidate_1, FILE_READ('classpath:/src/main/resources/images/boys_1.jpg'), false);
--INSERT INTO candidate_photo(CANDIDATE_ID, IMAGE, IS_MAIN) VALUES(@candidate_1, FILE_READ('classpath:/images/boys_1.jpg'), true);
--COMMIT;

INSERT INTO candidate_language(CANDIDATE_ID, LANGUAGE_ID, STATUS)
VALUES (@candidate_1, 10, 1);
INSERT INTO candidate_language(CANDIDATE_ID, LANGUAGE_ID, STATUS)
VALUES (@candidate_1, 15, 1);

INSERT INTO candidate_language(CANDIDATE_ID, LANGUAGE_ID, STATUS)
VALUES (@candidate_2, 11, 1);
INSERT INTO candidate_language(CANDIDATE_ID, LANGUAGE_ID, STATUS)
VALUES (@candidate_2, 14, 2);

INSERT INTO candidate_language( CANDIDATE_ID, LANGUAGE_ID, STATUS)
SELECT * FROM (
SELECT @candidate_1, 71, 0
UNION ALL
SELECT @candidate_2, 71, 0
UNION ALL
SELECT @candidate_3, 71, 0
UNION ALL
SELECT @candidate_4, 71, 0
UNION ALL
SELECT @candidate_5, 71, 0
UNION ALL
SELECT @candidate_6, 71, 0
);

INSERT INTO candidate_note( CANDIDATE_ID, created_by,NOTE)
VALUES(@candidate_1, @person_1, 'Быстрые ноги!');

INSERT INTO candidate_note( CANDIDATE_ID, created_by,NOTE)
VALUES(@candidate_1, @person_2, 'Крепкая голова!');

INSERT INTO candidate_note( CANDIDATE_ID, created_by,NOTE)
VALUES(@candidate_2, @person_1, 'The best!');

INSERT INTO education(created_by,CANDIDATE_ID, EDUCATION_NAME, EDUCATION_TYPE_ID, START_DATE, END_DATE)
VALUES(@main_person, @candidate_1, 'Школа №1', @education_type_school, DATE '1981-09-01', DATE '1991-06-01');

INSERT INTO education(created_by,CANDIDATE_ID, EDUCATION_NAME, EDUCATION_TYPE_ID, START_DATE, END_DATE)
VALUES(@main_person, @candidate_1, 'Институт народного хозяйства', @education_type_high_school, DATE '1991-09-01', DATE '1996-06-01');

INSERT INTO employment(created_by,CANDIDATE_ID, FIRM_NAME, POSITION, START_DATE, END_DATE)
VALUES(@main_person, @candidate_1, 'ООО Василек', 'Дворник', DATE '1996-06-02', DATE '1997-04-10');

INSERT INTO employment(created_by,CANDIDATE_ID, FIRM_NAME, POSITION, START_DATE, END_DATE)
VALUES(@main_person, @candidate_1, 'ООО Горячие пингвины', 'Сторож', DATE '1997-06-12', DATE '1997-08-10');

INSERT INTO employment(created_by,CANDIDATE_ID, FIRM_NAME, POSITION, START_DATE, END_DATE)
VALUES(@main_person, @candidate_1, 'ОАО Бум-бум-бум', 'Завхоз', DATE '1997-09-02', DATE '1998-01-01');

INSERT INTO demand( COMPANY_ID, created_by,CONTACT_PERSON_ID, RESPONSIBLE_PERSON_ID, CREATE_DATE, SALARY_AMOUNT, POSITION, DESCRIPTION)
VALUES(@company_1, @person_7, @person_7, @person_7, SYSDATE, 500, 'Старший курьер', 'Надо бы кого-нибудь найти, ибо Кузьмич стар уже стал, на ноги жалуется');
SELECT SET(@demand_1, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO demand( COMPANY_ID, created_by,CONTACT_PERSON_ID, RESPONSIBLE_PERSON_ID, CREATE_DATE, SALARY_AMOUNT, POSITION, DESCRIPTION, FINISH_DATE)
VALUES(@company_1, @person_7, @person_7, @person_7, SYSDATE, 1200, 'Старший дворник', 'Мы растущая компания, мы просто обязаны иметь главного дворника!', SYSDATE + 10);
SELECT SET(@demand_2, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO demand( COMPANY_ID, created_by,CONTACT_PERSON_ID, RESPONSIBLE_PERSON_ID, CREATE_DATE, SALARY_AMOUNT, POSITION, DESCRIPTION, FINISH_DATE)
VALUES(@company_2, @person_8, @person_7, @person_9, SYSDATE, 1700, 'Сантехник', 'У нас посрать нормально негде!', SYSDATE + 30);
SELECT SET(@demand_3, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO demand( COMPANY_ID, created_by,CONTACT_PERSON_ID, RESPONSIBLE_PERSON_ID, CREATE_DATE, SALARY_AMOUNT, POSITION, DESCRIPTION, FINISH_DATE)
VALUES(@company_5, @person_1, @person_1, @person_1, SYSDATE, 2750, 'Секретарь', 'Ну вы поняли!', SYSDATE + 30);
SELECT SET(@demand_4, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO demand( COMPANY_ID, created_by,CONTACT_PERSON_ID, RESPONSIBLE_PERSON_ID, CREATE_DATE, SALARY_AMOUNT, POSITION, DESCRIPTION, FINISH_DATE)
VALUES(@company_6, @person_1, @person_1, @person_2, SYSDATE, 5000, 'Генеральный директор', 'Должен уметь умное лицо!', SYSDATE + 30);
SELECT SET(@demand_5, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO demand_note (DEMAND_ID, created_by,NOTE)
VALUES(@demand_1, @person_2, 'Хрен вы кого найдете на такие деньги');

INSERT INTO demand_candidate (DEMAND_ID, CANDIDATE_ID, created_by,CREATE_DATE)
VALUES(@demand_1, @candidate_1, @person_1, SYSDATE);

SELECT SET(@demand_candidate_1, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO demand_candidate_note (DEMAND_CANDIDATE_ID, created_by,CREATE_DATE, NOTE)
VALUES(@demand_candidate_1, @person_1, SYSDATE, 'Идеальный!');

INSERT INTO demand_candidate (DEMAND_ID, CANDIDATE_ID, created_by,CREATE_DATE)
VALUES(@demand_1, @candidate_2, @person_1, SYSDATE);

SELECT SET(@demand_candidate_2, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO demand_candidate_note (DEMAND_CANDIDATE_ID, created_by,CREATE_DATE, NOTE)
VALUES(@demand_candidate_2, @person_1, SYSDATE, 'А зачем нам дворник на место курьера?');

INSERT INTO demand_candidate_meeting (DEMAND_CANDIDATE_ID, created_by,MEETING_DATETIME, MEETING_TYPE_ID)
VALUES(@demand_candidate_1, @person_1, '2018-07-18 21:00:00', 0);

SELECT SET(@demand_candidate_meeting_1, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO demand_candidate_meeting_note (DEMAND_CANDIDATE_MEETING_ID, created_by,CREATE_DATE, NOTE)
VALUES(@demand_candidate_meeting_1, @person_1, SYSDATE, 'Да нормальный пацанчик!');

INSERT INTO demand_candidate_meeting (DEMAND_CANDIDATE_ID, created_by,MEETING_DATETIME, MEETING_TYPE_ID)
VALUES(@demand_candidate_1, @person_1, '2018-07-19 15:30:00', 1);

SELECT SET(@demand_candidate_meeting_2, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO demand_candidate_meeting_note (DEMAND_CANDIDATE_MEETING_ID, created_by, CREATE_DATE, NOTE)
VALUES(@demand_candidate_meeting_2, @person_1, SYSDATE, 'Да он же бухой пришел!');

INSERT INTO demand_candidate_meeting (DEMAND_CANDIDATE_ID, created_by,MEETING_DATETIME, MEETING_TYPE_ID)
VALUES(@demand_candidate_1, @person_1, '2018-07-20 10:30:00', 2);

SELECT SET(@demand_candidate_meeting_3, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO demand_candidate_meeting_note (DEMAND_CANDIDATE_MEETING_ID, created_by, CREATE_DATE, NOTE)
VALUES(@demand_candidate_meeting_3, @person_1, SYSDATE, '3 км за полчаса. Если бы конечный адрес не забыл, был бы рекорд фирмы!');

INSERT INTO picked_demand(DEMAND_ID, PERSON_ID)
VALUES(@demand_1, 1);

INSERT INTO picked_demand(DEMAND_ID, PERSON_ID)
VALUES(@demand_2, 2);

/*
INSERT INTO company (
  NAME,
  HEAD_PERSON_ID,
  CONTACT_PERSON_ID,
  PHONE,   CELL,   EMAIL,  CONTRACT_NUMBER,
  START_DATE,   END_DATE,
  AGENCY_TYPE,   CONTRACT_TYPE,   STATUS,
  PAYMENT_TYPE, COMPANY_TYPE_ID)
VALUES('Кадры решают все!',
'Биг босс',
'Секретарь-машинист',
'Менагер',
'123456778', '005-123-55-66', 'test@asdf.ru', '#12345 asdf',
DATE '2012-02-04', null,
1, 1, 1,
'%', 2);
*/

INSERT INTO roles(ROLE_ID) VALUES('ROLE_USER');

INSERT INTO roles(ROLE_ID) VALUES('ROLE_MANAGER');

INSERT INTO roles(ROLE_ID) VALUES('ROLE_ADMIN');

INSERT INTO users(login, password, person_id, is_active)
VALUES('user1', 'asdf', @person_5, true);

SELECT SET(@user1_id, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO users(login, password, person_id, is_active)
VALUES('user2', 'asdf', @person_6, true);

SELECT SET(@user2_id, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO users(login, password, person_id, is_active)
VALUES('user3', 'asdf', @person_7, true);

SELECT SET(@user3_id, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO users(login, password, person_id, is_active)
VALUES('user4', 'asdf', @person_8, true);

SELECT SET(@user4_id, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO users(login, password, person_id, is_active)
VALUES('user5', 'asdf', @person_9, true);

SELECT SET(@user5_id, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO users(login, password, person_id, is_active)
VALUES('user6', 'asdf', @person_10, true);

SELECT SET(@user6_id, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO users(login, password, person_id, is_active)
VALUES('user7', 'asdf', @person_11, true);

SELECT SET(@user7_id, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

--INSERT INTO user_role (user_id, role_id)
--VALUES(@user1_id, 'ROLE_USER');

INSERT INTO user_role (user_id, role_id)
VALUES(@user1_id, 'ROLE_ADMIN');

INSERT INTO user_role (user_id, role_id)
VALUES(@user2_id, 'ROLE_USER');

--INSERT INTO user_role (user_id, role_id)
--VALUES(@user3_id, 'ROLE_USER');

INSERT INTO user_role (user_id, role_id)
VALUES(@user3_id, 'ROLE_MANAGER');
