/*
  DROP
*/

DROP TABLE IF EXISTS employment_agency;

DROP TABLE IF EXISTS user_role;

DROP TABLE IF EXISTS users;

DROP TABLE IF EXISTS roles;

DROP TABLE IF EXISTS picked_candidate;

DROP TABLE IF EXISTS picked_company;

DROP TABLE IF EXISTS picked_demand;

DROP TABLE IF EXISTS person;

DROP TABLE IF EXISTS company;

DROP TABLE IF EXISTS company_type;

DROP TABLE IF EXISTS department;

DROP TABLE IF EXISTS demand_candidate_meeting_note;

DROP TABLE IF EXISTS demand_candidate_meeting;

DROP TABLE IF EXISTS meeting_type;

DROP TABLE IF EXISTS demand_candidate_note;

DROP TABLE IF EXISTS demand_candidate;

DROP TABLE IF EXISTS candidate_language;

DROP TABLE IF EXISTS person_candidate;

DROP TABLE IF EXISTS candidate_note;

DROP TABLE IF EXISTS candidate_category;

DROP TABLE IF EXISTS candidate_photo;

DROP TABLE IF EXISTS category;

DROP TABLE IF EXISTS education;

DROP TABLE IF EXISTS employment;

DROP TABLE IF EXISTS candidate;

DROP TABLE IF EXISTS demand_note;

DROP TABLE IF EXISTS demand;

DROP TABLE IF EXISTS person;

DROP TABLE IF EXISTS language;

DROP TABLE IF EXISTS metro;

DROP TABLE IF EXISTS town;

DROP TABLE IF EXISTS region;

DROP TABLE IF EXISTS country;

DROP TABLE IF EXISTS education_type;


CREATE TABLE country (
  COUNTRY_ID SMALLINT PRIMARY KEY,
  name VARCHAR2(100),
  status INTEGER DEFAULT 0
);

CREATE TABLE region (
  REGION_ID SMALLINT PRIMARY KEY,
  COUNTRY_ID SMALLINT NOT NULL,
  name VARCHAR2(100),
  status SMALLINT DEFAULT 0
);

ALTER TABLE region
  ADD CONSTRAINT region_fk_country_id FOREIGN KEY (COUNTRY_ID)
  REFERENCES country (COUNTRY_ID);

CREATE TABLE town (
  TOWN_ID SMALLINT IDENTITY PRIMARY KEY,
  COUNTRY_ID SMALLINT,
  REGION_ID SMALLINT,
  name VARCHAR2(100) NOT NULL,
  status SMALLINT DEFAULT 0
);

ALTER TABLE town
  ADD CONSTRAINT town_fk_country_id FOREIGN KEY (COUNTRY_ID)
  REFERENCES country (COUNTRY_ID);

ALTER TABLE town
  ADD CONSTRAINT town_fk_region_id FOREIGN KEY (REGION_ID)
  REFERENCES region (REGION_ID);

CREATE TABLE metro (
  METRO_ID INTEGER IDENTITY PRIMARY KEY,
  TOWN_ID INTEGER NOT NULL,
  LINE_ID INTEGER NOT NULL,
  name VARCHAR2(100) NOT NULL
);

ALTER TABLE metro
  ADD CONSTRAINT metro_fk_town_id FOREIGN KEY (TOWN_ID)
  REFERENCES town (TOWN_ID);

/*
  Company type
1 - заказчик
2 - кадровое агентство
3 - владелец
*/

CREATE TABLE company_type (
  COMPANY_TYPE_ID SMALLINT PRIMARY KEY,
  name VARCHAR2(45) NOT NULL
);

/*
  Company
*/

/*  TYPE ARRAY,
  SELECT * FROM TEST1 where ARRAY_CONTAINS ( a , 3 )
*/

CREATE TABLE company (
  company_id IDENTITY PRIMARY KEY,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  name VARCHAR2(45) NOT NULL,
  ADDRESS VARCHAR(255) NOT NULL,
  HEAD_PERSON_ID BIGINT,
  CONTACT_PERSON_ID BIGINT,
  PHONE VARCHAR2(100),
  EMAIL VARCHAR2(100),
  COMPANY_TYPE_ID BIGINT NOT NULL,
  IS_ACTIVE BOOLEAN NOT NULL DEFAULT TRUE
);

ALTER TABLE company
  ADD CONSTRAINT company_fk_company_type_id FOREIGN KEY (COMPANY_TYPE_ID)
  REFERENCES company_type (COMPANY_TYPE_ID);

--ALTER TABLE company
--  ADD CONSTRAINT company_fk_created_by FOREIGN KEY (created_by)
--  REFERENCES person(person_id);

/*
  Person
*/

CREATE TABLE person (
  person_id IDENTITY PRIMARY KEY,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
--  company_id BIGINT NOT NULL,
  FIRST_NAME VARCHAR2(100) NOT NULL,
  MIDDLE_NAME VARCHAR2(100),  
  LAST_NAME VARCHAR2(100) NOT NULL,
  BIRTH_DATE DATE NOT NULL,
  PHONE VARCHAR2(100),
  CELL VARCHAR2(100),
  EMAIL VARCHAR2(100),
  ADDRESS VARCHAR(255),
  POSITION VARCHAR(255),
  START_DATE DATE,
  END_DATE DATE,
  department_id BIGINT,
  IS_ACTIVE BOOLEAN
);

--ALTER TABLE person
--  ADD CONSTRAINT person_fk_company_id FOREIGN KEY (company_id)
--  REFERENCES company (company_ID);

ALTER TABLE person
  ADD CONSTRAINT person_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

--ALTER TABLE company
--  ADD CONSTRAINT company_fk_head_person_id FOREIGN KEY (HEAD_PERSON_ID)
--  REFERENCES person (person_id);

--ALTER TABLE company
--  ADD CONSTRAINT company_fk_contact_person_id FOREIGN KEY (CONTACT_PERSON_ID)
--  REFERENCES person (person_id);

/*
  Employment Agency
*/

/*
CREATE TABLE employment_agency (
  EMPLOYMENT_AGENCY_ID IDENTITY NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  name VARCHAR2(300) NOT NULL,
  HEAD_PERSON VARCHAR2(100),
  CONTACT_PERSON VARCHAR2(100),
  RESPONSIBLE_PERSON VARCHAR2(100),
  PHONE VARCHAR2(100),
  CELL VARCHAR2(100),
  EMAIL VARCHAR2(100),
  CONTRACT_BIGINT VARCHAR2(100),
  START_DATE DATE,
  END_DATE DATE,
  AGENCY_TYPE INTEGER,
  CONTRACT_TYPE INTEGER,
  status INTEGER,
  PAYMENT_TYPE VARCHAR2(100)
);

ALTER TABLE employment_agency
  ADD CONSTRAINT employment_agency_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);
*/

/*
  Department
*/

CREATE TABLE department (
  department_id IDENTITY PRIMARY KEY,
  company_id BIGINT NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  name VARCHAR2(45) NOT NULL,
  HEAD_ID BIGINT,
  CONTACT_PERSON_ID BIGINT,
  PHONE VARCHAR2(100),
  EMAIL VARCHAR2(100),
  IS_ACTIVE BOOLEAN DEFAULT TRUE
);

ALTER TABLE department
  ADD CONSTRAINT department_fk_company_id FOREIGN KEY (company_id)
  REFERENCES company (company_id);

ALTER TABLE department
  ADD CONSTRAINT department_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

ALTER TABLE department
  ADD CONSTRAINT department_fk_head_id FOREIGN KEY (HEAD_ID)
  REFERENCES person (person_id) ON DELETE SET NULL;

ALTER TABLE department
  ADD CONSTRAINT department_fk_contact_person_id FOREIGN KEY (CONTACT_PERSON_ID)
  REFERENCES person (person_id) ON DELETE SET NULL;

ALTER TABLE person
  ADD CONSTRAINT person_fk_department_id FOREIGN KEY (department_id)
  REFERENCES department (department_id);

/*
  Users
*/

CREATE TABLE users (
  user_id IDENTITY PRIMARY KEY,
  LOGIN VARCHAR2(100) NOT NULL,
  PASSWORD VARCHAR2(100),
  START_DATE DATE,
  person_id BIGINT NOT NULL,
  IS_ACTIVE BOOLEAN
);

ALTER TABLE users
  ADD CONSTRAINT users_fk_person_id FOREIGN KEY (person_id)
  REFERENCES person (person_id);

CREATE UNIQUE INDEX IF NOT EXISTS indx_users_person_id ON users(person_id);

/*
  Roles
*/

CREATE TABLE roles (
  role_id VARCHAR2(20) PRIMARY KEY
);

CREATE TABLE user_role (
  user_id INTEGER NOT NULL,
  role_id VARCHAR2(20) NOT NULL
);

ALTER TABLE user_role
  ADD CONSTRAINT user_role_fk_user_id FOREIGN KEY (user_id)
  REFERENCES users (user_id) ON DELETE CASCADE;

ALTER TABLE user_role
  ADD CONSTRAINT user_role_fk_role_id FOREIGN KEY (role_id)
  REFERENCES roles (role_id) ON DELETE CASCADE;

/*
  Company
*/

/*
CREATE TABLE company (
  CUSTOMER_ID IDENTITY NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  name VARCHAR2(45) NOT NULL,
  ADDRESS VARCHAR(255) NOT NULL,
  HEAD_ID BIGINT,
  CONTACT_PERSON_ID BIGINT,
  PHONE VARCHAR2(100),
  EMAIL VARCHAR2(100),
  IS_ACTIVE BOOLEAN
);

ALTER TABLE company
  ADD CONSTRAINT customer_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

//ALTER TABLE CUSTOMER
//  ADD CONSTRAINT PK_CUSTOMER primary key (CUSTOMER_ID);

ALTER TABLE company
  ADD CONSTRAINT customer_fk_head_id FOREIGN KEY (HEAD_ID)
  REFERENCES person (person_id);

ALTER TABLE company
  ADD CONSTRAINT customer_fk_contact_person_id FOREIGN KEY (CONTACT_PERSON_ID)
  REFERENCES person (person_id);
*/

/*
  Category
*/

CREATE TABLE category (
  category_id IDENTITY(100) PRIMARY KEY,
  PARENT_ID BIGINT NULL,
  name VARCHAR2(100) NOT NULL,
  HAS_CHILD BOOLEAN DEFAULT FALSE);

--ALTER TABLE CATEGORY
--  ADD CONSTRAINT PK_CATEGORY primary key (category_id);

ALTER TABLE category
  ADD CONSTRAINT category_fk_parent_id FOREIGN KEY (PARENT_ID)
  REFERENCES category (category_id);

/*
  Candidate
*/

CREATE TABLE candidate (
  candidate_id IDENTITY NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  FIRST_NAME VARCHAR2(100) NOT NULL,
  MIDDLE_NAME VARCHAR2(100),
  LAST_NAME VARCHAR2(100) NOT NULL,
  ADDRESS VARCHAR2(1000),
  company_id BIGINT NOT NULL,
  METRO_ID BIGINT,
  BIRTH_DATE DATE NOT NULL,
  SALARY_AMOUNT BIGINT,
  SALARY_CURRENCY INTEGER DEFAULT 0,
  PHONE VARCHAR2(100),
  CELL VARCHAR2(100),
  EMAIL VARCHAR2(100),
  POSITION VARCHAR2(100),
  status INTEGER NOT NULL DEFAULT 0,
  SEX INTEGER NOT NULL DEFAULT 0,
  DEGREE VARCHAR2(100)
);

ALTER TABLE candidate
  ADD CONSTRAINT candidate_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

ALTER TABLE candidate
  ADD CONSTRAINT candidate_fk_metro_id FOREIGN KEY (METRO_ID)
  REFERENCES metro (METRO_ID);

ALTER TABLE candidate
  ADD CONSTRAINT candidate_fk_company_id FOREIGN KEY (company_id)
  REFERENCES company (company_id);

/*
  Candidate images
*/

CREATE TABLE candidate_photo (
  PHOTO_ID IDENTITY NOT NULL,
  candidate_id BIGINT NOT NULL,
  IMAGE BLOB NOT NULL,
  IS_MAIN BOOLEAN
);

ALTER TABLE candidate_photo
  ADD CONSTRAINT candidate_photo_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id) ON DELETE CASCADE;

/*
  Candidate category
*/

CREATE TABLE candidate_category (
  candidate_id BIGINT NOT NULL,
  category_id BIGINT NOT NULL
);

ALTER TABLE candidate_category
  ADD CONSTRAINT candidate_category_fk_category_id FOREIGN KEY (category_id)
  REFERENCES category (category_id);

ALTER TABLE candidate_category
  ADD CONSTRAINT candidate_category_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id) ON DELETE CASCADE;

ALTER TABLE candidate_category
  ADD CONSTRAINT candidate_category_pk PRIMARY KEY (candidate_id, category_id);

/*
  candidate_note
*/

CREATE TABLE candidate_note (
  CANDIDATE_NOTE_ID IDENTITY NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  candidate_id BIGINT NOT NULL,
  note VARCHAR2(4000)
);

ALTER TABLE candidate_note
  ADD CONSTRAINT candidate_note_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id) ON DELETE CASCADE;

ALTER TABLE candidate_note
  ADD CONSTRAINT candidate_note_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

/*
  Employment
*/

CREATE TABLE employment (
  EMPLOYMENT_ID IDENTITY NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  candidate_id INTEGER NOT NULL,
  FIRM_NAME VARCHAR2(300) NOT NULL,
  POSITION VARCHAR2(300) NOT NULL,
  DESCRIPTION VARCHAR2(10000),
  START_DATE DATE NOT NULL,
  END_DATE DATE
);

ALTER TABLE employment
  ADD CONSTRAINT employment_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

ALTER TABLE employment
  ADD CONSTRAINT employment_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id) ON DELETE CASCADE;

/*
  Education type
*/

CREATE TABLE education_type (
  EDUCATION_TYPE_ID IDENTITY NOT NULL,
  EDUCATION_TYPE_NAME VARCHAR2(100) NOT NULL
);

/*
  Education
*/

CREATE TABLE education (
  EDUCATION_ID IDENTITY NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  candidate_id INTEGER NOT NULL,
  EDUCATION_NAME VARCHAR2(300) NOT NULL,
  FACULTY_NAME VARCHAR2(300),
  EDUCATION_TYPE_ID SMALLINT NOT NULL,
  START_DATE DATE,
  END_DATE DATE
);

ALTER TABLE education
  ADD CONSTRAINT education_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

ALTER TABLE education
  ADD CONSTRAINT education_fk_education_type_id FOREIGN KEY (EDUCATION_TYPE_ID)
  REFERENCES education_type (EDUCATION_TYPE_ID);

ALTER TABLE education
  ADD CONSTRAINT candidate_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id) ON DELETE CASCADE;

/*
  Demand
*/

CREATE TABLE demand (
  demand_id IDENTITY NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  company_id BIGINT NOT NULL,
  CONTACT_PERSON_ID BIGINT NOT NULL,
  RESPONSIBLE_PERSON_ID BIGINT NOT NULL,
  FINISH_DATE DATE,
  SALARY_AMOUNT NUMBER,
  SALARY_CURRENCY INTEGER DEFAULT 0,
  POSITION VARCHAR2(100),
  status SMALLINT NOT NULL DEFAULT 0,
  DESCRIPTION VARCHAR2(4000)
);

ALTER TABLE demand
  ADD CONSTRAINT demand_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

ALTER TABLE demand
  ADD CONSTRAINT demand_fk_contact_person_id FOREIGN KEY (CONTACT_PERSON_ID)
  REFERENCES person (person_id);

ALTER TABLE demand
  ADD CONSTRAINT demand_fk_resp_person_id FOREIGN KEY (RESPONSIBLE_PERSON_ID)
  REFERENCES person (person_id);

ALTER TABLE demand
  ADD CONSTRAINT demand_fk_company_id FOREIGN KEY (company_id)
  REFERENCES company (company_id);

/*
  Demand_note
*/

CREATE TABLE demand_note (
  DEMAND_NOTE_ID IDENTITY NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  demand_id BIGINT NOT NULL,
  note VARCHAR2(4000)
);

ALTER TABLE demand_note
  ADD CONSTRAINT demand_note_fk_demand_id FOREIGN KEY (demand_id)
  REFERENCES DEMAND (demand_id) ON DELETE CASCADE;

ALTER TABLE demand_note
  ADD CONSTRAINT demand_note_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

CREATE TABLE person_candidate (
  person_id BIGINT NOT NULL,
  candidate_id BIGINT NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  status SMALLINT NOT NULL DEFAULT 0,
  note VARCHAR2(4000)
);

ALTER TABLE person_candidate
  ADD CONSTRAINT person_candidate_pk primary key (person_id, candidate_id);

ALTER TABLE person_candidate
  ADD CONSTRAINT person_candidate_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id);

ALTER TABLE person_candidate
  ADD CONSTRAINT person_candidate_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

CREATE TABLE demand_candidate (
  demand_candidate_id IDENTITY NOT NULL,
  created_by BIGINT NOT NULL,
  demand_id BIGINT NOT NULL,
  candidate_id BIGINT NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  status SMALLINT NOT NULL DEFAULT 0
);

ALTER TABLE demand_candidate
  ADD CONSTRAINT demand_candidate_fk_demand_id FOREIGN KEY (demand_id)
  REFERENCES DEMAND (demand_id) ON DELETE CASCADE;

ALTER TABLE demand_candidate
  ADD CONSTRAINT demand_candidate_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id);

ALTER TABLE demand_candidate
  ADD CONSTRAINT demand_candidate_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

CREATE TABLE demand_candidate_note (
  DEMAND_CANDIDATE_NOTE_ID IDENTITY NOT NULL,
  demand_candidate_id  BIGINT NOT NULL,
  created_by BIGINT NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  note VARCHAR2(4000)
);

ALTER TABLE demand_candidate_note
  ADD CONSTRAINT demand_candidate_note_fk_demand_candidate_id FOREIGN KEY (demand_candidate_id)
  REFERENCES demand_candidate (demand_candidate_id) ON DELETE CASCADE;

/*
  Meeting type
  0 - "Телефонное интервью"
  1 - "Встреча"
  2 - "Тестирование"
*/

CREATE TABLE meeting_type (
  MEETING_TYPE_ID SMALLINT NOT NULL,
  name VARCHAR2(45) NOT NULL
);

CREATE TABLE demand_candidate_meeting (
  DEMAND_CANDIDATE_MEETING_ID IDENTITY NOT NULL,
  demand_candidate_id  BIGINT NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  MEETING_DATETIME DATETIME NOT NULL,
  MEETING_TYPE_ID SMALLINT NOT NULL
);

ALTER TABLE demand_candidate_meeting
  ADD CONSTRAINT demand_candidate_meeting_fk_demand_candidate_id FOREIGN KEY (demand_candidate_id)
  REFERENCES demand_candidate (demand_candidate_id) ON DELETE CASCADE;

ALTER TABLE demand_candidate_meeting
  ADD CONSTRAINT demand_candidate_meeting_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

ALTER TABLE demand_candidate_meeting
  ADD CONSTRAINT demand_candidate_meeting_fk_meeting_type_id FOREIGN KEY (MEETING_TYPE_ID)
  REFERENCES MEETING_TYPE (MEETING_TYPE_ID);

ALTER TABLE demand_candidate_meeting
  ADD CONSTRAINT demand_candidate_meeting_UQ_meeting_datetime UNIQUE (demand_candidate_id, MEETING_DATETIME);

CREATE TABLE demand_candidate_meeting_note (
  DEMAND_CANDIDATE_MEETING_NOTE_ID IDENTITY NOT NULL,
  DEMAND_CANDIDATE_MEETING_ID BIGINT NOT NULL,
  create_date DATETIME NOT NULL DEFAULT SYSTIMESTAMP,
  created_by BIGINT NOT NULL,
  note VARCHAR2(4000)
);

ALTER TABLE demand_candidate_meeting_note
  ADD CONSTRAINT demand_candidate_meeting_note_fk_demand_candidate_meeting_id FOREIGN KEY (DEMAND_CANDIDATE_MEETING_ID)
  REFERENCES DEMAND_CANDIDATE_MEETING (DEMAND_CANDIDATE_MEETING_ID) ON DELETE CASCADE;

ALTER TABLE demand_candidate_meeting_note
  ADD CONSTRAINT demand_candidate_meeting_note_fk_created_by FOREIGN KEY (created_by)
  REFERENCES person (person_id);

CREATE TABLE language (
  language_id INTEGER IDENTITY PRIMARY KEY,
  name VARCHAR2(100)
);

CREATE TABLE candidate_language (
  candidate_id BIGINT NOT NULL,
  language_id BIGINT NOT NULL,
  status INTEGER NOT NULL
);

ALTER TABLE candidate_language
  ADD CONSTRAINT candidate_language_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id) ON DELETE CASCADE;

ALTER TABLE candidate_language
  ADD CONSTRAINT candidate_language_fk_language_id FOREIGN KEY (language_id)
  REFERENCES language (language_id);

ALTER TABLE candidate_language
  ADD CONSTRAINT candidate_language_pk PRIMARY KEY (candidate_id, language_id);

CREATE TABLE picked_candidate (
  candidate_id BIGINT NOT NULL,
  person_id BIGINT NOT NULL
);

ALTER TABLE picked_candidate
  ADD CONSTRAINT picked_candidate_pk PRIMARY KEY (candidate_id, person_id);

ALTER TABLE picked_candidate
  ADD CONSTRAINT picked_candidate_fk_candidate_id FOREIGN KEY (candidate_id)
  REFERENCES candidate (candidate_id) ON DELETE CASCADE;

ALTER TABLE picked_candidate
  ADD CONSTRAINT picked_candidate_fk_person_id FOREIGN KEY (person_id)
  REFERENCES person (person_id) ON DELETE CASCADE;

CREATE TABLE picked_company (
  company_id BIGINT NOT NULL,
  person_id BIGINT NOT NULL
);

ALTER TABLE picked_company
  ADD CONSTRAINT picked_company_fk_company_id FOREIGN KEY (company_id)
  REFERENCES company (company_id) ON DELETE CASCADE;

ALTER TABLE picked_company
  ADD CONSTRAINT picked_company_fk_person_id FOREIGN KEY (person_id)
  REFERENCES person (person_id) ON DELETE CASCADE;

ALTER TABLE picked_company
  ADD CONSTRAINT picked_company_pk PRIMARY KEY (company_id, person_id);


CREATE TABLE picked_demand (
  demand_id BIGINT NOT NULL,
  person_id BIGINT NOT NULL
);

ALTER TABLE picked_demand
  ADD CONSTRAINT picked_demand_fk_demand_id FOREIGN KEY (demand_id)
  REFERENCES demand (demand_id) ON DELETE CASCADE;

ALTER TABLE picked_demand
  ADD CONSTRAINT picked_demand_fk_person_id FOREIGN KEY (person_id)
  REFERENCES person (person_id) ON DELETE CASCADE;

ALTER TABLE picked_demand
  ADD CONSTRAINT picked_demand_pk PRIMARY KEY (demand_id, person_id);
