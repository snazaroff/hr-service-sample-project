package com.pkit.repository;

import com.pkit.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    List<Category> findByParentIdOrderByNameAsc(Long parentId);
    Category findByName(String name);
    Category findByNameAndParentId(String name, Long parentId);
}