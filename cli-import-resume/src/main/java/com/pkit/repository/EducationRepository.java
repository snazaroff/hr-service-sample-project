package com.pkit.repository;

import com.pkit.model.Education;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EducationRepository extends JpaRepository<Education, Long> {
    @Query(value = "SELECT e.* FROM education e WHERE e.candidate_id = ?1",nativeQuery = true)
    List<Education> findByCandidateId(Long candidateId);
}

