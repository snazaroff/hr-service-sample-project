package com.pkit.repository;

import com.pkit.model.Photo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhotoRepository extends JpaRepository<Photo, Long> {

    List<Photo> findByCandidateId(Long candidareId);

    @Query(value = "SELECT * FROM candidate_photo cp WHERE cp.candidate_id = ?1 AND is_main = ?2",nativeQuery = true)
    List<Photo> findByCandidateIdAndIsMain(Long candidareId, boolean isMain);

    @Modifying
    @Query(value = "DELETE FROM candidate_photo cp WHERE cp.candidate_id = ?1",nativeQuery = true)
    void deleteByCandidateId(Long candidareId);
}

