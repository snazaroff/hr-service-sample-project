package com.pkit.repository;

import com.pkit.model.Candidate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface CandidateRepository extends JpaRepository<Candidate, Long> { }