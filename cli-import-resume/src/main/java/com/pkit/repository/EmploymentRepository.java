package com.pkit.repository;

import com.pkit.model.Employment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmploymentRepository extends JpaRepository<Employment, Long> {
    @Query(value = "SELECT e.* FROM employment e WHERE e.candidate_id = ?1",nativeQuery = true)
    List<Employment> findByCandidateId(Long candidateId);
}

