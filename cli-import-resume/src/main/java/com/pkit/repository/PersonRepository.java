package com.pkit.repository;

import com.pkit.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    @Query(value =
            "SELECT p.* FROM person p " +
            "INNER JOIN department d ON d.department_id = p.department_id " +
            "WHERE d.company_id = ?1",
            nativeQuery = true)
    List<Person> findByCompanyId(Long companyId);

    @Query(value = "SELECT p.* FROM person p WHERE p.department_id = ?1 ORDER BY p.last_name, p.first_name, p.middle_name", nativeQuery = true)
    List<Person> findByDepartmentId(Long departmentId);
}

