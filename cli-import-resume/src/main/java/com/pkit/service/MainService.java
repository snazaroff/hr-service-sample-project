package com.pkit.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.file_store.service.ConvertService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

@Slf4j
@Service
public class MainService {

    @Autowired
    private ObjectMapper om;

    private final ConvertService importService;
    private final SaveService saveService;

    public MainService(SaveService saveService) {
        this.importService = new ConvertService();
        this.saveService = saveService;
    }

    public void loadFile(String filePath) {
        log.debug("loadFile( filePath: {})", filePath);
        try {
            Path file = Paths.get(filePath);
            byte[] bytes = Files.readAllBytes(Paths.get(filePath));

            Map<String, Object> candidate = importService.importResume(bytes, file.getFileName().toString());
            Files.write(Paths.get(file.getFileName() + ".json"), om.writerWithDefaultPrettyPrinter().writeValueAsBytes(candidate));

            saveService.save(candidate);

        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadFolder(String folder) {
        log.debug("loadFolder( folder: {})", folder);

        if (Files.exists(Paths.get(folder))) {
            try {
                Files.list(Paths.get(folder)).forEach(
                        f -> {
                            log.debug("file: {}", f.getFileName());
                            loadFile(f.toAbsolutePath().toString());
                        });
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }
}