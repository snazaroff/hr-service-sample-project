package com.pkit.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkit.model.*;
import com.pkit.repository.CandidateRepository;
import com.pkit.repository.CategoryRepository;
import com.pkit.repository.MetroRepository;
import com.pkit.repository.PhotoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SaveService {

    private final ObjectMapper om;

    private final CandidateRepository candidateRepository;
    private final CategoryRepository categoryRepository;
    private final MetroRepository metroRepository;
    private final PhotoRepository photoRepository;

    public SaveService(
            ObjectMapper om,
            CandidateRepository candidateRepository,
            CategoryRepository categoryRepository,
            MetroRepository metroRepository,
            PhotoRepository photoRepository
    ) {
        log.debug("SaveService()");

        this.om = om;
        this.candidateRepository = candidateRepository;
        this.categoryRepository = categoryRepository;
        this.metroRepository = metroRepository;
        this.photoRepository = photoRepository;
    }

    public void save(Map<String, Object> candidateMap) {
        List<String> photos = (List<String>) candidateMap.get("photos");
        candidateMap.remove("photos");

        List<Candidate> candidates = candidateRepository.findAll();
        log.debug("candidates: {}", candidates.size());

        Candidate c = candidates.get(0);

        try {
            log.debug("candidate: {}", om.writerWithDefaultPrettyPrinter().writeValueAsString(c));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        //Add created person
        candidateMap.put("created_by", new HashMap() {{
            put("person_id", 0);
        }});

        //Преобразуем данные, приводим к конктетным значениям в базе
        //Метро
        if (candidateMap.containsKey("metro")) {
            String metroName = (String) candidateMap.get("metro");
            Metro metro = metroRepository.findByName(metroName);
            if (metro != null) {
                candidateMap.put("metro", metro);
            } else {
                log.error("Metro '{}' not found", metroName);
                candidateMap.remove("metro");
            }
        }

        //Пол
        if (candidateMap.containsKey("sex")) {
            String sex = (String) candidateMap.get("sex");
            candidateMap.put("sex", SexStatus.getByName(sex));
        }

        List<String> categories = (List<String>) candidateMap.get("categories");
        candidateMap.remove("categories");
        Candidate candidate = null;

        try {
            log.debug("candidate:2: {}", om.writerWithDefaultPrettyPrinter().writeValueAsString(candidateMap));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        try {
            candidate = om.readValue(om.writeValueAsString(candidateMap), Candidate.class);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        //Категории
        if (candidateMap.containsKey("categories")) {
            Category parent = null;
            for (String categoryName : categories) {
                log.debug("category: {}", categoryName);

                CandidateCategory candidateCategory = new CandidateCategory();
                candidateCategory.setCandidate(candidate);
                Category category;
                if (parent == null)
                    category = categoryRepository.findByNameAndParentId(categoryName, null);
                else
                    category = categoryRepository.findByNameAndParentId(categoryName, parent.getCategoryId());
                if (category.getHasChild()) {
                    parent = category;
                }
                log.debug("category: {}", category);
                candidateCategory.setCategory(category);
                candidate.getCategories().add(candidateCategory);
            }
        }

        log.debug("candidate.createdBy: {}", candidate.getCreatedBy());
        candidate = candidateRepository.save(candidate);
        log.debug("candidate: {}", candidate.getCandidateId());

        //Сохраняем фотографии
        if (photos != null && photos.size() > 0) {
            Base64.Decoder decoder = Base64.getDecoder();
            for (String photoBody : photos) {
                Photo photo = new Photo();
                photo.setCandidateId(candidate.getCandidateId());
                photo.setImage(decoder.decode(photoBody));
                photoRepository.save(photo);
            }
        }
    }
}