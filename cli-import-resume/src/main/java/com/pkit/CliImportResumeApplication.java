package com.pkit;

import com.pkit.service.MainService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
@Slf4j
public class CliImportResumeApplication implements CommandLineRunner {

    private final MainService mainService;

    public CliImportResumeApplication(MainService service) {
        this.mainService = service;
    }

    public static void main(String[] args) {
        SpringApplication.run(CliImportResumeApplication.class, args);
    }

    @Override
    public void run(String... args) {

        log.debug("args: {}", Arrays.asList(args));
        log.debug("args.length: {}", args.length);
// create the command line parser
        CommandLineParser parser = new DefaultParser();

// create the Options
        Options options = new Options();
        options.addOption("folder", "folder", true, "Folder with docs");
        options.addOption("file", "file", true, "File with single doc");

        try {
            // parse the command line arguments
            CommandLine line = parser.parse(options, args);

            if (line.hasOption("folder")) {
                String folder = line.getOptionValue("folder");
                log.debug("folder: {}", folder);
                if (folder != null && !"".equals(folder.trim()))
                    mainService.loadFolder(folder.trim());
            }
            if (line.hasOption("file")) {
                String file = line.getOptionValue("file");
                log.debug("file: {}", file);
                if (file != null && !"".equals(file.trim()))
                    mainService.loadFile(file);
            }
        } catch (ParseException exp) {
            log.error("Unexpected exception:" + exp.getMessage(), exp);
        }
    }
}
