package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Slf4j
@Setter
@Getter
@ToString
@Entity
@Table(name = "category")
public class Category implements java.io.Serializable, Comparable<Category> {

    @JsonProperty("category_id")
    private Long categoryId;
    @JsonProperty("parent_id")
    private Long parentId;
    @JsonProperty("name")
    private String name;

    @JsonProperty("has_child")
    private boolean hasChild;

    @Id
    @Column(name = "category_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getCategoryId() {
        return categoryId;
    }

    @Column(name = "parent_id")
    public Long getParentId() {
        return parentId;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Column(name = "has_child")
    public boolean getHasChild() {
        return hasChild;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category c = (Category) o;
        return this.categoryId.equals(c.categoryId);
    }

    @Override
    public int compareTo(@NotNull Category c) {
        int result = 0;
        if (parentId != null && c.parentId != null) {
            result = parentId.compareTo(c.parentId);
        } else if (parentId != null) {
            result = parentId.compareTo(c.categoryId);
        } else if (c.parentId != null) {
            result = categoryId.compareTo(c.parentId);
        }

        if (result == 0) {
            result = categoryId.compareTo(c.categoryId);
        }
        return result;
    }
}
