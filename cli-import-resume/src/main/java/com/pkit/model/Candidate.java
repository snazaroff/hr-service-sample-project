package com.pkit.model;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.*;

@Slf4j
@Setter
@Getter
@Entity
@Table(name = "candidate")
@FilterDef(name = "pickedCandidate",
        parameters = @ParamDef(name = "currentPerson", type = "long"))
@JsonIgnoreProperties(ignoreUnknown = true)
public class Candidate extends Human implements java.io.Serializable {

    @JsonProperty("candidate_id")
    private Long candidateId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private LocalDateTime createDate;
    @JsonProperty("created_by")
    private Person createdBy;
    @JsonProperty("birth_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate birthDate;
    @JsonProperty("address")
    private String address;
    @JsonProperty("metro")
    private Metro metro;
    @JsonProperty("email")
    private String email;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("cell")
    private String cell;
    @JsonProperty("position")
    private String position;
    @JsonProperty("salary")
    private Value salary;
    @Enumerated(EnumType.ORDINAL)
    @JsonProperty("status")
    private CandidateStatus status;

    @Enumerated(EnumType.ORDINAL)
    @JsonProperty("sex")
    private SexStatus sex;
    private String degree;

    @JsonProperty("education")
    private Set<Education> educations = new HashSet<>();

    @JsonProperty("employment")
    private Set<Employment> employments = new HashSet<>();

    @JsonProperty("language")
    private Set<CandidateLanguage> languages = new HashSet<>();

    @JsonProperty("category")
    private Set<CandidateCategory> categories = new HashSet<>();

    @Id
    @Column(name = "candidate_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getCandidateId() {
        return candidateId;
    }

    @Column(name = "create_date", insertable = false, updatable = false)
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    public Person getCreatedBy() {
        return createdBy;
    }

    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    @Column(name = "middle_name")
    public String getMiddleName() {
        return middleName;
    }

    @Column(name = "last_name")
    public String getLastName() {
        return lastName;
    }

    @Column(name = "birth_date")
    public LocalDate getBirthDate() {
        return birthDate;
    }

    @Column(name = "status")
    public CandidateStatus getStatus() {
        return status;
    }

    @ManyToOne
    @JoinColumn(name = "metro_id")
    public Metro getMetro() {
        return metro;
    }

    @Column(name = "sex")
    public SexStatus getSex() {
        return sex;
    }

    @Column(name = "degree")
    public String getDegree() {
        return degree;
    }

    @OneToMany(mappedBy = "candidate", fetch = FetchType.EAGER, orphanRemoval = true)
    public Set<Education> getEducations() {
        return educations;
    }

    @OneToMany(mappedBy = "candidate", fetch = FetchType.EAGER, orphanRemoval = true)
    public Set<Employment> getEmployments() {
        return employments;
    }

    @OneToMany(mappedBy = "candidate", fetch = FetchType.EAGER, orphanRemoval = true)
    public Set<CandidateLanguage> getLanguages() {
        return languages;
    }

    @OneToMany(mappedBy = "candidate", fetch = FetchType.EAGER, orphanRemoval = true)
    public Set<CandidateCategory> getCategories() {
        return categories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Candidate c = (Candidate) o;
        return this.candidateId.equals(c.candidateId);
    }

    @Override
    public String toString() {
        return "Candidate{" +
                "candidateId=" + candidateId +
                ", createDate=" + createDate +
                ", createdBy=" + createdBy +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", address='" + address + '\'' +
                ", metro=" + metro +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", cell='" + cell + '\'' +
                ", position='" + position + '\'' +
                ", salary=" + salary +
                ", status=" + status +
                ", sex=" + sex +
                ", degree='" + degree + '\'' +
                ", educations=" + educations +
                ", employments=" + employments +
                ", languages=" + languages +
                ", categories=" + categories +
                '}';
    }
}