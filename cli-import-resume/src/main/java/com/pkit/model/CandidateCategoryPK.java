package com.pkit.model;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public class CandidateCategoryPK implements java.io.Serializable {

	private Long candidateId;
    private Long categoryId;

    public CandidateCategoryPK( ) {}

    public CandidateCategoryPK(Long candidateId, Long categoryId) {
        this.categoryId = categoryId;
        this.candidateId = candidateId;
    }

    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof CandidateCategoryPK)) return false;
        CandidateCategoryPK pk = (CandidateCategoryPK) obj;
        return categoryId.equals(pk.getCategoryId()) && candidateId.equals(pk.getCandidateId());
    }

    public int hashCode() {
        log.debug("hashCode(): candidateId: {}; categoryId: {}", candidateId, categoryId);
        return candidateId.hashCode() + 33*categoryId.hashCode();
    }
}
