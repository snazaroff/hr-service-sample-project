package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@Entity
@IdClass(CandidateLanguagePK.class)
@Table(name="candidate_language")
public class CandidateLanguage extends Jsonable implements Serializable, Comparable<CandidateLanguage> {

    public CandidateLanguage() {
    }

    public CandidateLanguage(Candidate candidate, Language language) {
        this.candidate = candidate;
        this.candidateId = candidate.getCandidateId();
        this.language = language;
        this.languageId = language.getLanguageId();
    }

    @Id
    @Column(name = "language_id", insertable = false, updatable = false)
    @JsonProperty("language_id")
    public Long languageId;

    @Id
    @Column(name = "candidate_id", insertable = false, updatable = false)
    @JsonProperty("candidate_id")
    public Long candidateId;

    @ManyToOne
    @JoinColumn(name="language_id", nullable=false, insertable = false, updatable = false)
	private Language language;
	
    @ManyToOne
    @JoinColumn(name="candidate_id", nullable=false, insertable = false, updatable = false)
    @JsonIgnore
	private Candidate candidate;

    @Column(name = "status")
    private LanguageStatus status;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Candidate c = ((CandidateLanguage) o).getCandidate();
        if (c == null) return false;
        Language l = ((CandidateLanguage) o).getLanguage();
        return l != null && this.candidateId != null && this.candidateId.equals(c.getCandidateId()) && this.languageId.equals(l.getLanguageId());
    }

    @Override
    public String toString() {
        return "CandidateLanguage{" +
                "languageId=" + languageId +
                ", candidateId=" + candidateId +
                ", language=" + language +
                ", status=" + status +
                '}';
    }

    @Override
    public int compareTo(@NotNull CandidateLanguage o) {
        if (null == o) return -1;
        if (null == o.candidateId || null == o.languageId) return 1;
        if (Objects.equals(candidateId, o.candidateId)) {
            return languageId.compareTo(o.languageId);
        } else {
            return candidateId.compareTo(o.candidateId);
        }
    }
}
