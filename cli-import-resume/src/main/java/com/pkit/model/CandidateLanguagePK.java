package com.pkit.model;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public class CandidateLanguagePK implements java.io.Serializable {

	private Long candidateId;
    private Long languageId;

    public CandidateLanguagePK( ) {}

    public CandidateLanguagePK(Long candidateId, Long languageId) {
        this.languageId = languageId;
        this.candidateId = candidateId;
    }

    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof CandidateLanguagePK)) return false;
        CandidateLanguagePK pk = (CandidateLanguagePK) obj;
        return languageId.equals(pk.getLanguageId()) && candidateId.equals(pk.getCandidateId());
    }

    public int hashCode() {
        log.debug("hashCode(): candidateId: {}; languageId: {}", candidateId, languageId);
        return candidateId.hashCode() + 33*languageId.hashCode();
    }
}
