package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@Setter
@Getter
public class Human implements Comparable<Human>{

    @JsonProperty("first_name")
    protected String firstName;

    @JsonProperty("middle_name")
    protected String middleName;

    @JsonProperty("last_name")
    protected String lastName;

    @Transient
    public String getFio() {
        return this.lastName + " " +  this.firstName + " " +  this.middleName;
    }

    @Override
    public int compareTo(@NotNull Human o) {
        return getFio().compareTo(o.getFio());
    }
}