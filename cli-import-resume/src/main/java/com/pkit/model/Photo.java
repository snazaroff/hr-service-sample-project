package com.pkit.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Slf4j
@Setter
@Getter
@ToString
@Entity
@Table(name="candidate_photo")
public class Photo implements Serializable {

    @JsonProperty("photo_id")
    private Long photoId;
    @JsonProperty("candidate_id")
    private Long candidateId;
    @JsonProperty("is_main")
    private boolean isMain;
    @JsonIgnore
    private byte[] image;

    @Id
    @Column(name = "photo_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getPhotoId() {
        return photoId;
    }

    @Column(name = "candidate_id")
    public Long getCandidateId() {
        return candidateId;
    }

    @Column(name = "is_main")
    public boolean isMain() {
        return isMain;
    }

    @Column(name = "image")
    public byte[] getImage() {
        return image;
    }
}
