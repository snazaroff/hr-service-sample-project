package com.pkit.model;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
@ToString
@Entity
@Table(name="metro")
public class Metro implements java.io.Serializable {

    @JsonProperty("metro_id")
    private Long metroId;
    @JsonProperty("line_id")
    private Long lineId;
    @JsonProperty("town_id")
    private Long townId;
    @JsonProperty("name")
    private String name;

    @Id
    @Column(name = "metro_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getMetroId() {
        return metroId;
    }

    @Column(name = "line_id")
    public Long getLineId() {
        return lineId;
    }

    @Column(name = "town_id")
    public Long getTownId() {
        return townId;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Metro m = (Metro) o;
        return this.metroId.equals(m.metroId);
    }

}
