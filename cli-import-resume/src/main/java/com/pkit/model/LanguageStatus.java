package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.ToString;

import java.io.IOException;
import java.util.Objects;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonSerialize(using = LanguageStatus.Serializer.class)
@JsonDeserialize(using = LanguageStatus.Deserializer.class)
@ToString
public enum LanguageStatus {

    NATIVE(         new Status((short)0, "родной")),
    BASIC(          new Status((short)1, "базовые знания")),
    CAN_READ(       new Status((short)2, "читаю профессиональную литературу")),
    CAN_PASS_INTERVIEW(new Status((short)3, "могу проходить интервью")),
    FLUENT(         new Status((short)4, "свободно владею"));

    @JsonProperty
    private final Status status;

    LanguageStatus(Status status) {
        this.status = status;
    }

    public static LanguageStatus getStatus(Short id) {

        if (id == null) {
            return null;
        }

        for (LanguageStatus status : LanguageStatus.values()) {
            if (id.equals(status.status.getStatusId())) {
                return status;
            }
        }
        throw new IllegalArgumentException("No matching type for id " + id);
    }

//    public static LanguageStatus get(Integer id) {
//
//        if (id == null)
//            return null;
//
//        for (LanguageStatus status : LanguageStatus.values()) {
//            if (id.equals(status.status.getStatusId())) {
//                return status;
//            }
//        }
//        return null;
//    }

    public static LanguageStatus getStatus(String text) {

        if (text == null || text.equals("")) {
            return null;
        }

        for (LanguageStatus status : LanguageStatus.values()) {
            if (text.equals(status.status.getName())) {
                return status;
            }
        }
        return null;
    }

    public Short getStatusId() {
        return status.getStatusId();
    }

    public String getName() {
        return status.getName();
    }

    public static class Serializer extends StdSerializer<LanguageStatus> {
        public Serializer() {
            super(LanguageStatus.class);
        }

        @Override
        public void serialize(LanguageStatus ds,
                              JsonGenerator jgen,
                              SerializerProvider sp) throws IOException {

            jgen.writeStartObject();
            jgen.writeNumberField("status_id", ds.status.getStatusId());
            jgen.writeStringField("name", ds.status.getName());
            jgen.writeEndObject();
        }
    }

    public static class Deserializer extends StdDeserializer<LanguageStatus> {
        public Deserializer() {
            super(LanguageStatus.class);
        }

        @Override
        public LanguageStatus deserialize(JsonParser jp, DeserializationContext dc) throws IOException {
            final JsonNode jsonNode = jp.readValueAsTree();
            short statusId = (short)jsonNode.get("status_id").asInt();

            for (LanguageStatus ds: LanguageStatus.values()) {
                if (Objects.equals(ds.status.getStatusId(), statusId)) {
                    return ds;
                }
            }
            throw dc.mappingException("Cannot deserialize Status from key " + statusId);
        }
    }
}