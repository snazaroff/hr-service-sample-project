package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.Objects;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonSerialize(using = EducationType.Serializer.class)
@JsonDeserialize(using = EducationType.Deserializer.class)
public enum EducationType {

    UNEDUCATED(         new Status((short)0, "Без образования")),
    PRIMARY_SCHOOL(     new Status((short)1, "Начальная школа")),
    SECONDARY_SCHOOL(   new Status((short)2, "Средняя школа")),
    SECONDARY_SPECIAL_SCHOOL(     new Status((short)3, "Среднее специальное")),
    HIGH_SCHOOL(        new Status((short)4, "Высшая школа")),
    BACHELOR(           new Status((short)5, "Бакалавр")),
    MASTER(             new Status((short)6, "Магистр")),
    CANDIDATE(          new Status((short)7, "Кандидат")),
    DOCTOR(             new Status((short)8, "Доктор")),
    COURSES(            new Status((short)9, "Курсы")),
    UNFINISHED_HIGHER(  new Status((short)10, "Неоконченное высшее"));

    @JsonProperty
    private final Status status;

    EducationType(Status status) {
        this.status = status;
    }

    public static Status getStatus(Short id) {

        if (id == null) {
            return null;
        }

        for (EducationType status : EducationType.values()) {
            if (id.equals(status.status.getStatusId())) {
                return status.status;
            }
        }
        throw new IllegalArgumentException("No matching type for id " + id);
    }

    public String getName() {
        return status.getName();
    }

    public Short getId() {
        return status.getStatusId();
    }

    public static class Serializer extends StdSerializer<EducationType> {
        public Serializer() {
            super(EducationType.class);
        }

        @Override
        public void serialize(EducationType ds,
                              JsonGenerator jgen,
                              SerializerProvider sp) throws IOException {

            jgen.writeStartObject();
            jgen.writeNumberField("education_type_id", ds.status.getStatusId());
            jgen.writeStringField("name", ds.status.getName());
            jgen.writeEndObject();
        }
    }

    public static class Deserializer extends StdDeserializer<EducationType> {
        public Deserializer() {
            super(EducationType.class);
        }

        @Override
        public EducationType deserialize(JsonParser jp, DeserializationContext dc) throws IOException {
            final JsonNode jsonNode = jp.readValueAsTree();
            Integer statusId = jsonNode.get("education_type_id").asInt();

            for (EducationType ds: EducationType.values()) {
                if (Objects.equals(ds.status.getStatusId(), statusId.shortValue())) {
                    return ds;
                }
            }
            throw dc.mappingException("Cannot deserialize EducationType from key " + statusId);
        }
    }
}