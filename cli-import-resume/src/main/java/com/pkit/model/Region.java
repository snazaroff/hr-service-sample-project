package com.pkit.model;

import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
@ToString
@Entity
@Table(name="region")
public class Region implements java.io.Serializable {

    @JsonProperty("region_id")
    private Long regionId;
    @JsonProperty("country_id")
    private Long countryId;
    @JsonProperty("name")
    private String name;

    @Id
    @Column(name = "region_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getRegionId() {
        return regionId;
    }

    @Column(name = "country_id")
    public Long getCountryId() {
        return countryId;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Region r = (Region) o;
        return this.regionId.equals(r.regionId);
    }
}
