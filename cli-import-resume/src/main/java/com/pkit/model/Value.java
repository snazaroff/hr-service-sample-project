package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Setter
@Getter
@Embeddable
public class Value {

    @JsonProperty("salary_amount")
    private long amount;
    @JsonProperty("salary_currency")
    private Currency currency = Currency.RUB;

    public Value() {}

    public Value(long amount, Currency currency) {
        this.amount = amount;
        this.currency = currency;
    }

    @Column(name = "salary_amount")
    public long getAmount() {
        return amount;
    }

    @Column(name = "salary_currency")
    public Currency getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return "Value{" +
                "amount=" + amount +
                ", currency=" + currency +
                '}';
    }
}
