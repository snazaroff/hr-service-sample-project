package com.pkit.model;

import lombok.ToString;

@ToString
public enum CompanyType {

    CUSTOMER(0, "Заказчик"),
    EMPLOYMENT_AGENCY(1, "Рекрутинговое агентство"),
    OWNER(2, "Собственник");

    private final String name;
    private final int id;

    CompanyType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}