package com.pkit.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Status {

    @JsonProperty("status_id")
    private Short statusId;
    private String name;

    public Status(Short statusId, String name) {
        this.statusId = statusId;
        this.name = name;
    }
}
